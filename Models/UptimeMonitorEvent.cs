using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SiteUptime.Helpers;
using System.Collections.Generic;
using System.Linq;
 
namespace SiteUptime.Models
{
    public class UptimeMonitorEvent
    { 
        [Key]
        public int UptimeMonitorEventId { get; set; }

        public int UptimeMonitorId { get; set; }

        public int? UptimeStatusTypeId { get; set; }

        public int? UptimeReasonTypeId { get; set; }

        public int? StatusCode { get; set; }

        public int Duration { get; set; }

        public int CreatedById { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? UpdatedById { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}