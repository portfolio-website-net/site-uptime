using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
 
namespace SiteUptime.Models
{
    public class UptimeMonitorCustomHttpStatus
    { 
        [Key]
        [JsonProperty("uptimeMonitorCustomHttpStatusId")]
        public int UptimeMonitorCustomHttpStatusId { get; set; }

        [JsonProperty("uptimeMonitorId")]
        public int UptimeMonitorId { get; set; }

        [JsonProperty("statusCode")]
        public int StatusCode { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("upStatusCode")]
        public bool UpStatusCode { get; set; }

        [JsonProperty("order")]
        public int Order { get; set; }

        [JsonIgnore]
        public int CreatedById { get; set; }

        [JsonIgnore]
        public DateTime CreatedDate { get; set; }

        [JsonIgnore]
        public int? UpdatedById { get; set; }

        [JsonIgnore]
        public DateTime? UpdatedDate { get; set; }
    }
}