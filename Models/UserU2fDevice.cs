using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace SiteUptime.Models
{
    public class UserU2fDevice
    { 
        [Key]
        public int UserU2fDeviceId { get; set; }

        public int UserId { get; set; }

        public string DeviceName { get; set; }

        public byte[] KeyHandle { get; set; }

        public byte[] PublicKey { get; set; }

        public byte[] AttestationCert { get; set; }

        public int Counter { get; set; }

        [Column(TypeName = "bit")]
        public bool IsCompromised { get; set; }

        [Column(TypeName = "bit")]
        public bool IsActive { get; set; }

        public int CreatedById { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? UpdatedById { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}