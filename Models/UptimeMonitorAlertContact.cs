using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace SiteUptime.Models
{
    public class UptimeMonitorAlertContact
    { 
        [Key]
        public int UptimeMonitorAlertContactId { get; set; }

        public int UptimeMonitorId { get; set; }

        public UptimeMonitor UptimeMonitor { get; set; }

        public int UptimeAlertContactId { get; set; }
        
        public int CreatedById { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? UpdatedById { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}