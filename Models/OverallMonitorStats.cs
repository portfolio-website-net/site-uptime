using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SiteUptime.Helpers;
using Newtonsoft.Json;
 
namespace SiteUptime.Models
{
    public class OverallMonitorStats
    { 
        [Key]
        [JsonIgnore]
        public int Id { get; set; }

        [JsonProperty("currentMonitorCount")]
        public int? CurrentMonitorCount { get; set; }

        [JsonProperty("maxMonitorCount")]
        public int? MaxMonitorCount { get; set; }

        [JsonProperty("minInterval")]
        public int? MinInterval { get; set; }

        [JsonProperty("upMonitorCount")]
        public int? UpMonitorCount { get; set; }

        [JsonProperty("downMonitorCount")]
        public int? DownMonitorCount { get; set; }

        [JsonProperty("pausedMonitorCount")]
        public int? PausedMonitorCount { get; set; }

        [JsonProperty("uptimePercentageLast30Days")]
        public double? UptimePercentageLast30Days { get; set; }

        [JsonProperty("uptimePercentageLast7Days")]
        public double? UptimePercentageLast7Days { get; set; }

        [JsonProperty("uptimePercentageLast24Hours")]
        public double? UptimePercentageLast24Hours { get; set; }

        [JsonProperty("uptimePercentage1DayAgo")]
        public double? UptimePercentage1DayAgo { get; set; }

        [JsonProperty("uptimePercentage2DaysAgo")]
        public double? UptimePercentage2DaysAgo { get; set; }

        [JsonProperty("uptimePercentage3DaysAgo")]
        public double? UptimePercentage3DaysAgo { get; set; }

        [JsonProperty("uptimePercentage4DaysAgo")]
        public double? UptimePercentage4DaysAgo { get; set; }

        [JsonProperty("uptimePercentage5DaysAgo")]
        public double? UptimePercentage5DaysAgo { get; set; }

        [JsonProperty("uptimePercentage6DaysAgo")]
        public double? UptimePercentage6DaysAgo { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("wasMonitorActive1DayAgo")]
        public bool? WasMonitorActive1DayAgo { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("wasMonitorActive2DaysAgo")]
        public bool? WasMonitorActive2DaysAgo { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("wasMonitorActive3DaysAgo")]
        public bool? WasMonitorActive3DaysAgo { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("wasMonitorActive4DaysAgo")]
        public bool? WasMonitorActive4DaysAgo { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("wasMonitorActive5DaysAgo")]
        public bool? WasMonitorActive5DaysAgo { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("wasMonitorActive6DaysAgo")]
        public bool? WasMonitorActive6DaysAgo { get; set; }

        [JsonProperty("latestDownTime_MonitorName")]
        public string LatestDownTime_MonitorName { get; set; }

        [JsonProperty("latestDownTime_Date")]
        public DateTime? LatestDownTime_Date { get; set; }

        [JsonProperty("latestDownTime_Duration")]
        public int? LatestDownTime_Duration { get; set; }

        [NotMapped]
        [JsonProperty("latestDownTimeMessage")]
        public string LatestDownTimeMessage { 
            get
            {
                if (!string.IsNullOrEmpty(LatestDownTime_MonitorName)
                    && LatestDownTime_Date != null
                    && LatestDownTime_Duration != null)
                {
                    var duration = string.Empty;
                    var durationTimeSpan = TimeSpan.FromSeconds(LatestDownTime_Duration.Value);
                    duration = StringHelpers.ConvertDurationToShortString(durationTimeSpan);
                    return $"It was recorded (for the monitor '{LatestDownTime_MonitorName}') on {LatestDownTime_Date.Value.ToString()} and the downtime lasted for {duration}.";
                }
                else
                {
                    return "No downtime recorded.";
                }
            }
        }
    }
}