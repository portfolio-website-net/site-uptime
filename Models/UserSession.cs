using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace SiteUptime.Models
{
    public class UserSession
    { 
        [Key]
        public int UserSessionId { get; set; }

        public int UserId { get; set; }

        public string SessionKey { get; set; }

        [Column(TypeName = "bit")]
        public bool IsValid { get; set; }

        public DateTime ExpirationDate { get; set; }

        public int CreatedById { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? UpdatedById { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}