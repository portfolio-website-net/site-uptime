using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SiteUptime.Helpers;
using Newtonsoft.Json;
using System.Linq;
using SiteUptime.Helpers.CacheHelpers;
using SiteUptime.Helpers.Types;
 
namespace SiteUptime.Models
{
    public class CurrentUptimeMonitorDetails
    { 
        [Key]
        [JsonProperty("uptimeMonitorId")]
        public int UptimeMonitorId { get; set; }

        [JsonProperty("uptimeMonitorTypeId")]
        public int? UptimeMonitorTypeId { get; set; }

        [NotMapped]
        [JsonProperty("uptimeMonitorTypeName")]
        public string UptimeMonitorTypeName
        {
            get
            {
                if (UptimeMonitorTypeId != null)
                {
                    return MemoryCacheHelper.UptimeMonitorTypes.First(x => x.UptimeMonitorTypeId == UptimeMonitorTypeId.Value).Name;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        [JsonProperty("uptimeStatusTypeId")]
        public int? UptimeStatusTypeId { get; set; }

        [NotMapped]
        [JsonProperty("uptimeStatusTypeName")]
        public string UptimeStatusTypeName
        {
            get
            {
                if (UptimeStatusTypeId != null)
                {
                    return MemoryCacheHelper.UptimeStatusTypes.First(x => x.UptimeStatusTypeId == UptimeStatusTypeId.Value).Name;
                }
                else
                {
                    return string.Empty;
                }
            }
        }        

        [JsonIgnore]
        public int Interval { get; set; }

        [NotMapped]
        [JsonProperty("intervalSliderValue")]
        public int IntervalSliderValue { get; set; }

        [JsonProperty("averageResponseTime")]
        public int? AverageResponseTime { get; set; }

        [JsonProperty("latestStatus_Date")]
        public DateTime? LatestStatus_Date { get; set; }

        [JsonProperty("latestStatus_Duration")]
        public int? LatestStatus_Duration { get; set; }

        [NotMapped]
        [JsonProperty("currentStatusMessage")]
        public string CurrentStatusMessage 
        {
            get
            {
                if (LatestStatus_Date != null
                    && LatestStatus_Duration != null)
                {
                    var duration = string.Empty;
                    var durationTimeSpan = TimeSpan.FromSeconds(LatestStatus_Duration.Value);
                    duration = StringHelpers.ConvertDurationToShortString(durationTimeSpan);
                    return $"Since {duration} ({LatestStatus_Date.Value.ToString()}).";
                }
                else
                {
                    return "No status yet.";
                }
            }
        }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("uptimePercentageLast30Days")]
        public double? UptimePercentageLast30Days { get; set; }

        [JsonProperty("uptimePercentageLast7Days")]
        public double? UptimePercentageLast7Days { get; set; }

        [JsonProperty("uptimePercentageLast24Hours")]
        public double? UptimePercentageLast24Hours { get; set; }

        [JsonProperty("uptimePercentage1DayAgo")]
        public double? UptimePercentage1DayAgo { get; set; }

        [JsonProperty("uptimePercentage2DaysAgo")]
        public double? UptimePercentage2DaysAgo { get; set; }

        [JsonProperty("uptimePercentage3DaysAgo")]
        public double? UptimePercentage3DaysAgo { get; set; }

        [JsonProperty("uptimePercentage4DaysAgo")]
        public double? UptimePercentage4DaysAgo { get; set; }

        [JsonProperty("uptimePercentage5DaysAgo")]
        public double? UptimePercentage5DaysAgo { get; set; }

        [JsonProperty("uptimePercentage6DaysAgo")]
        public double? UptimePercentage6DaysAgo { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("wasMonitorActive1DayAgo")]
        public bool? WasMonitorActive1DayAgo { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("wasMonitorActive2DaysAgo")]
        public bool? WasMonitorActive2DaysAgo { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("wasMonitorActive3DaysAgo")]
        public bool? WasMonitorActive3DaysAgo { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("wasMonitorActive4DaysAgo")]
        public bool? WasMonitorActive4DaysAgo { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("wasMonitorActive5DaysAgo")]
        public bool? WasMonitorActive5DaysAgo { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("wasMonitorActive6DaysAgo")]
        public bool? WasMonitorActive6DaysAgo { get; set; }

        [JsonProperty("latestDownTime_Date")]
        public DateTime? LatestDownTime_Date { get; set; }

        [JsonProperty("latestDownTime_Duration")]
        public int? LatestDownTime_Duration { get; set; }

        [NotMapped]
        [JsonProperty("latestDownTimeMessage")]
        public string LatestDownTimeMessage { 
            get
            {
                if (LatestDownTime_Date != null
                    && LatestDownTime_Duration != null)
                {
                    var duration = string.Empty;
                    var durationTimeSpan = TimeSpan.FromSeconds(LatestDownTime_Duration.Value);
                    duration = StringHelpers.ConvertDurationToShortString(durationTimeSpan);
                    return $"It was recorded on {LatestDownTime_Date.Value.ToString()} and the downtime lasted for {duration}.";
                }
                else
                {
                    return "No downtime recorded.";
                }
            }
        }
    }
}