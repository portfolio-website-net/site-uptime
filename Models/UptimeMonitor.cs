using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using Newtonsoft.Json;
using SiteUptime.Helpers.Api;
 
namespace SiteUptime.Models
{
    public class UptimeMonitor
    { 
        [Key]
        [JsonProperty("uptimeMonitorId")]
        public int UptimeMonitorId { get; set; }

        [JsonProperty("uptimeMonitorTypeId")]
        public int UptimeMonitorTypeId { get; set; }

        [JsonProperty("uptimeMonitorHttpMethodTypeId")]
        public int UptimeMonitorHttpMethodTypeId { get; set; }

        [JsonIgnore]
        public int UserId { get; set; }

        [JsonIgnore]
        public User User { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("port")]
        public int? Port { get; set; }

        [JsonProperty("keyword")]
        public string Keyword { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("keywordAlertWhenExists")]
        public bool? KeywordAlertWhenExists { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("useBasicAuthentication")]
        public bool? UseBasicAuthentication { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("ignoreSSLErrors")]
        public bool? IgnoreSSLErrors { get; set; }

        [JsonIgnore]
        public int Interval { get; set; }       

        [JsonIgnore]
        public int? ResponseTime { get; set; }

        [JsonIgnore]
        public int? StatusCode { get; set; }

        [JsonIgnore]
        public string ResponseContent { get; set; }

        [JsonProperty("uptimeStatusTypeId")]
        public int? UptimeStatusTypeId { get; set; }

        [JsonProperty("uptimeReasonTypeId")]
        public int? UptimeReasonTypeId { get; set; }

        [JsonProperty("uptimePercentageLast30Days")]
        public double? UptimePercentageLast30Days { get; set; }

        [JsonProperty("uptimePercentageLast7Days")]
        public double? UptimePercentageLast7Days { get; set; }

        [JsonProperty("uptimePercentageLast24Hours")]
        public double? UptimePercentageLast24Hours { get; set; }

        [JsonProperty("uptimePercentage1DayAgo")]
        public double? UptimePercentage1DayAgo { get; set; }

        [JsonProperty("uptimePercentage2DaysAgo")]
        public double? UptimePercentage2DaysAgo { get; set; }

        [JsonProperty("uptimePercentage3DaysAgo")]
        public double? UptimePercentage3DaysAgo { get; set; }

        [JsonProperty("uptimePercentage4DaysAgo")]
        public double? UptimePercentage4DaysAgo { get; set; }

        [JsonProperty("uptimePercentage5DaysAgo")]
        public double? UptimePercentage5DaysAgo { get; set; }

        [JsonProperty("uptimePercentage6DaysAgo")]
        public double? UptimePercentage6DaysAgo { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("wasMonitorActive1DayAgo")]
        public bool? WasMonitorActive1DayAgo { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("wasMonitorActive2DaysAgo")]
        public bool? WasMonitorActive2DaysAgo { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("wasMonitorActive3DaysAgo")]
        public bool? WasMonitorActive3DaysAgo { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("wasMonitorActive4DaysAgo")]
        public bool? WasMonitorActive4DaysAgo { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("wasMonitorActive5DaysAgo")]
        public bool? WasMonitorActive5DaysAgo { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("wasMonitorActive6DaysAgo")]
        public bool? WasMonitorActive6DaysAgo { get; set; }

        [JsonIgnore]
        public DateTime? NextMonitorDate { get; set; }

        [JsonIgnore]
        public DateTime? ProcessingTimeoutDate { get; set; }

        [JsonIgnore]
        public DateTime? ConfirmationTimeoutDate { get; set; }

        [JsonIgnore]
        public string UptimeMonitorLocation { get; set; }

        [JsonIgnore]
        public string UptimeMonitorIpAddress { get; set; }

        [JsonIgnore]
        public DateTime? HeartbeatDate { get; set;}

        [JsonIgnore]
        public int CreatedById { get; set; }

        [JsonIgnore]
        public DateTime CreatedDate { get; set; }

        [JsonIgnore]
        public int? UpdatedById { get; set; }

        [JsonIgnore]
        public DateTime? UpdatedDate { get; set; }

        [NotMapped]
        [JsonProperty("selectedContactIds")]
        public List<int> SelectedContactIds { get; set; }

        [NotMapped]
        [JsonProperty("updatedDateStr")]
        public string UpdatedDateStr
        { 
            get
            {
                return UpdatedDate.HasValue ? UpdatedDate.Value.ToString("MM/dd/yyyy h:mm:ss tt") : 
                    CreatedDate.ToString("MM/dd/yyyy h:mm:ss tt");
            }
        }

        [NotMapped]
        [JsonProperty("intervalSliderValue")]
        public int IntervalSliderValue { get; set; }

        [NotMapped]
        [JsonIgnore]
        public ApiError ApiError { get; set; }
    }
}