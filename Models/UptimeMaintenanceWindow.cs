using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;
using SiteUptime.Helpers.Api;
 
namespace SiteUptime.Models
{
    public class UptimeMaintenanceWindow
    { 
        [Key]
        [JsonProperty("uptimeMaintenanceWindowId")]
        public int UptimeMaintenanceWindowId { get; set; }

        [JsonProperty("uptimeMaintenanceWindowTypeId")]
        public int UptimeMaintenanceWindowTypeId { get; set; }

        [JsonIgnore]
        public int UserId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("startTime")]
        public string StartTime { get; set; }

        [JsonProperty("duration")]
        public int Duration { get; set; }

        [JsonProperty("days")]
        public string Days { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("isActive")]
        public bool IsActive { get; set; }

        [JsonIgnore]
        public int CreatedById { get; set; }

        [JsonIgnore]
        public DateTime CreatedDate { get; set; }

        [JsonIgnore]
        public int? UpdatedById { get; set; }

        [JsonIgnore]
        public DateTime? UpdatedDate { get; set; }

        [NotMapped]
        [JsonProperty("selectedMonitorIds")]
        public List<int> SelectedMonitorIds { get; set; }

        [NotMapped]
        [JsonProperty("assignToAllMonitors")]
        public bool AssignToAllMonitors {
            get
            {
                return SelectedMonitorIds == null
                    || SelectedMonitorIds.Count() == 0;
            }
        }

        [NotMapped]
        [JsonIgnore]
        public ApiError ApiError { get; set; }
    }
}