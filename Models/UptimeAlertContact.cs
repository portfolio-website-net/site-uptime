using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using SiteUptime.Helpers.Api;
 
namespace SiteUptime.Models
{
    public class UptimeAlertContact
    { 
        [Key]
        [JsonProperty("uptimeAlertContactId")]
        public int UptimeAlertContactId { get; set; }

        [JsonProperty("uptimeAlertContactTypeId")]
        public int UptimeAlertContactTypeId { get; set; }

        [JsonProperty("uptimeAlertContactStatusTypeId")]
        public int UptimeAlertContactStatusTypeId { get; set; }

        [JsonIgnore]
        public int UserId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }        

        [JsonProperty("uptimeNotificationEventTypeId")]
        public int UptimeNotificationEventTypeId { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("sendDefaultQueryStringForWebhook")]
        public bool SendDefaultQueryStringForWebhook { get; set; }

        [JsonProperty("postValue")]
        public string PostValue { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("sendJsonForWebhook")]
        public bool SendJsonForWebhook { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("sendDefaultPostForWebhook")]
        public bool SendDefaultPostForWebhook { get; set; }

        [JsonProperty("uptimeMobileProviderTypeId")]
        public int? UptimeMobileProviderTypeId { get; set; }

        [JsonIgnore]
        public string ActivationCode { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("isActivated")]
        public bool IsActivated { get; set; }

        [JsonIgnore]
        public int CreatedById { get; set; }

        [JsonIgnore]
        public DateTime CreatedDate { get; set; }

        [JsonIgnore]
        public int? UpdatedById { get; set; }

        [JsonIgnore]
        public DateTime? UpdatedDate { get; set; }

        [NotMapped]
        [JsonIgnore]
        public ApiError ApiError { get; set; }
    }
}