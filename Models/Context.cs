using Microsoft.EntityFrameworkCore;
 
namespace SiteUptime.Models
{
    public class Context : DbContext
    {
        public Context (DbContextOptions<Context> options)
            : base(options)
        {
        }

        public DbSet<EventLog> EventLogs { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<UserSession> UserSessions { get; set; }
 
        public DbSet<UptimeMonitor> UptimeMonitors { get; set; }

        public DbSet<UptimeMonitorType> UptimeMonitorTypes { get; set; }

        public DbSet<UptimeMonitorHttpMethodType> UptimeMonitorHttpMethodTypes { get; set; }

        public DbSet<UptimeStatusType> UptimeStatusTypes { get; set; }

        public DbSet<UptimeAlertContactType> UptimeAlertContactTypes { get; set; }

        public DbSet<UptimeAlertContactStatusType> UptimeAlertContactStatusTypes { get; set; }
        
        public DbSet<UptimeNotificationEventType> UptimeNotificationEventTypes { get; set; }

        public DbSet<UptimeMobileProviderType> UptimeMobileProviderTypes { get; set; }

        public DbSet<UptimeAlertContact> UptimeAlertContacts { get; set; }

        public DbSet<UptimeMonitorAlertContact> UptimeMonitorAlertContacts { get; set; }

        public DbSet<UptimeMonitorCustomHttpStatus> UptimeMonitorCustomHttpStatuses { get; set; }

        public DbSet<UptimeMonitorCustomHeader> UptimeMonitorCustomHeaders { get; set; }        

        public DbSet<UptimeReasonType> UptimeReasonTypes { get; set; }

        public DbSet<UptimeMonitorEvent> UptimeMonitorEvents { get; set; }

        public DbSet<UptimeMonitorHistory> UptimeMonitorHistory { get; set; }

        public DbSet<UptimeMonitorApiKey> UptimeMonitorApiKeys { get; set; }

        public DbSet<UptimeMonitorApiKeyType> UptimeMonitorApiKeyTypes { get; set; }

        public DbSet<Error> Errors { get; set; }

        public DbSet<UptimeMonitorAgent> UptimeMonitorAgents { get; set; }

        public DbSet<UptimeMaintenanceWindow> UptimeMaintenanceWindows { get; set; }

        public DbSet<UptimeMonitorMaintenanceWindow> UptimeMonitorMaintenanceWindows { get; set; }

        public DbSet<UptimeMaintenanceWindowType> UptimeMaintenanceWindowTypes { get; set; }

        public DbSet<UptimeMonitorSortType> UptimeMonitorSortTypes { get; set; }

        public DbSet<UptimePublicStatusPage> UptimePublicStatusPages { get; set; }

        public DbSet<UptimeMonitorPublicStatusPage> UptimeMonitorPublicStatusPages { get; set; }

        public DbSet<UserTimeZoneType> UserTimeZoneTypes { get; set; }

        public DbSet<UserSecurityQuestionType> UserSecurityQuestionTypes { get; set; }

        public DbSet<TwoFactorAuthenticationType> TwoFactorAuthenticationTypes { get; set; }

        public DbSet<UserU2fAuthenticationRequest> UserU2fAuthenticationRequests { get; set; }

        public DbSet<UserU2fDevice> UserU2fDevices { get; set; }

        // Stored Procedure return objects

        public DbSet<MonitorUptimeResult> MonitorUptimeResult { get; set; }

        public DbSet<SqlResult> SqlResults { get; set; }

        public DbSet<ProcessMonitorEventsResult> ProcessMonitorEventsResults { get; set; }

        public DbSet<UptimeMonitorChartData> UptimeMonitorChartData { get; set; }

        public DbSet<OverallMonitorStats> OverallMonitorStats { get; set; }

        public DbSet<CurrentUptimeMonitorDetails> CurrentUptimeMonitorDetails { get; set; }
    }
}