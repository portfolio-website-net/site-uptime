using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace SiteUptime.Models
{
    public class UptimeMonitorHistory
    { 
        [Key]
        public int UptimeMonitorHistoryId { get; set; }

        public int UptimeMonitorId { get; set; }

        public int? UptimeStatusTypeId { get; set; }

        public int? UptimeReasonTypeId { get; set; }

        public int? StatusCode { get; set; }

        public int? ResponseTime { get; set; }

        public string UptimeMonitorLocation { get; set; }

        public string UptimeMonitorIpAddress { get; set; }

        public int CreatedById { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? UpdatedById { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}