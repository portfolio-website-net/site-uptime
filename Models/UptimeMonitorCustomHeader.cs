using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
 
namespace SiteUptime.Models
{
    public class UptimeMonitorCustomHeader
    { 
        [Key]
        [JsonProperty("uptimeMonitorCustomHeaderId")]
        public int UptimeMonitorCustomHeaderId { get; set; }

        [JsonProperty("uptimeMonitorId")]
        public int UptimeMonitorId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("order")]
        public int Order { get; set; }

        [JsonIgnore]
        public int CreatedById { get; set; }

        [JsonIgnore]
        public DateTime CreatedDate { get; set; }

        [JsonIgnore]
        public int? UpdatedById { get; set; }

        [JsonIgnore]
        public DateTime? UpdatedDate { get; set; }
    }
}