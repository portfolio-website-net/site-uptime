using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace SiteUptime.Models
{
    public class UserU2fAuthenticationRequest
    { 
        [Key]
        public int UserU2fAuthenticationRequestId { get; set; }

        public int UserId { get; set; }

        public string KeyHandle { get; set; }

        public string Challenge { get; set; }

        public string AppId { get; set; }

        public string Version { get; set; }

        public int CreatedById { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? UpdatedById { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}