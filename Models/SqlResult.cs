using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace SiteUptime.Models
{
    public class SqlResult
    { 
        [Key]
        public int Id { get; set; }

        [Column(TypeName = "bit")]
        public bool Success { get; set; }
    }
}