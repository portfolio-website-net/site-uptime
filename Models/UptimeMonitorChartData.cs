using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
 
namespace SiteUptime.Models
{
    public class UptimeMonitorChartData
    { 
        [Key]
        [JsonIgnore]
        public int Id { get; set; }

        [JsonProperty("time")]
        public string Time { get; set; }

        [JsonProperty("responseTime")]
        public int? ResponseTime { get; set; }
    }
}