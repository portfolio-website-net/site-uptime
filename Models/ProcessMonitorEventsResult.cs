using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
 
namespace SiteUptime.Models
{
    public class ProcessMonitorEventsResult
    { 
        [Key]
        public int Id { get; set; }

        public int? MostRecentEventDuration { get; set; }
    }
}