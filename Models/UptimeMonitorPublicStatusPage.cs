using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
 
namespace SiteUptime.Models
{
    public class UptimeMonitorPublicStatusPage
    { 
        [Key]
        public int UptimeMonitorPublicStatusPageId { get; set; }

        public int UptimeMonitorId { get; set; }

        public int UptimePublicStatusPageId { get; set; }

        public UptimePublicStatusPage UptimePublicStatusPage { get; set; }

        public int CreatedById { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? UpdatedById { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}