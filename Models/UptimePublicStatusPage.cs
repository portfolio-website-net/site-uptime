using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;
using SiteUptime.Helpers.Api;
 
namespace SiteUptime.Models
{
    public class UptimePublicStatusPage
    { 
        [Key]
        [JsonProperty("uptimePublicStatusPageId")]
        public int UptimePublicStatusPageId { get; set; }

        [JsonIgnore]
        public int UserId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonIgnore]
        public string LogoKey { get; set; }

        [JsonProperty("customDomain")]
        public string CustomDomain { get; set; }

        [JsonProperty("statusPageKey")]
        public string StatusPageKey { get; set; }

        [JsonIgnore]
        public string PasswordHash { get; set; }

        [JsonIgnore]
        public string PasswordSalt { get; set; }

        [NotMapped]
        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("uptimeMonitorSortTypeId")]
        public int? UptimeMonitorSortTypeId { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("hideBrandingLogo")]
        public bool HideBrandingLogo { get; set; }

        [JsonProperty("announcementsHtml")]
        public string AnnouncementsHtml { get; set; }

        [Column(TypeName = "bit")]
        [JsonProperty("IsActive")]
        public bool IsActive { get; set; }        

        [JsonIgnore]
        public int CreatedById { get; set; }

        [JsonIgnore]
        public DateTime CreatedDate { get; set; }

        [JsonIgnore]
        public int? UpdatedById { get; set; }

        [JsonIgnore]
        public DateTime? UpdatedDate { get; set; }

        [NotMapped]
        [JsonProperty("selectedMonitorIds")]
        public List<int> SelectedMonitorIds { get; set; }

        [NotMapped]
        [JsonProperty("assignToAllMonitors")]
        public bool AssignToAllMonitors {
            get
            {
                return SelectedMonitorIds == null
                    || SelectedMonitorIds.Count() == 0;
            }
        }

        [NotMapped]
        [JsonIgnore]
        public ApiError ApiError { get; set; }
    }
}