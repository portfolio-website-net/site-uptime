using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace SiteUptime.Models
{
    public class UptimeMonitorMaintenanceWindow
    { 
        [Key]
        public int UptimeMonitorMaintenanceWindowId { get; set; }

        public int UptimeMonitorId { get; set; }

        public int UptimeMaintenanceWindowId { get; set; }

        public UptimeMaintenanceWindow UptimeMaintenanceWindow { get; set; }        

        public int CreatedById { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? UpdatedById { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}