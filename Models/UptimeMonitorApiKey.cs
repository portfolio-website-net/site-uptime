using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
 
namespace SiteUptime.Models
{
    public class UptimeMonitorApiKey
    { 
        [Key]
        [JsonProperty("uptimeMonitorApiKeyId")]
        public int UptimeMonitorApiKeyId { get; set; }

        [JsonProperty("uptimeMonitorApiKeyTypeId")]
        public int UptimeMonitorApiKeyTypeId { get; set; }

        [JsonProperty("uptimeMonitorId")]
        public int? UptimeMonitorId { get; set; }

        [JsonIgnore]
        public int? UserId { get; set; }

        [JsonProperty("apiKey")]
        public string ApiKey { get; set; }
        
        [JsonIgnore]
        public int CreatedById { get; set; }

        [JsonIgnore]
        public DateTime CreatedDate { get; set; }

        [JsonIgnore]
        public int? UpdatedById { get; set; }

        [JsonIgnore]
        public DateTime? UpdatedDate { get; set; }
    }
}