using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
 
namespace SiteUptime.Models
{
    public class MonitorUptimeResult
    {
        [Key]
        public int UptimeStatusTypeId { get; set; }

        public int TotalTime { get; set; }
    }
}