using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace SiteUptime.Models
{
    public class UptimeMonitorAgent
    { 
        [Key]
        public int UptimeMonitorAgentId { get; set; }

        public string UptimeMonitorLocation { get; set; }

        public string UptimeMonitorIpAddress { get; set; }

        public int? Capacity { get; set; }
        
        public int CreatedById { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? UpdatedById { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}