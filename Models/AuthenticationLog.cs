using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace SiteUptime.Models
{
    public class EventLog
    { 
        [Key]
        public int EventLogId { get; set; }

        public int UserId { get; set; }

        public string EventName { get; set; }

        public string Message { get; set; }

        public string Path { get; set; }

        public string IpAddress { get; set; }

        public string UserAgent { get; set; }

        public int CreatedById { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? UpdatedById { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}