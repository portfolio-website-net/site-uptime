using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using SiteUptime.Models;
using SiteUptime.Services;
using SiteUptime.Services.Interfaces;
using System.IO;
using SiteUptime.Helpers;
using SiteUptime.Helpers.CacheHelpers;
using MySql.Data.EntityFrameworkCore;
using MySql.Data.EntityFrameworkCore.Extensions;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc.Formatters;

namespace SiteUptime
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<Context>(options =>
                options.UseMySQL(Configuration.GetConnectionString("Context")));

            services.AddHttpContextAccessor();
            services.AddTransient<IUptimeMonitorService, UptimeMonitorService>();            
            services.AddTransient<IEventLogService, EventLogService>();
            services.AddTransient<IAuthenticationService, AuthenticationService>();
            services.AddTransient<IUserProfileService, UserProfileService>();
            services.AddTransient<ICommunicationService, CommunicationService>();
            services.AddTransient<ISyndicationFeedService, SyndicationFeedService>();

            var optionsBuilder = new DbContextOptionsBuilder<Context>();
            optionsBuilder.UseMySQL(Configuration.GetConnectionString("Context"));
            services.AddSingleton(new UptimeTimerService(optionsBuilder.Options));

            MemoryCacheHelper.Initialize(optionsBuilder.Options);

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            var configuration = builder.Build();

            services.AddOptions();
            services.Configure<Settings>(configuration.GetSection("Settings"));

            services.AddAntiforgery(options =>
            {
                options.HeaderName = "X-CSRF-TOKEN";
            });

            services.AddControllersWithViews()
                .AddXmlSerializerFormatters();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor |
                    ForwardedHeaders.XForwardedProto
            });

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseApiExceptionHandler();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
