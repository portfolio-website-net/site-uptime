#!/bin/bash
set -euo pipefail

# Script based on https://github.com/do-community/automated-setups/blob/master/Ubuntu-18.04/initial_server_setup.sh

# Begin: Configure the new non-root user

########################
### SCRIPT VARIABLES ###
########################

# Name of the user to create and grant sudo privileges
USERNAME=siteuptimeuser

# Whether to copy over the root user's `authorized_keys` file to the new sudo
# user.
COPY_AUTHORIZED_KEYS_FROM_ROOT=true

# Additional public keys to add to the new sudo user
# OTHER_PUBLIC_KEYS_TO_ADD=(
#     "ssh-rsa AAAAB..."
#     "ssh-rsa AAAAB..."
# )
OTHER_PUBLIC_KEYS_TO_ADD=(
)

####################
### SCRIPT LOGIC ###
####################

# Add sudo user and grant privileges
useradd --create-home --shell "/bin/bash" --groups sudo "${USERNAME}"

# Check whether the root account has a real password set
encrypted_root_pw="$(grep root /etc/shadow | cut --delimiter=: --fields=2)"

if [ "${encrypted_root_pw}" != "*" ]; then
    # Transfer auto-generated root password to user if present
    # and lock the root account to password-based access
    echo "${USERNAME}:${encrypted_root_pw}" | chpasswd --encrypted
    passwd --lock root
else
    # Delete invalid password for user if using keys so that a new password
    # can be set without providing a previous value
    passwd --delete "${USERNAME}"
fi

# Expire the sudo user's password immediately to force a change
chage --lastday 0 "${USERNAME}"

# Create SSH directory for sudo user
home_directory="$(eval echo ~${USERNAME})"
mkdir --parents "${home_directory}/.ssh"

# Copy `authorized_keys` file from root if requested
if [ "${COPY_AUTHORIZED_KEYS_FROM_ROOT}" = true ]; then
    cp /root/.ssh/authorized_keys "${home_directory}/.ssh"
fi

# Add additional provided public keys
for pub_key in "${OTHER_PUBLIC_KEYS_TO_ADD[@]}"; do
    echo "${pub_key}" >> "${home_directory}/.ssh/authorized_keys"
done

# Adjust SSH configuration ownership and permissions
chmod 0700 "${home_directory}/.ssh"
chmod 0600 "${home_directory}/.ssh/authorized_keys"
chown --recursive "${USERNAME}":"${USERNAME}" "${home_directory}/.ssh"

# Disable root SSH login with password
sed --in-place 's/^PermitRootLogin.*/PermitRootLogin prohibit-password/g' /etc/ssh/sshd_config
if sshd -t -q; then
    systemctl restart sshd
fi

# End: Configure the new non-root user


# Begin: Install Nginx

apt update
apt install nginx -y

# End: Install Nginx


# Begin: Configure the firewall to allow SSH, HTTP, and HTTPS

# Add exception for SSH and Nginx Full and then enable UFW firewall
ufw allow OpenSSH
ufw allow 'Nginx Full'
ufw --force enable

# End: Configure the firewall to allow SSH, HTTP, and HTTPS


# Begin: Configure Nginx to proxy the server traffic from any hostname to http://localhost:5000 for ASP.NET Core

# Configure the website for Nginx

git clone https://gitlab.com/portfolio-website-net/site-uptime.git /var/www/site-uptime

chmod -R 755 /var/www/site-uptime

printf 'server {
	
    listen 80;
    listen [::]:80;

    # Serve traffic to any hostname
    server_name server_name ~^.*$;

    location / {
        proxy_pass http://localhost:5000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection keep-alive;
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}' > /etc/nginx/sites-available/site-uptime

ln -s /etc/nginx/sites-available/site-uptime /etc/nginx/sites-enabled/

systemctl restart nginx

# End: Configure Nginx to proxy the server traffic from any hostname to http://localhost:5000 for ASP.NET Core


# Begin: Install .NET Core 3.1

wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb
dpkg -i packages-microsoft-prod.deb
add-apt-repository universe -y
apt install apt-transport-https -y
apt update
apt install dotnet-sdk-3.1 -y

rm packages-microsoft-prod.deb

# End: Install .NET Core 3.1


# Begin: Publish the ASP.NET Core app

cd /var/www/site-uptime
dotnet publish

nginx -s reload

# End: Publish the ASP.NET Core app


# Begin: Configure Systemd to manage the .NET app

cd /etc/systemd/system

printf '[Unit]
Description=Site Uptime

[Service]
WorkingDirectory=/var/www/site-uptime
ExecStart=/usr/bin/dotnet /var/www/site-uptime/bin/Debug/netcoreapp3.1/publish/site-uptime.dll
Restart=always
RestartSec=10
SyslogIdentifier=site-uptime
User=siteuptimeuser
Environment=ASPNETCORE_ENVIRONMENT=Production
Environment=DOTNET_PRINT_TELEMETRY_MESSAGE=false

[Install]
WantedBy=multi-user.target' > site-uptime.service

systemctl enable site-uptime.service
systemctl start site-uptime.service

# End: Configure Systemd to manage the .NET app


# Begin: Clean up the script files

rm -rf /root/site-uptime

# End: Clean up the script files