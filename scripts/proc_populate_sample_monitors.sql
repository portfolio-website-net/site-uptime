USE `siteuptime`;
DROP procedure IF EXISTS `siteuptime`.`PopulateSampleMonitors`;

DELIMITER $$
USE `siteuptime`$$
CREATE PROCEDURE `PopulateSampleMonitors`
(
	IN monitorCountParam INT
)
BEGIN
    SET @currentCount := 1;
 
    WHILE @currentCount <= monitorCountParam DO
    
		INSERT INTO `siteuptime`.`UptimeMonitors` (
			`UptimeMonitorTypeId`, 
			`UserId`, 
			`Name`,
			`Url`,
			`Port`,
			`Keyword`,
			`KeywordAlertWhenExists`,
			`Username`,
			`Password`,
			`UseBasicAuthentication`,
			`IgnoreSSLErrors`,
			`Interval`,
			`CreatedById`,
			`CreatedDate`
		)
		VALUES (1, 1, CONCAT('Sample Monitor ', @currentCount), CONCAT('http://example.com?', @currentCount), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 0, NOW());

        SET @currentCount = @currentCount + 1;
    END WHILE;
END$$

DELIMITER ;
;