USE `siteuptime`;
DROP procedure IF EXISTS `siteuptime`.`GetPublicStatusPageCurrentUptimeMonitorDetails`;

DELIMITER $$
USE `siteuptime`$$
CREATE PROCEDURE `GetPublicStatusPageCurrentUptimeMonitorDetails`
(
	IN statusPageKeyParam NVARCHAR(8)
)
BEGIN		
	SET @selectedMonitorCount := (SELECT COUNT(1)
									FROM `UptimePublicStatusPages` upsp
									JOIN `UptimeMonitorPublicStatusPages` umpsp ON upsp.UptimePublicStatusPageId = umpsp.UptimePublicStatusPageId);

    SELECT um1.`UptimeMonitorId`,
		   um1.`UptimeMonitorTypeId`,
		   um1.`UptimeStatusTypeId`,
		   um1.`Interval`,
		   (
			   SELECT AVG(umh.ResponseTime)
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeMonitorHistory` umh ON um.UptimeMonitorId = umh.UptimeMonitorId
				WHERE um.UptimeMonitorId = um1.UptimeMonitorId
				  AND umh.CreatedDate BETWEEN DATE_ADD(NOW(), interval -1 day) AND NOW()
		   ) AS `AverageResponseTime`,
		   (
			   SELECT ume.CreatedDate
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeMonitorEvents` ume ON um.UptimeMonitorId = ume.UptimeMonitorId
				WHERE um.UptimeMonitorId = um1.UptimeMonitorId
				ORDER BY ume.UpdatedDate DESC,
				      ume.CreatedDate DESC
				LIMIT 1
		   ) AS `LatestStatus_Date`,
		   (
			   SELECT ume.Duration
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeMonitorEvents` ume ON um.UptimeMonitorId = ume.UptimeMonitorId
				WHERE um.UptimeMonitorId = um1.UptimeMonitorId
				ORDER BY ume.UpdatedDate DESC,
				      ume.CreatedDate DESC
				LIMIT 1
		   ) AS `LatestStatus_Duration`,
		   um1.`Name`,
		   um1.`Url`,
		   um1.`UptimePercentageLast30Days`,
		   um1.`UptimePercentageLast7Days`,
		   um1.`UptimePercentageLast24Hours`,
		   um1.`UptimePercentage1DayAgo`,
		   um1.`UptimePercentage2DaysAgo`,
		   um1.`UptimePercentage3DaysAgo`,
		   um1.`UptimePercentage4DaysAgo`,
		   um1.`UptimePercentage5DaysAgo`,
		   um1.`UptimePercentage6DaysAgo`,
		   um1.`WasMonitorActive1DayAgo`,	
		   um1.`WasMonitorActive2DaysAgo`,
		   um1.`WasMonitorActive3DaysAgo`,
		   um1.`WasMonitorActive4DaysAgo`,
		   um1.`WasMonitorActive5DaysAgo`,
		   um1.`WasMonitorActive6DaysAgo`,
		   (
			   SELECT ume.CreatedDate
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeMonitorEvents` ume ON um.UptimeMonitorId = ume.UptimeMonitorId
				INNER JOIN `UptimeStatusTypes` ust ON ume.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UptimeMonitorId = um1.UptimeMonitorId
				  AND ust.Code IN ('Down')
				ORDER BY ume.UpdatedDate DESC,
				      ume.CreatedDate DESC
				LIMIT 1
		   ) AS `LatestDownTime_Date`,
		   (
			   SELECT ume.Duration
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeMonitorEvents` ume ON um.UptimeMonitorId = ume.UptimeMonitorId
				INNER JOIN `UptimeStatusTypes` ust ON ume.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UptimeMonitorId = um1.UptimeMonitorId
				  AND ust.Code IN ('Down')
				ORDER BY ume.UpdatedDate DESC,
				      ume.CreatedDate DESC
				LIMIT 1
		   ) AS `LatestDownTime_Duration`
	  FROM `UptimePublicStatusPages` upsp
	 INNER JOIN `UptimeMonitors` um1 ON upsp.UserId = um1.UserId
	  LEFT JOIN `UptimeMonitorPublicStatusPages` umpsp ON um1.UptimeMonitorId = umpsp.UptimeMonitorId
	 WHERE (@selectedMonitorCount = 0 OR umpsp.UptimePublicStatusPageId = upsp.UptimePublicStatusPageId)
	   AND upsp.StatusPageKey = statusPageKeyParam;
END$$

DELIMITER ;
;