USE `siteuptime`;
DROP procedure IF EXISTS `siteuptime`.`QueuePendingMonitors`;

DELIMITER $$
USE `siteuptime`$$
CREATE PROCEDURE `QueuePendingMonitors`
(
	IN defaultTimeoutParam INT,
	IN defaultUptimeCapacityParam INT,
	IN uptimeMonitorLocationParam NVARCHAR(50),
	IN uptimeMonitorIpAddressParam NVARCHAR(50),    
	IN requireDownStatusVerificationParam BIT
)
BEGIN
	DECLARE uptimeCapacityAvailable INT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;  -- Rollback any error in the transaction
    END;
	
	START TRANSACTION;	

	SET @currentlyProcessingCount := (
		SELECT COUNT(1)
		  FROM `UptimeMonitors`
		 WHERE UptimeMonitorLocation = uptimeMonitorLocationParam
		   AND UptimeMonitorIpAddress = uptimeMonitorIpAddressParam
		   AND (IFNULL(ProcessingTimeoutDate, NOW()) > NOW()
		        OR IFNULL(ConfirmationTimeoutDate, NOW()) > NOW())
	);

	SET uptimeCapacityAvailable := defaultUptimeCapacityParam - @currentlyProcessingCount;

	IF uptimeCapacityAvailable < 0 THEN
		SET uptimeCapacityAvailable := 0;
	END IF;

	DROP TEMPORARY TABLE IF EXISTS `UptimeMonitorsTempTable`;
	CREATE TEMPORARY TABLE `UptimeMonitorsTempTable` (
		`UptimeMonitorId` INT
    );

	INSERT INTO `UptimeMonitorsTempTable`
	SELECT `UptimeMonitorId`
	  FROM `UptimeMonitors` um
	  LEFT JOIN `UptimeStatusTypes` ust ON um.UptimeStatusTypeId = ust.UptimeStatusTypeId
	 WHERE (ust.Code <> 'Paused' OR ust.Code IS NULL)
	   AND
	   		(
				-- Monitors available to any uptime agent
				(
					(
						um.NextMonitorDate IS NULL
							OR um.NextMonitorDate <= NOW()
					)
					AND um.ProcessingTimeoutDate IS NULL
					AND um.ConfirmationTimeoutDate IS NULL
				)
				OR
                -- Monitors available to any uptime agent due to a processing timeout
				(
					um.ProcessingTimeoutDate IS NOT NULL
						AND um.ProcessingTimeoutDate <= NOW()
				)
				OR
                -- Monitors available to a different confirmation uptime agent to confirm DOWN status
				(
					(
						um.ConfirmationTimeoutDate IS NOT NULL
							OR um.ConfirmationTimeoutDate <= NOW()
					)
					AND 
					(
						(um.UptimeMonitorLocation <> uptimeMonitorLocationParam
							AND um.UptimeMonitorIpAddress <> uptimeMonitorIpAddressParam
						)
						OR requireDownStatusVerificationParam = 0
					)
				)
	   	   )
	 ORDER BY um.NextMonitorDate,
		   um.ConfirmationTimeoutDate,
		   um.ProcessingTimeoutDate
	 LIMIT uptimeCapacityAvailable;

	UPDATE `UptimeMonitors` um
      JOIN `UptimeMonitorsTempTable` umtt ON um.UptimeMonitorId = umtt.UptimeMonitorId
       SET um.ProcessingTimeoutDate = DATE_ADD(NOW(), interval defaultTimeoutParam second)       
	 WHERE um.ConfirmationTimeoutDate IS NULL;
     
	UPDATE `UptimeMonitors` um
      JOIN `UptimeMonitorsTempTable` umtt ON um.UptimeMonitorId = umtt.UptimeMonitorId
       SET um.ConfirmationTimeoutDate = DATE_ADD(NOW(), interval defaultTimeoutParam second)       
	 WHERE um.ConfirmationTimeoutDate IS NOT NULL;
     
	UPDATE `UptimeMonitors` um
      JOIN `UptimeMonitorsTempTable` umtt ON um.UptimeMonitorId = umtt.UptimeMonitorId
       SET um.UptimeMonitorLocation = uptimeMonitorLocationParam,
           um.UptimeMonitorIpAddress = uptimeMonitorIpAddressParam,
           um.UpdatedById = 0,
           um.UpdatedDate = NOW();

	SELECT um.*
	  FROM `UptimeMonitors` um
	  JOIN `UptimeMonitorsTempTable` umtt ON um.UptimeMonitorId = umtt.UptimeMonitorId;

	COMMIT;
END$$

DELIMITER ;
;