USE `siteuptime`;
DROP procedure IF EXISTS `siteuptime`.`GetCurrentUptimeMonitorDetails`;

DELIMITER $$
USE `siteuptime`$$
CREATE PROCEDURE `GetCurrentUptimeMonitorDetails`
(
	IN uptimeMonitorIdParam INT
)
BEGIN		
    SELECT `UptimeMonitorId`,
		   `UptimeMonitorTypeId`,
		   `UptimeStatusTypeId`,
		   `Interval`,
		   (
			   SELECT AVG(umh.ResponseTime)
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeMonitorHistory` umh ON um.UptimeMonitorId = umh.UptimeMonitorId
				WHERE um.UptimeMonitorId = uptimeMonitorIdParam
				  AND umh.CreatedDate BETWEEN DATE_ADD(NOW(), interval -1 day) AND NOW()
		   ) AS `AverageResponseTime`,
		   (
			   SELECT ume.CreatedDate
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeMonitorEvents` ume ON um.UptimeMonitorId = ume.UptimeMonitorId
				WHERE um.UptimeMonitorId = uptimeMonitorIdParam
				ORDER BY ume.UpdatedDate DESC,
				      ume.CreatedDate DESC
				LIMIT 1
		   ) AS `LatestStatus_Date`,
		   (
			   SELECT ume.Duration
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeMonitorEvents` ume ON um.UptimeMonitorId = ume.UptimeMonitorId
				WHERE um.UptimeMonitorId = uptimeMonitorIdParam
				ORDER BY ume.UpdatedDate DESC,
				      ume.CreatedDate DESC
				LIMIT 1
		   ) AS `LatestStatus_Duration`,
		   `Name`,
		   `Url`,
		   `UptimePercentageLast30Days`,
		   `UptimePercentageLast7Days`,
		   `UptimePercentageLast24Hours`,
		   `UptimePercentage1DayAgo`,
		   `UptimePercentage2DaysAgo`,
		   `UptimePercentage3DaysAgo`,
		   `UptimePercentage4DaysAgo`,
		   `UptimePercentage5DaysAgo`,
		   `UptimePercentage6DaysAgo`,
		   `WasMonitorActive1DayAgo`,	
		   `WasMonitorActive2DaysAgo`,
		   `WasMonitorActive3DaysAgo`,
		   `WasMonitorActive4DaysAgo`,
		   `WasMonitorActive5DaysAgo`,
		   `WasMonitorActive6DaysAgo`,
		   (
			   SELECT ume.CreatedDate
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeMonitorEvents` ume ON um.UptimeMonitorId = ume.UptimeMonitorId
				INNER JOIN `UptimeStatusTypes` ust ON ume.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UptimeMonitorId = uptimeMonitorIdParam
				  AND ust.Code IN ('Down')
				ORDER BY ume.UpdatedDate DESC,
				      ume.CreatedDate DESC
				LIMIT 1
		   ) AS `LatestDownTime_Date`,
		   (
			   SELECT ume.Duration
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeMonitorEvents` ume ON um.UptimeMonitorId = ume.UptimeMonitorId
				INNER JOIN `UptimeStatusTypes` ust ON ume.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UptimeMonitorId = uptimeMonitorIdParam
				  AND ust.Code IN ('Down')
				ORDER BY ume.UpdatedDate DESC,
				      ume.CreatedDate DESC
				LIMIT 1
		   ) AS `LatestDownTime_Duration`
	  FROM `UptimeMonitors` um1
     WHERE um1.UptimeMonitorId = uptimeMonitorIdParam;
END$$

DELIMITER ;
;