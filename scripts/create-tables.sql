CREATE DATABASE IF NOT EXISTS `siteuptime`;

USE `siteuptime`;

DROP TABLE IF EXISTS `UptimeMonitorApiKeys`;
DROP TABLE IF EXISTS `UptimeMonitorApiKeyTypes`;
DROP TABLE IF EXISTS `UptimeMonitorAlertContacts`;
DROP TABLE IF EXISTS `UptimeMonitorCustomHttpStatuses`;
DROP TABLE IF EXISTS `UptimeMonitorCustomHeaders`;
DROP TABLE IF EXISTS `UptimeMonitorEvents`;
DROP TABLE IF EXISTS `UptimeMonitorHistory`;
DROP TABLE IF EXISTS `UptimeMonitorMaintenanceWindows`;
DROP TABLE IF EXISTS `UptimeMaintenanceWindows`;
DROP TABLE IF EXISTS `UptimeMaintenanceWindowTypes`;
DROP TABLE IF EXISTS `UptimeMonitorPublicStatusPages`;
DROP TABLE IF EXISTS `UptimePublicStatusPages`;
DROP TABLE IF EXISTS `UptimeMonitors`;
DROP TABLE IF EXISTS `UptimeMonitorTypes`;
DROP TABLE IF EXISTS `UptimeMonitorHttpMethodTypes`;
DROP TABLE IF EXISTS `UptimeStatusTypes`;
DROP TABLE IF EXISTS `UptimeReasonTypes`;
DROP TABLE IF EXISTS `UptimeAlertContacts`;
DROP TABLE IF EXISTS `UptimeAlertContactTypes`;
DROP TABLE IF EXISTS `UptimeAlertContactStatusTypes`;
DROP TABLE IF EXISTS `UptimeNotificationEventTypes`;
DROP TABLE IF EXISTS `UptimeMobileProviderTypes`;
DROP TABLE IF EXISTS `EventLogs`;
DROP TABLE IF EXISTS `UserU2fAuthenticationRequests`;
DROP TABLE IF EXISTS `UserU2fDevices`;
DROP TABLE IF EXISTS `UserSessions`;
DROP TABLE IF EXISTS `Users`;
DROP TABLE IF EXISTS `Errors`;
DROP TABLE IF EXISTS `UptimeMonitorAgents`;
DROP TABLE IF EXISTS `UptimeMonitorSortTypes`;
DROP TABLE IF EXISTS `UptimeMonitorLocations`;
DROP TABLE IF EXISTS `UserTimeZoneTypes`;
DROP TABLE IF EXISTS `UserSecurityQuestionTypes`;
DROP TABLE IF EXISTS `TwoFactorAuthenticationTypes`;

CREATE TABLE `UptimeMonitorSortTypes` (
	`UptimeMonitorSortTypeId` INT AUTO_INCREMENT PRIMARY KEY,
	`Name` NVARCHAR(100) NOT NULL,
	`Code` NVARCHAR(50) NULL,
	`Order` INT NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL
);

INSERT INTO `UptimeMonitorSortTypes` (`Name`, `Code`, `Order`, `CreatedById`, `CreatedDate`)
SELECT 'Friendly Name (a-z)', 'FriendlyNameAZ', 1, 0, NOW() UNION
SELECT 'Friendly Name (z-a)', 'FriendlyNameZA', 2, 0, NOW() UNION
SELECT 'Status (up-down-paused)', 'StatusUpDownPaused', 3, 0, NOW() UNION
SELECT 'Status (down-up-paused)', 'StatusDownUpPaused', 4, 0, NOW() UNION
SELECT 'Status (paused-up-down)', 'StatusPausedUpDown', 5, 0, NOW() UNION
SELECT 'Type (http-kywd-ping-port-hrtb)', 'TypeHttpKywdPingPortHrtb', 6, 0, NOW() UNION
SELECT 'Type (kywd-http-ping-port-hrtb)', 'TypeKywdHttpPingPortHrtb', 7, 0, NOW() UNION
SELECT 'Type (ping-port-http-kywd-hrtb)', 'TypePingPortHttpKywdHrtb', 8, 0, NOW() UNION
SELECT 'Type (port-ping-http-kywd-hrtb)', 'TypePortPingHttpKywdHrtb', 9, 0, NOW() UNION
SELECT 'Type (hrtb-http-kywd-ping-port)', 'TypeHrtbHttpKywdPingPort', 10, 0, NOW();

CREATE TABLE `UserTimeZoneTypes` (
	`UserTimeZoneTypeId` INT AUTO_INCREMENT PRIMARY KEY,
	`Name` NVARCHAR(100) NOT NULL,
	`Code` NVARCHAR(50) NULL,
	`Order` INT NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL
);

INSERT INTO `UserTimeZoneTypes` (`Name`, `Code`, `Order`, `CreatedById`, `CreatedDate`)
SELECT 'Eastern Time (US & Canada), Bogota (GMT -05:00:00)', 'GMT__05', 1, 0, NOW();

CREATE TABLE `UserSecurityQuestionTypes` (
	`UserSecurityQuestionTypeId` INT AUTO_INCREMENT PRIMARY KEY,
	`Name` NVARCHAR(100) NOT NULL,
	`Code` NVARCHAR(50) NULL,
	`Order` INT NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL
);

INSERT INTO `UserSecurityQuestionTypes` (`Name`, `Code`, `Order`, `CreatedById`, `CreatedDate`)
SELECT 'What was the first concert you attended to?', '1', 1, 0, NOW() UNION
SELECT 'What is your favorite movie?', '2', 2, 0, NOW() UNION
SELECT 'What is the name of your first grade teacher?', '3', 3, 0, NOW() UNION
SELECT 'What school did you attend for first grade?', '4', 4, 0, NOW() UNION
SELECT 'What is your oldest cousin''s first name?', '5', 5, 0, NOW() UNION
SELECT 'What street did you live on in third grade?', '6', 6, 0, NOW();

CREATE TABLE `TwoFactorAuthenticationTypes` (
	`TwoFactorAuthenticationTypeId` INT AUTO_INCREMENT PRIMARY KEY,
	`Name` NVARCHAR(100) NOT NULL,
	`Code` NVARCHAR(50) NULL,
	`Order` INT NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL
);

INSERT INTO `TwoFactorAuthenticationTypes` (`Name`, `Code`, `Order`, `CreatedById`, `CreatedDate`)
SELECT 'Authentication App (TOTP)', 'Totp', 1, 0, NOW() UNION
SELECT 'Device Authentication (U2F)', 'U2f', 2, 0, NOW() UNION
SELECT 'Text Message Authentication', 'Text', 3, 0, NOW();

CREATE TABLE `EventLogs` (
	`EventLogId` INT AUTO_INCREMENT PRIMARY KEY,
	`UserId` INT NOT NULL,
	`EventName` NVARCHAR(100) NOT NULL,
	`Message` NVARCHAR(4000) NULL,
	`Path` NVARCHAR(4000) NULL,
	`IpAddress` NVARCHAR(50) NULL,	
	`UserAgent` NVARCHAR(4000) NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL
);

CREATE TABLE `Users` (
	`UserId` INT AUTO_INCREMENT PRIMARY KEY,
	`Username` NVARCHAR(100) NOT NULL,
	`PasswordHash` NVARCHAR(100) NOT NULL,
	`PasswordSalt` NVARCHAR(100) NOT NULL,
	`FirstName` NVARCHAR(100) NOT NULL,
	`LastName` NVARCHAR(100) NOT NULL,
	`Email` NVARCHAR(100) NOT NULL,
	`Phone` NVARCHAR(100) NULL,
	`Claims` NVARCHAR(100) NOT NULL,
	`UserSecurityQuestionTypeId` INT NULL,
	`UserSecurityAnswer` NVARCHAR(100) NULL,
	`TwoFactorAuthenticationTypeId` INT NULL,
	`TwoFactorKey` NVARCHAR(4) NULL,
	`OneTimePasscode` NVARCHAR(8) NULL,
	`NewRequestedEmail` NVARCHAR(100) NULL,
	`IsActivated` BIT NOT NULL,
	`IsLocked` BIT NOT NULL,
	`IncorrectAuthCount` INT NULL,
	`UptimeMonitorSortTypeId` INT NULL,
	`UserTimeZoneTypeId` INT NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL,
	FOREIGN KEY (`UserSecurityQuestionTypeId`) 
        REFERENCES UserSecurityQuestionTypes(`UserSecurityQuestionTypeId`),
	FOREIGN KEY (`TwoFactorAuthenticationTypeId`) 
        REFERENCES TwoFactorAuthenticationTypes(`TwoFactorAuthenticationTypeId`),
	FOREIGN KEY (`UptimeMonitorSortTypeId`) 
        REFERENCES UptimeMonitorSortTypes(`UptimeMonitorSortTypeId`),
	FOREIGN KEY (`UserTimeZoneTypeId`) 
        REFERENCES UserTimeZoneTypes(`UserTimeZoneTypeId`)
);

CREATE TABLE `UserU2fAuthenticationRequests` (
	`UserU2fAuthenticationRequestId` INT AUTO_INCREMENT PRIMARY KEY,
	`UserId` INT NOT NULL,
	`KeyHandle` NVARCHAR(200) NULL,
	`Challenge` NVARCHAR(200) NOT NULL,
	`AppId` NVARCHAR(200) NOT NULL,
	`Version` NVARCHAR(50) NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL,
	FOREIGN KEY (`UserId`) 
        REFERENCES Users(`UserId`)
);

CREATE TABLE `UserU2fDevices` (
	`UserU2fDeviceId` INT AUTO_INCREMENT PRIMARY KEY,
	`UserId` INT NOT NULL,
	`DeviceName` NVARCHAR(100) NOT NULL,
	`KeyHandle` VARBINARY(1000) NOT NULL,
	`PublicKey` VARBINARY(1000) NOT NULL,
	`AttestationCert` VARBINARY(1000) NOT NULL,
	`Counter` INT NOT NULL,
	`IsCompromised` BIT NOT NULL,
	`IsActive` BIT NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL,
	FOREIGN KEY (`UserId`) 
        REFERENCES Users(`UserId`)
);

CREATE TABLE `UserSessions` (
	`UserSessionId` INT AUTO_INCREMENT PRIMARY KEY,
	`UserId` INT NOT NULL,
	`SessionKey` NVARCHAR(8) NOT NULL,
	`IsValid` BIT NOT NULL,
	`ExpirationDate` DATETIME(3) NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL,
	FOREIGN KEY (`UserId`) 
        REFERENCES Users(`UserId`)
);

CREATE TABLE `UptimeMonitorTypes` (
	`UptimeMonitorTypeId` INT AUTO_INCREMENT PRIMARY KEY,
	`Name` NVARCHAR(100) NOT NULL,
	`Code` NVARCHAR(50) NULL,
	`Order` INT NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL
);

INSERT INTO `UptimeMonitorTypes` (`Name`, `Code`, `Order`, `CreatedById`, `CreatedDate`)
SELECT 'HTTP(s)', 'Http', 1, 0, NOW() UNION
SELECT 'Keyword', 'Keyword', 2, 0, NOW() UNION
SELECT 'Ping', 'Ping', 3, 0, NOW() UNION
SELECT 'Port', 'Port', 4, 0, NOW() UNION
SELECT 'Heartbeat', 'Heartbeat', 5, 0, NOW();

CREATE TABLE `UptimeMonitorHttpMethodTypes` (
	`UptimeMonitorHttpMethodTypeId` INT AUTO_INCREMENT PRIMARY KEY,
	`Name` NVARCHAR(100) NOT NULL,
	`Code` NVARCHAR(50) NULL,
	`Order` INT NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL
);

INSERT INTO `UptimeMonitorHttpMethodTypes` (`Name`, `Code`, `Order`, `CreatedById`, `CreatedDate`)
SELECT 'HEAD', 'Head', 1, 0, NOW() UNION
SELECT 'GET', 'Get', 2, 0, NOW() UNION
SELECT 'POST', 'Post', 3, 0, NOW() UNION
SELECT 'PUT', 'Put', 4, 0, NOW() UNION
SELECT 'PATCH', 'Patch', 5, 0, NOW() UNION
SELECT 'DELETE', 'Delete', 6, 0, NOW() UNION
SELECT 'OPTIONS', 'Options', 7, 0, NOW();

CREATE TABLE `UptimeStatusTypes` (
	`UptimeStatusTypeId` INT AUTO_INCREMENT PRIMARY KEY,
	`Name` NVARCHAR(100) NOT NULL,
	`Code` NVARCHAR(50) NULL,
	`Order` INT NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL
);

INSERT INTO `UptimeStatusTypes` (`Name`, `Code`, `Order`, `CreatedById`, `CreatedDate`)
SELECT 'Started', 'Started', 1, 0, NOW() UNION
SELECT 'Up', 'Up', 2, 0, NOW() UNION
SELECT 'Likely Down', 'LikelyDown', 3, 0, NOW() UNION
SELECT 'Down', 'Down', 4, 0, NOW() UNION
SELECT 'Paused', 'Paused', 5, 0, NOW();

CREATE TABLE `UptimeReasonTypes` (
	`UptimeReasonTypeId` INT AUTO_INCREMENT PRIMARY KEY,
	`Name` NVARCHAR(100) NOT NULL,
	`Code` NVARCHAR(50) NULL,
	`Order` INT NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL
);

INSERT INTO `UptimeReasonTypes` (`Name`, `Code`, `Order`, `CreatedById`, `CreatedDate`)
SELECT 'Started', 'Started', 1, 0, NOW() UNION
SELECT 'OK', 'Ok', 2, 0, NOW() UNION
SELECT 'No Response', 'NoResponse', 3, 0, NOW() UNION
SELECT 'Not Found (404)', 'NotFound', 4, 0, NOW() UNION
SELECT 'Error', 'Error', 5, 0, NOW() UNION
SELECT 'Keyword Found', 'KeywordFound', 6, 0, NOW() UNION
SELECT 'Keyword Not Found', 'KeywordNotFound', 7, 0, NOW() UNION
SELECT 'Paused', 'Paused', 8, 0, NOW();

CREATE TABLE `UptimeAlertContactTypes` (
	`UptimeAlertContactTypeId` INT AUTO_INCREMENT PRIMARY KEY,
	`Name` NVARCHAR(100) NOT NULL,
	`Code` NVARCHAR(50) NULL,
	`Order` INT NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL
);

INSERT INTO `UptimeAlertContactTypes` (`Name`, `Code`, `Order`, `CreatedById`, `CreatedDate`)
SELECT 'Email', 'Email', 1, 0, NOW() UNION
SELECT 'Webhook', 'Webhook', 2, 0, NOW() UNION
SELECT 'Email-to-SMS', 'EmailToSms', 3, 0, NOW();

CREATE TABLE `UptimeAlertContactStatusTypes` (
	`UptimeAlertContactStatusTypeId` INT AUTO_INCREMENT PRIMARY KEY,
	`Name` NVARCHAR(100) NOT NULL,
	`Code` NVARCHAR(50) NULL,
	`Order` INT NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL
);

INSERT INTO `UptimeAlertContactStatusTypes` (`Name`, `Code`, `Order`, `CreatedById`, `CreatedDate`)
SELECT 'Not Activated', 'NotActivated', 1, 0, NOW() UNION
SELECT 'Paused', 'Paused', 2, 0, NOW() UNION
SELECT 'Active', 'Active', 3, 0, NOW();

CREATE TABLE `UptimeNotificationEventTypes` (
	`UptimeNotificationEventTypeId` INT AUTO_INCREMENT PRIMARY KEY,
	`Name` NVARCHAR(100) NOT NULL,
	`Code` NVARCHAR(50) NULL,
	`Order` INT NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL
);

INSERT INTO `UptimeNotificationEventTypes` (`Name`, `Code`, `Order`, `CreatedById`, `CreatedDate`)
SELECT 'Up & down events', 'UpDown', 1, 0, NOW() UNION
SELECT 'Only down events', 'OnlyDown', 2, 0, NOW() UNION
SELECT 'Only up events', 'OnlyUp', 3, 0, NOW();

CREATE TABLE `UptimeMobileProviderTypes` (
	`UptimeMobileProviderTypeId` INT AUTO_INCREMENT PRIMARY KEY,
	`Name` NVARCHAR(100) NOT NULL,
	`Address` NVARCHAR(100) NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL
);

INSERT INTO `UptimeMobileProviderTypes` (`Name`, `Address`, `CreatedById`, `CreatedDate`)
SELECT 'T-Mobile (US)', 'tmomail.net', 0, NOW();

CREATE TABLE `UptimeAlertContacts` (
	`UptimeAlertContactId` INT AUTO_INCREMENT PRIMARY KEY,
	`UptimeAlertContactTypeId` INT NOT NULL,
	`UptimeAlertContactStatusTypeId` INT NOT NULL,
	`UserId` INT NOT NULL,
	`Name` NVARCHAR(100) NOT NULL,
	`Address` NVARCHAR(4000) NOT NULL,
	`UptimeNotificationEventTypeId` INT NOT NULL,
	`SendDefaultQueryStringForWebhook` BIT NOT NULL,
	`PostValue` NVARCHAR(4000) NULL,
	`SendJsonForWebhook` BIT NOT NULL,
	`SendDefaultPostForWebhook` BIT NOT NULL,
	`UptimeMobileProviderTypeId` INT NULL,	
	`ActivationCode` NVARCHAR(36) NULL,
	`IsActivated` BIT NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL,
	FOREIGN KEY (`UptimeAlertContactTypeId`) 
        REFERENCES UptimeAlertContactTypes(`UptimeAlertContactTypeId`),
	FOREIGN KEY (`UptimeAlertContactStatusTypeId`) 
        REFERENCES UptimeAlertContactStatusTypes(`UptimeAlertContactStatusTypeId`),
	FOREIGN KEY (`UserId`) 
        REFERENCES Users(`UserId`),
    FOREIGN KEY (`UptimeNotificationEventTypeId`) 
        REFERENCES UptimeNotificationEventTypes(`UptimeNotificationEventTypeId`),
	FOREIGN KEY (`UptimeMobileProviderTypeId`) 
        REFERENCES UptimeMobileProviderTypes(`UptimeMobileProviderTypeId`)
);

CREATE TABLE `UptimeMonitors` (
	`UptimeMonitorId` INT AUTO_INCREMENT PRIMARY KEY,
	`UptimeMonitorTypeId` INT NOT NULL,
	`UptimeMonitorHttpMethodTypeId` INT NOT NULL,
	`UserId` INT NOT NULL,
	`Name` NVARCHAR(100) NOT NULL,
	`Url` NVARCHAR(4000) NULL,
	`Port` INT NULL,
	`Keyword` NVARCHAR(200) NULL,
	`KeywordAlertWhenExists` BIT NULL,
	`Username` NVARCHAR(100) NULL,
	`Password` NVARCHAR(100) NULL,
	`UseBasicAuthentication` BIT NULL,
	`IgnoreSSLErrors` BIT NULL,
	`Interval` INT NOT NULL,
	`ResponseTime` INT NULL,
	`StatusCode` INT NULL,
	`ResponseContent` NVARCHAR(1000) NULL,
	`UptimeStatusTypeId` INT NULL,
	`UptimeReasonTypeId` INT NULL,
	`UptimePercentageLast30Days` FLOAT NULL,
	`UptimePercentageLast7Days` FLOAT NULL,
	`UptimePercentageLast24Hours` FLOAT NULL,
	`UptimePercentage1DayAgo` FLOAT NULL,
	`UptimePercentage2DaysAgo` FLOAT NULL,
	`UptimePercentage3DaysAgo` FLOAT NULL,
	`UptimePercentage4DaysAgo` FLOAT NULL,
	`UptimePercentage5DaysAgo` FLOAT NULL,
	`UptimePercentage6DaysAgo` FLOAT NULL,
	`WasMonitorActive1DayAgo` BIT NULL,	
	`WasMonitorActive2DaysAgo` BIT NULL,
	`WasMonitorActive3DaysAgo` BIT NULL,
	`WasMonitorActive4DaysAgo` BIT NULL,	
	`WasMonitorActive5DaysAgo` BIT NULL,
	`WasMonitorActive6DaysAgo` BIT NULL,
	`NextMonitorDate` DATETIME(3) NULL,
	`ProcessingTimeoutDate` DATETIME(3) NULL,
	`ConfirmationTimeoutDate` DATETIME(3) NULL,
	`UptimeMonitorLocation` NVARCHAR(50) NULL,
	`UptimeMonitorIpAddress` NVARCHAR(50) NULL,
	`HeartbeatDate` DATETIME(3) NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL,
	FOREIGN KEY (`UptimeMonitorTypeId`) 
        REFERENCES UptimeMonitorTypes(`UptimeMonitorTypeId`),
	FOREIGN KEY (`UptimeMonitorHttpMethodTypeId`) 
        REFERENCES UptimeMonitorHttpMethodTypes(`UptimeMonitorHttpMethodTypeId`),
	FOREIGN KEY (`UserId`) 
        REFERENCES Users(`UserId`),
    FOREIGN KEY (`UptimeStatusTypeId`) 
        REFERENCES UptimeStatusTypes(`UptimeStatusTypeId`),
	FOREIGN KEY (`UptimeReasonTypeId`) 
        REFERENCES UptimeReasonTypes(`UptimeReasonTypeId`)
);

CREATE TABLE `UptimeMonitorAlertContacts` (
	`UptimeMonitorAlertContactId` INT AUTO_INCREMENT PRIMARY KEY,
	`UptimeMonitorId` INT NOT NULL,
	`UptimeAlertContactId` INT NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL,
	FOREIGN KEY (`UptimeMonitorId`) 
        REFERENCES UptimeMonitors(`UptimeMonitorId`),
	FOREIGN KEY (`UptimeAlertContactId`) 
        REFERENCES UptimeAlertContacts(`UptimeAlertContactId`)
);

CREATE TABLE `UptimeMonitorCustomHttpStatuses` (
	`UptimeMonitorCustomHttpStatusId` INT AUTO_INCREMENT PRIMARY KEY,
	`UptimeMonitorId` INT NOT NULL,
	`StatusCode` INT NOT NULL,
	`UpStatusCode` BIT NOT NULL,
	`Order` INT NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL,
	FOREIGN KEY (`UptimeMonitorId`) 
        REFERENCES UptimeMonitors(`UptimeMonitorId`)
);

CREATE TABLE `UptimeMonitorCustomHeaders` (
	`UptimeMonitorCustomHeaderId` INT AUTO_INCREMENT PRIMARY KEY,
	`UptimeMonitorId` INT NOT NULL,
	`Name` NVARCHAR(100) NOT NULL,
	`Value` NVARCHAR(4000) NOT NULL,
	`Order` INT NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL,
	FOREIGN KEY (`UptimeMonitorId`) 
        REFERENCES UptimeMonitors(`UptimeMonitorId`)
);

CREATE TABLE `UptimeMaintenanceWindowTypes` (
	`UptimeMaintenanceWindowTypeId` INT AUTO_INCREMENT PRIMARY KEY,
	`Name` NVARCHAR(100) NOT NULL,
	`Code` NVARCHAR(50) NULL,
	`Order` INT NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL
);

INSERT INTO `UptimeMaintenanceWindowTypes` (`Name`, `Code`, `Order`, `CreatedById`, `CreatedDate`)
SELECT 'Once', 'Once', 1, 0, NOW() UNION
SELECT 'Daily', 'Daily', 2, 0, NOW() UNION
SELECT 'Weekly', 'Weekly', 3, 0, NOW() UNION
SELECT 'Monthly', 'Monthly', 4, 0, NOW();

CREATE TABLE `UptimeMaintenanceWindows` (
	`UptimeMaintenanceWindowId` INT AUTO_INCREMENT PRIMARY KEY,
	`UptimeMaintenanceWindowTypeId` INT NOT NULL,
	`UserId` INT NOT NULL,
	`Name` NVARCHAR(100) NOT NULL,	
	`StartTime` NVARCHAR(12) NOT NULL,
	`Duration` INT NOT NULL,
	`Days` NVARCHAR(1000) NULL,
	`IsActive` BIT NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL,
	FOREIGN KEY (`UptimeMaintenanceWindowTypeId`) 
        REFERENCES UptimeMaintenanceWindowTypes(`UptimeMaintenanceWindowTypeId`),
	FOREIGN KEY (`UserId`) 
        REFERENCES Users(`UserId`) 
);

CREATE TABLE `UptimeMonitorMaintenanceWindows` (
	`UptimeMonitorMaintenanceWindowId` INT AUTO_INCREMENT PRIMARY KEY,
	`UptimeMonitorId` INT NOT NULL,
	`UptimeMaintenanceWindowId` INT NOT NULL,	
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL,
	FOREIGN KEY (`UptimeMonitorId`) 
        REFERENCES UptimeMonitors(`UptimeMonitorId`),
	FOREIGN KEY (`UptimeMaintenanceWindowId`) 
        REFERENCES UptimeMaintenanceWindows(`UptimeMaintenanceWindowId`)
);

CREATE TABLE `UptimePublicStatusPages` (
	`UptimePublicStatusPageId` INT AUTO_INCREMENT PRIMARY KEY,
	`UserId` INT NOT NULL,
	`Name` NVARCHAR(100) NOT NULL,	
	`LogoKey` NVARCHAR(8) NULL,
	`CustomDomain` NVARCHAR(1000) NULL,
	`StatusPageKey` NVARCHAR(8) NOT NULL,
	`PasswordHash` NVARCHAR(100) NULL,
	`PasswordSalt` NVARCHAR(100) NULL,
	`UptimeMonitorSortTypeId` INT NULL,
	`HideBrandingLogo` BIT NOT NULL,
	`AnnouncementsHtml` NVARCHAR(4000) NULL,
	`IsActive` BIT NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL,
	FOREIGN KEY (`UserId`) 
        REFERENCES Users(`UserId`),
	FOREIGN KEY (`UptimeMonitorSortTypeId`) 
        REFERENCES UptimeMonitorSortTypes(`UptimeMonitorSortTypeId`) 
);

CREATE TABLE `UptimeMonitorPublicStatusPages` (
	`UptimeMonitorPublicStatusPageId` INT AUTO_INCREMENT PRIMARY KEY,
	`UptimeMonitorId` INT NOT NULL,
	`UptimePublicStatusPageId` INT NOT NULL,	
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL,
	FOREIGN KEY (`UptimeMonitorId`) 
        REFERENCES UptimeMonitors(`UptimeMonitorId`),
	FOREIGN KEY (`UptimePublicStatusPageId`) 
        REFERENCES UptimePublicStatusPages(`UptimePublicStatusPageId`)
);

CREATE TABLE `UptimeMonitorEvents` (
	`UptimeMonitorEventId` INT AUTO_INCREMENT PRIMARY KEY,
	`UptimeMonitorId` INT NOT NULL,
	`UptimeStatusTypeId` INT NULL,
	`UptimeReasonTypeId` INT NULL,
	`StatusCode` INT NULL,
	`Duration` INT NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL,
	FOREIGN KEY (`UptimeMonitorId`) 
        REFERENCES UptimeMonitors(`UptimeMonitorId`),
    FOREIGN KEY (`UptimeStatusTypeId`) 
        REFERENCES UptimeStatusTypes(`UptimeStatusTypeId`),
	FOREIGN KEY (`UptimeReasonTypeId`) 
        REFERENCES UptimeReasonTypes(`UptimeReasonTypeId`)
);

CREATE TABLE `UptimeMonitorHistory` (
	`UptimeMonitorHistoryId` INT AUTO_INCREMENT PRIMARY KEY,
	`UptimeMonitorId` INT NOT NULL,
	`UptimeStatusTypeId` INT NULL,
	`UptimeReasonTypeId` INT NULL,
	`StatusCode` INT NULL,
	`ResponseTime` INT NULL,
	`UptimeMonitorLocation` NVARCHAR(50) NULL,
	`UptimeMonitorIpAddress` NVARCHAR(50) NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL,
    FOREIGN KEY (`UptimeMonitorId`) 
        REFERENCES UptimeMonitors(`UptimeMonitorId`),
    FOREIGN KEY (`UptimeStatusTypeId`) 
        REFERENCES UptimeStatusTypes(`UptimeStatusTypeId`),
	FOREIGN KEY (`UptimeReasonTypeId`) 
        REFERENCES UptimeReasonTypes(`UptimeReasonTypeId`)
);

CREATE TABLE `UptimeMonitorApiKeyTypes` (
	`UptimeMonitorApiKeyTypeId` INT AUTO_INCREMENT PRIMARY KEY,
	`Name` NVARCHAR(100) NOT NULL,
	`Code` NVARCHAR(50) NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL
);

INSERT INTO `UptimeMonitorApiKeyTypes` (`Name`, `Code`, `CreatedById`, `CreatedDate`)
SELECT 'Main API Key', 'MainApiKey', 0, NOW() UNION
SELECT 'Monitor-Specific API Key', 'MonitorSpecificApiKey', 0, NOW() UNION
SELECT 'Read-Only API Key', 'ReadOnlyApiKey', 0, NOW() UNION
SELECT 'Syndication Feed Key', 'SyndicationFeedKey', 0, NOW();

CREATE TABLE `UptimeMonitorApiKeys` (
	`UptimeMonitorApiKeyId` INT AUTO_INCREMENT PRIMARY KEY,	
	`UptimeMonitorApiKeyTypeId` INT NOT NULL,
	`UptimeMonitorId` INT NULL,	
	`UserId` INT NULL,
	`ApiKey` NVARCHAR(36) NOT NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL,
	FOREIGN KEY (`UptimeMonitorApiKeyTypeId`) 
        REFERENCES UptimeMonitorApiKeyTypes(`UptimeMonitorApiKeyTypeId`),
	FOREIGN KEY (`UptimeMonitorId`) 
        REFERENCES UptimeMonitors(`UptimeMonitorId`),
	FOREIGN KEY (`UserId`) 
        REFERENCES Users(`UserId`)    
);

CREATE TABLE `Errors` (
	`ErrorId` INT AUTO_INCREMENT PRIMARY KEY,
	`UserId` INT NULL,
	`Code` NVARCHAR(50) NULL,
	`TraceIdentifier` NVARCHAR(50) NULL,
	`Message` NVARCHAR(4000) NULL,
	`Path` NVARCHAR(4000) NULL,
	`IpAddress` NVARCHAR(50) NULL,	
	`UserAgent` NVARCHAR(4000) NULL,
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL
);

CREATE TABLE `UptimeMonitorAgents` (
	`UptimeMonitorAgentId` INT AUTO_INCREMENT PRIMARY KEY,
	`UptimeMonitorLocation` NVARCHAR(50) NULL,
	`UptimeMonitorIpAddress` NVARCHAR(50) NULL,
	`Capacity` INT NULL,	
	`CreatedById` INT NOT NULL,
	`CreatedDate` DATETIME(3) NOT NULL,
	`UpdatedById` INT NULL,
	`UpdatedDate` DATETIME(3) NULL
);
