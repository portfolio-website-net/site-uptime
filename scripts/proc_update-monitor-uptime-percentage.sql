USE `siteuptime`;
DROP procedure IF EXISTS `siteuptime`.`UpdateMonitorUptimePercentage`;

DELIMITER $$
USE `siteuptime`$$
CREATE PROCEDURE `UpdateMonitorUptimePercentage`
(
	IN uptimeMonitorIdParam INT,
	IN endDateParam DATETIME(3),
	IN daysParam INT,
	IN fieldTypeParam INT
)
BEGIN
	DROP TEMPORARY TABLE IF EXISTS `UptimeMonitorUptimeTempTable`;
	CREATE TEMPORARY TABLE `UptimeMonitorUptimeTempTable` (
		`UptimeStatusTypeId` INT,
        `TotalTime` INT
    );
	
	-- Reference URL: https://stackoverflow.com/questions/32825551/sql-query-to-calculate-uptime-of-application/32828277#32828277
    INSERT INTO `UptimeMonitorUptimeTempTable`
	WITH t1 (`UptimeStatusTypeId`, `StartTime`, `Index`) AS (
	  SELECT `UptimeStatusTypeId`, 
			 `CreatedDate`, 
			 ROW_NUMBER() OVER(ORDER BY `CreatedDate`)
		FROM `UptimeMonitorHistory`
	   WHERE `UptimeMonitorId` = uptimeMonitorIdParam
         AND ( 
				(fieldTypeParam > 0 AND `CreatedDate` BETWEEN DATE_ADD(endDateParam, interval (daysParam * -1) day) AND endDateParam)
				OR
				-- Historical days
				(fieldTypeParam < 0 AND `CreatedDate` BETWEEN endDateParam AND DATE_ADD(endDateParam, interval 1 day))
		 	 )
	),
	t2 (`UptimeStatusTypeId`, `StartTime`, `EndTime`) AS (
	  SELECT t1.`UptimeStatusTypeId`, t1.`StartTime`, COALESCE(t2.`StartTime`, endDateParam)
	    FROM t1
	    LEFT JOIN t1 t2 ON t2.`Index` = t1.`Index` + 1
	),
	t3 (`UptimeStatusTypeId`, `TotalTime`) AS (
	  SELECT `UptimeStatusTypeId`, SUM(TIMESTAMPDIFF(second, `StartTime`, `EndTime`))
	    FROM t2
	   GROUP BY `UptimeStatusTypeId`
	)    
	SELECT `UptimeStatusTypeId`, 
           `TotalTime`
	  FROM t3;

	SET @activeMonitorRowCount := (
		SELECT COUNT(1)
		  FROM `UptimeMonitorUptimeTempTable` t
          JOIN `UptimeStatusTypes` ust ON t.UptimeStatusTypeId = ust.UptimeStatusTypeId
		 WHERE ust.Code IN ('Up', 'Down')
	); 

	SET @wasMonitorActive := 0;
	IF @activeMonitorRowCount > 0 THEN
		SET @wasMonitorActive := 1;
	END IF;
      
	SET @upTimeTotalSeconds := (
		SELECT IFNULL(t.`TotalTime`, 0)
		  FROM `UptimeMonitorUptimeTempTable` t
          JOIN `UptimeStatusTypes` ust ON t.UptimeStatusTypeId = ust.UptimeStatusTypeId
		 WHERE ust.Code = 'Up'
	);
    
    SET @downTimeTotalSeconds := (
		SELECT IFNULL(t.`TotalTime`, 0)
		  FROM `UptimeMonitorUptimeTempTable` t
          JOIN `UptimeStatusTypes` ust ON t.UptimeStatusTypeId = ust.UptimeStatusTypeId
		 WHERE ust.Code = 'Down'
	);
    
    SET @uptimePercentage := IFNULL(@upTimeTotalSeconds, 0) / (IFNULL(@upTimeTotalSeconds, 0) + IFNULL(@downTimeTotalSeconds, 0));
    
	IF fieldTypeParam = 30 THEN

		UPDATE `UptimeMonitors`
		SET UptimePercentageLast30Days = @uptimePercentage,
			UpdatedById = 0,
			UpdatedDate = NOW()
		WHERE UptimeMonitorId = uptimeMonitorIdParam;

	END IF;

	IF fieldTypeParam = 7 THEN

		UPDATE `UptimeMonitors`
		SET UptimePercentageLast7Days = @uptimePercentage,
			UpdatedById = 0,
			UpdatedDate = NOW()
		WHERE UptimeMonitorId = uptimeMonitorIdParam;
		
	END IF;

	IF fieldTypeParam = 1 THEN

		UPDATE `UptimeMonitors`
		SET UptimePercentageLast24Hours = @uptimePercentage,
			UpdatedById = 0,
			UpdatedDate = NOW()
		WHERE UptimeMonitorId = uptimeMonitorIdParam;
		
	END IF;

	IF fieldTypeParam = -1 THEN

		UPDATE `UptimeMonitors`
		SET UptimePercentage1DayAgo = @uptimePercentage,
		    WasMonitorActive1DayAgo = @wasMonitorActive,
			UpdatedById = 0,
			UpdatedDate = NOW()
		WHERE UptimeMonitorId = uptimeMonitorIdParam;
		
	END IF;

	IF fieldTypeParam = -2 THEN

		UPDATE `UptimeMonitors`
		SET UptimePercentage2DaysAgo = @uptimePercentage,
			WasMonitorActive1DayAgo = @wasMonitorActive,
			UpdatedById = 0,
			UpdatedDate = NOW()
		WHERE UptimeMonitorId = uptimeMonitorIdParam;
		
	END IF;

	IF fieldTypeParam = -3 THEN

		UPDATE `UptimeMonitors`
		SET UptimePercentage3DaysAgo = @uptimePercentage,
			WasMonitorActive1DayAgo = @wasMonitorActive,
			UpdatedById = 0,
			UpdatedDate = NOW()
		WHERE UptimeMonitorId = uptimeMonitorIdParam;
		
	END IF;

	IF fieldTypeParam = -4 THEN

		UPDATE `UptimeMonitors`
		SET UptimePercentage4DaysAgo = @uptimePercentage,
			WasMonitorActive1DayAgo = @wasMonitorActive,
			UpdatedById = 0,
			UpdatedDate = NOW()
		WHERE UptimeMonitorId = uptimeMonitorIdParam;
		
	END IF;

	IF fieldTypeParam = -5 THEN

		UPDATE `UptimeMonitors`
		SET UptimePercentage5DaysAgo = @uptimePercentage,
			WasMonitorActive1DayAgo = @wasMonitorActive,
			UpdatedById = 0,
			UpdatedDate = NOW()
		WHERE UptimeMonitorId = uptimeMonitorIdParam;
		
	END IF;

	IF fieldTypeParam = -6 THEN

		UPDATE `UptimeMonitors`
		SET UptimePercentage6DaysAgo = @uptimePercentage,
			WasMonitorActive1DayAgo = @wasMonitorActive,
			UpdatedById = 0,
			UpdatedDate = NOW()
		WHERE UptimeMonitorId = uptimeMonitorIdParam;
		
	END IF;
END$$

DELIMITER ;
;