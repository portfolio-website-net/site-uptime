USE `siteuptime`;
DROP procedure IF EXISTS `siteuptime`.`GetOverallMonitorStats`;

DELIMITER $$
USE `siteuptime`$$
CREATE PROCEDURE `GetOverallMonitorStats`
(
	IN userIdParam INT
)
BEGIN		
    SELECT 1 AS `Id`,
           (
			   SELECT COUNT(1)
			     FROM `UptimeMonitors` um
				WHERE um.UserId = userIdParam
		   ) AS `CurrentMonitorCount`,
           50 AS `MaxMonitorCount`,
		   1 AS `MinInterval`,
		   (
			   SELECT COUNT(1)
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeStatusTypes` ust ON um.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UserId = userIdParam
				  AND ust.Code = 'Up'
		   ) AS `UpMonitorCount`,
		   (
			   SELECT COUNT(1)
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeStatusTypes` ust ON um.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UserId = userIdParam
				  AND ust.Code = 'Down'
		   ) AS `DownMonitorCount`,
		   (
			   SELECT COUNT(1)
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeStatusTypes` ust ON um.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UserId = userIdParam
				  AND ust.Code = 'Paused'
		   ) AS `PausedMonitorCount`,
		   (
			   SELECT AVG(UptimePercentageLast30Days)
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeStatusTypes` ust ON um.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UserId = userIdParam
				  AND ust.Code IN ('Up', 'Down')
		   ) AS `UptimePercentageLast30Days`,		   
		   (
			   SELECT AVG(UptimePercentageLast7Days)
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeStatusTypes` ust ON um.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UserId = userIdParam
				  AND ust.Code IN ('Up', 'Down')
		   ) AS `UptimePercentageLast7Days`,
		   (
			   SELECT AVG(UptimePercentageLast24Hours)
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeStatusTypes` ust ON um.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UserId = userIdParam
				  AND ust.Code IN ('Up', 'Down')
		   ) AS `UptimePercentageLast24Hours`,
		   (
			   SELECT AVG(UptimePercentage1DayAgo)
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeStatusTypes` ust ON um.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UserId = userIdParam
				  AND ust.Code IN ('Up', 'Down')
		   ) AS `UptimePercentage1DayAgo`,
		   (
			   SELECT AVG(UptimePercentage2DaysAgo)
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeStatusTypes` ust ON um.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UserId = userIdParam
				  AND ust.Code IN ('Up', 'Down')
		   ) AS `UptimePercentage2DaysAgo`,
		   (
			   SELECT AVG(UptimePercentage3DaysAgo)
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeStatusTypes` ust ON um.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UserId = userIdParam
				  AND ust.Code IN ('Up', 'Down')
		   ) AS `UptimePercentage3DaysAgo`,
		   (
			   SELECT AVG(UptimePercentage4DaysAgo)
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeStatusTypes` ust ON um.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UserId = userIdParam
				  AND ust.Code IN ('Up', 'Down')
		   ) AS `UptimePercentage4DaysAgo`,
		   (
			   SELECT AVG(UptimePercentage5DaysAgo)
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeStatusTypes` ust ON um.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UserId = userIdParam
				  AND ust.Code IN ('Up', 'Down')
		   ) AS `UptimePercentage5DaysAgo`,
		   (
			   SELECT AVG(UptimePercentage6DaysAgo)
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeStatusTypes` ust ON um.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UserId = userIdParam
				  AND ust.Code IN ('Up', 'Down')
		   ) AS `UptimePercentage6DaysAgo`,
		   (
			   SELECT WasMonitorActive1DayAgo
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeStatusTypes` ust ON um.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UserId = userIdParam
				  AND ust.Code IN ('Up', 'Down')
				LIMIT 1
		   ) AS `WasMonitorActive1DayAgo`,
		   (
			   SELECT WasMonitorActive2DaysAgo
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeStatusTypes` ust ON um.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UserId = userIdParam
				  AND ust.Code IN ('Up', 'Down')
				LIMIT 1
		   ) AS `WasMonitorActive2DaysAgo`,
		   (
			   SELECT WasMonitorActive3DaysAgo
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeStatusTypes` ust ON um.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UserId = userIdParam
				  AND ust.Code IN ('Up', 'Down')
				LIMIT 1
		   ) AS `WasMonitorActive3DaysAgo`,
		   (
			   SELECT WasMonitorActive4DaysAgo
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeStatusTypes` ust ON um.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UserId = userIdParam
				  AND ust.Code IN ('Up', 'Down')
				LIMIT 1
		   ) AS `WasMonitorActive4DaysAgo`,
		   (
			   SELECT WasMonitorActive5DaysAgo
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeStatusTypes` ust ON um.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UserId = userIdParam
				  AND ust.Code IN ('Up', 'Down')
				LIMIT 1
		   ) AS `WasMonitorActive5DaysAgo`,
		   (
			   SELECT WasMonitorActive6DaysAgo
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeStatusTypes` ust ON um.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UserId = userIdParam
				  AND ust.Code IN ('Up', 'Down')
				LIMIT 1
		   ) AS `WasMonitorActive6DaysAgo`,
		   (
			   SELECT um.Name
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeMonitorEvents` ume ON um.UptimeMonitorId = ume.UptimeMonitorId
				INNER JOIN `UptimeStatusTypes` ust ON ume.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UserId = userIdParam
				  AND ust.Code IN ('Down')
				ORDER BY ume.UpdatedDate DESC,
				      ume.CreatedDate DESC
				LIMIT 1
		   ) AS `LatestDownTime_MonitorName`,
		   (
			   SELECT ume.CreatedDate
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeMonitorEvents` ume ON um.UptimeMonitorId = ume.UptimeMonitorId
				INNER JOIN `UptimeStatusTypes` ust ON ume.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UserId = userIdParam
				  AND ust.Code IN ('Down')
				ORDER BY ume.UpdatedDate DESC,
				      ume.CreatedDate DESC
				LIMIT 1
		   ) AS `LatestDownTime_Date`,
		   (
			   SELECT ume.Duration
			     FROM `UptimeMonitors` um
				INNER JOIN `UptimeMonitorEvents` ume ON um.UptimeMonitorId = ume.UptimeMonitorId
				INNER JOIN `UptimeStatusTypes` ust ON ume.UptimeStatusTypeId = ust.UptimeStatusTypeId
				WHERE um.UserId = userIdParam
				  AND ust.Code IN ('Down')
				ORDER BY ume.UpdatedDate DESC,
				      ume.CreatedDate DESC
				LIMIT 1
		   ) AS `LatestDownTime_Duration`;
END$$

DELIMITER ;
;