USE `siteuptime`;
DROP procedure IF EXISTS `siteuptime`.`GetUptimeMonitorChartData`;

DELIMITER $$
USE `siteuptime`$$
CREATE PROCEDURE `GetUptimeMonitorChartData`
(
	IN uptimeMonitorIdParam INT
)
BEGIN
	DROP TEMPORARY TABLE IF EXISTS `UptimeMonitorChartDataTempTable`;
	CREATE TEMPORARY TABLE `UptimeMonitorChartDataTempTable` (
		`CreatedDate` DATETIME(3),
        `Time` TIME,
        `ResponseTime` INT NULL
    );
    
    -- Reference URL: https://stackoverflow.com/questions/2793994/group-mysql-query-by-15-min-intervals
    INSERT INTO `UptimeMonitorChartDataTempTable` (`CreatedDate`, `Time`, `ResponseTime`)
    SELECT `CreatedDate`,
		   sec_to_time(time_to_sec(CreatedDate) - time_to_sec(CreatedDate) % (15*60)) AS `Time`,
		   AVG(ResponseTime) AS `ResponseTime`       
	  FROM `UptimeMonitorHistory`
	 WHERE UptimeMonitorId = uptimeMonitorIdParam
	   AND CreatedDate BETWEEN DATE_ADD(NOW(), interval -1 day) AND NOW()
	 GROUP BY `Time`;
    
    SET @startDate := DATE_ADD(NOW(), interval -1 day);
    SET @endDate := NOW();
    SET @currentDate := @startDate;
 
    WHILE @currentDate < @endDate DO
		SET @currentTime := sec_to_time(time_to_sec(@currentDate) - time_to_sec(@currentDate) % (15*60));
		IF (SELECT COUNT(1) 
			  FROM `UptimeMonitorChartDataTempTable`
			 WHERE @currentTime = `Time`) = 0 THEN
		BEGIN
			INSERT INTO `UptimeMonitorChartDataTempTable` (`CreatedDate`, `Time`, `ResponseTime`)
			VALUES (@currentDate, @currentTime, NULL);			
        END;
        END IF;
        
        SET @currentDate = DATE_ADD(@currentDate, interval 15 minute);
    END WHILE;
	
    SELECT ROW_NUMBER() OVER(ORDER BY `CreatedDate`) AS `Id`,
           LOWER(TIME_FORMAT(`Time`, '%l:%i %p')) AS `Time`,
           `ResponseTime`
	  FROM `UptimeMonitorChartDataTempTable`;
END$$

DELIMITER ;
;