USE `siteuptime`;
DROP procedure IF EXISTS `siteuptime`.`ProcessMonitorEvents`;

DELIMITER $$
USE `siteuptime`$$
CREATE PROCEDURE `ProcessMonitorEvents`
(
	IN uptimeMonitorIdParam INT
)
BEGIN
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;  -- Rollback any error in the transaction
    END;

	START TRANSACTION;

	INSERT INTO `UptimeMonitorHistory` 
	(
		`UptimeMonitorId`, 
		`UptimeStatusTypeId`, 
		`UptimeReasonTypeId`, 
		`StatusCode`, 
		`ResponseTime`, 
		`UptimeMonitorLocation`, 
		`UptimeMonitorIpAddress`, 
		`CreatedById`, 
		`CreatedDate`
	)
	SELECT UptimeMonitorId,
		   UptimeStatusTypeId,
		   UptimeReasonTypeId,
		   StatusCode,
		   ResponseTime,
		   UptimeMonitorLocation,
		   UptimeMonitorIpAddress,
		   0,
		   NOW()
	  FROM `UptimeMonitors`
	 WHERE UptimeMonitorId = uptimeMonitorIdParam;                
	
	SELECT UptimeStatusTypeId,
		   UptimeReasonTypeId,
		   StatusCode
	  INTO @current_UptimeStatusTypeId,
		   @current_UptimeReasonTypeId,
		   @current_StatusCode
	  FROM `UptimeMonitors`
	 WHERE UptimeMonitorId = uptimeMonitorIdParam;

	SELECT UptimeMonitorEventId,
		   UptimeStatusTypeId,
		   UptimeReasonTypeId,
		   StatusCode,
		   CreatedDate
	  INTO @mostRecent_EventId,
		   @mostRecent_UptimeStatusTypeId,
		   @mostRecent_UptimeReasonTypeId,
		   @mostRecent_StatusCode,
		   @mostRecent_EventCreatedDate
	  FROM `UptimeMonitorEvents`
	 WHERE UptimeMonitorId = uptimeMonitorIdParam
	 ORDER BY CreatedDate DESC
	 LIMIT 1;		  
	
	SET @createNewEvent := 1;

	SET @mostRecentEventDuration := 0;
	
	IF @mostRecent_EventId IS NOT NULL THEN

		SET @mostRecentEventDuration := (
			SELECT SUM(TIMESTAMPDIFF(second, @mostRecent_EventCreatedDate, NOW()))
		);
		
		IF @mostRecent_UptimeStatusTypeId = @current_UptimeStatusTypeId
			AND @mostRecent_UptimeReasonTypeId = @current_UptimeReasonTypeId
			AND IFNULL(@mostRecent_StatusCode, 0) = IFNULL(@current_StatusCode, 0) THEN
			
			UPDATE `UptimeMonitorEvents`
			   SET Duration = @mostRecentEventDuration,
				   UpdatedById = 0,
				   UpdatedDate = NOW()
			 WHERE UptimeMonitorEventId = @mostRecent_EventId;
			
			SET @createNewEvent := 0;

		END IF;

	END IF;
	
	IF @createNewEvent = 1 THEN

		INSERT INTO `UptimeMonitorEvents` 
		(
			`UptimeMonitorId`, 
			`UptimeStatusTypeId`, 
			`UptimeReasonTypeId`, 
			`StatusCode`, 
			`Duration`,
			`CreatedById`, 
			`CreatedDate`
		)
		SELECT UptimeMonitorId,
			   UptimeStatusTypeId,
			   UptimeReasonTypeId,
			   StatusCode,
			   0,
			   0,
			   NOW()
		  FROM `UptimeMonitors`
		 WHERE UptimeMonitorId = uptimeMonitorIdParam;
	END IF;

	SELECT 1 AS `Id`,
	       @mostRecentEventDuration AS `MostRecentEventDuration`;

	COMMIT;
END$$

DELIMITER ;
;