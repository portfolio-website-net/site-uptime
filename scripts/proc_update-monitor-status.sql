USE `siteuptime`;
DROP procedure IF EXISTS `siteuptime`.`UpdateMonitorStatus`;

DELIMITER $$
USE `siteuptime`$$
CREATE PROCEDURE `UpdateMonitorStatus`
(
	IN uptimeMonitorIdParam INT,
	IN statusCodeParam INT,
	IN responseContentParam NVARCHAR(1000),
	IN uptimeStatusTypeIdParam INT,    
	IN uptimeReasonTypeIdParam INT,
    IN responseTimeParam INT,
	IN uptimeMonitorLocationParam NVARCHAR(50),
	IN uptimeMonitorIpAddressParam NVARCHAR(50),
    IN defaultTimeoutParam INT
)
BEGIN
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;  -- Rollback any error in the transaction
    END;

	START TRANSACTION;
	
	SET @downStatusTypeId := (
		SELECT UptimeStatusTypeId
		  FROM `UptimeStatusTypes`
		 WHERE Code = 'Down'
	);     
    
    SET @likelyDownStatusTypeId := (
		SELECT UptimeStatusTypeId
		  FROM `UptimeStatusTypes`
		 WHERE Code = 'LikelyDown'
	);
    
    UPDATE `UptimeMonitors` um
	  LEFT JOIN `UptimeStatusTypes` ust ON um.UptimeStatusTypeId = ust.UptimeStatusTypeId
	   SET um.ResponseTime = if(responseTimeParam = 0, NULL, responseTimeParam),
		   um.StatusCode = if(statusCodeParam = 0, NULL, statusCodeParam),
		   um.ResponseContent = if(LENGTH(responseContentParam) = 0, NULL, responseContentParam),
		   um.UptimeStatusTypeId = uptimeStatusTypeIdParam,
		   um.UptimeReasonTypeId = uptimeReasonTypeIdParam,
		   um.ProcessingTimeoutDate = NULL,		   
		   um.NextMonitorDate = DATE_ADD(NOW(), interval um.Interval minute),
		   um.UptimeMonitorLocation = uptimeMonitorLocationParam,
		   um.UptimeMonitorIpAddress = uptimeMonitorIpAddressParam,
		   um.UpdatedById = 0,
		   um.UpdatedDate = NOW()
	 WHERE (ust.Code <> 'Paused' OR ust.Code IS NULL)
       AND um.UptimeMonitorId = uptimeMonitorIdParam;     
    
    SET @requiresConfirmation := 0;
    IF uptimeStatusTypeIdParam = @downStatusTypeId THEN
	
		SET @isHeartbeat := (
			SELECT COUNT(1)
			  FROM `UptimeMonitors` um
			  LEFT JOIN `UptimeMonitorTypes` umt ON um.UptimeMonitorTypeId = umt.UptimeMonitorTypeId
			   AND um.UptimeMonitorId = uptimeMonitorIdParam
			   AND umt.Code = 'Heartbeat'
		);

		SET @confirmationTimeoutDate := (
			SELECT ConfirmationTimeoutDate
			  FROM `UptimeMonitors`
			 WHERE UptimeMonitorId = uptimeMonitorIdParam
		);
		
		IF @confirmationTimeoutDate IS NULL and @isHeartbeat = 0 THEN
		
			SET @recentUptimeMonitorEventCount := (
				SELECT COUNT(1)
				  FROM `UptimeMonitorHistory`
				 WHERE UptimeMonitorId = uptimeMonitorIdParam
				   AND CreatedDate >= DATE_ADD(NOW(), interval -1 minute)
			);
			
			IF @recentUptimeMonitorEventCount < 2 THEN				
			
				UPDATE `UptimeMonitors` um
				   SET um.UptimeStatusTypeId = @likelyDownStatusTypeId,
					   um.ConfirmationTimeoutDate = DATE_ADD(NOW(), interval defaultTimeoutParam second)
				 WHERE um.UptimeMonitorId = uptimeMonitorIdParam;
				 
				 SET @requiresConfirmation := 1;

            END IF;

        END IF;

    END IF;
    
    IF @requiresConfirmation = 0 THEN

		UPDATE `UptimeMonitors` um
		  LEFT JOIN `UptimeStatusTypes` ust ON um.UptimeStatusTypeId = ust.UptimeStatusTypeId
		   SET um.ConfirmationTimeoutDate = NULL
		 WHERE (ust.Code <> 'Paused' OR ust.Code IS NULL)
		   AND um.UptimeMonitorId = uptimeMonitorIdParam;

    END IF;
    
    SET @uptimeStatusTypeId := (
		SELECT UptimeStatusTypeId
		  FROM `UptimeMonitors`
		 WHERE UptimeMonitorId = uptimeMonitorIdParam
	);   
    
    IF @uptimeStatusTypeId = @downStatusTypeId THEN

		-- Check DOWN status every minute
		UPDATE `UptimeMonitors` um
		   SET um.NextMonitorDate = DATE_ADD(NOW(), interval 1 minute)
		 WHERE um.UptimeMonitorId = uptimeMonitorIdParam;
  
    END IF;
        
    IF @uptimeStatusTypeId <> @likelyDownStatusTypeId THEN

		CALL ProcessMonitorEvents(uptimeMonitorIdParam);        
        CALL UpdateMonitorUptimePercentage(uptimeMonitorIdParam, DATE_ADD(NOW(), interval 1 second), 30, 30);
		CALL UpdateMonitorUptimePercentage(uptimeMonitorIdParam, DATE_ADD(NOW(), interval 1 second), 7, 7);
		CALL UpdateMonitorUptimePercentage(uptimeMonitorIdParam, DATE_ADD(NOW(), interval 1 second), 1, 1);
		CALL UpdateMonitorUptimePercentage(uptimeMonitorIdParam, DATE(DATE_ADD(NOW(), interval (1 * -1) day)), 1, -1);
		CALL UpdateMonitorUptimePercentage(uptimeMonitorIdParam, DATE(DATE_ADD(NOW(), interval (2 * -1) day)), 1, -2);
		CALL UpdateMonitorUptimePercentage(uptimeMonitorIdParam, DATE(DATE_ADD(NOW(), interval (3 * -1) day)), 1, -3);
		CALL UpdateMonitorUptimePercentage(uptimeMonitorIdParam, DATE(DATE_ADD(NOW(), interval (4 * -1) day)), 1, -4);
		CALL UpdateMonitorUptimePercentage(uptimeMonitorIdParam, DATE(DATE_ADD(NOW(), interval (5 * -1) day)), 1, -5);
		CALL UpdateMonitorUptimePercentage(uptimeMonitorIdParam, DATE(DATE_ADD(NOW(), interval (6 * -1) day)), 1, -6);
  
	ELSE

		SELECT 1 AS `Id`,
	           NULL AS `MostRecentEventDuration`;

    END IF;    

	COMMIT;
END$$

DELIMITER ;
;