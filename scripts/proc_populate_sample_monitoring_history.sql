USE `siteuptime`;
DROP procedure IF EXISTS `siteuptime`.`PopulateSampleMonitoringHistory`;

DELIMITER $$
USE `siteuptime`$$
CREATE PROCEDURE `PopulateSampleMonitoringHistory`
(
	IN uptimeMonitorIdParam INT
)
BEGIN
    SET @thirtyDaysAgo := DATE_ADD(NOW(), interval -30 day);
	SET @fiveMinutesAgo := DATE_ADD(NOW(), interval -5 minute);
	SET @currentTime := @thirtyDaysAgo;
 
    WHILE @currentTime < @fiveMinutesAgo DO
    
		SET @min := 150;
		SET @max := 350;
		SET @randomResponseTime := FLOOR(@min+RAND()*(@max-@min+1));

		INSERT INTO `siteuptime`.`UptimeMonitorHistory` (
			`UptimeMonitorId`, 
			`UptimeStatusTypeId`, 
			`UptimeReasonTypeId`,
			`StatusCode`,
			`ResponseTime`,
			`CreatedById`,
			`CreatedDate`
		)
		VALUES (uptimeMonitorIdParam, 2, 2, 200, @randomResponseTime, 0, @currentTime);

        SET @currentTime := DATE_ADD(@currentTime, interval 5 minute);
    END WHILE;

	UPDATE `siteuptime`.`UptimeMonitorEvents`
	   SET CreatedDate = @thirtyDaysAgo
	 WHERE UptimeMonitorId = uptimeMonitorIdParam;

END$$

DELIMITER ;
;