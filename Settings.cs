﻿namespace SiteUptime
{
    public class Settings
    {
        public string SessionCookieName { get; set; }

        public bool UseSecureCookies { get; set; }

        // Use the following for testing on any hostname without secure cookies:
        // dotnet run --urls "http://*:5000;"

        public int SessionTime { get; set; }

        public int MaxIncorrectAuthCount { get; set; }

        public string TwoFactorAuthIssuer { get; set; }

        public string TwoFactorAuthSecretKey { get; set; }
        
        public string PollingIntervals { get; set; }

        public int DefaultTimeout { get; set; }

        public int DefaultUptimeCapacity { get; set; }

        public string UptimeMonitorLocation { get; set; }

        public string UptimeMonitorIpAddress { get; set; }

        public bool RequireDownStatusVerification { get; set; }

        public string EmailHost { get; set; }

        public int EmailPort { get; set; }

        public string EmailUsername { get; set; }

        public string EmailPassword { get; set; }

        public string EmailFromAddress { get; set; }

        public string SiteUrl { get; set; }

        public string SiteName { get; set; }

        public string PublicStatusPageLogoBasePath { get; set; }

        public string GoogleRecaptchaVerificationUrl { get; set; }

        public string GoogleReCaptchaSiteKey { get; set; }

        public string GoogleRecaptchaSecretKey { get; set; }        

        public string PublicStatusPageCookieName { get; set; }

        public bool CreateSampleUsers { get; set; }

        public string SampleUserEmail { get; set; }

        public string SampleUserPassword { get; set; }

        public string DefaultUserAgent { get; set; }

        public int MaxRedirectsToFollow { get; set; }

        public int KeywordSearchContentMaxLength { get; set; }
    }
}
