using System;

namespace SiteUptime.Helpers.Types
{
    public static class SortTypes
    {
        public const string FriendlyNameAZ = "FriendlyNameAZ";
        public const string FriendlyNameZA = "FriendlyNameZA";
        public const string StatusUpDownPaused = "StatusUpDownPaused";
        public const string StatusDownUpPaused = "StatusDownUpPaused";
        public const string StatusPausedUpDown = "StatusPausedUpDown";
        public const string TypeHttpKywdPingPortHrtb = "TypeHttpKywdPingPortHrtb";
        public const string TypeKywdHttpPingPortHrtb = "TypeKywdHttpPingPortHrtb";
        public const string TypePingPortHttpKywdHrtb = "TypePingPortHttpKywdHrtb";
        public const string TypePortPingHttpKywdHrtb = "TypePortPingHttpKywdHrtb";
        public const string TypeHrtbHttpKywdPingPort = "TypeHrtbHttpKywdPingPort";
    }
}