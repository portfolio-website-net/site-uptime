using System;

namespace SiteUptime.Helpers.Types
{
    public static class MaintenanceWindowTypes
    {
        public const string Once = "Once";
        public const string Daily = "Daily";
        public const string Weekly = "Weekly";
        public const string Monthly = "Monthly";
    }
}