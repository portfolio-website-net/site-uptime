using System;

namespace SiteUptime.Helpers.Types
{
    public static class ReasonTypes
    {
        public const string Started = "Started";
        public const string Ok = "Ok";
        public const string NoResponse = "NoResponse";
        public const string NotFound = "NotFound";
        public const string Error = "Error";
        public const string KeywordFound = "KeywordFound";
        public const string KeywordNotFound = "KeywordNotFound";
        public const string Paused = "Paused";   
    }
}