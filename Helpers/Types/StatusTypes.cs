using System;

namespace SiteUptime.Helpers.Types
{
    public static class StatusTypes
    {
        public const string Started = "Started";
        public const string Up = "Up";
        public const string LikelyDown = "LikelyDown";
        public const string Down = "Down";
        public const string Paused = "Paused";
    }
}