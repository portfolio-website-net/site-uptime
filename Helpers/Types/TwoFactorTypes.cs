using System;

namespace SiteUptime.Helpers.Types
{
    public static class TwoFactorTypes
    {
        public const string Totp = "Totp";
        public const string U2f = "U2f";
        public const string Text = "Text";
    }
}