using System;

namespace SiteUptime.Helpers.Types
{
    public static class ContactTypes
    {
        public const string Email = "Email";
        public const string EmailToSms = "EmailToSms";
        public const string Webhook = "Webhook";
    }
}