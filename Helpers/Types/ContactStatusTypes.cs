using System;

namespace SiteUptime.Helpers.Types
{
    public static class ContactStatusTypes
    {
        public const string NotActivated = "NotActivated";
        public const string Paused = "Paused";
        public const string Active = "Active";
    }
}