using System;

namespace SiteUptime.Helpers.Types
{
    public static class MonitorTypes
    {
        public const string Http = "Http";
        public const string Keyword = "Keyword";
        public const string Ping = "Ping";
        public const string Port = "Port";
        public const string Heartbeat = "Heartbeat";
    }
}