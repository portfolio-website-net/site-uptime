using System;

namespace SiteUptime.Helpers.Types
{
    public static class HttpMethodTypes
    {        
        public const string Head = "Head";
        public const string Get = "Get";
        public const string Post = "Post";
        public const string Put = "Put";
        public const string Patch = "Patch";
        public const string Delete = "Delete";
        public const string Options = "Options";
    }
}