using System;

namespace SiteUptime.Helpers.Types
{
    public static class ApiKeyTypes
    {
        public const string MainApiKey = "MainApiKey";
        public const string MonitorSpecificApiKey = "MonitorSpecificApiKey";
        public const string ReadOnlyApiKey = "ReadOnlyApiKey";
        public const string SyndicationFeedKey = "SyndicationFeedKey";
    }
}