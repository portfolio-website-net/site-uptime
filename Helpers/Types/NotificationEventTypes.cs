using System;

namespace SiteUptime.Helpers.Types
{
    public static class NotificationEventTypes
    {
        public const string UpDown = "UpDown";
        public const string OnlyDown = "OnlyDown";
        public const string OnlyUp = "OnlyUp";
    }
}