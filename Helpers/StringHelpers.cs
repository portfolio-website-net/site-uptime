using System;
using shortid;

namespace SiteUptime.Helpers
{
    public static class StringHelpers
    {
        public static string Truncate(string value, int length)
        {
            if (!string.IsNullOrEmpty(value)
                && value.Length > length)
            {
                return value.Substring(0, length);
            }
            else
            {
                return value;
            }
        }

        public static string ConvertDurationToString(TimeSpan timeSpan)
        {
            var result = string.Empty;
            if (timeSpan.TotalHours >= 1)
            {
                result = $"{(int)timeSpan.TotalHours} hours, {(int)timeSpan.Minutes} minutes, and {(int)timeSpan.Seconds} seconds";
            }
            else if (timeSpan.TotalMinutes >= 1)
            {
                result = $"{(int)timeSpan.TotalMinutes} minutes and {(int)timeSpan.Seconds} seconds";
            }
            else
            {
                result = $"{(int)timeSpan.TotalSeconds} seconds";
            }

            return result;
        }

        public static string ConvertDurationToShortString(TimeSpan timeSpan)
        {
            return $"{(int)timeSpan.TotalHours} hrs, {(int)timeSpan.Minutes} mins";
        }

        public static string GenerateShortId()
        {
            return ShortId.Generate(useNumbers: true, useSpecial: false, length: 8);
        }

        // Source URL: https://lonewolfonline.net/replace-first-occurrence-string/
        public static string ReplaceFirstOccurrence (string source, string find, string replace)
        {
            int place = source.IndexOf(find);
            string result = source.Remove(place, find.Length).Insert(place, replace);
            return result;
        }

        public static string ReplaceLastOccurrence(string source, string find, string replace)
        {
            int place = source.LastIndexOf(find);
            string result = source.Remove(place, find.Length).Insert(place, replace);
            return result;
        }
    }
}