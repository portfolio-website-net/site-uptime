using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Diagnostics;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SiteUptime.Models;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Linq;

namespace SiteUptime.Helpers
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate next;

        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        // Reference URL: https://stackoverflow.com/questions/49241434/how-do-i-catch-unhandled-exceptions-in-asp-net-core-2-0-before-the-page-is-rend
        private static async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var message = ErrorLogging.Log(context, exception);

            context.Response.ContentType = "text/plain";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            await context.Response.WriteAsync(message);
        }
    }

    public static class ExceptionHandlingMiddlewareExtensions
    {
        public static IApplicationBuilder UseApiExceptionHandler(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionHandlingMiddleware>();
        }
    }

    public static class ErrorLogging
    {
        public static string Log(HttpContext context, Exception exception)
        {
            string errorCode = null;
            string message = null;
            if (context != null)
            {
                errorCode = CalculateErrorCode(context.TraceIdentifier);
                message = string.Format("An error occurred; please contact the help desk with the following error code: '{0}'  [{1}]", errorCode, context.TraceIdentifier);
                Console.WriteLine(message);
                Console.WriteLine(exception.ToString());
            }
            else
            {
                Console.WriteLine(exception.ToString());
            }

            try
            {               
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json");

                var configuration = builder.Build();

                var optionsBuilder = new DbContextOptionsBuilder<Context>();
                optionsBuilder.UseMySQL(configuration.GetConnectionString("Context"));
                var dbContextOptions = optionsBuilder.Options;

                User user = null;
                string path = null;
                string ipAddress = null;
                string userAgent = null;

                if (context != null)
                {
                    user = (User)context.Items["User"];
                    path = context.Request.Path.ToString();
                    ipAddress = context.Connection.RemoteIpAddress.ToString();
                    userAgent = context.Request.Headers["User-Agent"]; 
                }

                using (var dbContext = new Context(dbContextOptions))
                {
                    dbContext.Errors.Add(new Error {
                        UserId = user != null ? user.UserId : (int?)null,
                        Code = errorCode,
                        TraceIdentifier = context != null ? context.TraceIdentifier : null,
                        Message = StringHelpers.Truncate(exception.ToString(), 4000),
                        Path = path != null ? StringHelpers.Truncate(path, 4000) : null,
                        IpAddress = ipAddress,
                        UserAgent = userAgent != null ? StringHelpers.Truncate(userAgent, 4000) : null,
                        CreatedById = user != null ? user.UserId : 0,
                        CreatedDate = DateTime.Now
                    });

                    dbContext.SaveChanges();
                }          
            }  
            catch (Exception ex)
            {
                Console.WriteLine($"Error logging to database:\n{ex}");
            }

            return message != null ? message : exception.ToString();
        }

        private static string CalculateErrorCode(string traceIdentifier)
        {
            const int ErrorCodeLength = 6;
            const string CodeValues = "BCDFGHJKLMNPQRSTVWXYZ";

            MD5 hasher = MD5.Create();

            StringBuilder sb = new StringBuilder(10);

            byte[] traceBytes = hasher.ComputeHash(Encoding.UTF8.GetBytes(traceIdentifier));

            int codeValuesLength = CodeValues.Length;

            for (int i = 0; i < ErrorCodeLength; i++)
            {
                sb.Append(CodeValues[traceBytes[i] % codeValuesLength]);
            }

            return sb.ToString();
        }
    }
}