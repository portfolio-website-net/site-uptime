using SiteUptime.Helpers.CacheHelpers;
using SiteUptime.Helpers.Types;
using System.Linq;
using System.Collections.Generic;

namespace SiteUptime.Helpers.Api
{
    public static class ApiParseHelper
    {
        public static string GetMonitorStatusType(int? uptimeStatusTypeId)
        {
            var result = string.Empty;
            var uptimeStatusType = MemoryCacheHelper.UptimeStatusTypes.FirstOrDefault(x => x.UptimeStatusTypeId == uptimeStatusTypeId);
            if (uptimeStatusType != null)
            {
                if (uptimeStatusType.Code == StatusTypes.Paused)
                {
                    result = 0.ToString();
                }
                else if (uptimeStatusType.Code == StatusTypes.Started)
                {
                    result = 1.ToString();
                }
                else if (uptimeStatusType.Code == StatusTypes.Up)
                {
                    result = 2.ToString();
                }
                else if (uptimeStatusType.Code == StatusTypes.LikelyDown)
                {
                    result = 8.ToString();
                }
                else if (uptimeStatusType.Code == StatusTypes.Down)
                {
                    result = 9.ToString();
                }
            }
            else
            {
                result = 1.ToString();
            }

            return result;
        }

        public static int GetMonitoringIntervalSliderValue(string interval, List<int> pollingIntervals)
        {
            var result = 5;            
            if (int.TryParse(interval, out int intervalVal))
            {
                intervalVal = (int)(intervalVal / 60);

                if (pollingIntervals.Contains(intervalVal))
                {
                    result = pollingIntervals.IndexOf(intervalVal) + 1;
                }
            }

            return result;
        }

        public static List<int> GetAlertContacts(string alertContacts)
        {
            var result = new List<int>();            
            if (!string.IsNullOrEmpty(alertContacts))
            {
                var contacts = alertContacts.Split('-');
                foreach (var contact in contacts)
                {
                    var contactParts = contact.Split('_');
                    if (contactParts.Length == 1)
                    {
                        var contactId = int.TryParse(contactParts[0], out int contactIdVal) ? contactIdVal : 0;
                        if (contactId != 0)
                        {
                            result.Add(contactId);
                        }
                    }
                    else if (contactParts.Length == 3)
                    {
                        var contactId = int.TryParse(contactParts[0], out int contactIdVal) ? contactIdVal : 0;
                        if (contactId != 0)
                        {
                            result.Add(contactId);
                        }

                        var threshold = int.TryParse(contactParts[1], out int thresholdVal) ? thresholdVal : 0;
                        if (threshold != 0)
                        {
                            // TODO: Implement threshold
                        }

                        var recurrence = int.TryParse(contactParts[2], out int recurrenceVal) ? recurrenceVal : 0;
                        if (recurrence != 0)
                        {
                            // TODO: Implement recurrence
                        }
                    }
                }
            }

            return result;
        }

        public static string GetAlertContactType(int? uptimeAlertContactTypeId)
        {
            var result = string.Empty;
            var uptimeAlertContactType = MemoryCacheHelper.UptimeAlertContactTypes.FirstOrDefault(x => x.UptimeAlertContactTypeId == uptimeAlertContactTypeId);
            if (uptimeAlertContactType != null)
            {
                if (uptimeAlertContactType.Code == ContactTypes.Email)
                {
                    result = 2.ToString();
                }
                else if (uptimeAlertContactType.Code == ContactTypes.EmailToSms)
                {
                    result = 1.ToString();
                }
                else if (uptimeAlertContactType.Code == ContactTypes.Webhook)
                {
                    result = 5.ToString();
                }                
            }

            return result;
        }

        public static string GetAlertContactStatusType(int? uptimeAlertContactStatusTypeId)
        {
            var result = string.Empty;
            var uptimeAlertContactStatusType = MemoryCacheHelper.UptimeAlertContactStatusTypes.FirstOrDefault(x => x.UptimeAlertContactStatusTypeId == uptimeAlertContactStatusTypeId);
            if (uptimeAlertContactStatusType != null)
            {
                if (uptimeAlertContactStatusType.Code == ContactStatusTypes.NotActivated)
                {
                    result = 0.ToString();
                }
                else if (uptimeAlertContactStatusType.Code == ContactStatusTypes.Paused)
                {
                    result = 1.ToString();
                }
                else if (uptimeAlertContactStatusType.Code == ContactStatusTypes.Active)
                {
                    result = 2.ToString();
                }                
            }

            return result;
        }

        public static int GetInputAlertContactType(string type)
        {
            var result = 0;
            var uptimeAlertContactTypes = MemoryCacheHelper.UptimeAlertContactTypes;
            if (type == 2.ToString())
            {
                result = uptimeAlertContactTypes.First(x => x.Code == ContactTypes.Email).UptimeAlertContactTypeId;
            }
            else if (type == 1.ToString())
            {
                result = uptimeAlertContactTypes.First(x => x.Code == ContactTypes.EmailToSms).UptimeAlertContactTypeId;
            }
            else if (type == 5.ToString())
            {
                result = uptimeAlertContactTypes.First(x => x.Code == ContactTypes.Webhook).UptimeAlertContactTypeId;
            }

            return result;
        }

        public static string GetPublicStatusPageMonitors(List<int> selectedMonitorIds)
        {
            var result = string.Empty;
            if (selectedMonitorIds != null
                && selectedMonitorIds.Any())
            {
                result = string.Join("-", selectedMonitorIds.Select(x => x.ToString()));
            }
            else
            {
                result = "0";
            }

            return result;
        }

        public static List<int> GetInputPublicStatusPageMonitors(string monitors)
        {
            var result = new List<int>();
            if (!string.IsNullOrEmpty(monitors))
            {
                var selectedMonitors = monitors.Split('-');
                foreach (var monitor in selectedMonitors)
                {
                    var monitorId = int.TryParse(monitor, out int monitorIdVal) ? monitorIdVal : 0;
                    if (monitorId != 0)
                    {
                        result.Add(monitorId);
                    }
                }
            }

            return result;
        }
    }
}