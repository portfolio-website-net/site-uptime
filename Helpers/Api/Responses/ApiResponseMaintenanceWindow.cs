using System.Xml.Serialization;
using System.Collections.Generic;

namespace SiteUptime.Helpers.Api.Responses
{
    [XmlRoot("maintenance-window")]
    public class ApiResponseMaintenanceWindow
    {
        [XmlAttribute]
        public int id { get; set; }
    }
}