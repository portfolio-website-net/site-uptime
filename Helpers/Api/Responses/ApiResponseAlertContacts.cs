using System.Xml.Serialization;
using System.Collections.Generic;

namespace SiteUptime.Helpers.Api.Responses
{
    [XmlRoot("alert_contacts")]
    public class ApiResponseAlertContacts
    {
        [XmlAttribute]
        public int offset { get; set; }

        [XmlAttribute]
        public int limit { get; set; }

        [XmlAttribute]
        public int total { get; set; }

        [XmlElement]
        public List<ApiResponseAlertContactContact> alert_contact { get; set; }
    }

    [XmlRoot("alert_contact")]
    public class ApiResponseAlertContactContact
    {
        [XmlAttribute]
        public int id { get; set; }

        [XmlAttribute]
        public string friendly_name { get; set; }

        [XmlAttribute]
        public string type { get; set; }

        [XmlAttribute]
        public string status { get; set; }

        [XmlAttribute]
        public string value { get; set; }
    }
}