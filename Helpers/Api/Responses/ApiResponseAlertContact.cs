using System.Xml.Serialization;
using System.Collections.Generic;

namespace SiteUptime.Helpers.Api.Responses
{
    public class ApiResponseAlertContact
    {
    }

    [XmlRoot("alertcontact")]
    public class ApiResponseAlertContactNew : ApiResponseAlertContact
    {
        [XmlAttribute]
        public int id { get; set; }
    }

    [XmlRoot("alert_contact")]
    public class ApiResponseAlertContactEditDelete : ApiResponseAlertContact
    {
        [XmlAttribute]
        public int id { get; set; }
    }
}