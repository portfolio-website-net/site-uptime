using System.Xml.Serialization;
using System.Collections.Generic;

namespace SiteUptime.Helpers.Api.Responses
{
    [XmlRoot("psp")]
    public class ApiResponsePublicStatusPage
    {
        [XmlAttribute]
        public int id { get; set; }
    }
}