using System.Xml.Serialization;
using System.Collections.Generic;

namespace SiteUptime.Helpers.Api.Responses
{
    [XmlRoot("psps")]
    public class ApiResponsePublicStatusPages
    {
        public ApiResponsePublicStatusPagePagination pagination { get; set; }

        [XmlElement]
        public List<ApiResponsePublicStatusPagePage> psp { get; set; }
    }

    [XmlRoot("pagination")]
    public class ApiResponsePublicStatusPagePagination
    {
        [XmlAttribute]
        public int offset { get; set; }

        [XmlAttribute]
        public int limit { get; set; }

        [XmlAttribute]
        public int total { get; set; }
    }

    [XmlRoot("psp")]
    public class ApiResponsePublicStatusPagePage
    {
        [XmlAttribute]
        public int id { get; set; }

        [XmlAttribute]
        public string friendly_name { get; set; }

        [XmlAttribute]
        public string monitors { get; set; }

        [XmlAttribute]
        public string sort { get; set; }        

        [XmlAttribute]
        public string status { get; set; }

        [XmlAttribute]
        public string standard_url { get; set; }

        [XmlAttribute]
        public string custom_url { get; set; }
    }    
}