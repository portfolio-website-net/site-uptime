using System.Xml.Serialization;
using System.Collections.Generic;

namespace SiteUptime.Helpers.Api.Responses
{
    [XmlRoot("mwindows")]
    public class ApiResponseMaintenanceWindows
    {
        [XmlAttribute]
        public string stat { get; set; }

        public ApiResponseMaintenanceWindowPagination pagination { get; set; }

        [XmlElement]
        public List<ApiResponseMaintenanceWindowWindow> mwindow { get; set; }
    }

    [XmlRoot("pagination")]
    public class ApiResponseMaintenanceWindowPagination
    {
        [XmlAttribute]
        public int limit { get; set; }

        [XmlAttribute]
        public int offset { get; set; }        

        [XmlAttribute]
        public int total { get; set; }
    }

    [XmlRoot("mwindow")]
    public class ApiResponseMaintenanceWindowWindow
    {
        [XmlAttribute]
        public int id { get; set; }

        [XmlAttribute]
        public string type { get; set; }

        [XmlAttribute]
        public string friendly_name { get; set; }

        [XmlAttribute]
        public string start_time { get; set; }

        [XmlAttribute]
        public string duration { get; set; }

        [XmlAttribute]
        public string value { get; set; }

        [XmlAttribute]
        public string status { get; set; }
    }
}