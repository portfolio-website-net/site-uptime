using System.Xml.Serialization;
using System.Collections.Generic;

namespace SiteUptime.Helpers.Api.Responses
{
    [XmlRoot("monitors")]
    public class ApiResponseMonitors
    {
        [XmlAttribute]
        public string stat { get; set; }

        public ApiResponseMonitorPagination pagination { get; set; }

        [XmlElement]
        public List<ApiResponseMonitorMonitor> monitor { get; set; }
    }

    [XmlRoot("pagination")]
    public class ApiResponseMonitorPagination
    {
        public int offset { get; set; }

        public int limit { get; set; }

        public int total { get; set; }
    }

    [XmlRoot("monitor")]
    public class ApiResponseMonitorMonitor
    {
        [XmlAttribute]
        public int id { get; set; }

        [XmlAttribute]
        public string friendly_name { get; set; }

        [XmlAttribute]
        public string url { get; set; }

        [XmlAttribute]
        public string type { get; set; }

        [XmlAttribute]
        public string sub_type { get; set; }

        [XmlAttribute]
        public string keyword_type { get; set; }

        [XmlAttribute]
        public string keyword_value { get; set; }

        [XmlAttribute]
        public string http_username { get; set; }

        [XmlAttribute]
        public string http_password { get; set; }

        [XmlAttribute]
        public string port { get; set; }

        [XmlAttribute]
        public string interval { get; set; }

        [XmlAttribute]
        public string status { get; set; }

        [XmlAttribute]
        public string create_datetime { get; set; }
    }
}