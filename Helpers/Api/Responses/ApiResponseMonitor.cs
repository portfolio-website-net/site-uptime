using System.Xml.Serialization;
using System.Collections.Generic;

namespace SiteUptime.Helpers.Api.Responses
{
    [XmlRoot("monitor")]
    public class ApiResponseMonitor
    {
        [XmlAttribute]
        public string status { get; set; }

        [XmlAttribute]
        public int id { get; set; }
    }
}