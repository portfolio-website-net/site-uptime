using System.Xml.Serialization;

namespace SiteUptime.Helpers.Api.Responses
{
    [XmlRoot("account")]
    public class ApiResponseAccountDetails
    {
        [XmlAttribute]
        public string email { get; set; }

        [XmlAttribute]
        public int monitor_limit { get; set; }

        [XmlAttribute]
        public int monitor_interval { get; set; }

        [XmlAttribute]
        public int up_monitors { get; set; }

        [XmlAttribute]
        public int down_monitors { get; set; }

        [XmlAttribute]
        public int paused_monitors { get; set; }
    }
}