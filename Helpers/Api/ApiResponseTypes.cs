namespace SiteUptime.Helpers.Api
{
    public static class ApiResponseTypes
    {
        public const string Json = "json";
        public const string Xml = "xml";
    }
}