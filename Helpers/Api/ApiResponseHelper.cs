using System.Xml;
using System.Xml.Serialization;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace SiteUptime.Helpers.Api
{
    public static class ApiResponseHelper
    {
        // Reference URL: https://stackoverflow.com/questions/1772004/how-can-i-make-the-xmlserializer-only-serialize-plain-xml
        public static string SerializeXml(object dataToSerialize)
        {
            var emptyNamespaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            var serializer = new XmlSerializer(dataToSerialize.GetType());
            var settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;

            using (var stream = new StringWriter())
            using (var writer = XmlWriter.Create(stream, settings))
            {
                serializer.Serialize(writer, dataToSerialize, emptyNamespaces);
                return stream.ToString();
            }
        }

        public static ContentResult GetXmlResponse(object dataToSerialize)
        {
            return new ContentResult
            {
                ContentType = "application/xml",
                Content = SerializeXml(dataToSerialize),
                StatusCode = 200
            };
        }

        public static ContentResult GetJsonResponse(object dataToSerialize, string callback)
        {
            var serializedData = JsonConvert.SerializeObject(dataToSerialize, 
                Newtonsoft.Json.Formatting.None, 
                new JsonSerializerSettings { 
                    NullValueHandling = NullValueHandling.Ignore
                });

            if (!string.IsNullOrEmpty(callback))
            {
                return new ContentResult
                {
                    ContentType = "application/javascript",
                    Content = $"{callback}({serializedData})",
                    StatusCode = 200
                };
            }
            else
            {
                return new ContentResult
                {
                    ContentType = "application/json",
                    Content = serializedData,
                    StatusCode = 200
                };
            }
        }
    }
}