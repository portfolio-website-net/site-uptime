using System.Xml.Serialization;

namespace SiteUptime.Helpers.Api
{
    [XmlRoot("error")]
    public class ApiError
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("message")]
        public string Message { get; set; }

        [XmlAttribute("parameter_name")]
        public string ParameterName { get; set; }

        [XmlAttribute("passed_value")]
        public string PassedValue { get; set; }
    }
}