namespace SiteUptime.Helpers.Api.Requests
{
    public class ApiRequestAlertContact : ApiRequestBase
    {
        public int id { get; set; }

        public string type { get; set; }

        public string value { get; set; }

        public string friendly_name { get; set; }
    }
}