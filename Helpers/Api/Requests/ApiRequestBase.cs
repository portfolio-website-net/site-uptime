namespace SiteUptime.Helpers.Api.Requests
{
    public class ApiRequestBase
    {
        public string api_key { get; set; }

        public string format { get; set; }

        public string callback { get; set; }
    }
}