namespace SiteUptime.Helpers.Api.Requests
{
    public class ApiRequestPublicStatusPage : ApiRequestBase
    {
        public int id { get; set; }
        
        public string friendly_name { get; set; }

        public string monitors { get; set; }

        public string custom_domain { get; set; }

        public string password { get; set; }

        public string sort { get; set; }

        public string hide_url_links { get; set; }

        public string status { get; set; }
    }
}