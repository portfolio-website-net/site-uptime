namespace SiteUptime.Helpers.Api.Requests
{
    public class ApiRequestPublicStatusPages : ApiRequestBase
    {
        public string psps { get; set; }

        public string offset { get; set; }

        public string limit { get; set; }
    }
}