namespace SiteUptime.Helpers.Api.Requests
{
    public class ApiRequestMonitor : ApiRequestBase
    {
        public int id { get; set; }
        
        public string friendly_name { get; set; }

        public string url { get; set; }

        public string type { get; set; }

        public string sub_type { get; set; }

        public string port { get; set; }

        public string keyword_type { get; set; }

        public string keyword_value { get; set; }

        public string interval { get; set; }

        public string http_username { get; set; }

        public string http_password { get; set; }

        public string http_method { get; set; }

        public string post_type { get; set; }

        public string post_value { get; set; }

        public string post_content_type { get; set; }

        public string alert_contacts { get; set; }

        public string mwindows { get; set; }

        public string custom_http_headers { get; set; }

        public string custom_http_statuses { get; set; }

        public string ignore_ssl_errors { get; set; }
    }
}