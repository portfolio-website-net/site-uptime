namespace SiteUptime.Helpers.Api.Requests
{
    public class ApiRequestMonitors : ApiRequestBase
    {
        public string monitors { get; set; }

        public string types { get; set; }

        public string statuses { get; set; }

        public string custom_uptime_ratios { get; set; }

        public string custom_uptime_ranges { get; set; }

        public string all_time_uptime_ratio { get; set; }

        public string all_time_uptime_durations { get; set; }

        public string logs { get; set; }

        public string logs_start_date { get; set; }

        public string logs_end_date { get; set; }

        public string log_types { get; set; }

        public string logs_limit { get; set; }

        public string response_times { get; set; }

        public string response_times_limit { get; set; }

        public string response_times_average { get; set; }

        public string response_times_start_date { get; set; }

        public string response_times_end_date { get; set; }

        public string alert_contacts { get; set; }

        public string mwindows { get; set; }

        public string ssl { get; set; }

        public string custom_http_headers { get; set; }

        public string custom_http_statuses { get; set; }

        public string timezone { get; set; }

        public string offset { get; set; }

        public string limit { get; set; }

        public string search { get; set; }
    }
}