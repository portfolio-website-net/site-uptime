namespace SiteUptime.Helpers.Api.Requests
{
    public class ApiRequestAlertContacts : ApiRequestBase
    {
        public string alert_contacts { get; set; }

        public string offset { get; set; }

        public string limit { get; set; }
    }
}