namespace SiteUptime.Helpers.Api.Requests
{
    public class ApiRequestMaintenanceWindow : ApiRequestBase
    {
        public int id { get; set; }
        
        public string friendly_name { get; set; }

         public string type { get; set; }

        public string value { get; set; }

        public string start_time { get; set; }

        public string duration { get; set; }
    }
}