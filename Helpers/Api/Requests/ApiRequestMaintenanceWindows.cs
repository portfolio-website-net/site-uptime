namespace SiteUptime.Helpers.Api.Requests
{
    public class ApiRequestMaintenanceWindows : ApiRequestBase
    {
        public string mwindows { get; set; }

        public string offset { get; set; }

        public string limit { get; set; }
    }
}