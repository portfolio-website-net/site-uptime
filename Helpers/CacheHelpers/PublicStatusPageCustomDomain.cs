namespace SiteUptime.Helpers.CacheHelpers
{
    public class PublicStatusPageCustomDomain
    {        
        public string StatusPageKey { get; set; }
        
        public string CustomDomain { get; set; }        
    }
}