using SiteUptime.Models;
using System.Collections.Generic;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace SiteUptime.Helpers.CacheHelpers
{
    public static class MemoryCacheHelper
    {
        private static DbContextOptions<Context> _dbContextOptions;

        private const string _uptimeMonitorTypesKey = "UptimeMonitorTypes";
        private const string _uptimeMonitorHttpMethodTypesKey = "UptimeMonitorHttpMethodTypes";        
        private const string _uptimeStatusTypesKey = "UptimeStatusTypes";
        private const string _uptimeReasonTypesKey = "UptimeReasonTypes";
        private const string _uptimeAlertContactTypesKey = "UptimeAlertContactTypes";
        private const string _uptimeAlertContactStatusTypesKey = "UptimeAlertContactStatusTypes";
        private const string _uptimeMobileProviderTypesKey = "UptimeMobileProviderTypes";
        private const string _uptimeNotificationEventTypesKey = "UptimeNotificationEventTypes";
        private const string _uptimeMonitorApiKeyTypesKey = "UptimeMonitorApiKeyTypes";
        private const string _uptimeMaintenanceWindowTypesKey = "UptimeMaintenanceWindowTypes";
        private const string _uptimeMonitorSortTypesKey = "UptimeMonitorSortTypes";
        private const string _userTimeZoneTypesKey = "UserTimeZoneTypes";
        private const string _userSecurityQuestionTypesKey = "UserSecurityQuestionTypes";
        private const string _twoFactorAuthenticationTypesKey = "TwoFactorAuthenticationTypes";
        private const string _publicStatusPageCustomDomainsKey = "PublicStatusPageCustomDomains";

        private static IMemoryCache _cache = new MemoryCache(new MemoryCacheOptions());

        public static List<UptimeMonitorType> UptimeMonitorTypes 
        {
            get
            {
                List<UptimeMonitorType> uptimeMonitorTypes;
                if (!_cache.TryGetValue(_uptimeMonitorTypesKey, out uptimeMonitorTypes))
                {
                    using (var context = new Context(_dbContextOptions))
                    {
                        uptimeMonitorTypes = context.UptimeMonitorTypes.ToList();
                        _cache.Set(_uptimeMonitorTypesKey, uptimeMonitorTypes);
                    }
                }

                return uptimeMonitorTypes;
            }
        }

        public static List<UptimeMonitorHttpMethodType> UptimeMonitorHttpMethodTypes 
        {
            get
            {
                List<UptimeMonitorHttpMethodType> uptimeMonitorHttpMethodTypes;
                if (!_cache.TryGetValue(_uptimeMonitorHttpMethodTypesKey, out uptimeMonitorHttpMethodTypes))
                {
                    using (var context = new Context(_dbContextOptions))
                    {
                        uptimeMonitorHttpMethodTypes = context.UptimeMonitorHttpMethodTypes.ToList();
                        _cache.Set(_uptimeMonitorHttpMethodTypesKey, uptimeMonitorHttpMethodTypes);
                    }
                }

                return uptimeMonitorHttpMethodTypes;
            }
        }
        
        public static List<UptimeStatusType> UptimeStatusTypes
        {
            get
            {
                List<UptimeStatusType> uptimeStatusTypes;
                if (!_cache.TryGetValue(_uptimeStatusTypesKey, out uptimeStatusTypes))
                {
                    using (var context = new Context(_dbContextOptions))
                    {
                        uptimeStatusTypes = context.UptimeStatusTypes.ToList();
                        _cache.Set(_uptimeStatusTypesKey, uptimeStatusTypes);
                    }
                }

                return uptimeStatusTypes;
            }
        }

        public static List<UptimeReasonType> UptimeReasonTypes
        {
            get
            {
                List<UptimeReasonType> uptimeReasonTypes;
                if (!_cache.TryGetValue(_uptimeReasonTypesKey, out uptimeReasonTypes))
                {
                    using (var context = new Context(_dbContextOptions))
                    {
                        uptimeReasonTypes = context.UptimeReasonTypes.ToList();
                        _cache.Set(_uptimeReasonTypesKey, uptimeReasonTypes);
                    }
                }

                return uptimeReasonTypes;
            }
        }

        public static List<UptimeAlertContactType> UptimeAlertContactTypes
        {
            get
            {
                List<UptimeAlertContactType> uptimeAlertContactTypes;
                if (!_cache.TryGetValue(_uptimeAlertContactTypesKey, out uptimeAlertContactTypes))
                {
                    using (var context = new Context(_dbContextOptions))
                    {
                        uptimeAlertContactTypes = context.UptimeAlertContactTypes.ToList();
                        _cache.Set(_uptimeAlertContactTypesKey, uptimeAlertContactTypes);
                    }
                }

                return uptimeAlertContactTypes;
            }
        }

        public static List<UptimeAlertContactStatusType> UptimeAlertContactStatusTypes
        {
            get
            {
                List<UptimeAlertContactStatusType> uptimeAlertContactStatusTypes;
                if (!_cache.TryGetValue(_uptimeAlertContactStatusTypesKey, out uptimeAlertContactStatusTypes))
                {
                    using (var context = new Context(_dbContextOptions))
                    {
                        uptimeAlertContactStatusTypes = context.UptimeAlertContactStatusTypes.ToList();
                        _cache.Set(_uptimeAlertContactStatusTypesKey, uptimeAlertContactStatusTypes);
                    }
                }

                return uptimeAlertContactStatusTypes;
            }
        }

        public static List<UptimeMobileProviderType> UptimeMobileProviderTypes
        {
            get
            {
                List<UptimeMobileProviderType> uptimeMobileProviderTypes;
                if (!_cache.TryGetValue(_uptimeMobileProviderTypesKey, out uptimeMobileProviderTypes))
                {
                    using (var context = new Context(_dbContextOptions))
                    {
                        uptimeMobileProviderTypes = context.UptimeMobileProviderTypes.ToList();
                        _cache.Set(_uptimeMobileProviderTypesKey, uptimeMobileProviderTypes);
                    }
                }

                return uptimeMobileProviderTypes;
            }
        }

        public static List<UptimeNotificationEventType> UptimeNotificationEventTypes
        {
            get
            {
                List<UptimeNotificationEventType> uptimeNotificationEventTypes;
                if (!_cache.TryGetValue(_uptimeNotificationEventTypesKey, out uptimeNotificationEventTypes))
                {
                    using (var context = new Context(_dbContextOptions))
                    {
                        uptimeNotificationEventTypes = context.UptimeNotificationEventTypes.ToList();
                        _cache.Set(_uptimeNotificationEventTypesKey, uptimeNotificationEventTypes);
                    }
                }

                return uptimeNotificationEventTypes;
            }
        }

        public static List<UptimeMonitorApiKeyType> UptimeMonitorApiKeyTypes
        {
            get
            {
                List<UptimeMonitorApiKeyType> uptimeMonitorApiKeyTypes;
                if (!_cache.TryGetValue(_uptimeMonitorApiKeyTypesKey, out uptimeMonitorApiKeyTypes))
                {
                    using (var context = new Context(_dbContextOptions))
                    {
                        uptimeMonitorApiKeyTypes = context.UptimeMonitorApiKeyTypes.ToList();
                        _cache.Set(_uptimeMonitorApiKeyTypesKey, uptimeMonitorApiKeyTypes);
                    }
                }

                return uptimeMonitorApiKeyTypes;
            }
        }

        public static List<UptimeMaintenanceWindowType> UptimeMaintenanceWindowTypes
        {
            get
            {
                List<UptimeMaintenanceWindowType> uptimeMaintenanceWindowTypes;
                if (!_cache.TryGetValue(_uptimeMaintenanceWindowTypesKey, out uptimeMaintenanceWindowTypes))
                {
                    using (var context = new Context(_dbContextOptions))
                    {
                        uptimeMaintenanceWindowTypes = context.UptimeMaintenanceWindowTypes.ToList();
                        _cache.Set(_uptimeMaintenanceWindowTypesKey, uptimeMaintenanceWindowTypes);
                    }
                }

                return uptimeMaintenanceWindowTypes;
            }
        }

        public static List<UptimeMonitorSortType> UptimeMonitorSortTypes
        {
            get
            {
                List<UptimeMonitorSortType> uptimeMonitorSortTypes;
                if (!_cache.TryGetValue(_uptimeMonitorSortTypesKey, out uptimeMonitorSortTypes))
                {
                    using (var context = new Context(_dbContextOptions))
                    {
                        uptimeMonitorSortTypes = context.UptimeMonitorSortTypes.ToList();
                        _cache.Set(_uptimeMonitorSortTypesKey, uptimeMonitorSortTypes);
                    }
                }

                return uptimeMonitorSortTypes;
            }
        }

        public static List<UserTimeZoneType> UserTimeZoneTypes
        {
            get
            {
                List<UserTimeZoneType> userTimeZoneTypes;
                if (!_cache.TryGetValue(_userTimeZoneTypesKey, out userTimeZoneTypes))
                {
                    using (var context = new Context(_dbContextOptions))
                    {
                        userTimeZoneTypes = context.UserTimeZoneTypes.ToList();
                        _cache.Set(_userTimeZoneTypesKey, userTimeZoneTypes);
                    }
                }

                return userTimeZoneTypes;
            }
        }

        public static List<UserSecurityQuestionType> UserSecurityQuestionTypes
        {
            get
            {
                List<UserSecurityQuestionType> userSecurityQuestionTypes;
                if (!_cache.TryGetValue(_userSecurityQuestionTypesKey, out userSecurityQuestionTypes))
                {
                    using (var context = new Context(_dbContextOptions))
                    {
                        userSecurityQuestionTypes = context.UserSecurityQuestionTypes.ToList();
                        _cache.Set(_userSecurityQuestionTypesKey, userSecurityQuestionTypes);
                    }
                }

                return userSecurityQuestionTypes;
            }
        }

        public static List<TwoFactorAuthenticationType> TwoFactorAuthenticationTypes
        {
            get
            {
                List<TwoFactorAuthenticationType> twoFactorAuthenticationTypes;
                if (!_cache.TryGetValue(_twoFactorAuthenticationTypesKey, out twoFactorAuthenticationTypes))
                {
                    using (var context = new Context(_dbContextOptions))
                    {
                        twoFactorAuthenticationTypes = context.TwoFactorAuthenticationTypes.ToList();
                        _cache.Set(_twoFactorAuthenticationTypesKey, twoFactorAuthenticationTypes);
                    }
                }

                return twoFactorAuthenticationTypes;
            }
        }

        public static List<PublicStatusPageCustomDomain> PublicStatusPageCustomDomains
        {
            get
            {
                List<PublicStatusPageCustomDomain> publicStatusPageCustomDomains;
                if (!_cache.TryGetValue(_publicStatusPageCustomDomainsKey, out publicStatusPageCustomDomains))
                {
                    using (var context = new Context(_dbContextOptions))
                    {
                        publicStatusPageCustomDomains = context.UptimePublicStatusPages
                            .Where(x => x.CustomDomain != null)
                            .Select(x => new PublicStatusPageCustomDomain
                                {
                                    StatusPageKey = x.StatusPageKey,
                                    CustomDomain = x.CustomDomain
                                }
                            )
                            .ToList();
                        _cache.Set(_publicStatusPageCustomDomainsKey, publicStatusPageCustomDomains);
                    }
                }

                return publicStatusPageCustomDomains;
            }
        }

        public static void RemovePublicStatusPageCustomDomains()
        {
            _cache.Remove(_publicStatusPageCustomDomainsKey);
        }

        public static void Initialize(DbContextOptions<Context> dbContextOptions)
        {
            _dbContextOptions = dbContextOptions;
        }
    }
}