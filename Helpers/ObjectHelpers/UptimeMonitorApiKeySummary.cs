using System;
using SiteUptime.Models;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SiteUptime.Helpers.ObjectHelpers
{
    public class UptimeMonitorApiKeySummary
    {
        [JsonProperty("uptimeMonitorApiKeyId")]
        public int UptimeMonitorApiKeyId { get; set; }

        [JsonProperty("uptimeMonitorApiKeyTypeId")]
        public int UptimeMonitorApiKeyTypeId { get; set; }

        [JsonProperty("uptimeMonitorId")]
        public int? UptimeMonitorId { get; set; }        

        [JsonProperty("apiKey")]
        public string ApiKey { get; set; }
    }
}