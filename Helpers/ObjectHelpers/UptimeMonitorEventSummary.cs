using System;
using SiteUptime.Models;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SiteUptime.Helpers.ObjectHelpers
{
    public class UptimeMonitorEventSummary
    {
        [JsonProperty("eventName")]
        public string EventName { get; set; }

        [JsonProperty("monitorName")]
        public string MonitorName { get; set; }

        [JsonProperty("dateStr")]
        public string DateStr { get; set; }
        
        [JsonProperty("reasonName")]
        public string ReasonName { get; set; }
        
        [JsonProperty("durationStr")]
        public string DurationStr { get; set; }
    }
}