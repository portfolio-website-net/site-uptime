using System;
using SiteUptime.Models;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SiteUptime.Helpers.ObjectHelpers
{
    public class UptimePublicStatusPageSummary
    {
        [JsonProperty("uptimePublicStatusPageId")]
        public int UptimePublicStatusPageId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }        

        [JsonProperty("isActive")]
        public bool IsActive { get; set; }
    }
}