using System;
using SiteUptime.Models;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SiteUptime.Helpers.ObjectHelpers
{
    public class UserDetails
    {
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }
        
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("securityQuestion")]
        public string SecurityQuestion { get; set; } 

        [JsonProperty("twoFactorAuthenticationTypeId")]
        public int? TwoFactorAuthenticationTypeId { get; set; }   

        [JsonProperty("userTimeZoneTypeId")]
        public int UserTimeZoneTypeId { get; set; }

        [JsonIgnore]
        public bool IsInvalidRecaptcha { get; set; }
    }
}