using System;
using SiteUptime.Models;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SiteUptime.Helpers.ObjectHelpers
{
    public class UptimeMaintenanceWindowSummary
    {
        [JsonProperty("uptimeMaintenanceWindowId")]
        public int UptimeMaintenanceWindowId { get; set; }

        [JsonProperty("uptimeMaintenanceWindowTypeId")]
        public int UptimeMaintenanceWindowTypeId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("startTime")]
        public string StartTime { get; set; }

        [JsonProperty("duration")]
        public int Duration { get; set; }

        [JsonProperty("days")]
        public string Days { get; set; }

        [JsonProperty("isActive")]
        public bool IsActive { get; set; }
    }
}