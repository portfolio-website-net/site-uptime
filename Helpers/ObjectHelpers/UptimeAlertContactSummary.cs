using System;
using SiteUptime.Models;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SiteUptime.Helpers.ObjectHelpers
{
    public class UptimeAlertContactSummary
    {
        [JsonProperty("uptimeAlertContactId")]
        public int UptimeAlertContactId { get; set; }

        [JsonProperty("uptimeAlertContactTypeId")]
        public int UptimeAlertContactTypeId { get; set; }

        [JsonProperty("uptimeAlertContactStatusTypeId")]
        public int UptimeAlertContactStatusTypeId { get; set; }
        
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }     

        [JsonProperty("displayName")]
        public string DisplayName
        { 
            get
            {
                return $"{Name} ({Address})";
            }
        }

        [JsonProperty("requiresActivation")]
        public bool RequiresActivation { get; set; } 
    }
}