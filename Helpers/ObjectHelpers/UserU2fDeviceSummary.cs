using Newtonsoft.Json;
 
namespace SiteUptime.Models
{
    public class UserU2fDeviceSummary
    {
        [JsonProperty("userU2fDeviceId")]
        public int UserU2fDeviceId { get; set; }

        [JsonProperty("deviceName")]
        public string DeviceName { get; set; }
    }
}