using System;
using SiteUptime.Models;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SiteUptime.Helpers.ObjectHelpers
{
    public class UptimeMonitorSummary
    {
        [JsonProperty("uptimeMonitorId")]
        public int UptimeMonitorId { get; set; }

        [JsonProperty("uptimeMonitorTypeId")]
        public int UptimeMonitorTypeId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName
        { 
            get
            {
                return $"{Name} ({Url})";
            }
        }

        [JsonProperty("url")]
        public string Url { get; set; }        

        [JsonProperty("uptimeStatusTypeId")]
        public int? UptimeStatusTypeId { get; set; }
        
        [JsonProperty("uptimePercentage")]
        public double? UptimePercentage { get; set; }

        [JsonProperty("showConfiguration")]
        public bool ShowConfiguration { get; set; }
    }
}