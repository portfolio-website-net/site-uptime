using SiteUptime.Models;
using System.Collections.Generic;
using SiteUptime.Helpers.ObjectHelpers;

namespace SiteUptime.Helpers.Results
{
    public class PublicStatusPageResult
    {
        public string StatusPageKey { get; set; }

        public string Name { get; set; }

        public string LogoKey { get; set; }

        public string PasswordHash { get; set; }

        public string PasswordSalt { get; set; }

        public bool RequiresAuthentication { get; set; }

        public bool IsAuthenticated { get; set; }

        public bool InvalidCredentials { get; set; }

        public OverallMonitorStats OverallMonitorStats { get; set; }

        public List<CurrentUptimeMonitorDetails> CurrentUptimeMonitorDetails { get; set; }

        public CurrentUptimeMonitorDetails SelectedUptimeMonitorDetail { get; set; }

        public List<UptimeMonitorChartData> SelectedUptimeMonitorChartData { get; set; }

        public List<UptimeMonitorEventSummary> SelectedUptimeMonitorEventSummary { get; set; }
    }
}