namespace SiteUptime.Helpers.Authentication
{
    public class UserProfileUpdateResult
    {
        public UserProfileUpdateResultType UserProfileUpdateResultType { get; set; }

        public string ErrorMessage { get; set; }
    }

    public enum UserProfileUpdateResultType
    {
        None,
        Error,
        InvalidCurrentPassword,
        PasswordsDoNotMatch,
        UsernameMustBeUnique,
        Success
    }
}