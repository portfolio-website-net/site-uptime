namespace SiteUptime.Helpers.Authentication
{
    public class ActivateAccountResult
    {
        public ActivateAccountResultType ActivateAccountResultType { get; set; }
    }

    public enum ActivateAccountResultType
    {
        None,
        Success,
        InvalidCode
    }
}