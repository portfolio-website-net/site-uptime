namespace SiteUptime.Helpers.Authentication
{
    public class SignUpResult
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public SignUpResultType SignUpResultType { get; set; }
    }

    public enum SignUpResultType
    {
        None,
        Success,
        InvalidEmail,
        EmailInUse,
        InvalidPassword,
        InvalidRecaptcha,
        Error
    }
}