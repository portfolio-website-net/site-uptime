using System;
using System.Text;
using System.Security.Cryptography;
 
namespace SiteUptime.Helpers.Authentication
{
    // Source: https://dotnetcodr.com/2017/10/26/how-to-hash-passwords-with-a-salt-in-net-2/
    public class RNG
    {
        public string GenerateRandomCryptographicKey(int keyLength)
        {           
            return Convert.ToBase64String(GenerateRandomCryptographicBytes(keyLength));
        }
 
        public byte[] GenerateRandomCryptographicBytes(int keyLength)
        {
            RNGCryptoServiceProvider rngCryptoServiceProvider = new RNGCryptoServiceProvider();
            byte[] randomBytes = new byte[keyLength];
            rngCryptoServiceProvider.GetBytes(randomBytes);
            return randomBytes;
        }
    }
}