namespace SiteUptime.Helpers.Authentication
{
    public class ActivateEmailResult
    {
        public ActivateEmailResultType ActivateEmailResultType { get; set; }
    }

    public enum ActivateEmailResultType
    {
        None,
        Success,
        InvalidCode
    }
}