namespace SiteUptime.Helpers.Authentication
{
    public class UnlockAccountResult
    {
        public UnlockAccountResultType UnlockAccountResultType { get; set; }
    }

    public enum UnlockAccountResultType
    {
        None,
        Success,
        InvalidCode
    }
}