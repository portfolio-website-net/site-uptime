using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using SiteUptime.Services.Interfaces;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using SiteUptime.Models;
using SiteUptime.Helpers.Api;
using System.Text.Json;

namespace SiteUptime.Helpers.Authentication
{
    // Reference: https://github.com/derekgreer/authorize-example
    public class ClaimRequirementFilter : IAuthorizationFilter
    {
        private readonly Claim _claim;
        private readonly IAuthenticationService _authenticationService;
        private readonly Settings _settings;
        private readonly IEventLogService _eventLogService;

        public ClaimRequirementFilter(
            Claim claim, 
            IAuthenticationService authenticationService,
            IOptions<Settings> settings,
            IEventLogService eventLogService)
        {
            _claim = claim;
            _authenticationService = authenticationService;
            _settings = settings.Value;
            _eventLogService = eventLogService;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (ProcessApiUser(context))
            {
                return;
            }

            var path = context.HttpContext.Request.Path.ToString();
            var sessionCookie = context.HttpContext.Request.Cookies[_settings.SessionCookieName];

            var isCached = true;

            var currentUser = (User)context.HttpContext.Items["User"];
            var currentUserSession = (UserSession)context.HttpContext.Items["UserSession"];
            
            if (currentUser == null)
            {
                isCached = false;
                currentUser = _authenticationService.GetCurrentUser(sessionCookie, out currentUserSession);
            }

            if (currentUser != null)
            {
                if (!currentUserSession.IsValid
                    && path.ToLower().StartsWith("/Secure".ToLower()))
                {
                    context.Result = new RedirectResult("/Home/TwoFactorAuth");
                }

                var claims = new List<string>();
                if (!string.IsNullOrEmpty(_claim.Value))
                {
                    claims = _claim.Value.Split(',').Select(x => x.Trim()).ToList();
                }

                var hasClaim = _claim.Value == string.Empty
                    || currentUser.ClaimsList.Where(x => claims.Contains(x)).Any();
                if (!hasClaim)
                {
                    _eventLogService.LogMessage(currentUser, EventNameType.NoAccessToSecurePage);
                    context.Result = new RedirectResult("/Secure/NoAccess");
                }
                else if (!isCached)
                {
                    _eventLogService.LogMessage(currentUser, EventNameType.PageView);
                    context.HttpContext.Items["User"] = currentUser;
                    context.HttpContext.Items["UserSession"] = currentUserSession;
                }

                if (!isCached)
                {
                    _authenticationService.RenewSession(currentUserSession);
                }
            }
            else if (path.ToLower().StartsWith("/Secure".ToLower()))
            {
                _eventLogService.LogMessage(currentUser, EventNameType.AccessDenied);
                context.Result = new RedirectResult("/");
            }
            else
            {
                _eventLogService.LogMessage(null, EventNameType.PageView);
            }
        }

        private bool ProcessApiUser(AuthorizationFilterContext context)
        {
            var result = false;

            var path = context.HttpContext.Request.Path.ToString();
            if (context.HttpContext.Request.HasFormContentType)
            {
                var apiKey = context.HttpContext.Request.Form["api_key"];
                var format = context.HttpContext.Request.Form["format"];
                var callback = context.HttpContext.Request.Form["callback"];
                if (!string.IsNullOrEmpty(apiKey))
                {
                    result = true;

                    if (!path.ToLower().StartsWith("/v2".ToLower()))
                    {
                        context.Result = new BadRequestResult();
                    }
                    else
                    {
                        var isCached = true;

                        var currentUser = (User)context.HttpContext.Items["User"];
                        
                        if (currentUser == null)
                        {
                            isCached = false;
                            currentUser = _authenticationService.GetApiKeyUser(apiKey);
                        }

                        if (currentUser != null)
                        {
                            var claims = new List<string>();
                            if (!string.IsNullOrEmpty(_claim.Value))
                            {
                                claims = _claim.Value.Split(',').Select(x => x.Trim()).ToList();
                            }

                            var hasClaim = _claim.Value == string.Empty
                                || currentUser.ClaimsList.Where(x => claims.Contains(x)).Any();
                            if (!hasClaim)
                            {
                                _eventLogService.LogMessage(currentUser, EventNameType.ApiAccessDenied, apiKey);
                                SetBadApiRequestResult(context, apiKey, format, callback);
                            }
                            else if (!isCached)
                            {
                                if (currentUser.IsMainApiKey)
                                {
                                    _eventLogService.LogMessage(currentUser, EventNameType.MainApiKeyAccess, apiKey);
                                }
                                else if (currentUser.IsMonitorSpecificApiKey)
                                {
                                    _eventLogService.LogMessage(currentUser, EventNameType.MonitorSpecificApiKeyAccess, apiKey);
                                }
                                else if (currentUser.IsReadOnlyApiKey)
                                {
                                    _eventLogService.LogMessage(currentUser, EventNameType.ReadOnlyApiKeyAccess, apiKey);
                                }

                                context.HttpContext.Items["User"] = currentUser;
                            }
                        }
                        else
                        {
                            SetBadApiRequestResult(context, apiKey, format, callback);
                        }

                        if (string.IsNullOrEmpty(context.HttpContext.Response.Headers["Access-Control-Allow-Origin"]))
                        {
                            context.HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                        }
                    }
                }     
            }   

            return result; 
        }

        private void SetBadApiRequestResult(
            AuthorizationFilterContext context,
            string apiKey,
            string format,
            string callback)
        {
            var apiError = new ApiError
            {
                Type = "invalid_parameter",
                ParameterName = "api_key",
                PassedValue = apiKey
            };

            if (!string.IsNullOrEmpty(format)
                && format == ApiResponseTypes.Xml)
            {
                context.Result = ApiResponseHelper.GetXmlResponse(apiError);
            }
            else
            {
                var jsonApiError = new
                { 
                    stat = "fail",
                    error = new {
                        type = apiError.Type,
                        parameter_name = apiError.ParameterName,
                        passed_value = apiError.PassedValue
                    }
                };

                if (!string.IsNullOrEmpty(callback))
                {
                    context.Result = new ContentResult
                    {
                        ContentType = "application/javascript",
                        Content = $"{callback}({JsonSerializer.Serialize(jsonApiError)})",
                        StatusCode = 200
                    };
                }
                else
                {
                    context.Result = new JsonResult(jsonApiError);
                }
            }
        }
    }
}