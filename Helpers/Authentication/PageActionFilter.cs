using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using SiteUptime.Services.Interfaces;
using SiteUptime.Controllers.Base;

namespace SiteUptime.Helpers.Authentication
{
    // Reference: https://stackoverflow.com/questions/4154407/asp-net-mvc-razor-pass-model-to-layout/14882183#14882183
    public class PageActionFilter : Attribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var controller = context.Controller as BaseController;
            if (controller != null)
            {
                var currentUser = context.HttpContext.Items["User"];
                controller.ViewBag.User = currentUser;
            }
        }
    }
}