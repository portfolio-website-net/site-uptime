using Newtonsoft.Json;

namespace SiteUptime.Helpers.Authentication
{
    public class U2fChallenge
    {
        [JsonProperty("keyHandle")]
        public string KeyHandle { get; set; }

        [JsonProperty("challenge")]
        public string Challenge { get; set; }

        [JsonProperty("appId")]
        public string AppId { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }
    }
}