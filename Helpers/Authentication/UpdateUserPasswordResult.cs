namespace SiteUptime.Helpers.Authentication
{
    public class UpdateUserPasswordResult
    {
        public UpdateUserPasswordResultType UpdateUserPasswordResultType { get; set; }

        public string Result 
        {
            get
            {
                return UpdateUserPasswordResultType.ToString();
            }
        }
    }

    public enum UpdateUserPasswordResultType
    {
        None,
        Error,
        InvalidCurrentPassword,
        InvalidNewPassword,
        PasswordsDoNotMatch,
        Success
    }
}