namespace SiteUptime.Helpers.Authentication
{
    public class ActivateAlertContactResult
    {
        public ActivateAlertContactResultType ActivateAlertContactResultType { get; set; }
    }

    public enum ActivateAlertContactResultType
    {
        None,
        Success,
        InvalidCode
    }
}