namespace SiteUptime.Helpers.Authentication
{
    public class RequestUserEmailUpdateResult
    {
        public RequestUserEmailUpdateResultType RequestUserEmailUpdateResultType { get; set; }

        public string Result 
        {
            get
            {
                return RequestUserEmailUpdateResultType.ToString();
            }
        }
    }

    public enum RequestUserEmailUpdateResultType
    {
        None,
        Error,
        InvalidEmail,
        EmailInUse,        
        Success
    }
}