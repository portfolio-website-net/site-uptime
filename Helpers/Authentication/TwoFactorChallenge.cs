namespace SiteUptime.Helpers.Authentication
{
    public class TwoFactorChallenge
    {
        public string TwoFactorTypeCode { get; set; }

        public string AppId { get; set; }

        public string Challenge { get; set; }

        public string Challenges { get; set; }
    }
}