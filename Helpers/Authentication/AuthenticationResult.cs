namespace SiteUptime.Helpers.Authentication
{
    public class AuthenticationResult
    {
        public string Username { get; set; }

        public AuthenticationResultType AuthenticationResultType { get; set; }
    }

    public enum AuthenticationResultType
    {
        None,
        InvalidCredentials,
        InvalidTwoFactorResponse,
        InvalidTwoFactorSecurityAnswer,
        SignedOut
    }
}