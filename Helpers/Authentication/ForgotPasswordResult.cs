namespace SiteUptime.Helpers.Authentication
{
    public class ForgotPasswordResult
    {
        public string Code { get; set; }

        public string Username { get; set; }

        public ForgotPasswordResultType ForgotPasswordResultType { get; set; }
    }

    public enum ForgotPasswordResultType
    {
        None,
        Success,
        PasswordsDoNotMatch,
        InvalidCode,
        InvalidRecaptcha
    }
}