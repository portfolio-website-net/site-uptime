using System;
using SiteUptime.Models;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace SiteUptime.Helpers.Recaptcha
{
    public static class RecaptchaHelper
    {
        public static async Task<bool> ValidateResponse(string secret, string response, string ipAddress, string url)
        {
            var result = false;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                var parameters = new List<KeyValuePair<string, string>>();
                parameters.Add(new KeyValuePair<string, string>("secret", secret));
                parameters.Add(new KeyValuePair<string, string>("response", response));
                parameters.Add(new KeyValuePair<string, string>("remoteip", ipAddress));
                
                var clientResult = await client.PostAsync(string.Empty, new FormUrlEncodedContent(parameters));
                if (clientResult.IsSuccessStatusCode)
                {
                    var resultContent = await clientResult.Content.ReadAsStringAsync();
                    var recaptchaResponse = JsonConvert.DeserializeObject<RecaptchaResponse>(resultContent);

                    if (recaptchaResponse.Success)
                    {
                        result = true;
                    }
                }
            }

            return result;
        }

        public class RecaptchaResponse
        {
            [JsonProperty("success")]
            public bool Success { get; set; }

            [JsonProperty("challenge_ts")]
            public string ChallengeTimestamp { get; set; }

            [JsonProperty("hostname")]
            public string Hostname { get; set; }

            [JsonProperty("error-codes")]
            public List<string> ErrorCodes { get; set; }
        }
    }
}