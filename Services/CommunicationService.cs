using System;
using System.Linq;
using Microsoft.Extensions.Options;
using SiteUptime.Services.Interfaces;
using SiteUptime.Helpers;
using SiteUptime.Models;
using System.Net;
using System.Net.Mail;
using SiteUptime.Helpers.CacheHelpers;

namespace SiteUptime.Services
{
    public class CommunicationService : ICommunicationService
    {
        private readonly Settings _settings;

        public CommunicationService(IOptions<Settings> settings)
        {
            _settings = settings.Value;
        }

        public void SendEmailToSms(UptimeAlertContact uptimeAlertContact, string text)
        {
            var providerType = MemoryCacheHelper.UptimeMobileProviderTypes.First(x => uptimeAlertContact.UptimeMobileProviderTypeId == x.UptimeMobileProviderTypeId);

            SendEmail($"{uptimeAlertContact.Address}@{providerType.Address}", text, text, isBodyHtml: false);
        }

        public void SendEmail(string emailAddress, string subject, string body, bool isBodyHtml)
        {
            Console.WriteLine($"Sending mail to: {emailAddress}");
            Console.WriteLine($"{subject}");
            Console.WriteLine($"{body}");            
            
            try
            {
                var smtpClient = new SmtpClient
                {
                    Host = _settings.EmailHost,
                    Port = _settings.EmailPort,
                    EnableSsl = true,
                    Credentials = new NetworkCredential(_settings.EmailUsername, _settings.EmailPassword)
                };

                using (var message = new MailMessage(_settings.EmailFromAddress, emailAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = isBodyHtml
                })
                {
                    smtpClient.Send(message);
                }
            }
            catch (Exception exception)
            {
                ErrorLogging.Log(null, exception);
            }
        }
    }
}