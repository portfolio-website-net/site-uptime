﻿using System;
using System.Linq;
using System.Collections.Generic;
using SiteUptime.Models;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using SiteUptime.Helpers.Authentication;
using SiteUptime.Services.Interfaces;

namespace SiteUptime.Services
{
    public class UserProfileService : IUserProfileService
    {
        private readonly Context _context;
        private readonly IAuthenticationService _authenticationService;
        private readonly IEventLogService _eventLogService;

        public UserProfileService(
            Context context, 
            IAuthenticationService authenticationService,
            IEventLogService eventLogService)
        {
            _context = context;
            _authenticationService = authenticationService;
            _eventLogService = eventLogService;
        }
        
        public UserProfileUpdateResult UpdateUserProfile(ProfileFields profileFields)
        {
            var result = new UserProfileUpdateResult
            {
                UserProfileUpdateResultType = UserProfileUpdateResultType.Error
            };
            var user = _context.Users.FirstOrDefault(x => x.UserId == profileFields.UserId);
            if (user != null)
            {
                if (!string.IsNullOrEmpty(profileFields.CurrentPassword)
                    && _authenticationService.Authenticate(user.Username, profileFields.CurrentPassword, isProfile: true))
                {
                    if (!string.IsNullOrEmpty(profileFields.Username))
                    {
                        if (!_context.Users.Any(x => x.UserId != profileFields.UserId && x.Username == profileFields.Username))
                        {
                            if (user.Username != profileFields.Username)
                            {
                                _eventLogService.LogMessage(user, EventNameType.UsernameUpdate);
                                user.Username = profileFields.Username;
                            }
                        }
                        else
                        {
                            _eventLogService.LogMessage(user, EventNameType.UsernameMustBeUnique);
                            result.UserProfileUpdateResultType = UserProfileUpdateResultType.UsernameMustBeUnique;
                        }                        
                    }

                    if (!string.IsNullOrEmpty(profileFields.Password)
                        && profileFields.Password == profileFields.PasswordConfirm)
                    {
                        var hashResultSha512 = _authenticationService.GetHashedPasswordResult(profileFields.Password);
                        
                        _eventLogService.LogMessage(user, EventNameType.PasswordUpdate);
                        user.PasswordHash = hashResultSha512.Digest;
                        user.PasswordSalt = hashResultSha512.Salt;
                    }
                    else if (!string.IsNullOrEmpty(profileFields.Password))
                    {
                        _eventLogService.LogMessage(user, EventNameType.PasswordsDoNotMatch);
                        result.UserProfileUpdateResultType = UserProfileUpdateResultType.PasswordsDoNotMatch;
                    }

                    if (!string.IsNullOrEmpty(profileFields.FirstName))
                    {
                        user.FirstName = profileFields.FirstName;
                    }

                    if (!string.IsNullOrEmpty(profileFields.LastName))
                    {
                        user.LastName = profileFields.LastName;
                    }

                    if (!string.IsNullOrEmpty(profileFields.Email))
                    {
                        user.Email = profileFields.Email;
                    }

                    if (!string.IsNullOrEmpty(profileFields.Phone))
                    {
                        user.Phone = profileFields.Phone;
                    }

                    if (profileFields.Claims != null
                        && profileFields.Claims.Any())
                    {
                        var newClaims = string.Join(",", profileFields.Claims);
                        if (user.Claims != newClaims)
                        {
                            _eventLogService.LogMessage(user, EventNameType.PermissionUpdate, newClaims);
                            user.Claims = string.Join(",", profileFields.Claims);
                        }
                    }

                    if (result.UserProfileUpdateResultType == UserProfileUpdateResultType.Error)
                    {
                        user.UpdatedById = user.UserId;
                        user.UpdatedDate = DateTime.Now;

                        _eventLogService.LogMessage(user, EventNameType.ProfileUpdate);
                        _context.SaveChanges();

                        result.UserProfileUpdateResultType = UserProfileUpdateResultType.Success;
                    }
                }        
                else
                {
                    _eventLogService.LogMessage(user, EventNameType.InvalidProfilePassword);
                    result.UserProfileUpdateResultType = UserProfileUpdateResultType.InvalidCurrentPassword;
                }       
            }
            else
            {
                _eventLogService.LogMessage(user, EventNameType.ProfileUpdateError);
            } 

            return result;
        }
    }
}
