﻿using System;
using System.Linq;
using System.Collections.Generic;
using SiteUptime.Models;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using SiteUptime.Helpers.Authentication;
using SiteUptime.Services.Interfaces;
using Microsoft.Extensions.Options;
using AspNetCore.Totp;
using AspNetCore.Totp.Interface.Models;
using Microsoft.AspNetCore.Http;
using SiteUptime.Helpers.ObjectHelpers;
using SiteUptime.Helpers;
using SiteUptime.Helpers.Results;
using SiteUptime.Helpers.Types;
using SiteUptime.Helpers.CacheHelpers;
using U2F.Core.Crypto;
using U2F.Core.Models;
using U2F.Core.Utils;
using U2F.Core.Exceptions;
using Newtonsoft.Json;

namespace SiteUptime.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly Context _context;
        private readonly Settings _settings;
        private readonly IEventLogService _eventLogService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ICommunicationService _communicationService;

        public AuthenticationService(Context context, 
            IOptions<Settings> settings,
            IEventLogService eventLogService,
            IHttpContextAccessor httpContextAccessor,
            ICommunicationService communicationService)
        {
            _context = context;
            _settings = settings.Value;
            _eventLogService = eventLogService;
            _httpContextAccessor = httpContextAccessor;
            _communicationService = communicationService;
        }

        public void PopulateUsersIfEmptyDatabase()
        {
            if (_context.Users.Count() == 0
                && _settings.CreateSampleUsers)
            {
                var hashResultSha512 = GetHashedPasswordResult(_settings.SampleUserPassword);

                _context.Users.Add(new User {
                    Username = _settings.SampleUserEmail,
                    PasswordHash = hashResultSha512.Digest,
                    PasswordSalt = hashResultSha512.Salt,
                    FirstName = "Sample",
                    LastName = "User",
                    Email = _settings.SampleUserEmail,
                    Claims = "Read",
                    IsActivated = true,
                    UserTimeZoneTypeId = MemoryCacheHelper.UserTimeZoneTypes.First(x => x.Code == TimeZoneTypes.GMT__05).UserTimeZoneTypeId,
                    CreatedById = 0,
                    CreatedDate = DateTime.Now
                });            

                _context.SaveChanges();
            }
        }

        public HashWithSaltResult GetHashedPasswordResult(string password)
        {
            var pwHasher = new PasswordWithSaltHasher();
            var hashResultSha512 = pwHasher.HashWithSalt(password, 64, SHA512.Create());
            return hashResultSha512;
        }

        public bool Authenticate(string username, string password, bool isProfile = false)
        {
            var success = false;
            var user = _context.Users.FirstOrDefault(x => x.Username == username);
            if (user != null
                && user.IsActivated
                && !user.IsLocked)
            {
                if (!string.IsNullOrEmpty(password))
                {
                    var pwHasher = new PasswordWithSaltHasher();
                    var hashResultSha512 = pwHasher.HashWithSalt(password, user.PasswordSalt, SHA512.Create());
                    if (hashResultSha512.Digest == user.PasswordHash)
                    {
                        if (!isProfile)
                        {
                            _eventLogService.LogMessage(user, EventNameType.SignIn);
                        }
                        
                        success = true;
                    }
                    else if (!isProfile)
                    {
                        _eventLogService.LogMessage(user, EventNameType.FailedSignIn);
                    }
                }
                else
                {
                    _eventLogService.LogMessage(user, EventNameType.FailedSignIn);
                }

                if (!success)
                {
                    HandleInvalidSignInAttempt(user);
                }
            }
            else if (user != null
                && user.IsLocked)
            {
                _eventLogService.LogMessage(null, EventNameType.FailedSignIn, $"Username '{username}' is locked");
            }
            else if (user != null
                && !user.IsActivated)
            {
                _eventLogService.LogMessage(null, EventNameType.FailedSignIn, $"Username '{username}' is not activated");
            }
            else
            {
                _eventLogService.LogMessage(null, EventNameType.FailedSignIn, $"Username '{username}' does not exist");
            }

            return success;
        }

        public string CreateSession(string username)
        {
            var result = string.Empty;
            var user = _context.Users.FirstOrDefault(x => x.Username == username);
            if (user != null)
            {
                var currentDate = DateTime.Now;
                var sessionKey = StringHelpers.GenerateShortId();
                _context.UserSessions.Add(new UserSession {
                    UserId = user.UserId,
                    SessionKey = sessionKey.ToString(),
                    IsValid = user.TwoFactorAuthenticationTypeId == null,
                    ExpirationDate = currentDate.AddMinutes(_settings.SessionTime),
                    CreatedById = user.UserId,
                    CreatedDate = DateTime.Now
                });                

                _context.SaveChanges();

                result = sessionKey.ToString();
            }

            return result;
        }

        public bool IsValidSession(string sessionKey)
        {
            var userSession = GetSession(sessionKey);

            return userSession != null;
        }

        public bool EndSession(string sessionKey)
        {
            var success = false;
            var userSession = GetSession(sessionKey);
            if (userSession != null)
            {
                var currentDate = DateTime.Now;
                userSession.ExpirationDate = currentDate;

                var outdatedSessionDate = DateTime.Now.AddMinutes(-90);
                var outdatedUserSessions = _context.UserSessions.Where(x => x.ExpirationDate <= outdatedSessionDate);

                if (outdatedUserSessions.Count() > 0)
                {
                    _context.UserSessions.RemoveRange(outdatedUserSessions);
                }

                var outdatedEventLogDate = DateTime.Now.AddDays(-30);
                var outdatedEventLogs = _context.EventLogs.Where(x => x.CreatedDate <= outdatedEventLogDate);

                if (outdatedEventLogs.Count() > 0)
                {
                    _context.EventLogs.RemoveRange(outdatedEventLogs);
                }

                _eventLogService.LogMessage(new User { UserId = userSession.UserId }, EventNameType.SignOut);

                _context.SaveChanges();

                success = true;
            }            

            return success;
        }

        public bool RenewSession(string sessionKey)
        {
            var success = false;
            var userSession = GetSession(sessionKey);
            if (userSession != null)
            {
                var currentDate = DateTime.Now;
                userSession.ExpirationDate = currentDate.AddMinutes(_settings.SessionTime);
                userSession.UpdatedById = userSession.UserId;
                userSession.UpdatedDate = DateTime.Now;

                _context.SaveChanges();

                success = true;
            }            

            return success;
        }      

        public bool RenewSession(UserSession userSession)
        {
            var success = false;
            if (userSession != null)
            {
                var currentDate = DateTime.Now;
                userSession.ExpirationDate = currentDate.AddMinutes(_settings.SessionTime);
                userSession.UpdatedById = userSession.UserId;
                userSession.UpdatedDate = DateTime.Now;

                _context.SaveChanges();

                success = true;
            }            

            return success;
        }  

        public UserSession GetSession(string sessionKey)
        {
            var currentDate = DateTime.Now;
            var userSession = _context.UserSessions
                .FirstOrDefault(x => x.SessionKey == sessionKey && x.ExpirationDate > currentDate);
            
            return userSession;
        }

        public User GetCurrentUser(string sessionKey)
        {
            User user = null;
            if (!string.IsNullOrEmpty(sessionKey))
            {
                var userSession = GetSession(sessionKey);
                if (userSession != null)
                {
                    user = _context.Users.FirstOrDefault(x => x.UserId == userSession.UserId);
                }
            }
            
            return user;
        }

        public User GetCurrentUser(string sessionKey, out UserSession userSession)
        {
            User user = null;
            UserSession currentUserSession = null;
            if (!string.IsNullOrEmpty(sessionKey))
            {
                currentUserSession = GetSession(sessionKey);
                if (currentUserSession != null)
                {
                    user = _context.Users.FirstOrDefault(x => x.UserId == currentUserSession.UserId);                    
                }
            }

            userSession = currentUserSession;
            
            return user;
        }

        public bool ValidateSessionTwoFactorCode(UserSession userSession, User user, string code)
        {
            var success = false;
            if (userSession != null
                && user != null
                && IsTwoFactorTotpCodeValid(user, code))
            {
                userSession.IsValid = true;
                userSession.UpdatedById = userSession.UserId;
                userSession.UpdatedDate = DateTime.Now;

                _context.SaveChanges();

                success = true;                
            }         
            else
            {
                _eventLogService.LogMessage(user, EventNameType.InvalidTwoFactorCode);
                HandleInvalidSignInAttempt(user);
            }   

            return success;
        }

        public bool ValidateTwoFactorSecurityAnswer(UserSession userSession, User user, string answer)
        {
            var success = false;
            if (userSession != null
                && user != null
                && user.UserSecurityAnswer == answer)
            {
                userSession.IsValid = true;
                userSession.UpdatedById = userSession.UserId;
                userSession.UpdatedDate = DateTime.Now;

                _context.SaveChanges();

                success = true;                
            }            
            else
            {
                _eventLogService.LogMessage(user, EventNameType.InvalidTwoFactorSecurityAnswer);
                HandleInvalidSignInAttempt(user);
            }

            return success;
        }

        public TotpSetup GetTwoFactorTotpRegistrationQRCode(User user)
        {
            var issuer = _settings.TwoFactorAuthIssuer;            
            var applicationSecretKey = _settings.TwoFactorAuthSecretKey;
            var accountIdentity = user.Email;
            var userTwoFactorKey = GenerateUserTwoFactorKey(user);
            var accountSecretKey = applicationSecretKey + userTwoFactorKey;
            var totpSetupGenerator = new TotpSetupGenerator();
            var totpSetup = totpSetupGenerator.Generate(issuer, accountIdentity, accountSecretKey, qrCodePixelsPerModule: 4);

            return totpSetup;
        }

        public bool IsTwoFactorTotpCodeValid(User user, string code)
        {
            var result = false;
            if (int.TryParse(code, out int clientTotp))
            {
                var applicationSecretKey = _settings.TwoFactorAuthSecretKey;
                var userTwoFactorKey = user.TwoFactorKey;
                var accountSecretKey = applicationSecretKey + userTwoFactorKey;
                var totpGenerator = new TotpGenerator();
                var totpValidator = new TotpValidator(totpGenerator);
                var totp = totpGenerator.Generate(accountSecretKey);
                var valid = totpValidator.IsValid(accountSecretKey, clientTotp, timeToleranceInSeconds: 120);

                result = valid;
            }

            return result;
        }

        public bool ValidateTwoFactorTotpRegistrationCode(User user, string code)
        {
            var result = false;
            if (user != null
                && IsTwoFactorTotpCodeValid(user, code))
            {
                result = true;
            }

            return result;
        }

        public bool RegisterTwoFactorSecurityQuestion(
            User user, 
            int twoFactorAuthenticationTypeId, 
            int userSecurityQuestionTypeId, 
            string userSecurityAnswer)
        {
            var result = false;
            if (user != null)
            {
                user.UserSecurityQuestionTypeId = userSecurityQuestionTypeId;
                user.UserSecurityAnswer = StringHelpers.Truncate(userSecurityAnswer, 100);
                user.TwoFactorAuthenticationTypeId = twoFactorAuthenticationTypeId;
                user.UpdatedById = user.UserId;
                user.UpdatedDate = DateTime.Now;

                _context.SaveChanges();

                var twoFactorType = MemoryCacheHelper.TwoFactorAuthenticationTypes.FirstOrDefault(x => x.TwoFactorAuthenticationTypeId == user.TwoFactorAuthenticationTypeId);
                if (twoFactorType != null)
                {
                    if (twoFactorType.Code == TwoFactorTypes.U2f)
                    {
                        ActivateMostRecentU2fDevice(user);
                    }
                }

                result = true;
            }

            return result;
        }

        public string GenerateUserTwoFactorKey(User user)
        {
            if (user != null)
            {
                user.TwoFactorKey = StringHelpers.GenerateShortId().Substring(0, 4);
                user.UpdatedById = user.UserId;
                user.UpdatedDate = DateTime.Now;

                _context.SaveChanges();
            }   

            return user.TwoFactorKey;
        }

        public UserDetails UnregisterTwoFactor(User user)
        {
            if (user != null)
            {
                var existingUserU2fDevices = _context.UserU2fDevices
                    .Where(x => x.UserId == user.UserId)
                    .ToList();

                foreach (var existingUserU2fDevice in existingUserU2fDevices)
                {
                    _context.UserU2fDevices.Remove(existingUserU2fDevice);
                }

                user.TwoFactorAuthenticationTypeId = null;
                user.TwoFactorKey = null;
                user.UpdatedById = user.UserId;
                user.UpdatedDate = DateTime.Now;

                _context.SaveChanges();
            }

            return GetUserDetails(user);
        }

        public UserDetails GetUserDetails(User user)
        {
            return new UserDetails
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                SecurityQuestion = user.UserSecurityQuestionTypeId != null ? MemoryCacheHelper.UserSecurityQuestionTypes.First(x => x.UserSecurityQuestionTypeId == user.UserSecurityQuestionTypeId).Name : null,
                TwoFactorAuthenticationTypeId = user.TwoFactorAuthenticationTypeId,
                UserTimeZoneTypeId = user.UserTimeZoneTypeId
            };
        }

        public UserDetails UpdateUserDetails(User user, string firstName, string lastName, int userTimeZoneTypeId)
        {
            if (user != null)
            {
                user.FirstName = StringHelpers.Truncate(firstName, 100);
                user.LastName = StringHelpers.Truncate(lastName, 100);
                user.UserTimeZoneTypeId = userTimeZoneTypeId;
                user.UpdatedById = user.UserId;
                user.UpdatedDate = DateTime.Now;

                _context.SaveChanges();
            }

            return GetUserDetails(user);
        }

        public UpdateUserPasswordResult UpdateUserPassword(User user, string currentPassword, string newPassword, string repeatNewPassword)
        {
            var result = new UpdateUserPasswordResult
            {
                UpdateUserPasswordResultType = UpdateUserPasswordResultType.Error
            };

            if (user != null)
            {
                if (!string.IsNullOrEmpty(currentPassword)
                    && Authenticate(user.Username, currentPassword, isProfile: true))
                {
                    if (!string.IsNullOrEmpty(newPassword)
                        && newPassword.Length >= 6)
                    {
                        if (!string.IsNullOrEmpty(newPassword)
                            && newPassword == repeatNewPassword)
                        {
                            var hashResultSha512 = GetHashedPasswordResult(newPassword);
                            
                            _eventLogService.LogMessage(user, EventNameType.PasswordUpdate);
                            user.PasswordHash = hashResultSha512.Digest;
                            user.PasswordSalt = hashResultSha512.Salt;
                            user.UpdatedById = user.UserId;
                            user.UpdatedDate = DateTime.Now;

                            _context.SaveChanges();

                            result.UpdateUserPasswordResultType = UpdateUserPasswordResultType.Success;
                        }
                        else
                        {
                            result.UpdateUserPasswordResultType = UpdateUserPasswordResultType.PasswordsDoNotMatch;
                        }
                    }
                    else
                    {
                        result.UpdateUserPasswordResultType = UpdateUserPasswordResultType.InvalidNewPassword;
                    }
                }
                else
                {
                    result.UpdateUserPasswordResultType = UpdateUserPasswordResultType.InvalidCurrentPassword;
                }
            }

            return result;
        }

        public RequestUserEmailUpdateResult RequestUserEmailUpdate(User user, string newEmail)
        {
            var result = new RequestUserEmailUpdateResult
            {
                RequestUserEmailUpdateResultType = RequestUserEmailUpdateResultType.Error
            };

            if (user != null)
            {
                if (!string.IsNullOrEmpty(newEmail))
                {
                    if (_context.Users.Count(x => x.Email == newEmail) == 0)
                    {
                        var email = StringHelpers.Truncate(newEmail, 100);
                        var oneTimePasscode = StringHelpers.GenerateShortId();
                        
                        user.OneTimePasscode = oneTimePasscode;
                        user.NewRequestedEmail = email;
                        user.UpdatedById = user.UserId;
                        user.UpdatedDate = DateTime.Now;

                        _context.SaveChanges();

                        var subject = $"Activate New Email - {_settings.SiteName}";
                        var body = "Hi," + "<br /><br />"
                            + "Click the link below to activate your new email address: " + "<br /><br />"
                            + $"<a href=\"{_settings.SiteUrl}/Home/ActivateEmail?code={oneTimePasscode}\">Activate Email</a>"
                            + "<br /><br />"                   
                            + "Have a great day," + "<br /><br />"
                            + $"{_settings.SiteName}";
                            
                        _communicationService.SendEmail(email, subject, body, isBodyHtml: true);

                        _eventLogService.LogMessage(user, EventNameType.SentNewEmailActivationRequest, $"Email sent to '{email}'");

                        result.RequestUserEmailUpdateResultType = RequestUserEmailUpdateResultType.Success;
                    }
                    else
                    {
                        _eventLogService.LogMessage(user, EventNameType.EmailAlreadyInUse, $"Email '{newEmail}' already in use");

                        result.RequestUserEmailUpdateResultType = RequestUserEmailUpdateResultType.EmailInUse;
                    }
                }
                else
                {
                    result.RequestUserEmailUpdateResultType = RequestUserEmailUpdateResultType.InvalidEmail;
                }
            }

            return result;
        }

        public ActivateEmailResult ActivateEmail(string code)
        {
            var result = new ActivateEmailResult
            {
                ActivateEmailResultType = ActivateEmailResultType.InvalidCode
            };
            
            if (!string.IsNullOrEmpty(code))
            {
                var user = _context.Users.FirstOrDefault(x => x.OneTimePasscode == code);
                if (user != null)
                {
                    user.Username = user.NewRequestedEmail;
                    user.Email = user.NewRequestedEmail;
                    user.OneTimePasscode = null;
                    user.UpdatedById = user.UserId;
                    user.UpdatedDate = DateTime.Now;

                    _context.SaveChanges();

                    result.ActivateEmailResultType = ActivateEmailResultType.Success;

                    _eventLogService.LogMessage(user, EventNameType.NewEmailAssignedToAccount, $"Activated email for '{user.Username}'");
                }
            }

            if (result.ActivateEmailResultType == ActivateEmailResultType.InvalidCode)
            {
                _eventLogService.LogMessage(null, EventNameType.InvalidEmailActivationCode);
            }

            return result;
        }

        public void SendResetPasswordEmailLink(string email)
        {
            var user = _context.Users.FirstOrDefault(x => x.Email == email);
            if (user != null)
            {
                user.OneTimePasscode = StringHelpers.GenerateShortId();
                user.UpdatedById = 0;
                user.UpdatedDate = DateTime.Now;

                _context.SaveChanges();

                var subject = $"Reset Password - {_settings.SiteName}";
                var body = "Hi," + "<br /><br />"
                    + "Click the link below to reset the password for your account: " + "<br /><br />"
                    + $"<a href=\"{_settings.SiteUrl}/Home/ForgotPassword?code={user.OneTimePasscode}\">Reset Password</a>"
                    + "<br /><br />"                   
                    + "Have a great day," + "<br /><br />"
                    + $"{_settings.SiteName}";
                    
                _communicationService.SendEmail(user.Email, subject, body, isBodyHtml: true);

                _eventLogService.LogMessage(user, EventNameType.SentResetPasswordEmail, $"Email sent to '{user.Email}'");
            }            
        }

        public ForgotPasswordResult ValidateForgotPasswordEmailLinkCode(string code)
        {
            var result = new ForgotPasswordResult
            {
                Code = code,
                ForgotPasswordResultType = ForgotPasswordResultType.InvalidCode
            };
            
            if (!string.IsNullOrEmpty(code))
            {
                var user = _context.Users.FirstOrDefault(x => x.OneTimePasscode == code);
                if (user != null)
                {
                    result.Username = user.Username;
                    result.ForgotPasswordResultType = ForgotPasswordResultType.Success;

                    _eventLogService.LogMessage(user, EventNameType.ValidatedForgotPasswordEmailLinkCode, $"Validated code for '{user.Username}'");
                }
            }

            return result;
        }

        public ForgotPasswordResult ValidateAndApplyNewUserPasswordWithRecoveryCode(
            string code, 
            string username,
            string password, 
            string confirmPassword)
        {
            var result = new ForgotPasswordResult
            {
                Code = code
            };

            if (string.IsNullOrEmpty(password)
                || string.IsNullOrEmpty(confirmPassword)
                || password != confirmPassword)
            {
                result.ForgotPasswordResultType = ForgotPasswordResultType.PasswordsDoNotMatch;

                _eventLogService.LogMessage(null, EventNameType.PasswordsDoNotMatch, $"Username '{username}' attempted a password update where the passwords did not match");
            }
            else
            {
                var user = _context.Users.FirstOrDefault(x => x.OneTimePasscode == code);
                if (user != null)
                {
                    var hashResultSha512 = GetHashedPasswordResult(password);                        
                    user.PasswordHash = hashResultSha512.Digest;
                    user.PasswordSalt = hashResultSha512.Salt;

                    user.IsActivated = true;
                    user.IsLocked = false;
                    user.OneTimePasscode = null;
                    user.UpdatedById = user.UserId;
                    user.UpdatedDate = DateTime.Now;

                    _context.SaveChanges();

                    result.ForgotPasswordResultType = ForgotPasswordResultType.Success;

                    _eventLogService.LogMessage(user, EventNameType.PasswordUpdate, $"Username '{username}' performed a password update via a recovery code");
                }
                else
                {
                    result.ForgotPasswordResultType = ForgotPasswordResultType.InvalidCode;

                    _eventLogService.LogMessage(null, EventNameType.InvalidForgotPasswordCode, $"Username '{username}' attempted a password update using an invalid recovery code");
                }
            }

            return result;
        }

        public SignUpResult SignUp(string firstName, string lastName, string email, string password)
        {
            var result = new SignUpResult
            {
                FirstName = firstName,
                LastName = lastName,
                Email = email,                
                SignUpResultType = SignUpResultType.Error
            };

            if (string.IsNullOrEmpty(firstName)
                || string.IsNullOrEmpty(lastName)
                || string.IsNullOrEmpty(email)
                || string.IsNullOrEmpty(password))
            {
                result.SignUpResultType = SignUpResultType.Error;
            }
            else if (!string.IsNullOrEmpty(password)
                && password.Length < 6)
            {
                result.SignUpResultType = SignUpResultType.InvalidPassword;
            }
            else
            {
                var user = _context.Users.FirstOrDefault(x => x.Email == email);
                if (user != null)
                {
                    result.SignUpResultType = SignUpResultType.EmailInUse;

                    _eventLogService.LogMessage(null, EventNameType.EmailAlreadyInUse, $"Email '{email}' already in use");
                }
                else
                {
                    var oneTimePasscode = StringHelpers.GenerateShortId();

                    var hashResultSha512 = GetHashedPasswordResult(password);

                    _context.Users.Add(new User 
                        {
                            Username = StringHelpers.Truncate(email, 100),
                            PasswordHash = hashResultSha512.Digest,
                            PasswordSalt = hashResultSha512.Salt,
                            FirstName = StringHelpers.Truncate(firstName, 100),
                            LastName = StringHelpers.Truncate(lastName, 100),
                            Email = StringHelpers.Truncate(email, 100),
                            Claims = "Read",
                            OneTimePasscode = oneTimePasscode,
                            UserTimeZoneTypeId = MemoryCacheHelper.UserTimeZoneTypes.First(x => x.Code == TimeZoneTypes.GMT__05).UserTimeZoneTypeId,
                            CreatedById = 0,
                            CreatedDate = DateTime.Now
                        }
                    );

                    _context.SaveChanges();

                    _eventLogService.LogMessage(null, EventNameType.NewAccountCreated, $"Username '{email}' created");

                    var subject = $"Activate Account - {_settings.SiteName}";
                    var body = "Hi," + "<br /><br />"
                        + "Click the link below to activate your account: " + "<br /><br />"
                        + $"<a href=\"{_settings.SiteUrl}/Home/ActivateAccount?code={oneTimePasscode}\">Activate Account</a>"
                        + "<br /><br />"                   
                        + "Have a great day," + "<br /><br />"
                        + $"{_settings.SiteName}";
                        
                    _communicationService.SendEmail(email, subject, body, isBodyHtml: true);

                    _eventLogService.LogMessage(null, EventNameType.SentNewAccountActivationEmail, $"Email sent to '{email}'");

                    result.SignUpResultType = SignUpResultType.Success;
                }
            }

            return result;
        }

        public ActivateAccountResult ActivateAccount(string code)
        {
            var result = new ActivateAccountResult
            {
                ActivateAccountResultType = ActivateAccountResultType.InvalidCode
            };
            
            if (!string.IsNullOrEmpty(code))
            {
                var user = _context.Users.FirstOrDefault(x => x.OneTimePasscode == code);
                if (user != null)
                {
                    user.IsActivated = true;
                    user.OneTimePasscode = null;
                    user.UpdatedById = user.UserId;
                    user.UpdatedDate = DateTime.Now;

                    _context.SaveChanges();

                    result.ActivateAccountResultType = ActivateAccountResultType.Success;

                    _eventLogService.LogMessage(user, EventNameType.NewAccountActivated, $"Activated account for '{user.Username}'");
                }
            }

            if (result.ActivateAccountResultType == ActivateAccountResultType.InvalidCode)
            {
                _eventLogService.LogMessage(null, EventNameType.InvalidAccountActivationCode);
            }

            return result;
        }

        public UnlockAccountResult UnlockAccount(string code)
        {
            var result = new UnlockAccountResult
            {
                UnlockAccountResultType = UnlockAccountResultType.InvalidCode
            };
            
            if (!string.IsNullOrEmpty(code))
            {
                var user = _context.Users.FirstOrDefault(x => x.OneTimePasscode == code);
                if (user != null)
                {
                    user.IsLocked = false;
                    user.IncorrectAuthCount = 0;
                    user.OneTimePasscode = null;
                    user.UpdatedById = user.UserId;
                    user.UpdatedDate = DateTime.Now;

                    _context.SaveChanges();

                    result.UnlockAccountResultType = UnlockAccountResultType.Success;

                    _eventLogService.LogMessage(user, EventNameType.AccountUnlocked, $"Unlocked account for '{user.Username}'");
                }
            }

            if (result.UnlockAccountResultType == UnlockAccountResultType.InvalidCode)
            {
                _eventLogService.LogMessage(null, EventNameType.InvalidAccountUnlockCode);
            }

            return result;
        }

        public ActivateAlertContactResult ActivateAlertContact(string code, int? uptimeAlertContactId = null)
        {
            var result = new ActivateAlertContactResult
            {
                ActivateAlertContactResultType = ActivateAlertContactResultType.InvalidCode
            };
            
            if (!string.IsNullOrEmpty(code))
            {
                var uptimeAlertContact = _context.UptimeAlertContacts
                    .FirstOrDefault(x => x.ActivationCode == code
                        && (uptimeAlertContactId == null || x.UptimeAlertContactId == uptimeAlertContactId));
                if (uptimeAlertContact != null)
                {
                    var user = _context.Users.First(x => x.UserId == uptimeAlertContact.UserId);
                    uptimeAlertContact.IsActivated = true;
                    uptimeAlertContact.ActivationCode = null;
                    uptimeAlertContact.UptimeAlertContactStatusTypeId = MemoryCacheHelper.UptimeAlertContactStatusTypes.First(x => x.Code == ContactStatusTypes.Active).UptimeAlertContactStatusTypeId;
                    uptimeAlertContact.UpdatedById = user.UserId;
                    uptimeAlertContact.UpdatedDate = DateTime.Now;

                    _context.SaveChanges();

                    result.ActivateAlertContactResultType = ActivateAlertContactResultType.Success;

                    _eventLogService.LogMessage(user, EventNameType.NewAlertContactActivated, $"Activated alert contact '{uptimeAlertContact.Name}' for '{user.Username}'");
                }
            }

            if (result.ActivateAlertContactResultType == ActivateAlertContactResultType.InvalidCode)
            {
                _eventLogService.LogMessage(null, EventNameType.InvalidAlertContactActivationCode);
            }

            return result;
        }

        public User GetApiKeyUser(string apiKey)
        {
            User user = null;
            if (!string.IsNullOrEmpty(apiKey))
            {
                user = (from umak in _context.UptimeMonitorApiKeys
                        join u in _context.Users on umak.UserId equals u.UserId
                        where umak.ApiKey == apiKey && u.IsActivated && !u.IsLocked
                        select new User
                        {
                            UserId = u.UserId,
                            Username = u.Username,
                            FirstName = u.FirstName,
                            LastName = u.LastName,
                            Email = u.Email,
                            Phone = u.Phone,
                            Claims = u.Claims,
                            ApiKey = umak.ApiKey,
                            UptimeMonitorApiKeyTypeId = umak.UptimeMonitorApiKeyTypeId,
                            UptimeMonitorId = umak.UptimeMonitorId
                        }).FirstOrDefault();  

                if (user != null
                    && user.UptimeMonitorApiKeyTypeId != null)
                {
                    user.Claims = MemoryCacheHelper.UptimeMonitorApiKeyTypes.First(x => x.UptimeMonitorApiKeyTypeId == user.UptimeMonitorApiKeyTypeId).Code;                    
                }   
            }
            
            return user;
        }

        public U2fChallenge CreateU2fRegistrationChallenge(User user)
        {
            StartedRegistration startedRegistration = U2F.Core.Crypto.U2F.StartRegistration(_settings.SiteUrl);

            CleanupExistingU2fAuthenticationRequests(user);

            _context.UserU2fAuthenticationRequests.Add(new UserU2fAuthenticationRequest
                {
                    UserId = user.UserId,
                    Challenge = startedRegistration.Challenge,
                    AppId = startedRegistration.AppId,
                    Version = startedRegistration.Version,
                    CreatedById = user.UserId,
                    CreatedDate = DateTime.Now
                }
            );

            _context.SaveChanges();

            return new U2fChallenge
            {
                Challenge = startedRegistration.Challenge,
                AppId = startedRegistration.AppId,
                Version = startedRegistration.Version
            };
        }

        private void CleanupExistingU2fAuthenticationRequests(User user)
        {
            var existingAuthenticationRequests = _context.UserU2fAuthenticationRequests
                .Where(x => x.UserId == user.UserId)
                .ToList();

            foreach (var existingAuthenticationRequest in existingAuthenticationRequests)
            {
                _context.UserU2fAuthenticationRequests.Remove(existingAuthenticationRequest);
            }

            _context.SaveChanges();
        }

        public bool CompleteU2fRegistration(User user, string deviceName, string deviceResponse)
        {
            var result = false;

            if (!string.IsNullOrWhiteSpace(deviceResponse))
            {
                var existingAuthenticationRequest = _context.UserU2fAuthenticationRequests
                    .Where(x => x.UserId == user.UserId)
                    .FirstOrDefault();

                if (existingAuthenticationRequest != null)
                {
                    RegisterResponse registerResponse = RegisterResponse.FromJson<RegisterResponse>(deviceResponse);
                    
                    StartedRegistration startedRegistration = new StartedRegistration(
                        existingAuthenticationRequest.Challenge, 
                        existingAuthenticationRequest.AppId
                    );
                    DeviceRegistration registration = U2F.Core.Crypto.U2F.FinishRegistration(
                        startedRegistration, 
                        registerResponse);

                    var isU2fFactorActive = false;
                    var twoFactorType = MemoryCacheHelper.TwoFactorAuthenticationTypes.FirstOrDefault(x => x.TwoFactorAuthenticationTypeId == user.TwoFactorAuthenticationTypeId);
                    if (twoFactorType != null)
                    {
                        isU2fFactorActive = twoFactorType.Code == TwoFactorTypes.U2f;
                    }

                    _context.UserU2fDevices.Add(new UserU2fDevice
                        {
                            UserId = user.UserId,
                            DeviceName = deviceName,
                            KeyHandle = registration.KeyHandle,
                            PublicKey = registration.PublicKey,
                            AttestationCert = registration.AttestationCert,
                            Counter = Convert.ToInt32(registration.Counter),
                            IsActive = isU2fFactorActive,
                            CreatedById = user.UserId,
                            CreatedDate = DateTime.Now
                        }
                    );

                    _context.SaveChanges();

                    result = true;
                }                
            }

            CleanupExistingU2fAuthenticationRequests(user);

            return result;
        }

        public TwoFactorChallenge GetUserTwoFactorChallege(User user)
        {
            var twoFactorType = MemoryCacheHelper.TwoFactorAuthenticationTypes.FirstOrDefault(x => x.TwoFactorAuthenticationTypeId == user.TwoFactorAuthenticationTypeId);
            if (twoFactorType != null)
            {
                if (twoFactorType.Code == TwoFactorTypes.Totp)
                {
                    return new TwoFactorChallenge
                    {
                        TwoFactorTypeCode = TwoFactorTypes.Totp
                    };
                }
                else 
                {
                    return GenerateU2fChallenges(user);
                }
            }

            return new TwoFactorChallenge();
        }

        public TwoFactorChallenge GenerateU2fChallenges(User user)
        {
            CleanupExistingU2fAuthenticationRequests(user);

            var twoFactorChallenge = new TwoFactorChallenge
            {
                TwoFactorTypeCode = TwoFactorTypes.U2f
            };

            var u2fChallenges = new List<U2fChallenge>();

            var existingUserDevices = _context.UserU2fDevices
                .Where(x => x.UserId == user.UserId && !x.IsCompromised && x.IsActive)
                .ToList();

            if (existingUserDevices.Count() > 0)
            {
                string challenge = U2F.Core.Crypto.U2F.GenerateChallenge();

                foreach (var existingUserDevice in existingUserDevices)
                {
                    u2fChallenges.Add(new U2fChallenge
                        {
                            KeyHandle = existingUserDevice.KeyHandle.ByteArrayToBase64String(),
                            Challenge = challenge,
                            AppId = _settings.SiteUrl,                            
                            Version = U2F.Core.Crypto.U2F.U2FVersion                            
                        }
                    );

                    _context.UserU2fAuthenticationRequests.Add(new UserU2fAuthenticationRequest
                        {
                            UserId = user.UserId,
                            KeyHandle = existingUserDevice.KeyHandle.ByteArrayToBase64String(),
                            Challenge = challenge,
                            AppId = _settings.SiteUrl,
                            Version = U2F.Core.Crypto.U2F.U2FVersion,
                            CreatedById = user.UserId,
                            CreatedDate = DateTime.Now
                        }
                    );
                }

                _context.SaveChanges();

                twoFactorChallenge.AppId = u2fChallenges.First().AppId;
                twoFactorChallenge.Challenge = u2fChallenges.First().Challenge;
                twoFactorChallenge.Challenges = JsonConvert.SerializeObject(u2fChallenges);
            }

            return twoFactorChallenge;
        }

        public bool ValidateU2fChallenge(UserSession userSession, User user, string deviceResponse)
        {
            var success = false;

            if (userSession != null
                && user != null)
            {
                var existingAuthenticationRequest = _context.UserU2fAuthenticationRequests
                    .Where(x => x.UserId == user.UserId)
                    .FirstOrDefault();

                if (existingAuthenticationRequest != null)
                {
                    AuthenticateResponse authenticateResponse = null;
                    try
                    {
                        authenticateResponse = AuthenticateResponse
                            .FromJson<AuthenticateResponse>(deviceResponse);
                    }
                    catch (ArgumentException)
                    {
                        authenticateResponse = null;
                    }

                    if (authenticateResponse != null)
                    {
                        var userDevice = _context.UserU2fDevices
                            .FirstOrDefault(x => x.KeyHandle.SequenceEqual(authenticateResponse.KeyHandle.Base64StringToByteArray())
                                && x.UserId == user.UserId
                                && !x.IsCompromised
                                && x.IsActive);

                        if (userDevice != null)
                        {
                            var userAuthenticationRequest = _context.UserU2fAuthenticationRequests
                                .FirstOrDefault(x => x.KeyHandle.Equals(authenticateResponse.KeyHandle)
                                    && x.UserId == user.UserId);

                            if (userAuthenticationRequest != null)
                            {
                                DeviceRegistration registration = new DeviceRegistration(
                                    userDevice.KeyHandle,
                                    userDevice.PublicKey, 
                                    userDevice.AttestationCert, 
                                    Convert.ToUInt32(userDevice.Counter)
                                );

                                StartedAuthentication authentication = new StartedAuthentication(
                                    userAuthenticationRequest.Challenge, 
                                    userAuthenticationRequest.AppId, 
                                    userAuthenticationRequest.KeyHandle);

                                try
                                {
                                    U2F.Core.Crypto.U2F.FinishAuthentication(authentication, authenticateResponse, registration);
                                }
                                catch (U2fException)
                                {
                                    userDevice.IsCompromised = true;
                                }

                                userDevice.Counter = Convert.ToInt32(registration.Counter);
                                userDevice.UpdatedById = user.UserId;
                                userDevice.UpdatedDate = DateTime.Now;

                                _context.SaveChanges();

                                success = true;
                            }
                        }     
                    }               
                }

                if (success)
                {
                    userSession.IsValid = true;
                    userSession.UpdatedById = userSession.UserId;
                    userSession.UpdatedDate = DateTime.Now;

                    _context.SaveChanges();
                }           
            }         
            else
            {
                _eventLogService.LogMessage(user, EventNameType.InvalidTwoFactorU2fDevice);
                HandleInvalidSignInAttempt(user);
            }

            CleanupExistingU2fAuthenticationRequests(user);

            return success;
        }

        private void ActivateMostRecentU2fDevice(User user)
        {
            var userDevices = _context.UserU2fDevices
                .Where(x => x.UserId == user.UserId && !x.IsCompromised)
                .OrderByDescending(x => x.CreatedDate)
                .ToList();

            var isMostRecent = true;
            foreach (var userDevice in userDevices)
            {
                if (isMostRecent)
                {
                    userDevice.IsActive = true;
                    userDevice.UpdatedById = user.UserId;
                    userDevice.UpdatedDate = DateTime.Now;

                    isMostRecent = false;
                }
                else
                {
                    _context.UserU2fDevices.Remove(userDevice);
                }                                
            }

            _context.SaveChanges();
        }

        public List<UserU2fDeviceSummary> GetUserU2fDevices(User user)
        {
            var result = new List<UserU2fDeviceSummary>();

            var devices = _context.UserU2fDevices
                .Where(x => x.UserId == user.UserId && !x.IsCompromised && x.IsActive)
                .ToList();

            foreach (var device in devices)
            {
                result.Add(new UserU2fDeviceSummary()
                    {
                        UserU2fDeviceId = device.UserU2fDeviceId,
                        DeviceName = device.DeviceName
                    }
                );
            }

            return result;
        }

        public List<UserU2fDeviceSummary> RemoveUserU2fDevice(User user, int userU2fDeviceId)
        {
            var userU2fDevice = _context.UserU2fDevices
                .Where(x => x.UserId == user.UserId && x.UserU2fDeviceId == userU2fDeviceId)
                .FirstOrDefault();

            if (userU2fDevice != null)
            {
                _context.UserU2fDevices.Remove(userU2fDevice);

                _context.SaveChanges();
            }

            var userU2fDeviceCount = _context.UserU2fDevices
                .Where(x => x.UserId == user.UserId && !x.IsCompromised)
                .Count();

            if (userU2fDeviceCount == 0)
            {
                user.TwoFactorAuthenticationTypeId = null;
                user.TwoFactorKey = null;
                user.UpdatedById = user.UserId;
                user.UpdatedDate = DateTime.Now;

                _context.SaveChanges();
            }

            return GetUserU2fDevices(user);
        }

        private void HandleInvalidSignInAttempt(User user)
        {
            user.IncorrectAuthCount = user.IncorrectAuthCount != null 
                ? user.IncorrectAuthCount + 1 : 1;

            user.UpdatedById = user.UserId;
            user.UpdatedDate = DateTime.Now;

            _context.SaveChanges();
            
            if (user.IncorrectAuthCount >= _settings.MaxIncorrectAuthCount)
            {
                user.IsLocked = true;

                var oneTimePasscode = StringHelpers.GenerateShortId();                        
                user.OneTimePasscode = oneTimePasscode;

                _context.SaveChanges();

                SendLockedAccountMessage(user);
            } 
        }

        private void SendLockedAccountMessage(User user)
        {
            var subject = $"Locked Account - {_settings.SiteName}";
            var body = "Hi," + "<br /><br />"
                + "Click the link below to unlock your account: " + "<br /><br />"
                + $"<a href=\"{_settings.SiteUrl}/Home/UnlockAccount?code={user.OneTimePasscode}\">Unlock Account</a>"
                + "<br /><br />"                   
                + "Have a great day," + "<br /><br />"
                + $"{_settings.SiteName}";
                
            _communicationService.SendEmail(user.Email, subject, body, isBodyHtml: true);

            _eventLogService.LogMessage(null, EventNameType.SentLockedAccountEmail, $"Email sent to '{user.Email}'");
        }
    }
}
