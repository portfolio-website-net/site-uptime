﻿using SiteUptime.Models;
using SiteUptime.Helpers.Authentication;
using AspNetCore.Totp.Interface.Models;
using SiteUptime.Helpers.ObjectHelpers;
using System.Collections.Generic;

namespace SiteUptime.Services.Interfaces
{
    public interface IAuthenticationService
    {
        void PopulateUsersIfEmptyDatabase();      

        HashWithSaltResult GetHashedPasswordResult(string password);

        bool Authenticate(string username, string password, bool isProfile = false);          

        string CreateSession(string username);

        bool IsValidSession(string sessionKey);

        bool EndSession(string sessionKey);

        bool RenewSession(string sessionKey);

        bool RenewSession(UserSession userSession);

        UserSession GetSession(string sessionKey);

        User GetCurrentUser(string sessionKey);

        User GetCurrentUser(string sessionKey, out UserSession userSession);

        bool ValidateSessionTwoFactorCode(UserSession userSession, User user, string code);

        TotpSetup GetTwoFactorTotpRegistrationQRCode(User user);

        bool IsTwoFactorTotpCodeValid(User user, string code);

        bool ValidateTwoFactorTotpRegistrationCode(User user, string code);

        bool ValidateTwoFactorSecurityAnswer(UserSession userSession, User user, string answer);

        bool RegisterTwoFactorSecurityQuestion(
            User user, 
            int twoFactorAuthenticationTypeId, 
            int userSecurityQuestionTypeId, 
            string userSecurityAnswer);

        string GenerateUserTwoFactorKey(User user);

        UserDetails UnregisterTwoFactor(User user);

        UserDetails GetUserDetails(User user);

        UserDetails UpdateUserDetails(User user, string firstName, string lastName, int userTimeZoneTypeId);

        UpdateUserPasswordResult UpdateUserPassword(User user, string currentPassword, string newPassword, string repeatNewPassword);

        RequestUserEmailUpdateResult RequestUserEmailUpdate(User user, string newEmail);

        ActivateEmailResult ActivateEmail(string code);

        void SendResetPasswordEmailLink(string email);

        ForgotPasswordResult ValidateForgotPasswordEmailLinkCode(string code);

        ForgotPasswordResult ValidateAndApplyNewUserPasswordWithRecoveryCode(
            string code, 
            string username,
            string password, 
            string confirmPassword);

        SignUpResult SignUp(string firstName, string lastName, string email, string password);

        ActivateAccountResult ActivateAccount(string code);

        UnlockAccountResult UnlockAccount(string code);

        ActivateAlertContactResult ActivateAlertContact(string code, int? uptimeAlertContactId = null);

        User GetApiKeyUser(string apiKey);

        U2fChallenge CreateU2fRegistrationChallenge(User user);

        bool CompleteU2fRegistration(User user, string deviceName, string deviceResponse);

        TwoFactorChallenge GetUserTwoFactorChallege(User user);

        TwoFactorChallenge GenerateU2fChallenges(User user);

        bool ValidateU2fChallenge(UserSession userSession, User user, string deviceResponse);

        List<UserU2fDeviceSummary> GetUserU2fDevices(User user);

        List<UserU2fDeviceSummary> RemoveUserU2fDevice(User user, int userU2fDeviceId);
    }
}
