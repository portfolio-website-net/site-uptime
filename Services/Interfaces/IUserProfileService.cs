﻿using SiteUptime.Models;
using SiteUptime.Helpers.Authentication;

namespace SiteUptime.Services.Interfaces
{
    public interface IUserProfileService
    {
        UserProfileUpdateResult UpdateUserProfile(ProfileFields profileFields);
    }
}
