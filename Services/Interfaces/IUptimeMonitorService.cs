﻿using System.Collections.Generic;
using SiteUptime.Models;
using SiteUptime.Helpers.ObjectHelpers;
using SiteUptime.Helpers;
using SiteUptime.Helpers.Results;
using SiteUptime.Helpers.Types;

namespace SiteUptime.Services.Interfaces
{
    public interface IUptimeMonitorService
    {
        List<UptimeMonitorSummary> GetUptimeMonitors();   

        List<UptimeMonitor> GetUptimeMonitorsWithInternalDetails();

        UptimeMonitor GetUptimeMonitor(int uptimeMonitorId);

        List<UptimeAlertContactSummary> GetUptimeAlertContacts();

        List<UptimeAlertContact> GetUptimeAlertContactsWithInternalDetails();

        UptimeAlertContact GetUptimeAlertContact(int uptimeAlertContactId);

        List<UptimeMaintenanceWindowSummary> GetUptimeMaintenanceWindows();

        List<UptimeMaintenanceWindow> GetUptimeMaintenanceWindowsWithInternalDetails();

        UptimeMaintenanceWindow GetUptimeMaintenanceWindow(int uptimeMaintenanceWindowId);

        List<UptimePublicStatusPageSummary> GetUptimePublicStatusPages();   

        List<UptimePublicStatusPage> GetUptimePublicStatusPagesWithInternalDetails();

        UptimePublicStatusPage GetUptimePublicStatusPage(int uptimePublicStatusPageId);

        List<UptimeMonitorApiKeySummary> GetUptimeMonitorApiKeys();

        UptimeMonitorApiKey GetUptimeMonitorApiKey(int uptimeMonitorApiKeyId);

        List<UptimeMonitorType> GetUptimeMonitorTypes();

        List<UptimeStatusType> GetUptimeStatusTypes();

        List<UptimeAlertContactType> GetUptimeAlertContactTypes();

        List<UptimeAlertContactStatusType> GetUptimeAlertContactStatusTypes();

        List<UptimeMobileProviderType> GetUptimeMobileProviderTypes();

        List<UptimeNotificationEventType> GetUptimeNotificationEventTypes();

        List<UptimeMonitorApiKeyType> GetUptimeMonitorApiKeyTypes();

        List<UptimeMaintenanceWindowType> GetUptimeMaintenanceWindowTypes();

        List<UptimeMonitorSortType> GetUptimeMonitorSortTypes();

        List<UserTimeZoneType> GetUserTimeZoneTypes();

        List<UserSecurityQuestionType> GetUserSecurityQuestionTypes();

        List<TwoFactorAuthenticationType> GetTwoFactorAuthenticationTypes();

        List<UptimeMonitorSummary> UpdateUptimeMonitor(UptimeMonitor uptimeMonitor, bool isApiCall = false);

        List<UptimeMonitorSummary> RemoveUptimeMonitor(UptimeMonitor uptimeMonitor, bool isApiCall = false);

        List<UptimeMonitorSummary> ResetUptimeMonitor(UptimeMonitor uptimeMonitor, bool isApiCall = false);

        bool UptimeMonitorSetStatus(int uptimeMonitorId, bool isStarted);

        List<UptimeAlertContactSummary> UpdateUptimeAlertContact(
            UptimeAlertContact uptimeAlertContact,
            bool isApiCall = false);

        List<UptimeAlertContactSummary> RemoveUptimeAlertContact(
            UptimeAlertContact uptimeAlertContact, 
            bool isApiCall = false);

        List<UptimeMaintenanceWindowSummary> UpdateUptimeMaintenanceWindow(
            UptimeMaintenanceWindow uptimeMaintenanceWindow,
            bool isApiCall = false);

        List<UptimeMaintenanceWindowSummary> RemoveUptimeMaintenanceWindow(
            UptimeMaintenanceWindow uptimeMaintenanceWindow,
            bool isApiCall = false);

        List<UptimePublicStatusPageSummary> UpdateUptimePublicStatusPage(
            UptimePublicStatusPage uptimePublicStatusPage,
            bool isApiCall = false);

        List<UptimePublicStatusPageSummary> RemoveUptimePublicStatusPage(
            UptimePublicStatusPage uptimePublicStatusPage,
            bool isApiCall = false);

        bool UptimePublicStatusPageSetStatus(int uptimePublicStatusPageId, bool isActive);

        string GetPublicStatusPageLogoKey(int uptimePublicStatusPageId);

        PublicStatusPageResult GetPublicStatusPageResult(string statusPageKey);

        bool IsUptimeMonitorForPublicStatusPage(string statusPageKey, int uptimeMonitorId);

        List<UptimeMonitorEventSummary> GetLatestEvents(int? uptimeMonitorId = null, bool ignoreUserVerification = false);

        CurrentUptimeMonitorDetails GetCurrentUptimeMonitorDetails(int uptimeMonitorId, bool ignoreUserVerification = false);

        List<UptimeMonitorChartData> GetUptimeMonitorChartData(int uptimeMonitorId, bool ignoreUserVerification = false);

        OverallMonitorStats GetOverallMonitorStats();

        OverallMonitorStats GetPublicStatusPageOverallMonitorStats(string statusPageKey);

        List<CurrentUptimeMonitorDetails> GetPublicStatusPageCurrentUptimeMonitorDetails(string statusPageKey, bool ignoreUserVerification = false);

        List<UptimeMonitorApiKeySummary> UpdateUptimeMonitorApiKey(UptimeMonitorApiKey uptimeMonitorApiKey);
        
        List<UptimeMonitorApiKeySummary> RemoveUptimeMonitorApiKey(UptimeMonitorApiKey uptimeMonitorApiKey);
        
        List<UptimeMonitorApiKeySummary> CreateApiKey(string code);

        List<UptimeMonitorApiKeySummary> RemoveApiKey(string code);

        bool NotifyHeartbeatMonitor(int uptimeMonitorId, int? responseTime);
    }
}
