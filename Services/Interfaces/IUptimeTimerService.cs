﻿using SiteUptime.Models;

namespace SiteUptime.Services.Interfaces
{
    public interface IUptimeTimerService
    {
        void Start();
    }
}
