﻿using System.Threading.Tasks;

namespace SiteUptime.Services.Interfaces
{
    public interface ISyndicationFeedService
    {
        Task<string> GenerateFeed(string syndicationFeedKey);
    }
}
