﻿using SiteUptime.Models;
using SiteUptime.Helpers.Authentication;

namespace SiteUptime.Services.Interfaces
{
    public interface IEventLogService
    {
        void LogMessage(User user, EventNameType eventNameType, string message = null);
    }
}
