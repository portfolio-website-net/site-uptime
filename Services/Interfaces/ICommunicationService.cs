﻿using SiteUptime.Models;

namespace SiteUptime.Services.Interfaces
{
    public interface ICommunicationService
    {
        void SendEmailToSms(UptimeAlertContact uptimeAlertContact, string text);

        void SendEmail(string emailAddress, string subject, string body, bool isBodyHtml);
    }
}
