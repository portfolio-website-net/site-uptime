﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Extensions.Options;
using SiteUptime.Services.Interfaces;
using SiteUptime.Models;
using SiteUptime.Helpers;
using SiteUptime.Helpers.Results;
using SiteUptime.Helpers.Types;
using SiteUptime.Helpers.CacheHelpers;
using System.Threading;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using Microsoft.Extensions.Caching.Memory;
using System.Net.NetworkInformation;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Web;

namespace SiteUptime.Services
{
    public class UptimeTimerService : IUptimeTimerService
    {
        private DbContextOptions<Context> _dbContextOptions;
        private static Timer _timer;
        private static List<int> _pollingIntervals;
        private static int _defaultTimeout;
        private static int _defaultUptimeCapacity;
        private static string _uptimeMonitorLocation;
        private static string _uptimeMonitorIpAddress;
        private static bool _requireDownStatusVerification;
        private static string _emailHost;
        private static int _emailPort;
        private static string _emailUsername;
        private static string _emailPassword;
        private static string _emailFromAddress;
        private static string _siteUrl;
        private static string _siteName;
        private static string _defaultUserAgent;
        private static int _maxRedirectsToFollow;
        private static int _keywordSearchContentMaxLength;

        // Reference URL: https://aspnetmonsters.com/2016/08/2016-08-27-httpclientwrong/
        private static HttpClient _httpClient = new HttpClient();

        private static IOptions<Settings> _IOptionsSettings;
        private static Settings _settings;

        public UptimeTimerService(DbContextOptions<Context> dbContextOptions)
        {
            _dbContextOptions = dbContextOptions;

            Start();
        }

        public void Start()
        {
            if (_timer == null)
            {
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                var configuration = builder.Build();                

                var pollingIntervalsStr = configuration.GetValue<string>("Settings:PollingIntervals");
                _pollingIntervals = pollingIntervalsStr.Split(',').Select(x => int.Parse(x)).ToList();          
                _defaultTimeout = configuration.GetValue<int>("Settings:DefaultTimeout");
                _defaultUptimeCapacity = configuration.GetValue<int>("Settings:DefaultUptimeCapacity");
                _uptimeMonitorLocation = configuration.GetValue<string>("Settings:UptimeMonitorLocation");
                _uptimeMonitorIpAddress = configuration.GetValue<string>("Settings:UptimeMonitorIpAddress");
                _requireDownStatusVerification = configuration.GetValue<bool>("Settings:RequireDownStatusVerification");
                _emailHost = configuration.GetValue<string>("Settings:EmailHost");
                _emailPort = configuration.GetValue<int>("Settings:EmailPort");
                _emailUsername = configuration.GetValue<string>("Settings:EmailUsername");
                _emailPassword = configuration.GetValue<string>("Settings:EmailPassword");
                _emailFromAddress = configuration.GetValue<string>("Settings:EmailFromAddress");    
                _siteUrl = configuration.GetValue<string>("Settings:SiteUrl");   
                _siteName = configuration.GetValue<string>("Settings:SiteName");   
                _defaultUserAgent = configuration.GetValue<string>("Settings:DefaultUserAgent");
                _maxRedirectsToFollow = configuration.GetValue<int>("Settings:MaxRedirectsToFollow");
                _keywordSearchContentMaxLength = configuration.GetValue<int>("Settings:KeywordSearchContentMaxLength");
                
                _httpClient.DefaultRequestHeaders.UserAgent.TryParseAdd(_defaultUserAgent);
                _httpClient.Timeout = TimeSpan.FromSeconds(_defaultTimeout);

                _settings = new Settings
                {
                    DefaultTimeout = _defaultTimeout,
                    DefaultUptimeCapacity = _defaultUptimeCapacity,
                    UptimeMonitorLocation = _uptimeMonitorLocation,
                    UptimeMonitorIpAddress = _uptimeMonitorIpAddress,
                    RequireDownStatusVerification = _requireDownStatusVerification,
                    EmailHost = _emailHost,
                    EmailPort = _emailPort,
                    EmailUsername = _emailUsername,
                    EmailPassword = _emailPassword,
                    EmailFromAddress = _emailFromAddress,
                    SiteUrl = _siteUrl,
                    SiteName = _siteName,
                    DefaultUserAgent = _defaultUserAgent,
                    MaxRedirectsToFollow = _maxRedirectsToFollow,
                    KeywordSearchContentMaxLength = _keywordSearchContentMaxLength
                };    

                _IOptionsSettings = Options.Create(_settings);        

                var timer = new Timer(TimerElapsed, new AutoResetEvent(false), 5000, 5000);
                _timer = timer;
            }
        }

        private void TimerElapsed(Object stateInfo)
        {
            try
            {
                using (var context = new Context(_dbContextOptions))
                {
                    var uptimeMonitors = context.UptimeMonitors
                        .FromSql("CALL QueuePendingMonitors({0}, {1}, {2}, {3}, {4});",   
                        _defaultTimeout,                 
                        _defaultUptimeCapacity,                    
                        _uptimeMonitorLocation, 
                        _uptimeMonitorIpAddress,
                        _requireDownStatusVerification).ToList();

                    Console.WriteLine($"uptimeMonitors: {uptimeMonitors.Count()}");

                    Parallel.ForEach(
                        uptimeMonitors, 
                        uptimeMonitor =>
                        {
                            if (MemoryCacheHelper.UptimeMonitorTypes.First(x => x.UptimeMonitorTypeId == uptimeMonitor.UptimeMonitorTypeId).Code == MonitorTypes.Http)
                            {
                                ProcessHttp(uptimeMonitor);
                            }
                            else if (MemoryCacheHelper.UptimeMonitorTypes.First(x => x.UptimeMonitorTypeId == uptimeMonitor.UptimeMonitorTypeId).Code == MonitorTypes.Keyword)
                            {
                                ProcessHttp(uptimeMonitor, checkForKeyword: true);
                            }
                            else if (MemoryCacheHelper.UptimeMonitorTypes.First(x => x.UptimeMonitorTypeId == uptimeMonitor.UptimeMonitorTypeId).Code == MonitorTypes.Ping)
                            {
                                ProcessPing(uptimeMonitor);
                            }
                            else if (MemoryCacheHelper.UptimeMonitorTypes.First(x => x.UptimeMonitorTypeId == uptimeMonitor.UptimeMonitorTypeId).Code == MonitorTypes.Port)
                            {
                                ProcessPort(uptimeMonitor);
                            }
                            else if (MemoryCacheHelper.UptimeMonitorTypes.First(x => x.UptimeMonitorTypeId == uptimeMonitor.UptimeMonitorTypeId).Code == MonitorTypes.Heartbeat)
                            {
                                ProcessHeartbeat(uptimeMonitor);
                            }
                        }
                    );    
                }    
            }
            catch (Exception exception)
            {
                ErrorLogging.Log(null, exception);
            }    
        }

        private void ProcessHttp(UptimeMonitor uptimeMonitor, bool checkForKeyword = false)
        {
            Console.WriteLine($"ProcessHttp for {uptimeMonitor.Name}");

            var stopWatch = new Stopwatch();
            stopWatch.Start();

            try
            {
                var selectedHttpMethod = MemoryCacheHelper.UptimeMonitorHttpMethodTypes.First(x => x.UptimeMonitorHttpMethodTypeId == uptimeMonitor.UptimeMonitorHttpMethodTypeId).Code;
                var httpMethod = new HttpMethod(selectedHttpMethod);

                var request = new HttpRequestMessage(httpMethod, new Uri(uptimeMonitor.Url));    

                var response = _httpClient.SendAsync(request);
                response.Wait();

                var result = response.Result;

                var redirectCount = 0;
                while (!result.IsSuccessStatusCode
                    && ((int)result.StatusCode == 301
                        || (int)result.StatusCode == 302)
                    && redirectCount <= _maxRedirectsToFollow)
                {                         
                    var redirectLocation = result.Headers.GetValues("Location").FirstOrDefault();
                    if (!string.IsNullOrEmpty(redirectLocation))
                    {
                        request = new HttpRequestMessage(httpMethod, new Uri(redirectLocation));
                        response = _httpClient.SendAsync(request);
                        response.Wait();

                        result = response.Result;
                    }

                    redirectCount++;
                }

                if (result.IsSuccessStatusCode)
                {
                    if (checkForKeyword)
                    {
                        var buffer = new char[_keywordSearchContentMaxLength];

                        using (var content = result.Content)
                        {
                            var responseStream = content.ReadAsStreamAsync();
                            responseStream.Wait();
                            var resultStream = responseStream.Result;
                            using (var streamReader = new StreamReader(resultStream))
                            {                                
                                streamReader.Read(buffer, 0, _keywordSearchContentMaxLength);
                            }
                        }

                        string contentResult = new string(buffer);
                        contentResult = contentResult.Replace("\0", string.Empty);

                        var containsKeyword = !string.IsNullOrEmpty(contentResult)
                            && contentResult.Contains(uptimeMonitor.Keyword);

                        var keywordMessage = string.Empty;
                        var uptimeStatusTypeId = 0;
                        var uptimeReasonTypeId = 0;
                        
                        if (uptimeMonitor.KeywordAlertWhenExists == true && containsKeyword)
                        {
                            // Down
                            keywordMessage = "Keyword Found";
                            uptimeStatusTypeId = MemoryCacheHelper.UptimeStatusTypes.First(x => x.Code == StatusTypes.Down).UptimeStatusTypeId;
                            uptimeReasonTypeId = MemoryCacheHelper.UptimeReasonTypes.First(x => x.Code == ReasonTypes.KeywordFound).UptimeReasonTypeId;                    
                        }
                        else if (uptimeMonitor.KeywordAlertWhenExists == true && !containsKeyword)
                        {
                            // Up
                            keywordMessage = "Keyword Found";
                            uptimeStatusTypeId = MemoryCacheHelper.UptimeStatusTypes.First(x => x.Code == StatusTypes.Up).UptimeStatusTypeId;
                            uptimeReasonTypeId = MemoryCacheHelper.UptimeReasonTypes.First(x => x.Code == ReasonTypes.KeywordFound).UptimeReasonTypeId;
                        }
                        else if (uptimeMonitor.KeywordAlertWhenExists == false && containsKeyword)
                        {
                            // Up
                            keywordMessage = "Keyword Not Found";
                            uptimeStatusTypeId = MemoryCacheHelper.UptimeStatusTypes.First(x => x.Code == StatusTypes.Up).UptimeStatusTypeId;
                            uptimeReasonTypeId = MemoryCacheHelper.UptimeReasonTypes.First(x => x.Code == ReasonTypes.KeywordNotFound).UptimeReasonTypeId;
                            
                        }
                        else if (uptimeMonitor.KeywordAlertWhenExists == false && !containsKeyword)
                        {
                            // Down
                            keywordMessage = "Keyword Not Found";
                            uptimeStatusTypeId = MemoryCacheHelper.UptimeStatusTypes.First(x => x.Code == StatusTypes.Down).UptimeStatusTypeId;
                            uptimeReasonTypeId = MemoryCacheHelper.UptimeReasonTypes.First(x => x.Code == ReasonTypes.KeywordNotFound).UptimeReasonTypeId;
                        }

                        UpdateStatus(
                            uptimeMonitor.UptimeMonitorId,
                            (int?)result.StatusCode, 
                            $"{keywordMessage}\n{contentResult}",
                            uptimeStatusTypeId,
                            uptimeReasonTypeId,
                            (int?)stopWatch.ElapsedMilliseconds);
                    }
                    else
                    {
                        UpdateStatus(
                            uptimeMonitor.UptimeMonitorId,
                            (int?)result.StatusCode,
                            null,
                            MemoryCacheHelper.UptimeStatusTypes.First(x => x.Code == StatusTypes.Up).UptimeStatusTypeId,
                            MemoryCacheHelper.UptimeReasonTypes.First(x => x.Code == ReasonTypes.Ok).UptimeReasonTypeId,
                            (int?)stopWatch.ElapsedMilliseconds);
                    }
                }
                else
                {
                    Console.WriteLine($"Error: {result.StatusCode} {uptimeMonitor.Name}");
                    UpdateStatus(
                        uptimeMonitor.UptimeMonitorId,
                        (int?)result.StatusCode,
                        $"Error: {result.StatusCode}",
                        MemoryCacheHelper.UptimeStatusTypes.First(x => x.Code == StatusTypes.Down).UptimeStatusTypeId,
                        (int)result.StatusCode == 400 ? 
                            MemoryCacheHelper.UptimeReasonTypes.First(x => x.Code == ReasonTypes.NotFound).UptimeReasonTypeId :
                            MemoryCacheHelper.UptimeReasonTypes.First(x => x.Code == ReasonTypes.Error).UptimeReasonTypeId,
                        (int?)stopWatch.ElapsedMilliseconds);
                }                
                
            }
            catch (Exception exception)
            {
                ErrorLogging.Log(null, exception);

                UpdateStatus(
                    uptimeMonitor.UptimeMonitorId,
                    null,
                    $"Error: {uptimeMonitor.Name} {exception}",
                    MemoryCacheHelper.UptimeStatusTypes.First(x => x.Code == StatusTypes.Down).UptimeStatusTypeId,
                    MemoryCacheHelper.UptimeReasonTypes.First(x => x.Code == ReasonTypes.NoResponse).UptimeReasonTypeId,
                    null);
            }
        }

        private void ProcessPing(UptimeMonitor uptimeMonitor)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            try
            {   
                string targetHost = uptimeMonitor.Url;
                string data = "a quick brown fox jumped over the lazy dog";
                var pingSender = new Ping();
                var options = new PingOptions
                {
                    DontFragment = true
                };
                
                byte[] buffer = Encoding.ASCII.GetBytes(data);
                int timeout = 1024;

                var reply = pingSender.Send(targetHost, timeout, buffer, options);
                if (reply.Status == IPStatus.Success)
                {
                    UpdateStatus(
                        uptimeMonitor.UptimeMonitorId,
                        null,
                        $"Address: {reply.Address}\nRoundTrip time: {reply.RoundtripTime}\nTTL: {reply.Options.Ttl}\nBuffer size: {reply.Buffer.Length}",
                        MemoryCacheHelper.UptimeStatusTypes.First(x => x.Code == StatusTypes.Up).UptimeStatusTypeId,
                        MemoryCacheHelper.UptimeReasonTypes.First(x => x.Code == ReasonTypes.Ok).UptimeReasonTypeId,
                        (int?)stopWatch.ElapsedMilliseconds);
                }
                else
                {
                    Console.WriteLine($"Error: {reply.Status}");
                    UpdateStatus(
                        uptimeMonitor.UptimeMonitorId,
                        null,
                        $"Error: {reply.Status}",
                        MemoryCacheHelper.UptimeStatusTypes.First(x => x.Code == StatusTypes.Down).UptimeStatusTypeId,
                        MemoryCacheHelper.UptimeReasonTypes.First(x => x.Code == ReasonTypes.Error).UptimeReasonTypeId,
                        (int?)stopWatch.ElapsedMilliseconds);
                }
            }
            catch (Exception exception)
            {
                ErrorLogging.Log(null, exception);

                UpdateStatus(
                    uptimeMonitor.UptimeMonitorId,
                    null,
                    $"Error: {exception}",
                    MemoryCacheHelper.UptimeStatusTypes.First(x => x.Code == StatusTypes.Down).UptimeStatusTypeId,
                    MemoryCacheHelper.UptimeReasonTypes.First(x => x.Code == ReasonTypes.Error).UptimeReasonTypeId,
                    (int?)stopWatch.ElapsedMilliseconds);
            }
        }

        private void ProcessPort(UptimeMonitor uptimeMonitor)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            try
            {   
                var client = new TcpClient();                
                if (client.ConnectAsync(uptimeMonitor.Url, uptimeMonitor.Port.Value).Wait(_defaultTimeout * 1000))
                {
                    UpdateStatus(
                        uptimeMonitor.UptimeMonitorId,
                        null,
                        null,
                        MemoryCacheHelper.UptimeStatusTypes.First(x => x.Code == StatusTypes.Up).UptimeStatusTypeId,
                        MemoryCacheHelper.UptimeReasonTypes.First(x => x.Code == ReasonTypes.Ok).UptimeReasonTypeId,
                        (int?)stopWatch.ElapsedMilliseconds);
                }
                else
                {
                    Console.WriteLine($"Error: Could not connect to port {uptimeMonitor.Port.Value}");
                    UpdateStatus(
                        uptimeMonitor.UptimeMonitorId,
                        null,
                        $"Error: Could not connect to port {uptimeMonitor.Port.Value}",
                        MemoryCacheHelper.UptimeStatusTypes.First(x => x.Code == StatusTypes.Down).UptimeStatusTypeId,
                        MemoryCacheHelper.UptimeReasonTypes.First(x => x.Code == ReasonTypes.Error).UptimeReasonTypeId,
                        (int?)stopWatch.ElapsedMilliseconds);
                }
            }
            catch (Exception exception)
            {                
                ErrorLogging.Log(null, exception);

                UpdateStatus(
                    uptimeMonitor.UptimeMonitorId,
                    null,
                    $"Error: {exception}",
                    MemoryCacheHelper.UptimeStatusTypes.First(x => x.Code == StatusTypes.Down).UptimeStatusTypeId,
                    MemoryCacheHelper.UptimeReasonTypes.First(x => x.Code == ReasonTypes.Error).UptimeReasonTypeId,
                    (int?)stopWatch.ElapsedMilliseconds);
            }
        }

        private void ProcessHeartbeat(UptimeMonitor uptimeMonitor)
        {            
            if (uptimeMonitor.HeartbeatDate != null
                &&uptimeMonitor.HeartbeatDate.Value.AddMinutes(uptimeMonitor.Interval) >= uptimeMonitor.NextMonitorDate)
            {
                Console.WriteLine($"Heartbeat successful");
    
                UpdateStatus(
                    uptimeMonitor.UptimeMonitorId,
                    null,
                    null,
                    MemoryCacheHelper.UptimeStatusTypes.First(x => x.Code == StatusTypes.Up).UptimeStatusTypeId,
                    MemoryCacheHelper.UptimeReasonTypes.First(x => x.Code == ReasonTypes.Ok).UptimeReasonTypeId,
                    uptimeMonitor.ResponseTime);
            }
            else
            {
                Console.WriteLine($"Error: Heartbeat not received");

                UpdateStatus(
                    uptimeMonitor.UptimeMonitorId,
                    null,
                    $"Error: Heartbeat not received",
                    MemoryCacheHelper.UptimeStatusTypes.First(x => x.Code == StatusTypes.Down).UptimeStatusTypeId,
                    MemoryCacheHelper.UptimeReasonTypes.First(x => x.Code == ReasonTypes.Error).UptimeReasonTypeId,
                    null);
            }
        }

        private void UpdateStatus(
            int uptimeMonitorId, 
            int? statusCode, 
            string responseContent, 
            int uptimeStatusTypeId,
            int uptimeReasonTypeId,
            int? responseTime)
        {
            Console.WriteLine("Updating Status");

            try
            {
                using (var context = new Context(_dbContextOptions))
                {
                    var truncatedResponseContent = StringHelpers.Truncate(responseContent, 1000);                    
                    var processMonitorEventsResults = context.ProcessMonitorEventsResults
                        .FromSql("CALL UpdateMonitorStatus({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8});", 
                            uptimeMonitorId,
                            statusCode != null ? statusCode.Value : 0,
                            truncatedResponseContent != null ? truncatedResponseContent : string.Empty,
                            uptimeStatusTypeId,
                            uptimeReasonTypeId,
                            responseTime != null ? responseTime : 0,
                            _uptimeMonitorLocation,
                            _uptimeMonitorIpAddress,
                            _defaultTimeout).ToList();

                    var processMonitorEventsResult = processMonitorEventsResults.FirstOrDefault(x => x.Id == 1);
                    if (processMonitorEventsResult != null
                        && processMonitorEventsResult.MostRecentEventDuration != null)
                    {
                        var uptimeMonitor = context.UptimeMonitors.FirstOrDefault(x => x.UptimeMonitorId == uptimeMonitorId);
                        if (uptimeMonitor != null)
                        {
                            ProcessAlerts(context, uptimeMonitor, processMonitorEventsResult.MostRecentEventDuration.Value);
                        }
                    }
                }   
            }
            catch (Exception exception)
            {
                ErrorLogging.Log(null, exception);
            }
        }

        private void ProcessAlerts(Context context, UptimeMonitor uptimeMonitor, int mostRecentEventDuration)
        {
            var selectedContactIds = context.UptimeMonitorAlertContacts
                .Where(x => x.UptimeMonitorId == uptimeMonitor.UptimeMonitorId)
                .Select(x => x.UptimeAlertContactId)
                .ToList();

            foreach (var selectedContactId in selectedContactIds)
            {
                var uptimeAlertContact = context.UptimeAlertContacts.First(x => x.UptimeAlertContactId == selectedContactId);
                
                var downStatusTypeId = MemoryCacheHelper.UptimeStatusTypes.First(x => x.Code == StatusTypes.Down).UptimeStatusTypeId;
                var upStatusTypeId = MemoryCacheHelper.UptimeStatusTypes.First(x => x.Code == StatusTypes.Up).UptimeStatusTypeId;

                var upDownEventTypeId = MemoryCacheHelper.UptimeNotificationEventTypes.First(x => x.Code == NotificationEventTypes.UpDown).UptimeNotificationEventTypeId;
                var onlyDownEventTypeId = MemoryCacheHelper.UptimeNotificationEventTypes.First(x => x.Code == NotificationEventTypes.OnlyDown).UptimeNotificationEventTypeId;
                var onlyUpEventTypeId = MemoryCacheHelper.UptimeNotificationEventTypes.First(x => x.Code == NotificationEventTypes.OnlyUp).UptimeNotificationEventTypeId;

                if (uptimeMonitor.UptimeStatusTypeId == downStatusTypeId)
                {                    
                    if (uptimeAlertContact.UptimeNotificationEventTypeId == upDownEventTypeId
                        || uptimeAlertContact.UptimeNotificationEventTypeId == onlyDownEventTypeId)
                    {
                        SendAlert(StatusTypes.Down, context, uptimeMonitor, uptimeAlertContact, mostRecentEventDuration);
                    }                    
                }   
                else if (uptimeMonitor.UptimeStatusTypeId == upStatusTypeId)
                {
                    if (uptimeAlertContact.UptimeNotificationEventTypeId == upDownEventTypeId
                        || uptimeAlertContact.UptimeNotificationEventTypeId == onlyUpEventTypeId)
                    {
                        SendAlert(StatusTypes.Up, context, uptimeMonitor, uptimeAlertContact, mostRecentEventDuration);
                    }
                }    
            }     
        }

        private void SendAlert(
            string statusType,
            Context context, 
            UptimeMonitor uptimeMonitor, 
            UptimeAlertContact uptimeAlertContact,
            int mostRecentEventDuration)
        {
            var emailContactTypeId = MemoryCacheHelper.UptimeAlertContactTypes.First(x => x.Code == ContactTypes.Email).UptimeAlertContactTypeId;
            var emailToSmsContactTypeId = MemoryCacheHelper.UptimeAlertContactTypes.First(x => x.Code == ContactTypes.EmailToSms).UptimeAlertContactTypeId;
            var webhookContactTypeId = MemoryCacheHelper.UptimeAlertContactTypes.First(x => x.Code == ContactTypes.Webhook).UptimeAlertContactTypeId;

            if (uptimeAlertContact.UptimeAlertContactTypeId == emailContactTypeId)
            {
                var uptimeStatusType = MemoryCacheHelper.UptimeStatusTypes.First(x => x.UptimeStatusTypeId == uptimeMonitor.UptimeStatusTypeId);
                var uptimeReasonType = MemoryCacheHelper.UptimeReasonTypes.First(x => x.UptimeReasonTypeId == uptimeMonitor.UptimeReasonTypeId);
                
                DateTime dt;
                TimeZoneInfo tzf;
                tzf = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                dt = TimeZoneInfo.ConvertTime(DateTime.Now, tzf);
                var timeStamp = dt.ToString("yyyy-MM-dd HH:mm:ss UTCz");

                var duration = string.Empty;
                var mostRecentEventDurationTimeSpan = TimeSpan.FromSeconds(mostRecentEventDuration);
                duration = StringHelpers.ConvertDurationToString(mostRecentEventDurationTimeSpan);

                var subject = $"Monitor is {statusType.ToUpper()}: {uptimeMonitor.Name}";
                var body = "Hi," + "<br /><br />"
                    + (uptimeStatusType.Code == StatusTypes.Down ? 
                        $"The monitor {uptimeMonitor.Name} ({uptimeMonitor.Url}) is currently {statusType.ToUpper()} ({uptimeReasonType.Name})." :
                        $"The monitor {uptimeMonitor.Name} ({uptimeMonitor.Url}) is back {statusType.ToUpper()} ({uptimeReasonType.Name})  (It was down for {duration}).")
                        + "<br /><br />"
                    + $"<strong>Event timestamp:</strong> {timeStamp}" + "<br /><br />"
                    + (uptimeStatusType.Code == StatusTypes.Down ? 
                        $"{_siteName} will alert you when it is back up." + "<br /><br /><br />" :
                        "" + "<br />")
                    + (uptimeStatusType.Code == StatusTypes.Down ? 
                        "Sincerely," :
                        "Have a great day,") + "<br /><br />"
                    + $"{_siteName}";

                new CommunicationService(_IOptionsSettings).SendEmail(uptimeAlertContact.Address, subject, body, isBodyHtml: true);
            }
            else if (uptimeAlertContact.UptimeAlertContactTypeId == emailToSmsContactTypeId)
            {
                var text = $"Monitor is {statusType.ToUpper()}: {uptimeMonitor.Name} ({uptimeMonitor.Url})";
                new CommunicationService(_IOptionsSettings).SendEmailToSms(uptimeAlertContact, text);
            }
            else if (uptimeAlertContact.UptimeAlertContactTypeId == webhookContactTypeId)
            {
                SendWebhook(statusType, context, uptimeMonitor, uptimeAlertContact, mostRecentEventDuration);
            }
        }

        private async void SendWebhook(
            string statusType,
            Context context, 
            UptimeMonitor uptimeMonitor, 
            UptimeAlertContact uptimeAlertContact,
            int mostRecentEventDuration)
        {
            var webhookTemplate = "monitorID=*monitorID*&monitorURL=*monitorURL*&monitorFriendlyName=*monitorFriendlyName*&alertType=*alertType*&alertTypeFriendlyName=*alertTypeFriendlyName*&alertDetails=*alertDetails*&alertDuration=*alertDuration*&monitorAlertContacts=*monitorAlertContacts*&alertDateTime=*alertDateTime*";
            var jsonWebhookTemplate = "\"monitorID\": \"*monitorID*\", \"monitorURL\": \"*monitorURL*\", \"monitorFriendlyName\": \"*monitorFriendlyName*\", \"alertType\": \"*alertType*\", \"alertTypeFriendlyName\": \"*alertTypeFriendlyName*\", \"alertDetails\": \"*alertDetails*\", \"alertDuration\": \"*alertDuration*\", \"monitorAlertContacts\": \"*monitorAlertContacts*\", \"alertDateTime\": \"*alertDateTime*\"";
            var webhookPostWithVariableString = string.Empty;
            var webhookUrlWithVariableString = string.Empty;

            var webhookUrl = uptimeAlertContact.Address;
            if (uptimeAlertContact.SendDefaultQueryStringForWebhook)
            {   
                if (!webhookUrl.Contains("?"))
                {
                    webhookUrl += "?";
                }
                else if (webhookUrl.Contains("?")
                    && !webhookUrl.EndsWith("&"))
                {
                    webhookUrl += "&";
                }                       

                webhookUrlWithVariableString = webhookUrl + webhookTemplate;                
            }
            else
            {
                webhookUrlWithVariableString = webhookUrl;
            }

            long unixTime = ((DateTimeOffset)DateTime.UtcNow).ToUnixTimeSeconds();

            webhookUrlWithVariableString = webhookUrlWithVariableString.Replace("*monitorID*", uptimeMonitor.UptimeMonitorId.ToString());
            webhookUrlWithVariableString = webhookUrlWithVariableString.Replace("*monitorURL*", Uri.EscapeDataString(uptimeMonitor.Url));
            webhookUrlWithVariableString = webhookUrlWithVariableString.Replace("*monitorFriendlyName*", Uri.EscapeDataString(uptimeMonitor.Name));
            webhookUrlWithVariableString = webhookUrlWithVariableString.Replace("*alertType*", statusType == StatusTypes.Down ? "1" : "2");
            webhookUrlWithVariableString = webhookUrlWithVariableString.Replace("*alertTypeFriendlyName*", Uri.EscapeDataString(statusType));
            webhookUrlWithVariableString = webhookUrlWithVariableString.Replace("*alertDetails*", Uri.EscapeDataString(uptimeMonitor.UptimeReasonTypeId != null ? MemoryCacheHelper.UptimeReasonTypes.First(x => x.UptimeReasonTypeId == uptimeMonitor.UptimeReasonTypeId).Name : string.Empty));
            webhookUrlWithVariableString = webhookUrlWithVariableString.Replace("*alertDuration*", mostRecentEventDuration.ToString());
            webhookUrlWithVariableString = webhookUrlWithVariableString.Replace("*monitorAlertContacts*", Uri.EscapeDataString($"{uptimeAlertContact.UptimeAlertContactId};{uptimeAlertContact.UptimeAlertContactTypeId};{uptimeAlertContact.Address}"));
            webhookUrlWithVariableString = webhookUrlWithVariableString.Replace("*alertDateTime*", unixTime.ToString());
            
            try
            {            
                var selectedHttpMethod = "Get";
                if (uptimeAlertContact.SendDefaultPostForWebhook
                    || !string.IsNullOrEmpty(uptimeAlertContact.PostValue))
                {
                    selectedHttpMethod = "Post";
                }

                var httpMethod = new HttpMethod(selectedHttpMethod);
                var request = new HttpRequestMessage(httpMethod, new Uri(webhookUrlWithVariableString));
                
                if (uptimeAlertContact.SendJsonForWebhook)
                {                        
                    if (uptimeAlertContact.SendDefaultPostForWebhook)
                    {
                        webhookPostWithVariableString = jsonWebhookTemplate;
                    }

                    if (!string.IsNullOrEmpty(uptimeAlertContact.PostValue))
                    {
                        var jsonPostValue = uptimeAlertContact.PostValue;
                        if (jsonPostValue.Contains("{")
                            && jsonPostValue.Contains("}"))
                        {
                            jsonPostValue = StringHelpers.ReplaceFirstOccurrence(jsonPostValue, "{", string.Empty);
                            jsonPostValue = StringHelpers.ReplaceLastOccurrence(jsonPostValue, "}", string.Empty);
                        }

                        if (!string.IsNullOrEmpty(webhookPostWithVariableString))
                        {
                            webhookPostWithVariableString += ", " + jsonPostValue;
                        }
                        else
                        {
                            webhookPostWithVariableString += jsonPostValue;
                        }
                    }

                    webhookPostWithVariableString = webhookPostWithVariableString.Replace("*monitorID*", uptimeMonitor.UptimeMonitorId.ToString());
                    webhookPostWithVariableString = webhookPostWithVariableString.Replace("*monitorURL*", HttpUtility.JavaScriptStringEncode(uptimeMonitor.Url));
                    webhookPostWithVariableString = webhookPostWithVariableString.Replace("*monitorFriendlyName*", HttpUtility.JavaScriptStringEncode(uptimeMonitor.Name));
                    webhookPostWithVariableString = webhookPostWithVariableString.Replace("*alertType*", statusType == StatusTypes.Down ? "1" : "2");
                    webhookPostWithVariableString = webhookPostWithVariableString.Replace("*alertTypeFriendlyName*", HttpUtility.JavaScriptStringEncode(statusType));
                    webhookPostWithVariableString = webhookPostWithVariableString.Replace("*alertDetails*", HttpUtility.JavaScriptStringEncode(uptimeMonitor.UptimeReasonTypeId != null ? MemoryCacheHelper.UptimeReasonTypes.First(x => x.UptimeReasonTypeId == uptimeMonitor.UptimeReasonTypeId).Name : string.Empty));
                    webhookPostWithVariableString = webhookPostWithVariableString.Replace("*alertDuration*", mostRecentEventDuration.ToString());
                    webhookPostWithVariableString = webhookPostWithVariableString.Replace("*monitorAlertContacts*", HttpUtility.JavaScriptStringEncode($"{uptimeAlertContact.UptimeAlertContactId};{uptimeAlertContact.UptimeAlertContactTypeId};{uptimeAlertContact.Address}"));
                    webhookPostWithVariableString = webhookPostWithVariableString.Replace("*alertDateTime*", unixTime.ToString());

                    webhookPostWithVariableString = "{" + webhookPostWithVariableString + "}";                    

                    request.Content = new StringContent(webhookPostWithVariableString, Encoding.UTF8, "application/json");
                }
                else if (uptimeAlertContact.SendDefaultPostForWebhook
                    || !string.IsNullOrEmpty(uptimeAlertContact.PostValue))
                {
                    if (uptimeAlertContact.SendDefaultPostForWebhook)
                    {
                        webhookPostWithVariableString = webhookTemplate;
                    }

                    if (!string.IsNullOrEmpty(uptimeAlertContact.PostValue))
                    {
                        if (!string.IsNullOrEmpty(webhookPostWithVariableString))
                        {
                           webhookPostWithVariableString += "&" + uptimeAlertContact.PostValue;
                        }
                        else
                        {
                            webhookPostWithVariableString += uptimeAlertContact.PostValue;
                        }                        
                    }
                    
                    webhookPostWithVariableString = webhookPostWithVariableString.Replace("*monitorID*", uptimeMonitor.UptimeMonitorId.ToString());
                    webhookPostWithVariableString = webhookPostWithVariableString.Replace("*monitorURL*", Uri.EscapeDataString(uptimeMonitor.Url));
                    webhookPostWithVariableString = webhookPostWithVariableString.Replace("*monitorFriendlyName*", Uri.EscapeDataString(uptimeMonitor.Name));
                    webhookPostWithVariableString = webhookPostWithVariableString.Replace("*alertType*", statusType == StatusTypes.Down ? "1" : "2");
                    webhookPostWithVariableString = webhookPostWithVariableString.Replace("*alertTypeFriendlyName*", Uri.EscapeDataString(statusType));
                    webhookPostWithVariableString = webhookPostWithVariableString.Replace("*alertDetails*", Uri.EscapeDataString(uptimeMonitor.UptimeReasonTypeId != null ? MemoryCacheHelper.UptimeReasonTypes.First(x => x.UptimeReasonTypeId == uptimeMonitor.UptimeReasonTypeId).Name : string.Empty));
                    webhookPostWithVariableString = webhookPostWithVariableString.Replace("*alertDuration*", mostRecentEventDuration.ToString());
                    webhookPostWithVariableString = webhookPostWithVariableString.Replace("*monitorAlertContacts*", Uri.EscapeDataString($"{uptimeAlertContact.UptimeAlertContactId};{uptimeAlertContact.UptimeAlertContactTypeId};{uptimeAlertContact.Address}"));
                    webhookPostWithVariableString = webhookPostWithVariableString.Replace("*alertDateTime*", unixTime.ToString());

                    request.Content = new StringContent(webhookPostWithVariableString, Encoding.UTF8, "application/x-www-form-urlencoded");
                }

                var response = await _httpClient.SendAsync(request);
            }
            catch (Exception exception)
            {
                ErrorLogging.Log(null, exception);
            }
        }
    }
}
