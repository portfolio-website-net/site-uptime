﻿using System;
using System.Linq;
using System.Collections.Generic;
using SiteUptime.Models;
using Microsoft.EntityFrameworkCore;
using SiteUptime.Helpers.Authentication;
using SiteUptime.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using SiteUptime.Helpers;

namespace SiteUptime.Services
{
    public class EventLogService : IEventLogService
    {
        private readonly Context _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public EventLogService(Context context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        public void LogMessage(User user, EventNameType eventNameType, string message = null)
        {
            var path = _httpContextAccessor.HttpContext.Request.Path.ToString();
            var ipAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
            var userAgent = _httpContextAccessor.HttpContext.Request.Headers["User-Agent"]; 

            if (user == null)
            {
                user = (User)_httpContextAccessor.HttpContext.Items["User"];
                if (user == null)
                {
                    user = new User();
                }
            }

            _context.EventLogs.Add(new EventLog {
                UserId = user.UserId,
                EventName = eventNameType.ToString(),
                Message = StringHelpers.Truncate(message, 4000),
                Path = StringHelpers.Truncate(path, 4000),
                IpAddress = ipAddress,
                UserAgent = StringHelpers.Truncate(userAgent, 4000),
                CreatedById = user.UserId,
                CreatedDate = DateTime.Now
            });            

            _context.SaveChanges();            
        }
    }
}
