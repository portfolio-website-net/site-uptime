﻿using System;
using System.Linq;
using System.Collections.Generic;
using SiteUptime.Models;
using Microsoft.EntityFrameworkCore;
using SiteUptime.Services.Interfaces;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Http;
using SiteUptime.Helpers.ObjectHelpers;
using SiteUptime.Helpers;
using Microsoft.SyndicationFeed;
using Microsoft.SyndicationFeed.Rss;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using System.Text;
using SiteUptime.Helpers.CacheHelpers;
using SiteUptime.Helpers.Types;

namespace SiteUptime.Services
{
    public class SyndicationFeedService : ISyndicationFeedService
    {
        private readonly Context _context;
        private readonly Settings _settings;
        private readonly IEventLogService _eventLogService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUptimeMonitorService _uptimeMonitorService;

        public SyndicationFeedService(Context context, 
            IOptions<Settings> settings,
            IEventLogService eventLogService,
            IHttpContextAccessor httpContextAccessor,
            IUptimeMonitorService uptimeMonitorService)
        {
            _context = context;
            _settings = settings.Value;
            _eventLogService = eventLogService;
            _httpContextAccessor = httpContextAccessor;
            _uptimeMonitorService = uptimeMonitorService;
        }

        public async Task<string> GenerateFeed(string syndicationFeedKey)
        {
            var syndicationFeedKeyTypeId = MemoryCacheHelper.UptimeMonitorApiKeyTypes.First(x => x.Code == ApiKeyTypes.SyndicationFeedKey).UptimeMonitorApiKeyTypeId;
            var latestEvents = (from ume in _context.UptimeMonitorEvents
                                join ust in _context.UptimeStatusTypes on ume.UptimeStatusTypeId equals ust.UptimeStatusTypeId
                                join urt in _context.UptimeReasonTypes on ume.UptimeReasonTypeId equals urt.UptimeReasonTypeId
                                join um in _context.UptimeMonitors on ume.UptimeMonitorId equals um.UptimeMonitorId
                                join umak in _context.UptimeMonitorApiKeys on um.UserId equals umak.UserId
                                where umak.ApiKey == syndicationFeedKey
                                    && umak.UptimeMonitorApiKeyTypeId == syndicationFeedKeyTypeId
                                orderby ume.CreatedDate descending
                                select new {
                                    Title = $"{um.Name} is {ust.Name} ({um.Url})",
                                    Link = um.Url,
                                    Description = urt.Name,
                                    Published = ume.CreatedDate,
                                    Duration = ume.Duration
                                }).Take(20).ToList();

            var sw = new StringWriterWithEncoding(Encoding.UTF8);

            if (latestEvents.Any())
            {
                using (var xmlWriter = XmlWriter.Create(sw, new XmlWriterSettings() { Async = true , Indent = true }))
                {
                    var attributes = new List<SyndicationAttribute>()
                    {
                        new SyndicationAttribute("xmlns:details", _settings.SiteUrl)
                    };
                    var formatter = new RssFormatter(attributes, xmlWriter.Settings);
                    var writer = new RssFeedWriter(xmlWriter);
                    await writer.WriteTitle($"{_settings.SiteName} - All Monitors");
                    await writer.Write(new SyndicationLink(new Uri($"{_settings.SiteUrl}/Home/Feed/{syndicationFeedKey}")));
                    await writer.WriteDescription($"Current uptime RSS feed via {_settings.SiteName}");
                    await writer.WriteLastBuildDate(latestEvents.OrderByDescending(x => x.Published).First().Published);

                    foreach (var latestEvent in latestEvents)
                    {
                        var item = new SyndicationItem()
                        {
                            Title = latestEvent.Title,
                            Description = latestEvent.Description,
                            Published = latestEvent.Published
                        };

                        item.AddLink(new SyndicationLink(new Uri(latestEvent.Link)));
                        
                        var content = new SyndicationContent(formatter.CreateContent(item));
                        content.AddField(new SyndicationContent("duration", _settings.SiteUrl, latestEvent.Duration.ToString()));

                        await writer.Write(content);
                    }

                    xmlWriter.Flush();
                }
            }

            return sw.ToString();
        }    

        class StringWriterWithEncoding : StringWriter
        {
            private readonly Encoding _encoding;

            public StringWriterWithEncoding(Encoding encoding)
            {
                this._encoding = encoding;
            }

            public override Encoding Encoding {
                get { return _encoding; }
            }
        }
    }
}
