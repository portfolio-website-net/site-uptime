﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Extensions.Options;
using SiteUptime.Services.Interfaces;
using SiteUptime.Models;
using SiteUptime.Helpers;
using SiteUptime.Helpers.CacheHelpers;
using SiteUptime.Helpers.Results;
using SiteUptime.Helpers.Types;
using SiteUptime.Helpers.Authentication;
using Microsoft.AspNetCore.Http;
using SiteUptime.Helpers.ObjectHelpers;
using Microsoft.EntityFrameworkCore;
using SiteUptime.Helpers.Api;

namespace SiteUptime.Services
{
    public class UptimeMonitorService : IUptimeMonitorService
    {
        private readonly Context _context;
        private readonly Settings _settings;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ICommunicationService _communicationService;
        private readonly IEventLogService _eventLogService;
        private readonly IAuthenticationService _authenticationService;

        private readonly List<int> _pollingIntervals;
        private readonly User _currentUser;

        public UptimeMonitorService(
            Context context, 
            IOptions<Settings> settings, 
            IHttpContextAccessor httpContextAccessor,
            ICommunicationService communicationService,
            IEventLogService eventLogService,
            IAuthenticationService authenticationService)
        {
            _context = context;
            _settings = settings.Value;
            _httpContextAccessor = httpContextAccessor;
            _communicationService = communicationService;
            _eventLogService = eventLogService;
            _authenticationService = authenticationService;

            _pollingIntervals = _settings.PollingIntervals.Split(',').Select(x => int.Parse(x)).ToList();
            _currentUser = GetCurrentUser();
        }

        public List<UptimeMonitorSummary> GetUptimeMonitors()
        {            
            var uptimeMonitorResults = new List<UptimeMonitorSummary>();
            var userId = _currentUser != null ? _currentUser.UserId : 0;
            var apiUptimeMonitorId = _currentUser != null && _currentUser.UptimeMonitorId != null ? _currentUser.UptimeMonitorId : null;
            var uptimeMonitors = _context.UptimeMonitors.Where(x => x.UserId == userId
                && (apiUptimeMonitorId == null || x.UptimeMonitorId == apiUptimeMonitorId)).ToList();
            foreach (var uptimeMonitor in uptimeMonitors)
            {
                uptimeMonitorResults.Add(new UptimeMonitorSummary
                    {
                        UptimeMonitorId = uptimeMonitor.UptimeMonitorId,
                        UptimeMonitorTypeId = uptimeMonitor.UptimeMonitorTypeId,
                        Name = uptimeMonitor.Name,
                        Url = uptimeMonitor.Url,
                        UptimeStatusTypeId = uptimeMonitor.UptimeStatusTypeId,
                        UptimePercentage = uptimeMonitor.UptimePercentageLast30Days
                    }
                );
            }

            return uptimeMonitorResults;
        }

        public List<UptimeMonitor> GetUptimeMonitorsWithInternalDetails()
        {            
            var userId = _currentUser != null ? _currentUser.UserId : 0;
            var apiUptimeMonitorId = _currentUser != null && _currentUser.UptimeMonitorId != null ? _currentUser.UptimeMonitorId : null;
            var uptimeMonitors = _context.UptimeMonitors.Where(x => x.UserId == userId
                && (apiUptimeMonitorId == null || x.UptimeMonitorId == apiUptimeMonitorId)).ToList();

            return uptimeMonitors;
        }

        public UptimeMonitor GetUptimeMonitor(int uptimeMonitorId)
        {
            var uptimeMonitor = new UptimeMonitor();
            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                var record = _context.UptimeMonitors
                    .FirstOrDefault(x => x.UptimeMonitorId == uptimeMonitorId && x.UserId == userId);
                if (record != null)
                {
                    uptimeMonitor = record;

                    uptimeMonitor.IntervalSliderValue = _pollingIntervals.IndexOf(uptimeMonitor.Interval) + 1;

                    var selectedContactIds = _context.UptimeMonitorAlertContacts
                        .Where(x => x.UptimeMonitorId == uptimeMonitor.UptimeMonitorId)
                        .Select(x => x.UptimeAlertContactId)
                        .ToList();

                    uptimeMonitor.SelectedContactIds = selectedContactIds;
                }
            }

            return uptimeMonitor;
        }

        public List<UptimeAlertContactSummary> GetUptimeAlertContacts()
        {
            var uptimeAlertContactResults = new List<UptimeAlertContactSummary>();
            var userId = _currentUser != null ? _currentUser.UserId : 0;
            var uptimeAlertContacts = _context.UptimeAlertContacts.Where(x => x.UserId == userId).ToList();

            var emailToSmsContactTypeId = MemoryCacheHelper.UptimeAlertContactTypes.First(x => x.Code == ContactTypes.EmailToSms).UptimeAlertContactTypeId;

            foreach (var uptimeAlertContact in uptimeAlertContacts)
            {
                uptimeAlertContactResults.Add(new UptimeAlertContactSummary
                    {
                        UptimeAlertContactId = uptimeAlertContact.UptimeAlertContactId,
                        UptimeAlertContactTypeId = uptimeAlertContact.UptimeAlertContactTypeId,
                        UptimeAlertContactStatusTypeId = uptimeAlertContact.UptimeAlertContactStatusTypeId,
                        Name = uptimeAlertContact.Name,
                        Address = uptimeAlertContact.Address,
                        RequiresActivation = uptimeAlertContact.UptimeAlertContactTypeId == emailToSmsContactTypeId
                            && !uptimeAlertContact.IsActivated
                    }
                );
            }

            return uptimeAlertContactResults;
        }

        public List<UptimeAlertContact> GetUptimeAlertContactsWithInternalDetails()
        {            
            var uptimeAlertContactResults = new List<UptimeAlertContactSummary>();
            var userId = _currentUser != null ? _currentUser.UserId : 0;
            var uptimeAlertContacts = _context.UptimeAlertContacts.Where(x => x.UserId == userId).ToList();

            return uptimeAlertContacts;
        }

        public UptimeAlertContact GetUptimeAlertContact(int uptimeAlertContactId)
        {
            var uptimeAlertContact = new UptimeAlertContact();
            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                var record = _context.UptimeAlertContacts
                    .FirstOrDefault(x => x.UptimeAlertContactId == uptimeAlertContactId && x.UserId == userId);
                if (record != null)
                {
                    uptimeAlertContact = record;
                }
            }

            return uptimeAlertContact;
        }

        public List<UptimeMaintenanceWindowSummary> GetUptimeMaintenanceWindows()
        {            
            var uptimeMaintenanceWindowResults = new List<UptimeMaintenanceWindowSummary>();
            var userId = _currentUser != null ? _currentUser.UserId : 0;
            var uptimeMaintenanceWindows = _context.UptimeMaintenanceWindows.Where(x => x.UserId == userId).ToList();
            foreach (var uptimeMaintenanceWindow in uptimeMaintenanceWindows)
            {
                uptimeMaintenanceWindowResults.Add(new UptimeMaintenanceWindowSummary
                    {
                        UptimeMaintenanceWindowId = uptimeMaintenanceWindow.UptimeMaintenanceWindowId,
                        UptimeMaintenanceWindowTypeId = uptimeMaintenanceWindow.UptimeMaintenanceWindowTypeId,
                        Name = uptimeMaintenanceWindow.Name,
                        StartTime = uptimeMaintenanceWindow.StartTime,
                        Duration = uptimeMaintenanceWindow.Duration,
                        Days = uptimeMaintenanceWindow.Days,
                        IsActive = uptimeMaintenanceWindow.IsActive
                    }
                );
            }

            return uptimeMaintenanceWindowResults;
        }

        public List<UptimeMaintenanceWindow> GetUptimeMaintenanceWindowsWithInternalDetails()
        {
            var userId = _currentUser != null ? _currentUser.UserId : 0;
            var uptimeMaintenanceWindows = _context.UptimeMaintenanceWindows.Where(x => x.UserId == userId).ToList();

            return uptimeMaintenanceWindows;
        }

        public UptimeMaintenanceWindow GetUptimeMaintenanceWindow(int uptimeMaintenanceWindowId)
        {
            var uptimeMaintenanceWindow = new UptimeMaintenanceWindow();
            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                var record = _context.UptimeMaintenanceWindows
                    .FirstOrDefault(x => x.UptimeMaintenanceWindowId == uptimeMaintenanceWindowId && x.UserId == userId);
                if (record != null)
                {
                    uptimeMaintenanceWindow = record;

                    var selectedMonitorIds = _context.UptimeMonitorMaintenanceWindows
                        .Where(x => x.UptimeMaintenanceWindowId == uptimeMaintenanceWindow.UptimeMaintenanceWindowId)
                        .Select(x => x.UptimeMonitorId)
                        .ToList();

                    uptimeMaintenanceWindow.SelectedMonitorIds = selectedMonitorIds;
                }
            }

            return uptimeMaintenanceWindow;
        }

        public List<UptimePublicStatusPageSummary> GetUptimePublicStatusPages()
        {            
            var uptimePublicStatusPageResults = new List<UptimePublicStatusPageSummary>();
            var userId = _currentUser != null ? _currentUser.UserId : 0;
            var uptimePublicStatusPages = _context.UptimePublicStatusPages.Where(x => x.UserId == userId).ToList();
            foreach (var uptimePublicStatusPage in uptimePublicStatusPages)
            {
                uptimePublicStatusPageResults.Add(new UptimePublicStatusPageSummary
                    {
                        UptimePublicStatusPageId = uptimePublicStatusPage.UptimePublicStatusPageId,                        
                        Name = uptimePublicStatusPage.Name,
                        Url = !string.IsNullOrEmpty(uptimePublicStatusPage.CustomDomain) ?
                            "https://" + uptimePublicStatusPage.CustomDomain :
                            _settings.SiteUrl + "/?" + uptimePublicStatusPage.StatusPageKey,
                        IsActive = uptimePublicStatusPage.IsActive
                    }
                );
            }

            return uptimePublicStatusPageResults;
        }

        public List<UptimePublicStatusPage> GetUptimePublicStatusPagesWithInternalDetails()
        {
            var userId = _currentUser != null ? _currentUser.UserId : 0;
            var uptimePublicStatusPages = _context.UptimePublicStatusPages.Where(x => x.UserId == userId).ToList();

            return uptimePublicStatusPages;
        }

        public UptimePublicStatusPage GetUptimePublicStatusPage(int uptimePublicStatusPageId)
        {
            var uptimePublicStatusPage = new UptimePublicStatusPage();
            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                var record = _context.UptimePublicStatusPages
                    .FirstOrDefault(x => x.UptimePublicStatusPageId == uptimePublicStatusPageId && x.UserId == userId);
                if (record != null)
                {
                    uptimePublicStatusPage = record;

                    var selectedMonitorIds = _context.UptimeMonitorPublicStatusPages
                        .Where(x => x.UptimePublicStatusPageId == uptimePublicStatusPage.UptimePublicStatusPageId)
                        .Select(x => x.UptimeMonitorId)
                        .ToList();

                    uptimePublicStatusPage.SelectedMonitorIds = selectedMonitorIds;
                }
            }

            return uptimePublicStatusPage;
        }

        public List<UptimeMonitorApiKeySummary> GetUptimeMonitorApiKeys()
        {            
            var uptimeMonitorApiKeyResults = new List<UptimeMonitorApiKeySummary>();
            var userId = _currentUser != null ? _currentUser.UserId : 0;
            var uptimeMonitorApiKeys = _context.UptimeMonitorApiKeys.Where(x => x.UserId == userId).ToList();
            foreach (var uptimeMonitorApiKey in uptimeMonitorApiKeys)
            {
                uptimeMonitorApiKeyResults.Add(new UptimeMonitorApiKeySummary
                    {
                        UptimeMonitorApiKeyId = uptimeMonitorApiKey.UptimeMonitorApiKeyId,
                        UptimeMonitorApiKeyTypeId = uptimeMonitorApiKey.UptimeMonitorApiKeyTypeId,
                        UptimeMonitorId = uptimeMonitorApiKey.UptimeMonitorId,
                        ApiKey = uptimeMonitorApiKey.ApiKey
                    }
                );
            }

            return uptimeMonitorApiKeyResults;
        }

        public UptimeMonitorApiKey GetUptimeMonitorApiKey(int uptimeMonitorApiKeyId)
        {
            var uptimeMonitorApiKey = new UptimeMonitorApiKey();
            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                var record = _context.UptimeMonitorApiKeys
                    .FirstOrDefault(x => x.UptimeMonitorApiKeyId == uptimeMonitorApiKeyId && x.UserId == userId);
                if (record != null)
                {
                    uptimeMonitorApiKey = record;
                }
            }

            return uptimeMonitorApiKey;
        }

        public List<UptimeMonitorType> GetUptimeMonitorTypes()
        {
            return MemoryCacheHelper.UptimeMonitorTypes;
        }

        public List<UptimeMonitorHttpMethodType> GetUptimeMonitorHttpMethodTypes()
        {
            return MemoryCacheHelper.UptimeMonitorHttpMethodTypes;
        }

        public List<UptimeStatusType> GetUptimeStatusTypes()
        {
            return MemoryCacheHelper.UptimeStatusTypes;
        }

        public List<UptimeAlertContactType> GetUptimeAlertContactTypes()
        {
            return MemoryCacheHelper.UptimeAlertContactTypes;
        }

        public List<UptimeAlertContactStatusType> GetUptimeAlertContactStatusTypes()
        {
            return MemoryCacheHelper.UptimeAlertContactStatusTypes;
        }

        public List<UptimeMobileProviderType> GetUptimeMobileProviderTypes()
        {
            return MemoryCacheHelper.UptimeMobileProviderTypes;
        }

        public List<UptimeNotificationEventType> GetUptimeNotificationEventTypes()
        {
            return MemoryCacheHelper.UptimeNotificationEventTypes;
        }

        public List<UptimeMonitorApiKeyType> GetUptimeMonitorApiKeyTypes()
        {
            return MemoryCacheHelper.UptimeMonitorApiKeyTypes;
        }

        public List<UptimeMaintenanceWindowType> GetUptimeMaintenanceWindowTypes()
        {
            return MemoryCacheHelper.UptimeMaintenanceWindowTypes;
        }

        public List<UptimeMonitorSortType> GetUptimeMonitorSortTypes()
        {
            return MemoryCacheHelper.UptimeMonitorSortTypes;
        }

        public List<UserTimeZoneType> GetUserTimeZoneTypes()
        {
            return MemoryCacheHelper.UserTimeZoneTypes;
        }

        public List<UserSecurityQuestionType> GetUserSecurityQuestionTypes()
        {
            return MemoryCacheHelper.UserSecurityQuestionTypes;
        }

        public List<TwoFactorAuthenticationType> GetTwoFactorAuthenticationTypes()
        {
            return MemoryCacheHelper.TwoFactorAuthenticationTypes;
        }

        public List<UptimeMonitorSummary> UpdateUptimeMonitor(UptimeMonitor uptimeMonitor, bool isApiCall = false)
        {   
            if (uptimeMonitor.SelectedContactIds == null)
            {
                uptimeMonitor.SelectedContactIds = new List<int>();
            }         

            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                var record = _context.UptimeMonitors
                    .FirstOrDefault(x => x.UptimeMonitorId == uptimeMonitor.UptimeMonitorId && x.UserId == userId);

                var availableUptimeAlertContactIds = _context.UptimeAlertContacts
                    .Where(x => x.UserId == userId && uptimeMonitor.SelectedContactIds.Contains(x.UptimeAlertContactId))
                    .Select(x => x.UptimeAlertContactId).ToList();

                var heartbeatMonitorTypeId = MemoryCacheHelper.UptimeMonitorTypes.First(x => x.Code == MonitorTypes.Heartbeat).UptimeMonitorTypeId;

                if (record != null)
                {
                    var existingContacts = _context.UptimeMonitorAlertContacts
                        .Where(x => x.UptimeMonitorId == record.UptimeMonitorId)                    
                        .ToList();

                    record.SelectedContactIds = existingContacts.Select(x => x.UptimeAlertContactId).ToList();                

                    foreach (var existingContactId in record.SelectedContactIds.Where(x => !uptimeMonitor.SelectedContactIds.Contains(x)))
                    {
                        var existingContact = existingContacts.FirstOrDefault(x => x.UptimeAlertContactId == existingContactId);
                        if (existingContact != null)
                        {
                            _context.UptimeMonitorAlertContacts.Remove(existingContact);
                        }
                    }

                    record.UptimeMonitorHttpMethodTypeId = MemoryCacheHelper.UptimeMonitorHttpMethodTypes.First(x => x.Code == HttpMethodTypes.Get).UptimeMonitorHttpMethodTypeId;
                    record.Name = StringHelpers.Truncate(uptimeMonitor.Name, 100);
                    record.Url = StringHelpers.Truncate(uptimeMonitor.Url, 4000);
                    record.Port = uptimeMonitor.Port;
                    record.Keyword = StringHelpers.Truncate(uptimeMonitor.Keyword, 200);
                    record.KeywordAlertWhenExists = uptimeMonitor.KeywordAlertWhenExists;
                    record.Interval = _pollingIntervals[uptimeMonitor.IntervalSliderValue - 1];
                    
                    if (record.UptimeMonitorTypeId == heartbeatMonitorTypeId)
                    {
                        record.NextMonitorDate = DateTime.Now.AddMinutes(record.Interval);
                        record.HeartbeatDate = null;
                    }
                    else
                    {
                        record.NextMonitorDate = null;
                    }

                    record.UpdatedById = userId;
                    record.UpdatedDate = DateTime.Now;
                    
                    foreach (var selectedContactId in uptimeMonitor.SelectedContactIds
                        .Where(x => !record.SelectedContactIds.Contains(x)
                            && availableUptimeAlertContactIds.Contains(x)))
                    {
                        _context.UptimeMonitorAlertContacts.Add(new UptimeMonitorAlertContact
                            {
                                UptimeMonitor = record,
                                UptimeAlertContactId = selectedContactId,
                                CreatedById = userId,
                                CreatedDate = DateTime.Now
                            }
                        );
                    }
                                        
                    _context.SaveChanges();

                    uptimeMonitor.UptimeStatusTypeId = record.UptimeStatusTypeId;
                    uptimeMonitor.UptimeMonitorId = record.UptimeMonitorId;                    
                }
                else if (uptimeMonitor.UptimeMonitorId != 0)
                {
                    uptimeMonitor.ApiError = new ApiError
                    {
                        Type = "not_found",
                        Message = "Monitor not found."
                    };
                }
                else
                {
                    var alreadyExistingMonitor = _context.UptimeMonitors
                        .FirstOrDefault(x => x.UptimeMonitorTypeId == uptimeMonitor.UptimeMonitorTypeId
                            && x.Url == uptimeMonitor.Url
                            && x.UserId == userId);

                    if (alreadyExistingMonitor == null)
                    {
                        var newUptimeMonitor = new UptimeMonitor 
                            {
                                UptimeMonitorTypeId = uptimeMonitor.UptimeMonitorTypeId,
                                UptimeMonitorHttpMethodTypeId = MemoryCacheHelper.UptimeMonitorHttpMethodTypes.First(x => x.Code == HttpMethodTypes.Get).UptimeMonitorHttpMethodTypeId,
                                UserId = userId,
                                Name = StringHelpers.Truncate(uptimeMonitor.Name, 100),
                                Url = StringHelpers.Truncate(uptimeMonitor.Url, 4000),
                                Port = uptimeMonitor.Port,
                                Keyword = StringHelpers.Truncate(uptimeMonitor.Keyword, 200),
                                KeywordAlertWhenExists = uptimeMonitor.KeywordAlertWhenExists,
                                Interval = _pollingIntervals[uptimeMonitor.IntervalSliderValue - 1],
                                UptimeStatusTypeId = MemoryCacheHelper.UptimeStatusTypes.First(x => x.Code == StatusTypes.Started).UptimeStatusTypeId,
                                UptimeReasonTypeId = MemoryCacheHelper.UptimeReasonTypes.First(x => x.Code == ReasonTypes.Started).UptimeReasonTypeId,
                                CreatedById = userId,
                                CreatedDate = DateTime.Now
                            };

                            if (newUptimeMonitor.UptimeMonitorTypeId == heartbeatMonitorTypeId)
                            {
                                newUptimeMonitor.NextMonitorDate = DateTime.Now.AddMinutes(record.Interval);
                                newUptimeMonitor.HeartbeatDate = null;
                            }

                        _context.UptimeMonitors.Add(newUptimeMonitor);

                        foreach (var selectedContactId in uptimeMonitor.SelectedContactIds
                            .Where(x => availableUptimeAlertContactIds.Contains(x)))
                        {
                            _context.UptimeMonitorAlertContacts.Add(new UptimeMonitorAlertContact
                                {
                                    UptimeMonitor = newUptimeMonitor,
                                    UptimeAlertContactId = selectedContactId,
                                    CreatedById = userId,
                                    CreatedDate = DateTime.Now
                                }
                            );
                        }

                        _context.SaveChanges();

                        uptimeMonitor.UptimeStatusTypeId = newUptimeMonitor.UptimeStatusTypeId;
                        uptimeMonitor.UptimeMonitorId = newUptimeMonitor.UptimeMonitorId;                        
                    }
                    else
                    {
                        uptimeMonitor.ApiError = new ApiError
                        {
                            Type = "already_exists",
                            Message = "monitor already exists."
                        };
                    }                    
                }
            }

            if (isApiCall)
            {
                return new List<UptimeMonitorSummary>();
            }
            else
            {
                return GetUptimeMonitors();
            }
        }

        public List<UptimeMonitorSummary> RemoveUptimeMonitor(UptimeMonitor uptimeMonitor, bool isApiCall = false)
        {
            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                var record = _context.UptimeMonitors
                    .FirstOrDefault(x => x.UptimeMonitorId == uptimeMonitor.UptimeMonitorId && x.UserId == userId);
                if (record != null)
                {
                    var existingContactLinks = _context.UptimeMonitorAlertContacts
                        .Where(x => x.UptimeMonitorId == record.UptimeMonitorId)
                        .ToList();

                    _context.UptimeMonitorAlertContacts.RemoveRange(existingContactLinks);                    

                    _context.SaveChanges();

                    var existingEvents = _context.UptimeMonitorEvents
                        .Where(x => x.UptimeMonitorId == record.UptimeMonitorId)
                        .ToList();

                    _context.UptimeMonitorEvents.RemoveRange(existingEvents);                    

                    _context.SaveChanges();

                    var existingMaintenanceWindows = _context.UptimeMonitorMaintenanceWindows
                        .Where(x => x.UptimeMonitorId == record.UptimeMonitorId)
                        .ToList();

                    _context.UptimeMonitorMaintenanceWindows.RemoveRange(existingMaintenanceWindows);                    

                    _context.SaveChanges();

                    var existingPublicStatusPages = _context.UptimeMonitorPublicStatusPages
                        .Where(x => x.UptimeMonitorId == record.UptimeMonitorId)
                        .ToList();

                    _context.UptimeMonitorPublicStatusPages.RemoveRange(existingPublicStatusPages);                    

                    _context.SaveChanges();

                    var existingHistory = _context.UptimeMonitorHistory
                        .Where(x => x.UptimeMonitorId == record.UptimeMonitorId)
                        .ToList();

                    _context.UptimeMonitorHistory.RemoveRange(existingHistory);                    

                    _context.SaveChanges();

                    var existingApiKeys = _context.UptimeMonitorApiKeys
                        .Where(x => x.UptimeMonitorId == record.UptimeMonitorId)
                        .ToList();

                    _context.UptimeMonitorApiKeys.RemoveRange(existingApiKeys);                    

                    _context.SaveChanges();

                    _context.UptimeMonitors.Remove(record);
                    
                    _context.SaveChanges();
                }
                else
                {
                    uptimeMonitor.ApiError = new ApiError
                    {
                        Type = "not_found",
                        Message = "Monitor not found."
                    };
                }
            }

            if (isApiCall)
            {
                return new List<UptimeMonitorSummary>();
            }
            else
            {
                return GetUptimeMonitors();
            }
        }

        public List<UptimeMonitorSummary> ResetUptimeMonitor(UptimeMonitor uptimeMonitor, bool isApiCall = false)
        {
            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                var record = _context.UptimeMonitors
                    .FirstOrDefault(x => x.UptimeMonitorId == uptimeMonitor.UptimeMonitorId && x.UserId == userId);
                if (record != null)
                {
                    var existingEvents = _context.UptimeMonitorEvents
                        .Where(x => x.UptimeMonitorId == record.UptimeMonitorId)
                        .ToList();

                    _context.UptimeMonitorEvents.RemoveRange(existingEvents);                    

                    _context.SaveChanges();

                    var existingHistory = _context.UptimeMonitorHistory
                        .Where(x => x.UptimeMonitorId == record.UptimeMonitorId)
                        .ToList();

                    _context.UptimeMonitorHistory.RemoveRange(existingHistory);   

                    record.UptimeStatusTypeId = MemoryCacheHelper.UptimeStatusTypes.First(x => x.Code == StatusTypes.Started).UptimeStatusTypeId;
                    record.UptimeReasonTypeId = MemoryCacheHelper.UptimeReasonTypes.First(x => x.Code == ReasonTypes.Started).UptimeReasonTypeId;
                    record.ResponseTime = null;
                    record.StatusCode = null;
                    record.ResponseContent = null;
                    record.UptimeStatusTypeId = null;
                    record.UptimeReasonTypeId = null;
                    record.UptimePercentageLast30Days = null;
                    record.UptimePercentageLast7Days = null;
                    record.UptimePercentageLast24Hours = null;
                    record.UptimePercentage1DayAgo = null;
                    record.UptimePercentage2DaysAgo = null;
                    record.UptimePercentage3DaysAgo = null;
                    record.UptimePercentage4DaysAgo = null;
                    record.UptimePercentage5DaysAgo = null;
                    record.UptimePercentage6DaysAgo = null;
                    record.WasMonitorActive1DayAgo = null;
                    record.WasMonitorActive2DaysAgo = null;
                    record.WasMonitorActive3DaysAgo = null;
                    record.WasMonitorActive4DaysAgo = null;
                    record.WasMonitorActive5DaysAgo = null;
                    record.WasMonitorActive6DaysAgo = null;
                    record.NextMonitorDate = DateTime.Now.AddMinutes(uptimeMonitor.Interval);
                    record.HeartbeatDate = null;
                    record.ProcessingTimeoutDate = null;
                    record.ConfirmationTimeoutDate = null;
                    record.UptimeMonitorLocation = null;
                    record.UptimeMonitorIpAddress = null;

                    _context.SaveChanges();
                }
                else
                {
                    uptimeMonitor.ApiError = new ApiError
                    {
                        Type = "not_found",
                        Message = "Monitor not found."
                    };
                }
            }

            if (isApiCall)
            {
                return new List<UptimeMonitorSummary>();
            }
            else
            {
                return GetUptimeMonitors();
            }
        }

        public bool UptimeMonitorSetStatus(int uptimeMonitorId, bool isStarted)
        {
            var result = false;
            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                var record = _context.UptimeMonitors
                    .FirstOrDefault(x => x.UptimeMonitorId == uptimeMonitorId && x.UserId == userId);
                if (record != null)
                {
                    if (isStarted)
                    {
                        record.UptimeStatusTypeId = MemoryCacheHelper.UptimeStatusTypes.First(x => x.Code == StatusTypes.Started).UptimeStatusTypeId;
                        record.UptimeReasonTypeId = MemoryCacheHelper.UptimeReasonTypes.First(x => x.Code == StatusTypes.Started).UptimeReasonTypeId;
                    }
                    else
                    {
                        record.UptimeStatusTypeId = MemoryCacheHelper.UptimeStatusTypes.First(x => x.Code == StatusTypes.Paused).UptimeStatusTypeId;
                        record.UptimeReasonTypeId = MemoryCacheHelper.UptimeReasonTypes.First(x => x.Code == StatusTypes.Paused).UptimeReasonTypeId;
                    }

                    record.UpdatedById = userId;
                    record.UpdatedDate = DateTime.Now;

                    _context.SaveChanges();

                    var processMonitorEventsResults = _context.ProcessMonitorEventsResults
                        .FromSql("CALL ProcessMonitorEvents({0});", uptimeMonitorId).ToList();

                    result = true;
                }
            }

            return result;
        }

        public List<UptimeAlertContactSummary> UpdateUptimeAlertContact(
            UptimeAlertContact uptimeAlertContact,
            bool isApiCall = false)
        {
            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                if (uptimeAlertContact.UptimeNotificationEventTypeId == 0)
                {
                    uptimeAlertContact.UptimeNotificationEventTypeId = MemoryCacheHelper.UptimeNotificationEventTypes.First(x => x.Code == NotificationEventTypes.UpDown).UptimeNotificationEventTypeId;
                }

                var record = _context.UptimeAlertContacts
                    .FirstOrDefault(x => x.UptimeAlertContactId == uptimeAlertContact.UptimeAlertContactId 
                        && x.UserId == userId);
                if (record != null)
                {
                    record.Name = StringHelpers.Truncate(uptimeAlertContact.Name, 100);
                    record.Address = StringHelpers.Truncate(uptimeAlertContact.Address, 4000);
                    record.UptimeNotificationEventTypeId = uptimeAlertContact.UptimeNotificationEventTypeId;
                    record.SendDefaultQueryStringForWebhook = uptimeAlertContact.SendDefaultQueryStringForWebhook;
                    record.PostValue = StringHelpers.Truncate(uptimeAlertContact.PostValue, 4000);      
                    record.SendJsonForWebhook = uptimeAlertContact.SendJsonForWebhook;
                    record.SendDefaultPostForWebhook = uptimeAlertContact.SendDefaultPostForWebhook;
                    record.UptimeMobileProviderTypeId = uptimeAlertContact.UptimeMobileProviderTypeId;
                    record.UpdatedById = userId;
                    record.UpdatedDate = DateTime.Now;
                    
                    _context.SaveChanges();

                    uptimeAlertContact.UptimeAlertContactId = record.UptimeAlertContactId;
                }
                else if (uptimeAlertContact.UptimeAlertContactId != 0)
                {
                    uptimeAlertContact.ApiError = new ApiError
                    {
                        Type = "not_found",
                        Message = "Alert contact not found."
                    };
                }
                else
                {
                    var alreadyExistingAlertContact = _context.UptimeAlertContacts
                        .FirstOrDefault(x => x.UptimeAlertContactTypeId == uptimeAlertContact.UptimeAlertContactTypeId
                            && x.Address == uptimeAlertContact.Address
                            && x.UserId == userId);

                    if (alreadyExistingAlertContact == null)
                    {
                        var emailContactTypeId = MemoryCacheHelper.UptimeAlertContactTypes.First(x => x.Code == ContactTypes.Email).UptimeAlertContactTypeId;
                        var emailToSmsContactTypeId = MemoryCacheHelper.UptimeAlertContactTypes.First(x => x.Code == ContactTypes.EmailToSms).UptimeAlertContactTypeId;
                        var webhookContactTypeId = MemoryCacheHelper.UptimeAlertContactTypes.First(x => x.Code == ContactTypes.Webhook).UptimeAlertContactTypeId;
                        var activationCode = StringHelpers.GenerateShortId();

                        if (uptimeAlertContact.UptimeAlertContactTypeId == emailToSmsContactTypeId)
                        {
                            activationCode = activationCode.Substring(0, 6);
                        }

                        var newUptimeAlertContact = new UptimeAlertContact 
                            {
                                UptimeAlertContactTypeId = uptimeAlertContact.UptimeAlertContactTypeId,
                                UptimeAlertContactStatusTypeId = MemoryCacheHelper.UptimeAlertContactStatusTypes.First(x => x.Code == ContactStatusTypes.NotActivated).UptimeAlertContactStatusTypeId,
                                UserId = userId,
                                Name = StringHelpers.Truncate(uptimeAlertContact.Name, 100),
                                Address = StringHelpers.Truncate(uptimeAlertContact.Address, 4000),
                                UptimeNotificationEventTypeId = uptimeAlertContact.UptimeNotificationEventTypeId,
                                SendDefaultQueryStringForWebhook = uptimeAlertContact.SendDefaultQueryStringForWebhook,
                                PostValue = StringHelpers.Truncate(uptimeAlertContact.PostValue, 4000),
                                SendJsonForWebhook = uptimeAlertContact.SendJsonForWebhook,
                                SendDefaultPostForWebhook = uptimeAlertContact.SendDefaultPostForWebhook,
                                UptimeMobileProviderTypeId = uptimeAlertContact.UptimeMobileProviderTypeId,
                                ActivationCode = activationCode,
                                CreatedById = userId,
                                CreatedDate = DateTime.Now
                            };

                        if (newUptimeAlertContact.UptimeAlertContactTypeId == webhookContactTypeId)
                        {
                            newUptimeAlertContact.ActivationCode = null;
                            newUptimeAlertContact.IsActivated = true;
                            newUptimeAlertContact.UptimeAlertContactStatusTypeId = MemoryCacheHelper.UptimeAlertContactStatusTypes.First(x => x.Code == ContactStatusTypes.Active).UptimeAlertContactStatusTypeId;
                        }

                        _context.UptimeAlertContacts.Add(newUptimeAlertContact);

                        _context.SaveChanges();      

                        uptimeAlertContact.UptimeAlertContactId = newUptimeAlertContact.UptimeAlertContactId;
                                                
                        if (newUptimeAlertContact.UptimeAlertContactTypeId == emailToSmsContactTypeId)
                        {
                            var text = $"{newUptimeAlertContact.Name} activation code is: {activationCode}";
                            _communicationService.SendEmailToSms(newUptimeAlertContact, text);

                            _eventLogService.LogMessage(_currentUser, EventNameType.SentAlertContactActivationSms, $"SMS sent to '{newUptimeAlertContact.Address}'");
                        }
                        else if (newUptimeAlertContact.UptimeAlertContactTypeId == emailContactTypeId)
                        {
                            var subject = $"Activate Alert Contact - {_settings.SiteName}";
                            var body = "Hi," + "<br /><br />"
                                + "Click the link below to activate your alert contact: " + "<br /><br />"
                                + $"<a href=\"{_settings.SiteUrl}/Home/ActivateAlertContact?code={activationCode}\">Activate Alert Contact</a>"
                                + "<br /><br />"                   
                                + "Have a great day," + "<br /><br />"
                                + $"{_settings.SiteName}";

                            _communicationService.SendEmail(newUptimeAlertContact.Address, subject, body, isBodyHtml: true);

                            _eventLogService.LogMessage(_currentUser, EventNameType.SentAlertContactActivationEmail, $"Email sent to '{newUptimeAlertContact.Address}'");
                        }
                    }
                    else
                    {
                        uptimeAlertContact.ApiError = new ApiError
                        {
                            Type = "already_exists",
                            Message = "Alert Contact already exists."
                        };
                    }
                }
            }

            if (isApiCall)
            {
                return new List<UptimeAlertContactSummary>();
            }
            else
            {
                return GetUptimeAlertContacts();
            }
        }
        
        public List<UptimeAlertContactSummary> RemoveUptimeAlertContact(
            UptimeAlertContact uptimeAlertContact, 
            bool isApiCall = false)
        {
            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                var record = _context.UptimeAlertContacts
                    .FirstOrDefault(x => x.UptimeAlertContactId == uptimeAlertContact.UptimeAlertContactId
                        && x.UserId == userId);
                if (record != null)
                {
                    var existingMonitorLinks = _context.UptimeMonitorAlertContacts
                        .Where(x => x.UptimeAlertContactId == record.UptimeAlertContactId)
                        .ToList();

                    _context.UptimeMonitorAlertContacts.RemoveRange(existingMonitorLinks);                    

                    _context.SaveChanges();

                    _context.UptimeAlertContacts.Remove(record);
                    
                    _context.SaveChanges();
                }
                else
                {
                    uptimeAlertContact.ApiError = new ApiError
                    {
                        Type = "not_found",
                        Message = $"Alert Contact(id: {uptimeAlertContact.UptimeAlertContactId}) not found."
                    };
                }
            }

            if (isApiCall)
            {
                return new List<UptimeAlertContactSummary>();
            }
            else
            {
                return GetUptimeAlertContacts();
            }
        }

        public List<UptimeMaintenanceWindowSummary> UpdateUptimeMaintenanceWindow(
            UptimeMaintenanceWindow uptimeMaintenanceWindow,
            bool isApiCall = false)
        {
            if (uptimeMaintenanceWindow.SelectedMonitorIds == null)
            {
                uptimeMaintenanceWindow.SelectedMonitorIds = new List<int>();
            }

            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                var record = _context.UptimeMaintenanceWindows
                    .FirstOrDefault(x => x.UptimeMaintenanceWindowId == uptimeMaintenanceWindow.UptimeMaintenanceWindowId 
                        && x.UserId == userId);

                var availableUptimeMonitorIds = _context.UptimeMonitors
                    .Where(x => x.UserId == userId && uptimeMaintenanceWindow.SelectedMonitorIds.Contains(x.UptimeMonitorId))
                    .Select(x => x.UptimeMonitorId).ToList();

                if (record != null)
                {   
                    var existingMonitors = _context.UptimeMonitorMaintenanceWindows
                        .Where(x => x.UptimeMaintenanceWindowId == record.UptimeMaintenanceWindowId)                    
                        .ToList();

                    record.SelectedMonitorIds = existingMonitors.Select(x => x.UptimeMonitorId).ToList();                

                    foreach (var existingMonitorId in record.SelectedMonitorIds.Where(x => !uptimeMaintenanceWindow.SelectedMonitorIds.Contains(x)))
                    {
                        var existingMonitor = existingMonitors.FirstOrDefault(x => x.UptimeMonitorId == existingMonitorId);
                        if (existingMonitor != null)
                        {
                            _context.UptimeMonitorMaintenanceWindows.Remove(existingMonitor);
                        }
                    }
                    
                    record.Name = StringHelpers.Truncate(uptimeMaintenanceWindow.Name, 100);
                    record.StartTime = uptimeMaintenanceWindow.StartTime;
                    record.Duration = uptimeMaintenanceWindow.Duration;      
                    record.Days = StringHelpers.Truncate(uptimeMaintenanceWindow.Days, 1000);
                    record.UpdatedById = userId;
                    record.UpdatedDate = DateTime.Now;

                    foreach (var selectedMonitorId in uptimeMaintenanceWindow.SelectedMonitorIds
                        .Where(x => !record.SelectedMonitorIds.Contains(x)
                            && availableUptimeMonitorIds.Contains(x)))
                    {
                        _context.UptimeMonitorMaintenanceWindows.Add(new UptimeMonitorMaintenanceWindow
                            {
                                UptimeMaintenanceWindow = record,
                                UptimeMonitorId = selectedMonitorId,
                                CreatedById = userId,
                                CreatedDate = DateTime.Now
                            }
                        );
                    }
                    
                    _context.SaveChanges();

                    uptimeMaintenanceWindow.UptimeMaintenanceWindowTypeId = record.UptimeMaintenanceWindowTypeId;
                    uptimeMaintenanceWindow.UptimeMaintenanceWindowId = record.UptimeMaintenanceWindowId;                    
                }
                else if (uptimeMaintenanceWindow.UptimeMaintenanceWindowId != 0)
                {
                    uptimeMaintenanceWindow.ApiError = new ApiError
                    {
                        Type = "not_found",
                        Message = "Maintenance window not found."
                    };
                }
                else
                {
                    var alreadyExistingMaintenanceWindow = _context.UptimeMaintenanceWindows
                        .FirstOrDefault(x => x.UptimeMaintenanceWindowTypeId == uptimeMaintenanceWindow.UptimeMaintenanceWindowTypeId
                            && x.StartTime == uptimeMaintenanceWindow.StartTime
                            && x.Duration == uptimeMaintenanceWindow.Duration
                            && x.Days == uptimeMaintenanceWindow.Days
                            && x.UserId == userId);

                    if (alreadyExistingMaintenanceWindow == null)
                    {                        
                        var newUptimeMaintenanceWindow = new UptimeMaintenanceWindow 
                        {
                            UptimeMaintenanceWindowTypeId = uptimeMaintenanceWindow.UptimeMaintenanceWindowTypeId,
                            UserId = userId,
                            Name = StringHelpers.Truncate(uptimeMaintenanceWindow.Name, 100),
                            StartTime = uptimeMaintenanceWindow.StartTime,
                            Duration = uptimeMaintenanceWindow.Duration,
                            Days = StringHelpers.Truncate(uptimeMaintenanceWindow.Days, 1000),
                            IsActive = true,
                            CreatedById = userId,
                            CreatedDate = DateTime.Now
                        };

                        _context.UptimeMaintenanceWindows.Add(newUptimeMaintenanceWindow);

                        foreach (var selectedMonitorId in uptimeMaintenanceWindow.SelectedMonitorIds
                            .Where(x => availableUptimeMonitorIds.Contains(x)))
                        {
                            _context.UptimeMonitorMaintenanceWindows.Add(new UptimeMonitorMaintenanceWindow
                                {                                
                                    UptimeMaintenanceWindow = newUptimeMaintenanceWindow,
                                    UptimeMonitorId = selectedMonitorId,
                                    CreatedById = userId,
                                    CreatedDate = DateTime.Now
                                }
                            );
                        }

                        _context.SaveChanges();

                        uptimeMaintenanceWindow.UptimeMaintenanceWindowTypeId = newUptimeMaintenanceWindow.UptimeMaintenanceWindowTypeId;
                        uptimeMaintenanceWindow.UptimeMaintenanceWindowId = newUptimeMaintenanceWindow.UptimeMaintenanceWindowId;                    
                    }
                    else
                    {
                        uptimeMaintenanceWindow.ApiError = new ApiError
                        {
                            Type = "already_exists",
                            Message = "Maintenance Window already exists."
                        };
                    }
                }
            }

            if (isApiCall)
            {
                return new List<UptimeMaintenanceWindowSummary>();
            }
            else
            {
                return GetUptimeMaintenanceWindows();
            }
        }

        public List<UptimeMaintenanceWindowSummary> RemoveUptimeMaintenanceWindow(
            UptimeMaintenanceWindow uptimeMaintenanceWindow,
            bool isApiCall = false)
        {
            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                var record = _context.UptimeMaintenanceWindows
                    .FirstOrDefault(x => x.UptimeMaintenanceWindowId == uptimeMaintenanceWindow.UptimeMaintenanceWindowId
                        && x.UserId == userId);
                if (record != null)
                {
                    var existingMonitorLinks = _context.UptimeMonitorMaintenanceWindows
                        .Where(x => x.UptimeMaintenanceWindowId == record.UptimeMaintenanceWindowId)
                        .ToList();
                    
                    _context.UptimeMonitorMaintenanceWindows.RemoveRange(existingMonitorLinks);                    

                    _context.SaveChanges();

                    _context.UptimeMaintenanceWindows.Remove(record);
                    
                    _context.SaveChanges();
                }
                else
                {
                    uptimeMaintenanceWindow.ApiError = new ApiError
                    {
                        Type = "not_found",
                        Message = "Maintenance window not found."
                    };
                }
            }

            if (isApiCall)
            {
                return new List<UptimeMaintenanceWindowSummary>();
            }
            else
            {
                return GetUptimeMaintenanceWindows();
            }
        }

        public List<UptimePublicStatusPageSummary> UpdateUptimePublicStatusPage(
            UptimePublicStatusPage uptimePublicStatusPage,
            bool isApiCall = false)
        {
            if (uptimePublicStatusPage.SelectedMonitorIds == null)
            {
                uptimePublicStatusPage.SelectedMonitorIds = new List<int>();
            }

            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                var record = _context.UptimePublicStatusPages
                    .FirstOrDefault(x => x.UptimePublicStatusPageId == uptimePublicStatusPage.UptimePublicStatusPageId 
                        && x.UserId == userId);

                var availableUptimeMonitorIds = _context.UptimeMonitors
                    .Where(x => x.UserId == userId && uptimePublicStatusPage.SelectedMonitorIds.Contains(x.UptimeMonitorId))
                    .Select(x => x.UptimeMonitorId).ToList();

                if (record != null)
                {   
                    var existingMonitors = _context.UptimeMonitorPublicStatusPages
                        .Where(x => x.UptimePublicStatusPageId == record.UptimePublicStatusPageId)                    
                        .ToList();

                    record.SelectedMonitorIds = existingMonitors.Select(x => x.UptimeMonitorId).ToList();                

                    foreach (var existingMonitorId in record.SelectedMonitorIds.Where(x => !uptimePublicStatusPage.SelectedMonitorIds.Contains(x)))
                    {
                        var existingMonitor = existingMonitors.FirstOrDefault(x => x.UptimeMonitorId == existingMonitorId);
                        if (existingMonitor != null)
                        {
                            _context.UptimeMonitorPublicStatusPages.Remove(existingMonitor);
                        }
                    }

                    record.Name = StringHelpers.Truncate(uptimePublicStatusPage.Name, 100);
                    record.CustomDomain = StringHelpers.Truncate(uptimePublicStatusPage.CustomDomain, 1000);  

                    if (!string.IsNullOrEmpty(record.CustomDomain))        
                    {
                        record.CustomDomain = record.CustomDomain.ToLower();
                        
                        MemoryCacheHelper.RemovePublicStatusPageCustomDomains();
                    }        
                            
                    if (!string.IsNullOrEmpty(uptimePublicStatusPage.Password))
                    {
                        var hashResultSha512 = _authenticationService.GetHashedPasswordResult(uptimePublicStatusPage.Password);
                        record.PasswordHash = hashResultSha512.Digest;
                        record.PasswordSalt = hashResultSha512.Salt;
                    }
                    else
                    {
                        record.PasswordHash = null;
                        record.PasswordSalt = null;
                    }
                            
                    record.UptimeMonitorSortTypeId = uptimePublicStatusPage.UptimeMonitorSortTypeId;
                    record.HideBrandingLogo = uptimePublicStatusPage.HideBrandingLogo;
                    record.AnnouncementsHtml = uptimePublicStatusPage.AnnouncementsHtml;
                    record.UpdatedById = userId;
                    record.UpdatedDate = DateTime.Now;

                    foreach (var selectedMonitorId in uptimePublicStatusPage.SelectedMonitorIds
                        .Where(x => !record.SelectedMonitorIds.Contains(x)
                            && availableUptimeMonitorIds.Contains(x)))
                    {
                        _context.UptimeMonitorPublicStatusPages.Add(new UptimeMonitorPublicStatusPage
                            {
                                UptimePublicStatusPage = record,
                                UptimeMonitorId = selectedMonitorId,
                                CreatedById = userId,
                                CreatedDate = DateTime.Now
                            }
                        );
                    }
                    
                    _context.SaveChanges();

                    uptimePublicStatusPage.UptimePublicStatusPageId = record.UptimePublicStatusPageId;                    
                }
                else if (uptimePublicStatusPage.UptimePublicStatusPageId != 0)
                {
                    uptimePublicStatusPage.ApiError = new ApiError
                    {
                        Type = "not_found",
                        Message = "Public status page not found."
                    };
                }
                else
                {
                    var alreadyExistingPublicStatusPage = _context.UptimePublicStatusPages
                        .FirstOrDefault(x => (x.Name == uptimePublicStatusPage.Name
                                || (x.CustomDomain != null && x.CustomDomain == uptimePublicStatusPage.CustomDomain))
                            && x.UserId == userId);

                    if (alreadyExistingPublicStatusPage == null)
                    {
                        var newUptimePublicStatusPage = new UptimePublicStatusPage 
                        {
                            UserId = userId,
                            Name = StringHelpers.Truncate(uptimePublicStatusPage.Name, 100),
                            CustomDomain = StringHelpers.Truncate(uptimePublicStatusPage.CustomDomain, 1000),
                            StatusPageKey = StringHelpers.GenerateShortId(),  
                            UptimeMonitorSortTypeId = uptimePublicStatusPage.UptimeMonitorSortTypeId,
                            HideBrandingLogo = uptimePublicStatusPage.HideBrandingLogo,
                            AnnouncementsHtml = uptimePublicStatusPage.AnnouncementsHtml,
                            IsActive = true,
                            CreatedById = userId,
                            CreatedDate = DateTime.Now
                        };

                        if (!string.IsNullOrEmpty(newUptimePublicStatusPage.CustomDomain))        
                        {
                            newUptimePublicStatusPage.CustomDomain = newUptimePublicStatusPage.CustomDomain.ToLower();

                            MemoryCacheHelper.RemovePublicStatusPageCustomDomains();
                        }

                        if (!string.IsNullOrEmpty(uptimePublicStatusPage.Password))
                        {
                            var hashResultSha512 = _authenticationService.GetHashedPasswordResult(uptimePublicStatusPage.Password);
                            newUptimePublicStatusPage.PasswordHash = hashResultSha512.Digest;
                            newUptimePublicStatusPage.PasswordSalt = hashResultSha512.Salt;
                        }
                        else
                        {
                            newUptimePublicStatusPage.PasswordHash = null;
                            newUptimePublicStatusPage.PasswordSalt = null;
                        }

                        _context.UptimePublicStatusPages.Add(newUptimePublicStatusPage);

                        foreach (var selectedMonitorId in uptimePublicStatusPage.SelectedMonitorIds
                            .Where(x => availableUptimeMonitorIds.Contains(x)))
                        {
                            _context.UptimeMonitorPublicStatusPages.Add(new UptimeMonitorPublicStatusPage
                                {                                
                                    UptimePublicStatusPage = newUptimePublicStatusPage,
                                    UptimeMonitorId = selectedMonitorId,
                                    CreatedById = userId,
                                    CreatedDate = DateTime.Now
                                }
                            );
                        }

                        _context.SaveChanges();

                        uptimePublicStatusPage.UptimePublicStatusPageId = newUptimePublicStatusPage.UptimePublicStatusPageId;
                    }
                    else
                    {
                        uptimePublicStatusPage.ApiError = new ApiError
                        {
                            Type = "already_exists",
                            Message = "Public Status Page already exists."
                        };
                    }
                }
            }

            if (isApiCall)
            {
                return new List<UptimePublicStatusPageSummary>();
            }
            else
            {
                return GetUptimePublicStatusPages();
            }
        }

        public List<UptimePublicStatusPageSummary> RemoveUptimePublicStatusPage(
            UptimePublicStatusPage uptimePublicStatusPage,
            bool isApiCall = false)
        {
            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                var record = _context.UptimePublicStatusPages
                    .FirstOrDefault(x => x.UptimePublicStatusPageId == uptimePublicStatusPage.UptimePublicStatusPageId
                        && x.UserId == userId);
                if (record != null)
                {
                    var logoImages = System.IO.Directory.GetFiles(_settings.PublicStatusPageLogoBasePath, 
                        record.LogoKey + "*", System.IO.SearchOption.TopDirectoryOnly);
                    if (logoImages.Any())
                    {
                        var filePath = logoImages.First();
                        System.IO.File.Delete(filePath);
                    }

                    var existingMonitorLinks = _context.UptimeMonitorPublicStatusPages
                        .Where(x => x.UptimePublicStatusPageId == record.UptimePublicStatusPageId)
                        .ToList();

                    _context.UptimeMonitorPublicStatusPages.RemoveRange(existingMonitorLinks);                    

                    _context.SaveChanges();

                    _context.UptimePublicStatusPages.Remove(record);
                    
                    _context.SaveChanges();
                }
                else
                {
                    uptimePublicStatusPage.ApiError = new ApiError
                    {
                        Type = "not_found",
                        Message = "Public status page not found."
                    };
                }
            }

            if (isApiCall)
            {
                return new List<UptimePublicStatusPageSummary>();
            }
            else
            {
                return GetUptimePublicStatusPages();
            }
        }

        public bool UptimePublicStatusPageSetStatus(int uptimePublicStatusPageId, bool isActive)
        {
            var result = false;
            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                var record = _context.UptimePublicStatusPages
                    .FirstOrDefault(x => x.UptimePublicStatusPageId == uptimePublicStatusPageId && x.UserId == userId);
                if (record != null)
                {
                    record.IsActive = isActive;                    
                    record.UpdatedById = userId;
                    record.UpdatedDate = DateTime.Now;

                    _context.SaveChanges();

                    result = true;
                }
            }

            return result;
        }
        
        public string GetPublicStatusPageLogoKey(int uptimePublicStatusPageId)
        {
            var result = string.Empty;

            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                var record = _context.UptimePublicStatusPages
                    .FirstOrDefault(x => x.UptimePublicStatusPageId == uptimePublicStatusPageId
                        && x.UserId == userId);
                if (record != null)
                {
                    if (record.LogoKey == null)
                    {
                        record.LogoKey = record.StatusPageKey;
                    }
                    
                    record.UpdatedById = userId;
                    record.UpdatedDate = DateTime.Now;
                    
                    _context.SaveChanges();

                    result = record.LogoKey;
                }
            }

            return result;
        }

        public PublicStatusPageResult GetPublicStatusPageResult(string statusPageKey)
        {
            var result = new PublicStatusPageResult();

            var record = _context.UptimePublicStatusPages
                .FirstOrDefault(x => x.StatusPageKey == statusPageKey && x.IsActive);
            if (record != null)
            {
                result.StatusPageKey = record.StatusPageKey;
                result.Name = record.Name;                
                result.LogoKey = record.LogoKey;
                result.PasswordHash = record.PasswordHash;
                result.PasswordSalt = record.PasswordSalt;
                result.RequiresAuthentication = !string.IsNullOrEmpty(record.PasswordHash);
            }

            return result;
        }

        public List<UptimeMonitorEventSummary> GetLatestEvents(int? uptimeMonitorId = null, bool ignoreUserVerification = false)
        {
            var events = new List<UptimeMonitorEventSummary>();

            var userId = _currentUser != null ? _currentUser.UserId : 0;

            var latestEvents = (from ume in _context.UptimeMonitorEvents
                                join um in _context.UptimeMonitors on ume.UptimeMonitorId equals um.UptimeMonitorId
                                where (ignoreUserVerification == true || um.UserId == userId)
                                    && (uptimeMonitorId == null || um.UptimeMonitorId == uptimeMonitorId)
                                orderby ume.CreatedDate descending
                                select ume).Take(20).ToList();

            var uptimeMonitors = _context.UptimeMonitors
                .Where(x => (ignoreUserVerification == true || x.UserId == userId)
                    && (uptimeMonitorId == null || x.UptimeMonitorId == uptimeMonitorId)).ToList();

            foreach (var latestEvent in latestEvents)
            {
                events.Add(new UptimeMonitorEventSummary
                    {                        
                        EventName = MemoryCacheHelper.UptimeStatusTypes.First(x => x.UptimeStatusTypeId == latestEvent.UptimeStatusTypeId).Name,
                        MonitorName = uptimeMonitors.First(x => x.UptimeMonitorId == latestEvent.UptimeMonitorId).Name,
                        DateStr = latestEvent.CreatedDate.ToString("MM/dd/yyyy h:mm:ss tt"),
                        ReasonName = latestEvent.UptimeReasonTypeId != null ? 
                            MemoryCacheHelper.UptimeReasonTypes.First(x => x.UptimeReasonTypeId == latestEvent.UptimeReasonTypeId).Name :
                            string.Empty,
                        DurationStr = StringHelpers.ConvertDurationToShortString(DateTime.Now.AddSeconds(latestEvent.Duration) - DateTime.Now)
                    }
                );
            }

            return events;
        }

        public bool IsUptimeMonitorForPublicStatusPage(string statusPageKey, int uptimeMonitorId)
        {
            var result = false;

            var selectedMonitorCount = (from upsp in _context.UptimePublicStatusPages
                                        join umpsp in _context.UptimeMonitorPublicStatusPages on upsp.UptimePublicStatusPageId equals umpsp.UptimePublicStatusPageId
                                        select upsp).Count();

            if (selectedMonitorCount > 0)
            {
                var uptimeMonitor = (from um in _context.UptimeMonitors
                                     join upsp in _context.UptimePublicStatusPages on um.UserId equals upsp.UserId
                                     join umpsp in _context.UptimeMonitorPublicStatusPages on um.UptimeMonitorId equals umpsp.UptimeMonitorId
                                     where umpsp.UptimePublicStatusPageId == upsp.UptimePublicStatusPageId
                                        && upsp.StatusPageKey == statusPageKey
                                        && um.UptimeMonitorId == uptimeMonitorId
                                     select um).FirstOrDefault();
                
                if (uptimeMonitor != null)
                {
                    result = true;
                }
            }
            else
            {
                var uptimeMonitor = (from um in _context.UptimeMonitors
                                     join upsp in _context.UptimePublicStatusPages on um.UserId equals upsp.UserId                                
                                     where upsp.StatusPageKey == statusPageKey
                                        && um.UptimeMonitorId == uptimeMonitorId
                                     select um).FirstOrDefault();

                if (uptimeMonitor != null)
                {
                    result = true;
                }
            }

            return result;
        }

        public CurrentUptimeMonitorDetails GetCurrentUptimeMonitorDetails(int uptimeMonitorId, bool ignoreUserVerification = false)
        {
            var result = new CurrentUptimeMonitorDetails();

            if (_currentUser != null
                || ignoreUserVerification)
            {
                var currentUptimeMonitorDetails = _context.CurrentUptimeMonitorDetails
                    .FromSql("CALL GetCurrentUptimeMonitorDetails({0});", uptimeMonitorId).ToList();

                result = currentUptimeMonitorDetails.First();       

                result.IntervalSliderValue = _pollingIntervals.IndexOf(result.Interval) + 1;
            }

            return result;
        }

        public List<UptimeMonitorChartData> GetUptimeMonitorChartData(int uptimeMonitorId, bool ignoreUserVerification = false)
        {
            var result = new List<UptimeMonitorChartData>();

            if (_currentUser != null
                || ignoreUserVerification)
            {
                var userId = _currentUser != null ? _currentUser.UserId : 0;

                var record = _context.UptimeMonitors
                    .FirstOrDefault(x => x.UptimeMonitorId == uptimeMonitorId && (ignoreUserVerification || x.UserId == userId));
                if (record != null)
                {
                    var uptimeMonitorChartData = _context.UptimeMonitorChartData
                        .FromSql("CALL GetUptimeMonitorChartData({0});", uptimeMonitorId).ToList();

                    result = uptimeMonitorChartData;
                }
            }

            return result;
        }        

        public OverallMonitorStats GetOverallMonitorStats()
        {
            var result = new OverallMonitorStats();

            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                var overallMonitorStats = _context.OverallMonitorStats
                    .FromSql("CALL GetOverallMonitorStats({0});", userId).ToList();

                result = overallMonitorStats.First();                
            }

            return result;
        }        

        public OverallMonitorStats GetPublicStatusPageOverallMonitorStats(string statusPageKey)
        {
            var result = new OverallMonitorStats();

            var overallMonitorStats = _context.OverallMonitorStats
                .FromSql("CALL GetPublicStatusPageOverallMonitorStats({0});", statusPageKey).ToList();

            result = overallMonitorStats.First();            

            return result;
        }

        public List<CurrentUptimeMonitorDetails> GetPublicStatusPageCurrentUptimeMonitorDetails(string statusPageKey, bool ignoreUserVerification = false)
        {
            var result = new List<CurrentUptimeMonitorDetails>();

            if (_currentUser != null
                || ignoreUserVerification)
            {                
                var currentUptimeMonitorDetails = _context.CurrentUptimeMonitorDetails
                    .FromSql("CALL GetPublicStatusPageCurrentUptimeMonitorDetails({0});", statusPageKey).ToList();

                result = currentUptimeMonitorDetails;    

                foreach (var item in result)
                {
                    item.IntervalSliderValue = _pollingIntervals.IndexOf(item.Interval) + 1;            
                }
            }

            return result;
        }

        public List<UptimeMonitorApiKeySummary> UpdateUptimeMonitorApiKey(UptimeMonitorApiKey uptimeMonitorApiKey)
        {
            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                var monitorSpecificApiKeyTypeId = MemoryCacheHelper.UptimeMonitorApiKeyTypes.First(x => x.Code == ApiKeyTypes.MonitorSpecificApiKey).UptimeMonitorApiKeyTypeId;

                var record = _context.UptimeMonitorApiKeys
                    .FirstOrDefault(x => x.UptimeMonitorApiKeyId == uptimeMonitorApiKey.UptimeMonitorApiKeyId 
                        && x.UserId == userId);
                if (record != null)
                {                    
                    record.UptimeMonitorApiKeyTypeId = uptimeMonitorApiKey.UptimeMonitorId != null 
                        ? monitorSpecificApiKeyTypeId : uptimeMonitorApiKey.UptimeMonitorApiKeyTypeId;
                    record.UptimeMonitorId = uptimeMonitorApiKey.UptimeMonitorId;
                    record.UpdatedById = userId;
                    record.UpdatedDate = DateTime.Now;
                    
                    _context.SaveChanges();
                }
                else
                {
                    var newUptimeMonitorApiKey = new UptimeMonitorApiKey 
                    {
                        UptimeMonitorApiKeyTypeId = uptimeMonitorApiKey.UptimeMonitorId != null 
                            ? monitorSpecificApiKeyTypeId : uptimeMonitorApiKey.UptimeMonitorApiKeyTypeId,
                        UptimeMonitorId = uptimeMonitorApiKey.UptimeMonitorId,
                        UserId = userId,
                        ApiKey = Guid.NewGuid().ToString(),
                        CreatedById = userId,
                        CreatedDate = DateTime.Now
                    };

                    _context.UptimeMonitorApiKeys.Add(newUptimeMonitorApiKey);

                    _context.SaveChanges();
                }
            }

            return GetUptimeMonitorApiKeys();
        }

        public List<UptimeMonitorApiKeySummary> RemoveUptimeMonitorApiKey(UptimeMonitorApiKey uptimeMonitorApiKey)
        {
            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                var record = _context.UptimeMonitorApiKeys
                    .FirstOrDefault(x => x.UptimeMonitorApiKeyId == uptimeMonitorApiKey.UptimeMonitorApiKeyId
                        && x.UserId == userId);
                if (record != null)
                {
                    _context.UptimeMonitorApiKeys.Remove(record);
                    
                    _context.SaveChanges();
                }
            }

            return GetUptimeMonitorApiKeys();
        }

        public List<UptimeMonitorApiKeySummary> CreateApiKey(string code)
        {
            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                var uptimeMonitorApiKeyTypeId = MemoryCacheHelper.UptimeMonitorApiKeyTypes.First(x => x.Code == code).UptimeMonitorApiKeyTypeId;

                var newUptimeMonitorApiKey = new UptimeMonitorApiKey 
                {
                    UptimeMonitorApiKeyTypeId = uptimeMonitorApiKeyTypeId,
                    UserId = userId,
                    ApiKey = Guid.NewGuid().ToString(),
                    CreatedById = userId,
                    CreatedDate = DateTime.Now
                };

                _context.UptimeMonitorApiKeys.Add(newUptimeMonitorApiKey);

                _context.SaveChanges();                
            }

            return GetUptimeMonitorApiKeys();
        }

        public List<UptimeMonitorApiKeySummary> RemoveApiKey(string code)
        {
            if (_currentUser != null)
            {
                var userId = _currentUser.UserId;

                var uptimeMonitorApiKeyTypeId = MemoryCacheHelper.UptimeMonitorApiKeyTypes.First(x => x.Code == code).UptimeMonitorApiKeyTypeId;

                var record = _context.UptimeMonitorApiKeys
                    .FirstOrDefault(x => x.UptimeMonitorApiKeyTypeId == uptimeMonitorApiKeyTypeId
                        && x.UserId == userId);
                if (record != null)
                {
                    _context.UptimeMonitorApiKeys.Remove(record);
                    
                    _context.SaveChanges();
                }
            }

            return GetUptimeMonitorApiKeys();
        }

        public bool NotifyHeartbeatMonitor(int uptimeMonitorId, int? responseTime = null)
        {
            var result = false;

            var userId = _currentUser != null ? _currentUser.UserId : 0;

            var uptimeMonitorTypes = MemoryCacheHelper.UptimeMonitorTypes;
            var uptimeStatusTypes = MemoryCacheHelper.UptimeStatusTypes;

            var heartbeatMonitorTypeId = uptimeMonitorTypes.First(x => x.Code == MonitorTypes.Heartbeat).UptimeMonitorTypeId;

            var uptimeMonitor = _context.UptimeMonitors.FirstOrDefault(x => x.UptimeMonitorId == uptimeMonitorId
                && x.UptimeMonitorTypeId == heartbeatMonitorTypeId);
            if (uptimeMonitor != null)
            {                
                uptimeMonitor.ResponseTime = responseTime;                                
                uptimeMonitor.HeartbeatDate = DateTime.Now;

                uptimeMonitor.UpdatedById = userId;
                uptimeMonitor.UpdatedDate = DateTime.Now;

                _context.SaveChanges();

                result = true;
            }

            return result;
        }

        private User GetCurrentUser()
        {
            return (User)_httpContextAccessor.HttpContext.Items["User"];
        }
    }
}
