﻿if ($('#app').length)
{   
    var chart     

    var app = new Vue({
        el: '#app',
        data: {
            overallMonitorStats: {
                currentMonitorCount: null,
                maxMonitorCount: null,
                minInterval: null,
                upMonitorCount: null,
                downMonitorCount: null,
                pausedMonitorCount: null,
                uptimePercentageLast7Days: null,
                uptimePercentageLast24Hours: null,                
                uptimePercentage1DayAgo: null,
                uptimePercentage2DaysAgo: null,
                uptimePercentage3DaysAgo: null,
                uptimePercentage4DaysAgo: null,
                uptimePercentage5DaysAgo: null,
                uptimePercentage6DaysAgo: null,
                wasMonitorActive1DayAgo: null,	
                wasMonitorActive2DaysAgo: null,
                wasMonitorActive3DaysAgo: null,
                wasMonitorActive4DaysAgo: null,
                wasMonitorActive5DaysAgo: null,
                wasMonitorActive6DaysAgo: null,
                latestDownTime_MonitorName: null,
                latestDownTime_Date: null,
                latestDownTime_Duration: null,
                latestDownTimeMessage: null
            },
            currentUptimeMonitorDetails: [
                {
                    uptimeMonitorId: null,
                    uptimeMonitorTypeId: null,
                    uptimeMonitorTypeName: null,
                    uptimeStatusTypeId: null,
                    uptimeStatusTypeName: null,    
                    intervalSliderValue: null,                
                    averageResponseTime: null,
                    latestStatus_Date: null,
                    latestStatus_Duration: null,
                    currentStatusMessage: null,
                    name: null,
                    url: null,
                    uptimePercentageLast30Days: null,
                    uptimePercentageLast7Days: null,
                    uptimePercentageLast24Hours: null,
                    uptimePercentage1DayAgo: null,
                    uptimePercentage2DaysAgo: null,
                    uptimePercentage3DaysAgo: null,
                    uptimePercentage4DaysAgo: null,
                    uptimePercentage5DaysAgo: null,
                    uptimePercentage6DaysAgo: null,
                    wasMonitorActive1DayAgo: null,	
                    wasMonitorActive2DaysAgo: null,
                    wasMonitorActive3DaysAgo: null,
                    wasMonitorActive4DaysAgo: null,
                    wasMonitorActive5DaysAgo: null,
                    wasMonitorActive6DaysAgo: null,
                    latestDownTime_Date: null,
                    latestDownTime_Duration: null,
                    latestDownTimeMessage: null
                }
            ],
            selectedUptimeMonitorDetail: {
                uptimeMonitorId: null,
                uptimeMonitorTypeId: null,
                uptimeMonitorTypeName: null,                
                uptimeStatusTypeId: null,
                uptimeStatusTypeName: null,
                intervalSliderValue: null,
                averageResponseTime: null,
                latestStatus_Date: null,
                latestStatus_Duration: null,
                currentStatusMessage: null,
                name: null,
                url: null,
                uptimePercentageLast30Days: null,
                uptimePercentageLast7Days: null,
                uptimePercentageLast24Hours: null,
                uptimePercentage1DayAgo: null,
                uptimePercentage2DaysAgo: null,
                uptimePercentage3DaysAgo: null,
                uptimePercentage4DaysAgo: null,
                uptimePercentage5DaysAgo: null,
                uptimePercentage6DaysAgo: null,
                wasMonitorActive1DayAgo: null,	
                wasMonitorActive2DaysAgo: null,
                wasMonitorActive3DaysAgo: null,
                wasMonitorActive4DaysAgo: null,
                wasMonitorActive5DaysAgo: null,
                wasMonitorActive6DaysAgo: null,
                latestDownTime_Date: null,
                latestDownTime_Duration: null,
                latestDownTimeMessage: null
            },
            uptimeMonitorChartData: [
                {
                    time: null,
                    responseTime: null
                }
            ],
            latestEvents: [
                {
                    eventName: null,
                    monitorName: null,
                    dateStr: null,
                    reasonName: null,
                    durationStr: null
                }
            ],
        },
        methods: {
            getFormattedMonitorChartData: function () {
                let xValues = []
                let yValues = []
                this.uptimeMonitorChartData.forEach(function(item, index) {
                    xValues.push(item.time)
                    yValues.push(item.responseTime)                    
                })

                return { xValues, yValues }
            },
            drawUptimeMonitorChart: function () {        
                if ($('#uptimeMonitorChart').length) {        
                    $('#uptimeMonitorChart').show()
                    let ctx = document.getElementById('uptimeMonitorChart').getContext('2d')
                    let monitorData = this.getFormattedMonitorChartData()
                    if (chart) {
                        chart.destroy()
                    }
                    chart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: monitorData.xValues,
                            datasets: [
                                {
                                    label: 'Milliseconds',
                                    backgroundColor: 'lightblue',
                                    borderColor: 'black',
                                    pointBorderColor: 'black',
                                    pointBackgroundColor: 'yellow',
                                    data: monitorData.yValues,
                                    fill: true,
                                    borderWidth: 2,
                                    type: 'line',
                                    pointRadius: 4,
                                    tension: 0
                                }
                            ]
                        },
                        options: {
                            responsive: true,
                            maintainAspectRatio: false,
                            title: {
                                display: true,
                                text: 'Response Time (Past 24 Hours)'
                            },
                            tooltips: {
                                intersect: false,
                                mode: 'index'
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        maxTicksLimit: 5
                                    }
                                }],
                                xAxes: [{
                                    ticks: {
                                        maxTicksLimit: 12
                                    }
                                }]
                            }
                        }
                    })
                }
            },
            getPercentHistoryClassColor: function (percentage, wasMonitorActive, uptimeMonitorTypeName) {
                let result = ''

                percentage = percentage * 100

                if (percentage >= 98) {
                    result = 'success'
                }
                else if (percentage > 1) {
                    result = 'warning'
                }
                else if (percentage <= 1 && uptimeMonitorTypeName === 'Keyword') {
                    result = 'warning'
                }
                else if (wasMonitorActive) {
                    result = 'warning'
                }      
                else {
                    result = 'dark'
                }           

                return result
            },
            getStatusClassColor: function (uptimeStatusTypeName) {
                let result = ''

                if (uptimeStatusTypeName === 'Up') {
                    result = 'success'
                }
                else if (uptimeStatusTypeName === 'Down' || uptimeStatusTypeName === 'Likely Down') {
                    result = 'danger'
                }
                else {
                    result = 'dark'
                }

                return result
            }
        },
        computed: {
            hasValidLatestEvents: function () {
                function latestEventMatches(value, index, self) { 
                    return value.eventName != null
                }             

                let latestEvents = this.latestEvents.filter(latestEventMatches)                

                return latestEvents.length > 0
            },
            monitoringIntervalText: function () {
                let result = ''                
                let intervalSliderValue = this.selectedUptimeMonitorDetail.intervalSliderValue
                if (intervalSliderValue != null) {
                    if (intervalSliderValue == 1)
                    {
                        result = 'Checked every minute'
                    }
                    else if (intervalSliderValue >= 2 && intervalSliderValue <= 13)
                    {
                        result = 'Checked every ' + pollingIntervals[intervalSliderValue - 1] + ' minutes'
                    }
                    else if (intervalSliderValue == 14 ) {
                        result = 'Checked every hour'
                    }
                    else if (intervalSliderValue > 14 ) {
                        result = 'Checked every ' + pollingIntervals[intervalSliderValue - 1] / 60 + ' hours'
                    }
                }

                return result
            }
        }
    })


    $(function() {
        $('#app').show()   
        
        if (typeof overallMonitorStats !== 'undefined') {
            app.overallMonitorStats = overallMonitorStats
            app.currentUptimeMonitorDetails = currentUptimeMonitorDetails
        }

        if (typeof selectedUptimeMonitorChartData !== 'undefined') {
            app.selectedUptimeMonitorDetail = selectedUptimeMonitorDetail
            app.uptimeMonitorChartData = selectedUptimeMonitorChartData
            app.latestEvents = selectedUptimeMonitorEventSummary
        
            app.drawUptimeMonitorChart()
        }
    })
}