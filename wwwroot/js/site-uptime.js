﻿if ($('#app').length)
{
    Vue.component('uptime-monitor-type', {
        props: ['uptimeMonitorType'],
        template: '<option>{{ uptimeMonitorType.name }}</option>'
    })

    Vue.component('v-select', VueSelect.VueSelect)

    var siteUrl
    var chart
    var upMonitorCountChart
    var downMonitorCountChart
    var pausedMonitorCountChart
    
    var app = new Vue({
        el: '#app',
        data: {
            siteUrl: siteUrl,
            currentScreen: 'Dashboard',
            isProcessing: false,
            loadingModalObj: null,
            isLoadError: false,
            isUpdateError: false,
            userDetails: {
                firstName: null,
                lastName: null,
                email: null,
                securityQuestion: null,
                twoFactorAuthenticationTypeId: null,
                userTimeZoneTypeId: null,
                currentPassword: null,
                newPassword: null,
                repeatNewPassword: null,
                newEmail: null,
            },
            isUserDetailsUpdateError: false,
            isUserDetailsProcessing: false,
            isUserDetailsUpdateSuccess: false,
            isUserPasswordUpdateError: false,
            userPasswordUpdateErrorMessage: null,
            isUserPasswordProcessing: false,
            isUserPasswordUpdateSuccess: false,
            isUserEmailUpdateError: false,
            userEmailUpdateErrorMessage: null,
            isUserEmailProcessing: false,
            isUserEmailUpdateSuccess: false,
            overallMonitorStats: {
                currentMonitorCount: null,
                maxMonitorCount: null,
                minInterval: null,
                upMonitorCount: null,
                downMonitorCount: null,
                pausedMonitorCount: null,
                uptimePercentageLast30Days: null,
                uptimePercentageLast7Days: null,
                uptimePercentageLast24Hours: null,
                uptimePercentage1DayAgo: null,
                uptimePercentage2DaysAgo: null,
                uptimePercentage3DaysAgo: null,
                uptimePercentage4DaysAgo: null,
                uptimePercentage5DaysAgo: null,
                uptimePercentage6DaysAgo: null,
                wasMonitorActive1DayAgo: null,	
                wasMonitorActive2DaysAgo: null,
                wasMonitorActive3DaysAgo: null,
                wasMonitorActive4DaysAgo: null,
                wasMonitorActive5DaysAgo: null,
                wasMonitorActive6DaysAgo: null,
                latestDownTime_MonitorName: null,
                latestDownTime_Date: null,
                latestDownTime_Duration: null,
                latestDownTimeMessage: null
            },
            selectedUptimeMonitorDetail: {
                uptimeMonitorId: null,
                uptimeMonitorTypeId: null,
                uptimeMonitorTypeName: null,                
                uptimeStatusTypeId: null,
                uptimeStatusTypeName: null,
                intervalSliderValue: null,
                averageResponseTime: null,
                latestStatus_Date: null,
                latestStatus_Duration: null,
                currentStatusMessage: null,
                name: null,
                url: null,
                uptimePercentageLast30Days: null,
                uptimePercentageLast7Days: null,
                uptimePercentageLast24Hours: null,
                uptimePercentage1DayAgo: null,
                uptimePercentage2DaysAgo: null,
                uptimePercentage3DaysAgo: null,
                uptimePercentage4DaysAgo: null,
                uptimePercentage5DaysAgo: null,
                uptimePercentage6DaysAgo: null,
                latestDownTime_Date: null,
                latestDownTime_Duration: null,
                latestDownTimeMessage: null
            },
            editUptimeMonitorParameters: {           
                uptimeMonitorId: null,
                uptimeMonitorTypeId: null,
                name: null,
                url: null,
                selectedPortOption: null,
                port: null,
                keyword: null,
                keywordAlertWhenExists: null,
                intervalSliderValue: null,
                selectedContactIds: []
            },
            selectedUptimeAlertContactIds: [],
            editUptimeAlertContactParameters: {           
                uptimeAlertContactId: null,
                uptimeAlertContactTypeId: null,
                name: null,
                address: null,
                uptimeNotificationEventTypeId: null,
                sendDefaultQueryStringForWebhook: null,
                postValue: null,
                sendJsonForWebhook: null,
                sendDefaultPostForWebhook: null,
                uptimeMobileProviderTypeId: null
            },
            registerTwoFactorTotpParameters: {
                manualSetupKey: null,
                qrCodeImage: null,
                verificationCode: null
            },
            registerTwoFactorU2fParameters: {
                deviceName: null,
                errorMessage: null,
                hasDuplicateError: false,
                stepNumber: 0
            },
            registerTwoFactorSecurityQuestionParameters: {
                display: false,
                twoFactorAuthenticationTypeId: null,
                userSecurityQuestionTypeId: '',
                userSecurityAnswer: null
            },
            editUptimeMaintenanceWindowParameters: {
                uptimeMaintenanceWindowId: null,
                uptimeMaintenanceWindowTypeId: null,
                name: null,
                startTime: null,
                duration: null,
                days: null,
                isActive: null,
                selectedMonitorIds: [],
                assignToAllMonitors: null
            },
            selectedMaintenanceWindowUptimeMonitorIds: [],
            editUptimePublicStatusPageParameters: {
                uptimePublicStatusPageId: null,
                name: null,
                customDomain: null,
                password: null,
                uptimeMonitorSortTypeId: null,
                hideBrandingLogo: null,
                announcementsHtml: null,
                isActive: null,
                selectedMonitorIds: [],
                assignToAllMonitors: null
            },
            selectedPublicStatusPageUptimeMonitorIds: [],
            editUptimeMonitorApiKeyParameters: {
                uptimeMonitorApiKeyId: null,
                uptimeMonitorId: null
            },
            uptimeMonitors: [
                {
                    uptimeMonitorId: null,
                    uptimeMonitorTypeId: null,
                    name: null,
                    displayName: null,
                    url: null,
                    uptimeStatusTypeId: null,
                    uptimePercentage: null,
                    showConfiguration: false
                }
            ],
            uptimeAlertContacts: [
                {
                    uptimeAlertContactId: null,
                    uptimeAlertContactTypeId: null,
                    uptimeAlertContactStatusTypeId: null,
                    name: null,
                    displayName: null,
                    address: null,
                    requiresActivation: null
                }
            ],
            latestEvents: [
                {
                    eventName: null,
                    monitorName: null,
                    dateStr: null,
                    reasonName: null,
                    durationStr: null
                }
            ],
            uptimeMaintenanceWindows: [
                {
                    uptimeMaintenanceWindowId: null,
                    name: null,
                    startTime: null,
                    duration: null,
                    days: null,
                    isActive: null
                }
            ],
            uptimePublicStatusPages: [
                {
                    uptimePublicStatusPageId: null,
                    name: null,
                    url: null,
                    isActive: null
                }
            ],
            uptimeMonitorApiKeys: [
                {
                    uptimeMonitorApiKeyId: null,
                    uptimeMonitorApiKeyTypeId: null,
                    uptimeMonitorId: null,
                    apiKey: null
                }
            ],
            uptimeMonitorChartData: [
                {
                    time: null,
                    responseTime: null
                }
            ],
            uptimeMonitorTypes: [
                {
                    uptimeMonitorTypeId: null,
                    name: null,
                    code: null,
                    order: null
                }
            ],
            uptimeStatusTypes: [
                {
                    uptimeStatusTypeId: null,
                    name: null,
                    code: null,
                    order: null
                }
            ],
            uptimeAlertContactTypes: [
                {
                    uptimeAlertContactTypeId: null,
                    name: null,
                    code: null,
                    order: null
                }
            ],
            uptimeAlertContactStatusTypes: [
                {
                    uptimeAlertContactStatusTypeId: null,
                    name: null,
                    code: null,
                    order: null
                }
            ],
            uptimeMobileProviderTypes: [
                {
                    uptimeMobileProviderTypeId: null,
                    name: null,
                    address: null
                }
            ],
            uptimeNotificationEventTypes: [
                {
                    uptimeNotificationEventTypeId: null,
                    name: null,
                    code: null,
                    order: null
                }
            ],
            uptimeMonitorApiKeyTypes: [
                {
                    uptimeMonitorApiKeyTypeId: null,
                    name: null,
                    code: null
                }
            ],
            uptimeMaintenanceWindowTypes: [
                {
                    uptimeMaintenanceWindowTypeId: null,
                    name: null,
                    code: null,
                    order: null
                }
            ],
            uptimeMonitorSortTypes: [
                {
                    uptimeMonitorSortTypeId: null,
                    name: null,
                    code: null,
                    order: null
                }
            ],
            userTimeZoneTypes: [
                {
                    userTimeZoneTypeId: null,
                    name: null,
                    code: null,
                    order: null
                }
            ],
            userSecurityQuestionTypes: [
                {
                    userSecurityQuestionTypeId: null,
                    name: null,
                    code: null,
                    order: null
                }
            ],       
            twoFactorAuthenticationTypes: [
                {
                    twoFactorAuthenticationTypeId: null,
                    name: null,
                    code: null,
                    order: null
                }
            ],  
            twoFactorU2fDevices: [
                {
                    userU2fDeviceId: null,
                    name: null
                }
            ],   
            portOptionGroups: {
                'Common Ports': [
                    { text: 'HTTP (80)', value: 80 },
                    { text: 'HTTPS (443)', value: 443 },
                    { text: 'FTP (21)', value: 21 },
                    { text: 'SMTP (25)', value: 25 },
                    { text: 'POP3 (110)', value: 110 },
                    { text: 'IMAP (143)', value: 143 }
                ],
                'Custom Port': [
                    { text: 'Custom Port', value: -1 }
                ]
            }
        },
        methods: {            
            getUptimeAlertContacts: function () {
                app.showLoader()
                $.ajax(
                    {
                        url: baseUrl + 'Secure/GetUptimeAlertContacts', 
                        type: 'POST',
                        success: function(result) {
                            app.hideLoader()
                            
                            app.uptimeAlertContacts = result
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            app.hideLoader()
                        }
                    }
                )
            },            
            getLatestEvents: function () {
                app.showLoader()
                $.ajax(
                    {
                        url: baseUrl + 'Secure/GetLatestEvents', 
                        type: 'POST',
                        data: {
                            uptimeMonitorId: this.selectedUptimeMonitorDetail.uptimeMonitorId != null ? 
                                this.selectedUptimeMonitorDetail.uptimeMonitorId : null
                        },
                        success: function(result) {
                            app.hideLoader()

                            app.latestEvents = result
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            app.hideLoader()
                        }
                    }
                )
            },
            getUptimeMonitorChartData: function () {
                app.showLoader()
                $.ajax(
                    {
                        url: baseUrl + 'Secure/GetUptimeMonitorChartData', 
                        type: 'POST',
                        data: {
                            uptimeMonitorId: this.selectedUptimeMonitorDetail.uptimeMonitorId
                        },
                        success: function(result) {
                            app.hideLoader()

                            app.uptimeMonitorChartData = result
                            app.drawUptimeMonitorChart()
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            app.hideLoader()
                        }
                    }
                )
            },
            setPageObjects: function (pageObjects) {
                app.uptimeMonitorTypes = pageObjects.uptimeMonitorTypes
                app.uptimeStatusTypes = pageObjects.uptimeStatusTypes
                app.uptimeAlertContactTypes = pageObjects.uptimeAlertContactTypes
                app.uptimeAlertContactStatusTypes = pageObjects.uptimeAlertContactStatusTypes
                app.uptimeMobileProviderTypes = pageObjects.uptimeMobileProviderTypes
                app.uptimeNotificationEventTypes = pageObjects.uptimeNotificationEventTypes        
                app.uptimeMonitorApiKeyTypes = pageObjects.uptimeMonitorApiKeyTypes
                app.uptimeMaintenanceWindowTypes = pageObjects.uptimeMaintenanceWindowTypes
                app.uptimeMonitorSortTypes = pageObjects.uptimeMonitorSortTypes
                app.userTimeZoneTypes = pageObjects.userTimeZoneTypes
                app.userSecurityQuestionTypes = pageObjects.userSecurityQuestionTypes
                app.twoFactorU2fDevices = pageObjects.twoFactorU2fDevices
                app.twoFactorAuthenticationTypes = pageObjects.twoFactorAuthenticationTypes
                app.userDetails = pageObjects.userDetails
                app.overallMonitorStats = pageObjects.overallMonitorStats
                app.uptimeMonitors = pageObjects.uptimeMonitors
                app.uptimeAlertContacts = pageObjects.uptimeAlertContacts
                app.uptimeMaintenanceWindows = pageObjects.uptimeMaintenanceWindows
                app.uptimePublicStatusPages = pageObjects.uptimePublicStatusPages
                app.uptimeMonitorApiKeys = pageObjects.uptimeMonitorApiKeys
                app.latestEvents = pageObjects.latestEvents

                app.drawOverallStatusCharts()            
            },
            getUserDetails: function () {
                app.showLoader()
                $.ajax(
                    {
                        url: baseUrl + 'Secure/GetUserDetails', 
                        type: 'POST',
                        success: function(result) {
                            app.hideLoader()

                            app.userDetails = result

                            if (app.isTwoFactorAuthenticationType('U2f')) {
                                app.getTwoFactorU2fDevices()
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            app.hideLoader()
                        }
                    }
                )
            },
            getOverallMonitorStats: function () {
                app.showLoader()
                $.ajax(
                    {
                        url: baseUrl + 'Secure/GetOverallMonitorStats', 
                        type: 'POST',
                        success: function(result) {
                            app.hideLoader()

                            app.overallMonitorStats = result
                            app.drawOverallStatusCharts()
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            app.hideLoader()
                        }
                    }
                )
            },
            updateUptimeMonitor: function () {
                let form = $('#updateUptimeMonitorForm')[0]                
                if (form.checkValidity() === true) {
                    this.isUpdateError = false
                    this.showLoader()

                    if (this.editUptimeMonitorParameters.selectedPortOption !== -1) {
                        this.editUptimeMonitorParameters.port = this.editUptimeMonitorParameters.selectedPortOption
                    }

                    this.editUptimeMonitorParameters.selectedContactIds = this.selectedUptimeAlertContactIds

                    $.ajax(
                        {
                            url: baseUrl + 'Secure/UpdateUptimeMonitor', 
                            type: 'POST',
                            data: this.editUptimeMonitorParameters,
                            success: function(result) {
                                app.uptimeMonitors = result                                
                                $('#updateUptimeMonitorModal').modal('hide')
                                app.hideLoader()
                                form.classList.remove('was-validated')
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.hideLoader()
                                app.isUpdateError = true
                            }
                        }
                    )
                }

                form.classList.add('was-validated')
            },
            removeUptimeMonitor: function () {
                this.isUpdateError = false
                if (confirm('Are you sure you want to remove this monitor?')) {
                    this.showLoader()

                    $.ajax(
                        {
                            url: baseUrl + 'Secure/RemoveUptimeMonitor', 
                            type: 'POST',
                            data: this.editUptimeMonitorParameters,
                            success: function(result) {
                                app.uptimeMonitors = result                                
                                $('#updateUptimeMonitorModal').modal('hide')
                                $('#removeUptimeMonitorModal').modal('hide')
                                app.hideLoader()
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.hideLoader()
                                app.isUpdateError = true
                            }
                        }
                    )
                }
            },
            resetUptimeMonitor: function () {
                this.isUpdateError = false
                if (confirm('Are you sure you want to reset this monitor?')) {
                    this.showLoader()

                    $.ajax(
                        {
                            url: baseUrl + 'Secure/ResetUptimeMonitor', 
                            type: 'POST',
                            data: this.editUptimeMonitorParameters,
                            success: function(result) {
                                app.uptimeMonitors = result                                
                                $('#resetUptimeMonitorModal').modal('hide')
                                app.hideLoader()
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.hideLoader()
                                app.isUpdateError = true
                            }
                        }
                    )
                }
            },
            showEditForUptimeMonitor: function (uptimeMonitorId) {
                this.isUpdateError = false
                function matches(value, index, self) { 
                    return value.uptimeMonitorId === uptimeMonitorId
                }             

                let form = $('#updateUptimeMonitorForm')[0]   
                form.classList.remove('was-validated')
                app.selectedUptimeAlertContactIds = []

                let parameters = this.uptimeMonitors.filter(matches)
                if (parameters.length) {
                    let selection = parameters[0]
                    
                    this.showLoader()
                    $.ajax(
                        {
                            url: baseUrl + 'Secure/GetUptimeMonitor', 
                            type: 'POST',
                            data: {
                                uptimeMonitorId: selection.uptimeMonitorId
                            },
                            selection: selection,
                            success: function(result) {
                                app.editUptimeMonitorParameters = result

                                if (!app.isCommonPort(app.editUptimeMonitorParameters.selectedPortOption)) {
                                    app.editUptimeMonitorParameters.selectedPortOption = -1
                                }
                                
                                app.selectedUptimeAlertContactIds = app.editUptimeMonitorParameters.selectedContactIds                                

                                app.hideLoader()
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.hideLoader()
                                app.isUpdateError = true
                            }
                        }
                    )                    
                }
                else {
                    this.editUptimeMonitorParameters = {
                        uptimeMonitorId: 0,
                        uptimeMonitorTypeId: '',         
                        name: null,
                        url: null,
                        selectedPortOption: null,
                        port: null,
                        keyword: null,
                        keywordAlertWhenExists: null,
                        intervalSliderValue: 5,
                        selectedContactIds: []
                    }
                }
            },
            showUptimeMonitorDetails: function (uptimeMonitorId) {
                function matches(value, index, self) { 
                    return value.uptimeMonitorId === uptimeMonitorId
                }             

                let parameters = this.uptimeMonitors.filter(matches)
                if (parameters.length) {
                    let selection = parameters[0]

                    this.showLoader()
                    $.ajax(
                        {
                            url: baseUrl + 'Secure/GetCurrentUptimeMonitorDetails', 
                            type: 'POST',
                            data: {
                                uptimeMonitorId: selection.uptimeMonitorId
                            },
                            uptimeMonitorId: uptimeMonitorId,
                            success: function(result) {
                                app.selectedUptimeMonitorDetail = result

                                app.getLatestEvents()
                                app.getUptimeMonitorChartData()

                                app.displayScreen('Dashboard_' + uptimeMonitorId, false)

                                app.hideLoader()
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.hideLoader()
                            }
                        }
                    )                    
                }                
            },
            isEditForUptimeMonitorType: function (type) {
                return this.editUptimeMonitorParameters.uptimeMonitorTypeId != null
                    && this.editUptimeMonitorParameters.uptimeMonitorTypeId != '' 
                    && this.getUptimeMonitorType(this.editUptimeMonitorParameters.uptimeMonitorTypeId).code == type
            },
            changeUptimeMonitorType:  function () {
                if (this.isEditForUptimeMonitorType('Http') || this.isEditForUptimeMonitorType('Keyword')) {
                    if (this.editUptimeMonitorParameters.url == null
                        || this.editUptimeMonitorParameters.url.lastIndexOf('http', 0) !== 0) {
                        this.editUptimeMonitorParameters.url = 'http://'
                    }
                }
                else if (this.editUptimeMonitorParameters.url != null 
                    && this.editUptimeMonitorParameters.url.lastIndexOf('http', 0) === 0) {
                    this.editUptimeMonitorParameters.url = null
                }

                let form = $('#updateUptimeMonitorForm')[0]
                form.classList.remove('was-validated')
            },
            changeSelectedPortOption: function () {
                if (this.editUptimeMonitorParameters.selectedPortOption === -1) {
                    this.editUptimeMonitorParameters.port = null
                }

                let form = $('#updateUptimeMonitorForm')[0]
                form.classList.remove('was-validated')
            },
            uptimeMonitorSetStatus: function (uptimeMonitorId, isStarted) {
                this.isUpdateError = false
                this.showLoader()

                $.ajax(
                    {
                        url: baseUrl + 'Secure/UptimeMonitorSetStatus', 
                        type: 'POST',
                        data: {
                            uptimeMonitorId: uptimeMonitorId,
                            isStarted: isStarted
                        },
                        uptimeMonitorId: uptimeMonitorId,
                        success: function(result) {
                            
                            if (result == true)
                            {
                                let uptimeMonitorId = this.uptimeMonitorId
                                function matches(value, index, self) { 
                                    return value.uptimeMonitorId === uptimeMonitorId
                                }             
                
                                let parameters = app.uptimeMonitors.filter(matches)
                                if (parameters.length) {
                                    let selection = parameters[0]
                                    if (isStarted) {
                                        selection.uptimeStatusTypeId = app.getUptimeStatusTypeByCode('Started').uptimeStatusTypeId
                                    }
                                    else {
                                        selection.uptimeStatusTypeId = app.getUptimeStatusTypeByCode('Paused').uptimeStatusTypeId
                                    }
                                }
                            }

                            app.hideLoader()
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            app.hideLoader()
                            app.isUpdateError = true
                        }
                    }
                )
            },
            uptimePublicStatusPageSetStatus: function (uptimePublicStatusPageId, isActive) {
                this.isUpdateError = false
                this.showLoader()

                $.ajax(
                    {
                        url: baseUrl + 'Secure/UptimePublicStatusPageSetStatus', 
                        type: 'POST',
                        data: {
                            uptimePublicStatusPageId: uptimePublicStatusPageId,
                            isActive: isActive
                        },
                        uptimePublicStatusPageId: uptimePublicStatusPageId,
                        success: function(result) {
                            
                            if (result == true)
                            {
                                let uptimePublicStatusPageId = this.uptimePublicStatusPageId
                                function matches(value, index, self) { 
                                    return value.uptimePublicStatusPageId === uptimePublicStatusPageId
                                }             
                
                                let parameters = app.uptimePublicStatusPages.filter(matches)
                                if (parameters.length) {
                                    let selection = parameters[0]
                                    selection.isActive = isActive
                                }
                            }
                            
                            app.hideLoader()
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            app.hideLoader()
                            app.isUpdateError = true
                        }
                    }
                )
            },
            updateUptimeAlertContact: function () {
                let form = $('#updateUptimeAlertContactForm')[0]                
                if (form.checkValidity() === true) {
                    this.isUpdateError = false
                    this.showLoader()

                    let uptimeAlertContactId = this.editUptimeAlertContactParameters.uptimeAlertContactId
                    let isNewEmailContact = uptimeAlertContactId === 0 
                        && this.getUptimeAlertContactType(this.editUptimeAlertContactParameters.uptimeAlertContactTypeId).code == 'Email'

                    let isNewEmailToSmsContact = uptimeAlertContactId === 0 
                        && this.getUptimeAlertContactType(this.editUptimeAlertContactParameters.uptimeAlertContactTypeId).code == 'EmailToSms'

                    $.ajax(
                        {
                            url: baseUrl + 'Secure/UpdateUptimeAlertContact', 
                            type: 'POST',
                            data: this.editUptimeAlertContactParameters,
                            isNewEmailContact: isNewEmailContact,
                            isNewEmailToSmsContact: isNewEmailToSmsContact,
                            success: function(result) {
                                app.uptimeAlertContacts = result.uptimeAlertContacts
                                $('#updateUptimeAlertContactModal').modal('hide')
                                app.hideLoader()
                                form.classList.remove('was-validated')

                                if (this.isNewEmailContact) {
                                    $('#verifyEmailModal').modal('show')
                                }
                                else if (this.isNewEmailToSmsContact) {
                                    app.showEditForActivateSMSAlertContact(result.uptimeAlertContactId)
                                    $('#activateSMSAlertContactModal').modal('show')
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.hideLoader()
                                app.isUpdateError = true
                            }
                        }
                    )
                }

                form.classList.add('was-validated')
            },
            removeUptimeAlertContact: function () {
                this.isUpdateError = false
                if (confirm('Are you sure you want to remove this alert contact?')) {
                    this.showLoader()

                    $.ajax(
                        {
                            url: baseUrl + 'Secure/RemoveUptimeAlertContact', 
                            type: 'POST',
                            data: this.editUptimeAlertContactParameters,
                            success: function(result) {
                                app.uptimeAlertContacts = result                                
                                $('#updateUptimeAlertContactModal').modal('hide')
                                app.hideLoader()
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.hideLoader()
                                app.isUpdateError = true
                            }
                        }
                    )
                }
            },
            showEditForUptimeAlertContact: function (uptimeAlertContactId) {
                this.isUpdateError = false
                function matches(value, index, self) { 
                    return value.uptimeAlertContactId === uptimeAlertContactId
                }             

                let form = $('#updateUptimeAlertContactForm')[0]   
                form.classList.remove('was-validated')

                let parameters = this.uptimeAlertContacts.filter(matches)
                if (parameters.length) {
                    let selection = parameters[0]

                    this.showLoader()
                    $.ajax(
                        {
                            url: baseUrl + 'Secure/GetUptimeAlertContact', 
                            type: 'POST',
                            data: {
                                uptimeAlertContactId: selection.uptimeAlertContactId
                            },
                            success: function(result) {
                                app.editUptimeAlertContactParameters = result                               

                                app.hideLoader()
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.hideLoader()
                                app.isUpdateError = true
                            }
                        }
                    )
                }
                else {
                    this.editUptimeAlertContactParameters = {
                        uptimeAlertContactId: 0,
                        uptimeAlertContactTypeId: '',         
                        name: null,
                        address: null,
                        uptimeNotificationEventTypeId: null,
                        sendDefaultQueryStringForWebhook: null,
                        postValue: null,
                        sendJsonForWebhook: null,
                        sendDefaultPostForWebhook: null,
                        uptimeMobileProviderTypeId: null
                    }
                }
            },
            isEditForUptimeAlertContactType: function (type) {
                return this.editUptimeAlertContactParameters.uptimeAlertContactTypeId != null
                    && this.editUptimeAlertContactParameters.uptimeAlertContactTypeId != '' 
                    && this.getUptimeAlertContactType(this.editUptimeAlertContactParameters.uptimeAlertContactTypeId).code == type
            },
            changeUptimeAlertContactType:  function () {
                let form = $('#updateUptimeAlertContactForm')[0]
                form.classList.remove('was-validated')
            },
            showEditForActivateSMSAlertContact: function (uptimeAlertContactId) {
                this.isUpdateError = false
                function matches(value, index, self) { 
                    return value.uptimeAlertContactId === uptimeAlertContactId
                }             

                let form = $('#updateUptimeAlertContactForm')[0]   
                form.classList.remove('was-validated')

                let parameters = this.uptimeAlertContacts.filter(matches)
                if (parameters.length) {
                    let selection = parameters[0]
                    this.editUptimeAlertContactParameters = {
                        uptimeAlertContactId: selection.uptimeAlertContactId,
                        name: selection.name,
                        activationCode: null
                    }
                }

                setTimeout(function() {
                    $('#ActivationCode').focus()
                }, 500)
            },
            activateSMSAlertContact: function () {
                let form = $('#activateSMSAlertContactForm')[0]                
                
                this.isUpdateError = false
                this.showLoader()

                $.ajax(
                    {
                        url: baseUrl + 'Secure/ActivateAlertContact', 
                        type: 'POST',
                        data: {
                            code: this.editUptimeAlertContactParameters.activationCode,
                            uptimeAlertContactId: this.editUptimeAlertContactParameters.uptimeAlertContactId
                        },
                        success: function(result) {                            
                            if (result === true) {
                                app.getUptimeAlertContacts()
                                $('#activateSMSAlertContactModal').modal('hide')
                                app.hideLoader()
                                form.classList.remove('was-validated')
                            }
                            else {
                                app.hideLoader()
                                app.isUpdateError = true
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            app.hideLoader()
                            app.isUpdateError = true
                        }
                    }
                )
            },
            showRegisterForTwoFactorTotp: function () {
                this.registerTwoFactorTotpParameters = {
                    manualSetupKey: null,
                    qrCodeImage: null,
                    verificationCode: null
                }         

                this.getTwoFactorTotpRegistrationQRCode()                       
            },
            getTwoFactorTotpRegistrationQRCode: function () {         
                this.isLoadError = false
                this.isUpdateError = false       
                this.showLoader()

                $.ajax(
                    {
                        url: baseUrl + 'Secure/GetTwoFactorTotpRegistrationQRCode', 
                        type: 'POST',
                        success: function(result) {
                            app.registerTwoFactorTotpParameters = result
                            app.hideLoader()
                            $('#registerTwoFactorTotpModal').modal('show')
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            app.hideLoader()
                            app.isLoadError = true
                        }
                    }
                )
            },
            verifyTwoFactorTotpCode: function () {
                let form = $('#registerTwoFactorTotpForm')[0]                
                if (form.checkValidity() === true) {
                    this.isUpdateError = false
                    this.showLoader()

                    $.ajax(
                        {
                            url: baseUrl + 'Secure/ValidateTwoFactorTotpRegistrationCode', 
                            type: 'POST',
                            data: { 
                                code: this.registerTwoFactorTotpParameters.verificationCode 
                            },
                            success: function(result) {
                                if (result === true) {                                
                                    app.hideLoader()
                                    form.classList.remove('was-validated')
                                    app.registerTwoFactorSecurityQuestionParameters.display = true
                                    app.registerTwoFactorSecurityQuestionParameters.twoFactorAuthenticationTypeId = 
                                        app.getTwoFactorAuthenticationTypeByCode('Totp').twoFactorAuthenticationTypeId
                                }
                                else {
                                    app.hideLoader()
                                    app.isUpdateError = true
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.hideLoader()
                                app.isUpdateError = true
                            }
                        }
                    )
                }

                form.classList.add('was-validated')
            },
            showRegisterForTwoFactorU2f: function () {
                this.registerTwoFactorU2fParameters = {
                    deviceName: null,
                    errorMessage: 'Please enter a device name.',
                    hasDuplicateError: false,
                    stepNumber: 1
                }         

                $('#registerTwoFactorU2fModal').modal('show')         
                
                setTimeout(function() {
                    $('#DeviceName').focus()
                }, 500)
            },
            getTwoFactorU2fRegistrationChallenge: function (retry) {       
                let form = $('#registerTwoFactorU2fDeviceNameForm')[0]                

                if (this.isDuplicateU2fDeviceName(this.registerTwoFactorU2fParameters.deviceName)) {
                    this.registerTwoFactorU2fParameters.errorMessage = 'The device name is already registered.  Please enter a new device name.'
                    this.registerTwoFactorU2fParameters.hasDuplicateError = true
                }
                else if (retry || form.checkValidity() === true) {  
                    this.isLoadError = false
                    this.isUpdateError = false       
                    this.showLoader()

                    $.ajax(
                        {
                            url: baseUrl + 'Secure/GetTwoFactorU2fRegistrationChallenge', 
                            type: 'POST',
                            success: function(result) {
                                app.registerTwoFactorU2fParameters.stepNumber = 2
                                app.hideLoader()

                                registerU2fDevice(result.challenge, result.appId, result.version)
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.hideLoader()
                                app.isLoadError = true
                            }
                        }
                    )
                }

                if (form) {
                    form.classList.add('was-validated')
                }
            },
            verifyTwoFactorU2fRegistrationResponse: function (deviceResponse) {
                let form = $('#registerTwoFactorU2fForm')[0]                
                
                this.isUpdateError = false
                this.showLoader()

                $.ajax(
                    {
                        url: baseUrl + 'Secure/ValidateTwoFactorU2fRegistrationResponse', 
                        type: 'POST',
                        data: { 
                            deviceName: this.registerTwoFactorU2fParameters.deviceName,
                            deviceResponse: deviceResponse
                        },
                        success: function(result) {
                            if (result === true) {                                
                                app.hideLoader()
                                app.registerTwoFactorSecurityQuestionParameters.display = true
                                app.registerTwoFactorSecurityQuestionParameters.twoFactorAuthenticationTypeId = 
                                    app.getTwoFactorAuthenticationTypeByCode('U2f').twoFactorAuthenticationTypeId
                                
                                if (!app.isTwoFactorAuthenticationType('U2f')) {
                                    app.registerTwoFactorU2fParameters.stepNumber = 3
                                }
                                else {
                                    app.registerTwoFactorU2fParameters.stepNumber = 4
                                    app.getUserDetails()
                                }                                
                            }
                            else {
                                app.hideLoader()
                                app.isUpdateError = true
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            app.hideLoader()
                            app.isUpdateError = true
                        }
                    }
                )
            },
            registerTwoFactorSecurityQuestion: function () {
                let form = $('#registerTwoFactorSecurityQuestionForm')[0]                
                if (form.checkValidity() === true) {
                    this.isUpdateError = false
                    this.showLoader()

                    $.ajax(
                        {
                            url: baseUrl + 'Secure/RegisterTwoFactorSecurityQuestion', 
                            type: 'POST',
                            data: { 
                                twoFactorAuthenticationTypeId: this.registerTwoFactorSecurityQuestionParameters.twoFactorAuthenticationTypeId,
                                userSecurityQuestionTypeId: this.registerTwoFactorSecurityQuestionParameters.userSecurityQuestionTypeId,
                                userSecurityAnswer: this.registerTwoFactorSecurityQuestionParameters.userSecurityAnswer
                            },
                            success: function(result) {
                                if (result === true) {                                
                                    app.hideLoader()
                                    form.classList.remove('was-validated')
                                    $('#registerTwoFactorTotpModal').modal('hide')
                                    $('#registerTwoFactorU2fModal').modal('hide')

                                    setTimeout(function() {
                                        app.registerTwoFactorSecurityQuestionParameters = {
                                            display: false,
                                            twoFactorAuthenticationTypeId: null,
                                            userSecurityQuestionTypeId: '',
                                            userSecurityAnswer: null
                                        }
                                    }, 1000)                                    

                                    app.getUserDetails()
                                }
                                else {
                                    app.hideLoader()
                                    app.isUpdateError = true
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.hideLoader()
                                app.isUpdateError = true
                            }
                        }
                    )
                }

                form.classList.add('was-validated')
            },
            getTwoFactorU2fDevices: function () {
                this.showLoader()
                $.ajax(
                    {
                        url: baseUrl + 'Secure/GetTwoFactorU2fDevices', 
                        type: 'POST',
                        success: function(result) {
                            app.hideLoader()
                            
                            app.twoFactorU2fDevices = result                            
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            app.hideLoader()
                        }
                    }
                )
            },
            removeTwoFactorU2fDevice: function(userU2fDeviceId) {
                if (confirm('Are you sure you want to remove this device?')) {
                    this.isLoadError = false
                    this.isUpdateError = false       
                    this.showLoader()

                    $.ajax(
                        {
                            url: baseUrl + 'Secure/RemoveTwoFactorU2fDevice', 
                            type: 'POST',
                            data: {
                                userU2fDeviceId: userU2fDeviceId
                            },
                            success: function(result) {
                                app.hideLoader()

                                app.twoFactorU2fDevices = result
                                app.getUserDetails()
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.hideLoader()
                                app.isLoadError = true
                            }
                        }
                    )
                }
            },
            isDuplicateU2fDeviceName: function (deviceName) {
                let result = false
                function matches(value, index, self) { 
                    return value.deviceName === deviceName
                }

                let parameters = this.twoFactorU2fDevices.filter(matches)
                if (parameters.length) {
                    result = true
                }

                return result
            },
            unregisterTwoFactor: function (disableWarningMessage) {    
                var warningMessage = 'Are you sure you want to disable two-factor authentication?'
                if (this.isTwoFactorAuthenticationType('U2f')) {
                    warningMessage = 'Are you sure you want to disable two-factor authentication?'
                        + '\n\nNote this action will also remove any U2F devices from your account.'
                }

                if (disableWarningMessage || confirm(warningMessage)) {     
                    this.isLoadError = false
                    this.isUpdateError = false       
                    this.showLoader()

                    $.ajax(
                        {
                            url: baseUrl + 'Secure/UnregisterTwoFactor', 
                            type: 'POST',
                            success: function(result) {
                                app.userDetails = result
                                app.twoFactorU2fDevices = []
                                app.hideLoader()
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.hideLoader()
                                app.isLoadError = true
                            }
                        }
                    )
                }
            },
            updateUptimeMaintenanceWindow: function () {
                let form = $('#updateUptimeMaintenanceWindowForm')[0]    

                if (this.editUptimeMaintenanceWindowParameters.assignToAllMonitors == false
                    && this.selectedMaintenanceWindowUptimeMonitorIds.length == 0) {
                    $('#UptimeMaintenanceWindowAssignedUptimeMonitors_ValidationError').show()                    
                }
                else {                                
                    if (form.checkValidity() === true) {
                        this.isUpdateError = false
                        this.showLoader()

                        this.editUptimeMaintenanceWindowParameters.selectedMonitorIds = this.selectedMaintenanceWindowUptimeMonitorIds
                        
                        $.ajax(
                            {
                                url: baseUrl + 'Secure/UpdateUptimeMaintenanceWindow', 
                                type: 'POST',
                                data: this.editUptimeMaintenanceWindowParameters,
                                success: function(result) {
                                    app.uptimeMaintenanceWindows = result
                                    $('#updateUptimeMaintenanceWindowModal').modal('hide')
                                    app.hideLoader()
                                    form.classList.remove('was-validated')
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    app.hideLoader()
                                    app.isUpdateError = true
                                }
                            }
                        )
                    }
                }

                form.classList.add('was-validated')
            },
            removeUptimeMaintenanceWindow: function () {
                this.isUpdateError = false
                if (confirm('Are you sure you want to remove this maintenance window?')) {
                    this.showLoader()

                    $.ajax(
                        {
                            url: baseUrl + 'Secure/RemoveUptimeMaintenanceWindow', 
                            type: 'POST',
                            data: this.editUptimeMaintenanceWindowParameters,
                            success: function(result) {
                                app.uptimeMaintenanceWindows = result                                
                                $('#updateUptimeMaintenanceWindowModal').modal('hide')
                                app.hideLoader()
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.hideLoader()
                                app.isUpdateError = true
                            }
                        }
                    )
                }
            },
            showEditForUptimeMaintenanceWindow: function (uptimeMaintenanceWindowId) {
                this.isUpdateError = false
                function matches(value, index, self) { 
                    return value.uptimeMaintenanceWindowId === uptimeMaintenanceWindowId
                }             

                let form = $('#updateUptimeMaintenanceWindowForm')[0]   
                form.classList.remove('was-validated')
                $('#UptimeMaintenanceWindowAssignedUptimeMonitors_ValidationError').hide()       
                app.selectedMaintenanceWindowUptimeMonitorIds = []         

                let parameters = this.uptimeMaintenanceWindows.filter(matches)
                if (parameters.length) {
                    let selection = parameters[0]

                    this.showLoader()
                    $.ajax(
                        {
                            url: baseUrl + 'Secure/GetUptimeMaintenanceWindow', 
                            type: 'POST',
                            data: {
                                uptimeMaintenanceWindowId: selection.uptimeMaintenanceWindowId
                            },
                            success: function(result) {
                                app.editUptimeMaintenanceWindowParameters = result

                                app.selectedMaintenanceWindowUptimeMonitorIds = app.editUptimeMaintenanceWindowParameters.selectedMonitorIds                                 
                                
                                app.hideLoader()
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.hideLoader()
                                app.isUpdateError = true
                            }
                        }
                    )
                }
                else {
                    this.editUptimeMaintenanceWindowParameters = {
                        uptimeMaintenanceWindowId: null,
                        uptimeMaintenanceWindowTypeId: '',
                        name: null,
                        startTime: null,
                        duration: null,
                        days: null,
                        isActive: null,
                        selectedMonitorIds: [],
                        assignToAllMonitors: true
                    }
                }
            },
            isEditForUptimeMaintenanceWindowType: function (type) {
                return this.editUptimeMaintenanceWindowParameters.uptimeMaintenanceWindowTypeId != null
                    && this.editUptimeMaintenanceWindowParameters.uptimeMaintenanceWindowTypeId != '' 
                    && this.getUptimeMaintenanceWindowType(this.editUptimeMaintenanceWindowParameters.uptimeMaintenanceWindowTypeId).code == type
            },
            changeUptimeMaintenanceWindowType:  function () {
                let form = $('#updateUptimeMaintenanceWindowForm')[0]
                form.classList.remove('was-validated')
            },
            validateUploadLogo: function(event) {
                if (event.target.files.length) {
                    if (!this.endsWith(event.target.files[0].name.toLowerCase(), '.jpg')
                        && !this.endsWith(event.target.files[0].name.toLowerCase(), '.png')) {
                        $("#UploadLogo").val(null)
                    }

                    if (event.target.files[0].size >= 153600 /* 150 KB */) {
                        $("#UploadLogo").val(null)
                    }
                }
            },
            endsWith: function (str, suffix) {
                return str.indexOf(suffix, str.length - suffix.length) !== -1
            },
            updateUptimePublicStatusPage: function () {
                let form = $('#updateUptimePublicStatusPageForm')[0]   
                
                if (this.editUptimePublicStatusPageParameters.assignToAllMonitors == false
                    && this.selectedPublicStatusPageUptimeMonitorIds.length == 0) {
                    $('#UptimePublicStatusPageAssignedUptimeMonitors_ValidationError').show()                    
                }
                else {  
                    if (form.checkValidity() === true) {
                        this.isUpdateError = false
                        this.showLoader()

                        if (this.editUptimePublicStatusPageParameters.assignToAllMonitors) {
                            this.selectedPublicStatusPageUptimeMonitorIds = []
                        }

                        this.editUptimePublicStatusPageParameters.selectedMonitorIds = this.selectedPublicStatusPageUptimeMonitorIds
                        
                        $.ajax(
                            {
                                url: baseUrl + 'Secure/UpdateUptimePublicStatusPage', 
                                type: 'POST',
                                data: this.editUptimePublicStatusPageParameters,
                                success: function(result) {
                                    app.uptimePublicStatusPages = result.uptimePublicStatusPages

                                    if ($("#UploadLogo").val()) {
                                        let formData = new FormData()
                                        formData.append('logoFile', $("#UploadLogo")[0].files[0])
                                        formData.append('uptimePublicStatusPageId', result.uptimePublicStatusPageId)

                                        var request = new XMLHttpRequest()
                                        request.open('POST', baseUrl + 'Secure/UploadLogo')                                
                                        request.send(formData)

                                        $("#UploadLogo").val(null)
                                    }

                                    $('#updateUptimePublicStatusPageModal').modal('hide')
                                    app.hideLoader()
                                    form.classList.remove('was-validated')
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    app.hideLoader()
                                    app.isUpdateError = true
                                }
                            }
                        )
                    }
                }

                form.classList.add('was-validated')
            },
            removeUptimePublicStatusPage: function () {
                this.isUpdateError = false
                if (confirm('Are you sure you want to remove this public status page?')) {
                    this.showLoader()

                    $.ajax(
                        {
                            url: baseUrl + 'Secure/RemoveUptimePublicStatusPage', 
                            type: 'POST',
                            data: this.editUptimePublicStatusPageParameters,
                            success: function(result) {
                                app.uptimePublicStatusPages = result                                
                                $('#updateUptimePublicStatusPageModal').modal('hide')
                                app.hideLoader()
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.hideLoader()
                                app.isUpdateError = true
                            }
                        }
                    )
                }
            },
            showEditForUptimePublicStatusPage: function (uptimePublicStatusPageId) {
                this.isUpdateError = false
                function matches(value, index, self) { 
                    return value.uptimePublicStatusPageId === uptimePublicStatusPageId
                }             

                let form = $('#updateUptimePublicStatusPageForm')[0]   
                form.classList.remove('was-validated')
                $('#UptimePublicStatusPageAssignedUptimeMonitors_ValidationError').hide()
                app.selectedPublicStatusPageUptimeMonitorIds = []

                let parameters = this.uptimePublicStatusPages.filter(matches)
                if (parameters.length) {
                    let selection = parameters[0]

                    this.showLoader()
                    $.ajax(
                        {
                            url: baseUrl + 'Secure/GetUptimePublicStatusPage', 
                            type: 'POST',
                            data: {
                                uptimePublicStatusPageId: selection.uptimePublicStatusPageId
                            },
                            success: function(result) {
                                app.editUptimePublicStatusPageParameters = result

                                app.selectedPublicStatusPageUptimeMonitorIds = app.editUptimePublicStatusPageParameters.selectedMonitorIds
                                
                                app.hideLoader()
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.hideLoader()
                                app.isUpdateError = true
                            }
                        }
                    )
                }
                else {
                    this.editUptimePublicStatusPageParameters = {
                        uptimePublicStatusPageId: null,
                        name: null,
                        customDomain: null,
                        password: null,
                        uptimeMonitorSortTypeId: this.uptimeMonitorSortTypes[0].uptimeMonitorSortTypeId,
                        hideBrandingLogo: null,
                        announcementsHtml: null,
                        isActive: null,
                        selectedMonitorIds: [],
                        assignToAllMonitors: true
                    }

                    $("#UploadLogo").val(null)
                }
            },
            updateUserDetails: function () {
                let form = $('#updateUserDetailsForm')[0]                
                if (form.checkValidity() === true) {
                    this.isUserDetailsUpdateSuccess = false
                    this.isUserDetailsUpdateError = false
                    this.isUserDetailsProcessing = true
                    this.showLoader()
                    
                    $.ajax(
                        {
                            url: baseUrl + 'Secure/UpdateUserDetails', 
                            type: 'POST',
                            data: {
                                firstName: this.userDetails.firstName,
                                lastName: this.userDetails.lastName,
                                userTimeZoneTypeId: this.userDetails.userTimeZoneTypeId
                            },
                            success: function(result) {
                                app.userDetails = result

                                app.isUserDetailsUpdateSuccess = true
                                app.isUserDetailsProcessing = false
                                app.hideLoader()
                                form.classList.remove('was-validated')
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.isUserDetailsProcessing = false
                                app.hideLoader()
                                app.isUserDetailsUpdateError = true
                            }
                        }
                    )
                }

                form.classList.add('was-validated')
            },
            updateUserPassword: function () {
                let form = $('#updateUserPasswordForm')[0]                
                if (form.checkValidity() === true) {
                    this.isUserPasswordUpdateSuccess = false
                    this.isUserPasswordUpdateError = false
                    this.isUserPasswordProcessing = true
                    this.showLoader()
                    
                    $.ajax(
                        {
                            url: baseUrl + 'Secure/UpdateUserPassword', 
                            type: 'POST',
                            data: {
                                currentPassword: this.userDetails.currentPassword,
                                newPassword: this.userDetails.newPassword,
                                repeatNewPassword: this.userDetails.repeatNewPassword
                            },
                            success: function(result) {
                                if (result.result === 'Success') {
                                    app.isUserPasswordUpdateError = false   
                                    app.isUserPasswordUpdateSuccess = true                                    
                                }
                                else
                                {
                                    if (result.result === 'InvalidCurrentPassword') {
                                        app.userPasswordUpdateErrorMessage = 'Your current password is invalid.'
                                    }
                                    else if (result.result === 'InvalidNewPassword') {
                                        app.userPasswordUpdateErrorMessage = 'The new password must be at least 6 characters.'
                                    }
                                    else if (result.result === 'PasswordsDoNotMatch') {
                                        app.userPasswordUpdateErrorMessage = 'The new passwords do not match.'
                                    }
                                    else {
                                        app.userPasswordUpdateErrorMessage = 'Your password could not be updated.  Please try again.'
                                    }

                                    app.isUserPasswordUpdateError = true
                                }

                                app.userDetails.currentPassword = null
                                app.userDetails.newPassword = null
                                app.userDetails.repeatNewPassword = null
                                
                                app.isUserPasswordProcessing = false
                                app.hideLoader()
                                form.classList.remove('was-validated')
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.isUserPasswordProcessing = false
                                app.hideLoader()
                                app.userPasswordUpdateErrorMessage = 'Your password could not be updated.  Please try again.'
                                app.isUserPasswordUpdateError = true
                            }
                        }
                    )
                }

                form.classList.add('was-validated')
            },
            requestUserEmailUpdate: function () {
                let form = $('#requestUserEmailUpdateForm')[0]                
                if (form.checkValidity() === true) {
                    this.isUserEmailUpdateSuccess = false
                    this.isUserEmailUpdateError = false
                    this.isUserEmailProcessing = true
                    this.showLoader()
                    
                    $.ajax(
                        {
                            url: baseUrl + 'Secure/RequestUserEmailUpdate', 
                            type: 'POST',
                            data: {
                                newEmail: this.userDetails.newEmail
                            },
                            success: function(result) {
                                if (result.result === 'Success') {
                                    app.isUserEmailUpdateError = false   
                                    app.isUserEmailUpdateSuccess = true                                    
                                }
                                else
                                {
                                    if (result.result === 'InvalidEmail') {
                                        app.userEmailUpdateErrorMessage = 'The email address is invalid.'
                                    }
                                    else if (result.result === 'EmailInUse') {
                                        app.userEmailUpdateErrorMessage = 'The email address is already in use.'
                                    }
                                    else {
                                        app.userEmailUpdateErrorMessage = 'Your email address could not be updated.  Please try again.'
                                    }

                                    app.isUserEmailUpdateError = true
                                }

                                app.userDetails.newEmail = null
                                
                                app.isUserEmailProcessing = false
                                app.hideLoader()
                                form.classList.remove('was-validated')
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.isUserEmailProcessing = false
                                app.hideLoader()
                                app.userEmailUpdateErrorMessage = 'Your email address could not be updated.  Please try again.'
                                app.isUserEmailUpdateError = true
                            }
                        }
                    )
                }

                form.classList.add('was-validated')
            },
            updateUptimeMonitorApiKey: function () {
                let form = $('#updateUptimeMonitorApiKeyForm')[0]   
                
                if (form.checkValidity() === true) {
                    this.isUpdateError = false
                    this.showLoader()

                    $.ajax(
                        {
                            url: baseUrl + 'Secure/UpdateUptimeMonitorApiKey', 
                            type: 'POST',
                            data: this.editUptimeMonitorApiKeyParameters,
                            success: function(result) {
                                app.uptimeMonitorApiKeys = result

                                $('#updateUptimeMonitorApiKeyModal').modal('hide')
                                app.hideLoader()
                                form.classList.remove('was-validated')
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.hideLoader()
                                app.isUpdateError = true
                            }
                        }
                    )
                }                

                form.classList.add('was-validated')
            },
            removeUptimeMonitorApiKey: function () {
                this.isUpdateError = false
                if (confirm('Are you sure you want to remove this monitor API key?')) {
                    this.showLoader()

                    $.ajax(
                        {
                            url: baseUrl + 'Secure/RemoveUptimeMonitorApiKey', 
                            type: 'POST',
                            data: this.editUptimeMonitorApiKeyParameters,
                            success: function(result) {
                                app.uptimeMonitorApiKeys = result                                
                                $('#updateUptimeMonitorApiKeyModal').modal('hide')
                                app.hideLoader()
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.hideLoader()
                                app.isUpdateError = true
                            }
                        }
                    )
                }
            },
            showEditForUptimeMonitorApiKey: function (uptimeMonitorApiKeyId) {
                this.isUpdateError = false
                function matches(value, index, self) { 
                    return value.uptimeMonitorApiKeyId === uptimeMonitorApiKeyId
                }             

                let form = $('#updateUptimeMonitorApiKeyForm')[0]   
                form.classList.remove('was-validated')
                
                let parameters = this.uptimeMonitorApiKeys.filter(matches)
                if (parameters.length) {
                    let selection = parameters[0]

                    this.showLoader()
                    $.ajax(
                        {
                            url: baseUrl + 'Secure/GetUptimeMonitorApiKey', 
                            type: 'POST',
                            data: {
                                uptimeMonitorApiKeyId: selection.uptimeMonitorApiKeyId
                            },
                            success: function(result) {
                                app.editUptimeMonitorApiKeyParameters = result

                                app.hideLoader()
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.hideLoader()
                                app.isUpdateError = true
                            }
                        }
                    )
                }
                else {
                    this.editUptimeMonitorApiKeyParameters = {
                        uptimeMonitorApiKeyId: null,
                        uptimeMonitorId: null
                    }
                }
            },
            getUptimeMonitorName: function (uptimeMonitorId) {
                function matches(value, index, self) { 
                    return value.uptimeMonitorId === uptimeMonitorId
                }             

                return this.uptimeMonitors.filter(matches)[0]
            },
            getApiKeyByTypeCode: function (typeCode) {
                function uptimeMonitorApiKeyTypeMatches(value, index, self) { 
                    return value.code === typeCode
                }             

                let uptimeMonitorApiKeyTypes = this.uptimeMonitorApiKeyTypes.filter(uptimeMonitorApiKeyTypeMatches)                
                
                if (uptimeMonitorApiKeyTypes.length) {
                    function uptimeMonitorApiKeyMatches(value, index, self) { 
                        return value.uptimeMonitorApiKeyTypeId === uptimeMonitorApiKeyTypes[0].uptimeMonitorApiKeyTypeId
                    }             

                    let uptimeMonitorApiKeys = this.uptimeMonitorApiKeys.filter(uptimeMonitorApiKeyMatches)

                    return uptimeMonitorApiKeys.length > 0 ? uptimeMonitorApiKeys[0] : null
                }
                else {
                    return null
                }
            },
            createApiKey: function (typeCode) {
                this.isUpdateError = false
                this.showLoader()
                $.ajax(
                    {
                        url: baseUrl + 'Secure/CreateApiKey', 
                        type: 'POST',
                        data: {
                            code: typeCode
                        },
                        success: function(result) {
                            app.uptimeMonitorApiKeys = result
                            
                            app.hideLoader()
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            app.hideLoader()
                            app.isUpdateError = true
                        }
                    }
                )
            },
            removeApiKey: function (typeCode, confirmMessage) {
                this.isUpdateError = false
                if (confirm(confirmMessage != null ? confirmMessage : 'Are you sure you want to remove this API key?')) {
                    this.showLoader()
                    $.ajax(
                        {
                            url: baseUrl + 'Secure/RemoveApiKey', 
                            type: 'POST',
                            data: {
                                code: typeCode
                            },
                            success: function(result) {
                                app.uptimeMonitorApiKeys = result
                                
                                app.hideLoader()
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                app.hideLoader()
                                app.isUpdateError = true
                            }
                        }
                    )
                }
            },
            showLoader: function () {
                this.isProcessing = true

                if (!$('.modal-dialog-window:visible').length) {
                    if (this.loadingModalObj) {
                        clearTimeout(this.loadingModalObj)
                        this.loadingModalObj = null
                    }
                    this.loadingModalObj = setTimeout(function() {
                        if (!$('.modal-dialog-window:visible').length) {
                            $('#loadingModal').modal({
                                keyboard: false,
                                backdrop: 'static'
                            })
                        }
                        
                        app.loadingModalObj = null
                    }, 500)                     
                }
            },            
            hideLoader: function () {
                this.isProcessing = false

                var form = $('#updateUptimeMonitorForm')[0]
                form.classList.remove('was-validated')

                form = $('#updateUptimeAlertContactForm')[0]
                form.classList.remove('was-validated')

                if (this.loadingModalObj) {
                    clearTimeout(this.loadingModalObj)
                    this.loadingModalObj = null
                }                    
                
                if ($('.modal-dialog-window:visible').length) {
                    $('#loadingModal').modal('hide')
                }

                setTimeout(function() {
                    if (!$('.modal-dialog-window:visible').length) {
                        $('#loadingModal').modal('hide')
                    }
                }, 500)                
            },
            getUptimeMonitorType: function (uptimeMonitorTypeId) {
                function matches(value, index, self) { 
                    return value.uptimeMonitorTypeId === uptimeMonitorTypeId
                }             

                return this.uptimeMonitorTypes.filter(matches)[0]
            },
            getUptimeStatusType: function (uptimeStatusTypeId) {
                function matches(value, index, self) { 
                    return value.uptimeStatusTypeId === uptimeStatusTypeId
                }             

                return this.uptimeStatusTypes.filter(matches)[0]
            },
            getUptimeStatusTypeByCode: function (code) {
                function matches(value, index, self) { 
                    return value.code === code
                }             
                
                return this.uptimeStatusTypes.filter(matches)[0]
            },
            getUptimeAlertContactType: function (uptimeAlertContactTypeId) {
                function matches(value, index, self) { 
                    return value.uptimeAlertContactTypeId === uptimeAlertContactTypeId
                }             

                return this.uptimeAlertContactTypes.filter(matches)[0]
            },
            getUptimeAlertContactStatusType: function (uptimeAlertContactStatusTypeId) {
                function matches(value, index, self) { 
                    return value.uptimeAlertContactStatusTypeId === uptimeAlertContactStatusTypeId
                }             

                return this.uptimeAlertContactStatusTypes.filter(matches)[0]
            },
            getUptimeNotificationEventType: function (uptimeNotificationEventTypeId) {
                function matches(value, index, self) { 
                    return value.uptimeNotificationEventTypeId === uptimeNotificationEventTypeId
                }             

                return this.uptimeNotificationEventTypes.filter(matches)[0]
            },
            getUptimeMaintenanceWindowType: function (uptimeMaintenanceWindowTypeId) {
                function matches(value, index, self) { 
                    return value.uptimeMaintenanceWindowTypeId === uptimeMaintenanceWindowTypeId
                }             

                return this.uptimeMaintenanceWindowTypes.filter(matches)[0]
            },
            getUptimeMonitorApiKeyType: function (uptimeMonitorApiKeyTypeId) {
                function matches(value, index, self) { 
                    return value.uptimeMonitorApiKeyTypeId === uptimeMonitorApiKeyTypeId
                }             

                return this.uptimeMonitorApiKeyTypes.filter(matches)[0]
            },
            getTwoFactorAuthenticationType: function (twoFactorAuthenticationTypeId) {
                function matches(value, index, self) { 
                    return value.twoFactorAuthenticationTypeId === twoFactorAuthenticationTypeId
                }             

                return this.twoFactorAuthenticationTypes.filter(matches)[0]
            },
            isTwoFactorAuthenticationType: function (type) {
                return this.userDetails.twoFactorAuthenticationTypeId != null
                    && this.getTwoFactorAuthenticationType(this.userDetails.twoFactorAuthenticationTypeId).code == type
            },
            getTwoFactorAuthenticationTypeByCode: function (code) {
                function matches(value, index, self) { 
                    return value.code === code
                }             

                return this.twoFactorAuthenticationTypes.filter(matches)[0]
            },
            isCommonPort: function (port) {
                function matches(value, index, self) { 
                    return value.value === port
                }

                let commonPorts = this.portOptionGroups['Common Ports'].filter(matches)
                return commonPorts.length == 1
            },
            getFormattedMonitorChartData: function () {
                let xValues = []
                let yValues = []
                this.uptimeMonitorChartData.forEach(function(item, index) {
                    xValues.push(item.time)
                    yValues.push(item.responseTime)                    
                })

                return { xValues, yValues }
            },
            drawUptimeMonitorChart: function () {        
                if ($('#uptimeMonitorChart').length) {        
                    $('#uptimeMonitorChart').show()
                    let ctx = document.getElementById('uptimeMonitorChart').getContext('2d')
                    let monitorData = this.getFormattedMonitorChartData()
                    if (chart) {
                        chart.destroy()
                    }
                    chart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: monitorData.xValues,
                            datasets: [
                                {
                                    label: 'Milliseconds',
                                    backgroundColor: 'lightblue',
                                    borderColor: 'black',
                                    pointBorderColor: 'black',
                                    pointBackgroundColor: 'yellow',
                                    data: monitorData.yValues,
                                    fill: true,
                                    borderWidth: 2,
                                    type: 'line',
                                    pointRadius: 4,
                                    tension: 0
                                }
                            ]
                        },
                        options: {
                            responsive: true,
                            maintainAspectRatio: false,
                            title: {
                                display: true,
                                text: 'Response Time (Past 24 Hours)'
                            },
                            tooltips: {
                                intersect: false,
                                mode: 'index'
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        maxTicksLimit: 5
                                    }
                                }],
                                xAxes: [{
                                    ticks: {
                                        maxTicksLimit: 12
                                    }
                                }]
                            }
                        }
                    })
                }
            },
            drawOverallStatusCharts: function () {        
                if ($('#upMonitorCountChart').length) {        
                    $('#upMonitorCountChart').show()
                    let ctx = document.getElementById('upMonitorCountChart').getContext('2d')
                    if (upMonitorCountChart) {
                        upMonitorCountChart.destroy()
                    }

                    let count1 = this.overallMonitorStats.upMonitorCount
                    let count2 = this.overallMonitorStats.currentMonitorCount - this.overallMonitorStats.upMonitorCount

                    if (count1 == 0 && count2 == 0)
                    {
                        count2 = 1
                    }

                    upMonitorCountChart = new Chart(ctx, {
                        type: 'doughnut',
                        data: {
                            datasets: [{
                                data: [
                                    count1,
                                    count2
                                ],
                                backgroundColor: [
                                    'green',
                                    'gray'
                                ]
                            }]
                        },
                        options: {
                            cutoutPercentage: 75,
                            responsive: true,
                            maintainAspectRatio: false,
                            legend: {
                                display: false
                            },
                            title: {
                                display: false
                            },
                            animation: {
                                animateScale: true,
                                animateRotate: true
                            },
                            tooltips: {
                                enabled: false
                            },
                            elements: {
                                center: {
                                    text: this.overallMonitorStats.upMonitorCount,
                                    color: 'black',
                                    fontStyle: 'Helvetica',
                                    sidePadding: 20
                                }
                            }
                        }
                    })
                }

                if ($('#downMonitorCountChart').length) {        
                    $('#downMonitorCountChart').show()
                    let ctx = document.getElementById('downMonitorCountChart').getContext('2d')
                    if (downMonitorCountChart) {
                        downMonitorCountChart.destroy()
                    }

                    let count1 = this.overallMonitorStats.downMonitorCount
                    let count2 = this.overallMonitorStats.currentMonitorCount - this.overallMonitorStats.downMonitorCount

                    if (count1 == 0 && count2 == 0)
                    {
                        count2 = 1
                    }

                    downMonitorCountChart = new Chart(ctx, {
                        type: 'doughnut',
                        data: {
                            datasets: [{
                                data: [
                                    count1,
                                    count2
                                ],
                                backgroundColor: [
                                    'darkred',
                                    'gray'
                                ]
                            }]
                        },
                        options: {
                            cutoutPercentage: 75,
                            responsive: true,
                            maintainAspectRatio: false,
                            legend: {
                                display: false
                            },
                            title: {
                                display: false
                            },
                            animation: {
                                animateScale: true,
                                animateRotate: true
                            },
                            tooltips: {
                                enabled: false
                            },
                            elements: {
                                center: {
                                    text: this.overallMonitorStats.downMonitorCount,
                                    color: 'black',
                                    fontStyle: 'Helvetica',
                                    sidePadding: 20
                                }
                            }
                        }
                    })
                }

                if ($('#pausedMonitorCountChart').length) {        
                    $('#pausedMonitorCountChart').show()
                    let ctx = document.getElementById('pausedMonitorCountChart').getContext('2d')
                    if (pausedMonitorCountChart) {
                        pausedMonitorCountChart.destroy()
                    }

                    let count1 = this.overallMonitorStats.pausedMonitorCount
                    let count2 = this.overallMonitorStats.currentMonitorCount - this.overallMonitorStats.pausedMonitorCount

                    if (count1 == 0 && count2 == 0)
                    {
                        count2 = 1
                    }

                    pausedMonitorCountChart = new Chart(ctx, {
                        type: 'doughnut',
                        data: {
                            datasets: [{
                                data: [
                                    count1,
                                    count2
                                ],
                                backgroundColor: [
                                    'black',
                                    'gray'
                                ]
                            }]
                        },
                        options: {
                            cutoutPercentage: 75,
                            responsive: true,
                            maintainAspectRatio: false,
                            legend: {
                                display: false
                            },
                            title: {
                                display: false
                            },
                            animation: {
                                animateScale: true,
                                animateRotate: true
                            },
                            tooltips: {
                                enabled: false
                            },
                            elements: {
                                center: {
                                    text: this.overallMonitorStats.pausedMonitorCount,
                                    color: 'black',
                                    fontStyle: 'Helvetica',
                                    sidePadding: 20
                                }
                            }
                        }
                    })
                }
            },
            displayScreen: function (hash, loadMonitorDetails, resetToOverview) {
                let hashValues = hash.split('_')
                let screenName = null
                let monitorId = null
                screenName = hashValues[0]

                if (hashValues.length > 1) {
                    monitorId = parseInt(hashValues[1])
                }

                if (screenName === 'Dashboard'
                    || screenName === 'Settings') {
                    
                    this.currentScreen = screenName
                    
                    if (monitorId != null && loadMonitorDetails) {
                        this.showUptimeMonitorDetails(monitorId)
                    }
                    
                    if (resetToOverview) {
                        monitorId = null
                        this.selectedUptimeMonitorDetail.uptimeMonitorId = null
                    }

                    window.location.hash = this.currentScreen + (monitorId != null ? '_' + monitorId : '')
                }
            },
            getPercentHistoryClassColor: function (percentage, wasMonitorActive, uptimeMonitorTypeName) {
                let result = ''

                percentage = percentage * 100

                if (percentage >= 98) {
                    result = 'success'
                }
                else if (percentage > 1) {
                    result = 'warning'
                }
                else if (percentage <= 1 && uptimeMonitorTypeName === 'Keyword') {
                    result = 'warning'
                }
                else if (wasMonitorActive) {
                    result = 'warning'
                }      
                else {
                    result = 'dark'
                }           

                return result
            },
            getStatusClassColor: function (uptimeStatusTypeName) {
                let result = ''

                if (uptimeStatusTypeName === 'Up') {
                    result = 'success'
                }
                else if (uptimeStatusTypeName === 'Down' || uptimeStatusTypeName === 'Likely Down') {
                    result = 'danger'
                }
                else {
                    result = 'dark'
                }

                return result
            },
            getMonitoringIntervalText: function (intervalSliderValue) {
                let result = ''                
                if (intervalSliderValue != null) {
                    if (intervalSliderValue == 1)
                    {
                        result = 'Checked every minute'
                    }
                    else if (intervalSliderValue >= 2 && intervalSliderValue <= 13)
                    {
                        result = 'Checked every ' + pollingIntervals[intervalSliderValue - 1] + ' minutes'
                    }
                    else if (intervalSliderValue == 14 ) {
                        result = 'Checked every hour'
                    }
                    else if (intervalSliderValue > 14 ) {
                        result = 'Checked every ' + pollingIntervals[intervalSliderValue - 1] / 60 + ' hours'
                    }
                }

                return result
            },
            copyTextToClipboard: function (text, event) {                                
                copyTextToClipboard(text)                

                setTimeout(this.copyTextToClipboardCallback.bind(undefined, event.currentTarget.id, event.currentTarget.innerText), 2000)

                event.currentTarget.innerText = 'Copied...'
            },
            copyTextToClipboardCallback: function (id, originalText) {     
                $('#' + id)[0].innerText = originalText
            }
        },
        computed: {
            hasValidUptimeMonitors: function () {
                function uptimeMonitorMatches(value, index, self) { 
                    return value.uptimeMonitorId != null
                }             

                let uptimeMonitors = this.uptimeMonitors.filter(uptimeMonitorMatches)                

                return uptimeMonitors.length > 0
            },
            hasValidUptimeAlertContacts: function () {
                function uptimeAlertContactMatches(value, index, self) { 
                    return value.uptimeAlertContactId != null
                }             

                let uptimeAlertContacts = this.uptimeAlertContacts.filter(uptimeAlertContactMatches)                

                return uptimeAlertContacts.length > 0
            },
            hasValidLatestEvents: function () {
                function latestEventMatches(value, index, self) { 
                    return value.eventName != null
                }             

                let latestEvents = this.latestEvents.filter(latestEventMatches)                

                return latestEvents.length > 0
            },
            hasValidUptimeMaintenanceWindows: function () {
                function uptimeMaintenanceWindowMatches(value, index, self) { 
                    return value.name != null
                }             

                let uptimeMaintenanceWindows = this.uptimeMaintenanceWindows.filter(uptimeMaintenanceWindowMatches)                

                return uptimeMaintenanceWindows.length > 0
            },
            hasValidUptimePublicStatusPages: function () {
                function uptimeStatusPageMatches(value, index, self) { 
                    return value.name != null
                }             

                let uptimePublicStatusPages = this.uptimePublicStatusPages.filter(uptimeStatusPageMatches)                

                return uptimePublicStatusPages.length > 0
            },
            hasValidUptimeMonitorApiKeys: function () {
                let getUptimeMonitorApiKeyType = this.getUptimeMonitorApiKeyType
                function uptimeMonitorApiKeyMatches(value, index, self) { 
                    return value.uptimeMonitorApiKeyId != null
                        && getUptimeMonitorApiKeyType(value.uptimeMonitorApiKeyTypeId).code == 'MonitorSpecificApiKey'
                }             

                let uptimeMonitorApiKeys = this.uptimeMonitorApiKeys.filter(uptimeMonitorApiKeyMatches)                

                return uptimeMonitorApiKeys.length > 0
            },
            uptimeMonitorApiKeysAssignedToMonitors: function () {
                function uptimeMonitorApiKeyMatches(value, index, self) { 
                    return value.uptimeMonitorId != null
                }             

                let uptimeMonitorApiKeys = this.uptimeMonitorApiKeys.filter(uptimeMonitorApiKeyMatches)                

                return uptimeMonitorApiKeys
            },
            uptimeMonitorsWithoutApiKeys: function () {
                let resultUptimeMonitors = []
                for (let i = 0; i < this.uptimeMonitors.length; i++) {
                    let uptimeMonitor = this.uptimeMonitors[i]
                    function uptimeMonitorApiKeyMatches(value, index, self) { 
                        return value.uptimeMonitorId == uptimeMonitor.uptimeMonitorId
                    }             

                    let uptimeMonitorApiKeys = this.uptimeMonitorApiKeys.filter(uptimeMonitorApiKeyMatches)   
                    
                    if (uptimeMonitorApiKeys.length == 0) {
                        resultUptimeMonitors.push(uptimeMonitor)
                    }             
                }

                return resultUptimeMonitors
            },
        },
        updated: function () {
            this.$nextTick(function () {
                $('[data-toggle="tooltip"]').tooltip('dispose')
                $('[data-toggle="tooltip"]').tooltip()
            })
        }
    })


    $(function() {
        $('#app').show()
        app.setPageObjects(pageObjects)

        configureCustomChartJsDoughnutTextSupport()

        let hash = window.location.hash.substr(1)
        if (hash) {
            app.displayScreen(hash, true)
        }
    })

    // Source URL: https://stackoverflow.com/questions/20966817/how-to-add-text-inside-the-doughnut-chart-using-chart-js
    function configureCustomChartJsDoughnutTextSupport() {
        Chart.pluginService.register({
            beforeDraw: function (chart) {
                if (chart.config.options.elements.center) {
                    //Get ctx from string
                    var ctx = chart.chart.ctx;
                
                    //Get options from the center object in options
                    var centerConfig = chart.config.options.elements.center;
                    var fontStyle = centerConfig.fontStyle || 'Arial';
                    var txt = centerConfig.text;
                    var color = centerConfig.color || '#000';
                    var sidePadding = centerConfig.sidePadding || 20;
                    var sidePaddingCalculated = (sidePadding/100) * (chart.innerRadius * 2)
                    //Start with a base font of 30px
                    ctx.font = "30px " + fontStyle;
                
                    //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                    var stringWidth = ctx.measureText(txt).width;
                    var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;
                
                    // Find out how much the font can grow in width.
                    var widthRatio = elementWidth / stringWidth;
                    var newFontSize = Math.floor(30 * widthRatio);
                    var elementHeight = (chart.innerRadius * 2);
                
                    // Pick a new font size so it will not be larger than the height of label.
                    var fontSizeToUse = Math.min(newFontSize, elementHeight);

                    // Override font size
                    fontSizeToUse = 30;
                
                    //Set font settings to draw it correctly.
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                    var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
                    ctx.font = fontSizeToUse+"px " + fontStyle;
                    ctx.fillStyle = color;
                
                    //Draw text in center
                    ctx.fillText(txt, centerX, centerY);
                }
            }
        });
    }

    // Source URL: https://stackoverflow.com/questions/400212/how-do-i-copy-to-the-clipboard-in-javascript
    function fallbackCopyTextToClipboard(text) {
        var textArea = document.createElement("textarea");
        textArea.value = text;
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();
      
        try {
            var successful = document.execCommand("copy");
            var msg = successful ? "successful" : "unsuccessful";
            console.log("Fallback: Copying text command was " + msg);
        } catch (err) {
            console.error("Fallback: Oops, unable to copy", err);
        }
      
        document.body.removeChild(textArea);
    }
    function copyTextToClipboard(text) {
        if (!navigator.clipboard) {
            fallbackCopyTextToClipboard(text);
            return;
        }
        navigator.clipboard.writeText(text).then(
            function() {
                //console.log("Async: Copying to clipboard was successful!");
            },
            function(err) {
                console.error("Async: Could not copy text: ", err);
            }
        );
    }

    // Device Authentication (U2F)
    function registerU2fDevice(challenge, appId, version) {        
        var request = { 
            challenge: challenge,             
            appId: appId,
            version: version
        };

        var registerRequests = [
            {
                version: request.version, 
                challenge: request.challenge
            }
        ];

        u2f.register(request.appId, 
            registerRequests, 
            [],
            function(data) {
                if (data.errorCode) {
                    app.isUpdateError = true
                }
                else {                
                    app.verifyTwoFactorU2fRegistrationResponse(JSON.stringify(data));
                }

                return false;
            }
        );
    }
}