﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SiteUptime.Models;
using SiteUptime.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using SiteUptime.Helpers;
using SiteUptime.Helpers.Authentication;
using SiteUptime.Controllers.Base;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace SiteUptime.Controllers
{
    [ClaimRequirement(SecureClaimTypes.Permission, "Admin,Read")]
    public class SecureController : BaseController
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IAuthenticationService _authenticationService;
        private readonly IUserProfileService _userProfileService;
        private readonly Settings _settings;
        private readonly IUptimeMonitorService _uptimeMonitorService;
        private readonly IEventLogService _eventLogService;

        public SecureController(ILogger<HomeController> logger,
            IAuthenticationService authenticationService,
            IUserProfileService userProfileService,
            IOptions<Settings> settings,
            IUptimeMonitorService uptimeMonitorService,
            IEventLogService eventLogService)
        {
            _logger = logger;
            _authenticationService = authenticationService;
            _userProfileService = userProfileService;
            _settings = settings.Value;
            _uptimeMonitorService = uptimeMonitorService;
            _eventLogService = eventLogService;
        }
        
        public IActionResult Index()
        {
            return RedirectToAction("Dashboard");
        }

        [ClaimRequirement(SecureClaimTypes.Permission, "Admin")]
        public IActionResult Admin()
        {
            return View();
        }

        public IActionResult NoAccess()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Profile()
        {
            return View(new UserProfileUpdateResult());
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult Profile([FromForm] ProfileFields profileFields)
        {
            var result = new UserProfileUpdateResult
            {
                UserProfileUpdateResultType = UserProfileUpdateResultType.Error
            };
            var user = (User)HttpContext.Items["User"];
            if (user != null)
            {
                profileFields.UserId = user.UserId;
                result = _userProfileService.UpdateUserProfile(profileFields);
            }

            return View(result);
        }

        public IActionResult Dashboard()
        {
            var pageObjects = new {
                uptimeMonitorTypes = _uptimeMonitorService.GetUptimeMonitorTypes(),
                uptimeStatusTypes = _uptimeMonitorService.GetUptimeStatusTypes(),
                uptimeAlertContactTypes = _uptimeMonitorService.GetUptimeAlertContactTypes(),
                uptimeAlertContactStatusTypes = _uptimeMonitorService.GetUptimeAlertContactStatusTypes(),
                uptimeMobileProviderTypes = _uptimeMonitorService.GetUptimeMobileProviderTypes(),
                uptimeNotificationEventTypes = _uptimeMonitorService.GetUptimeNotificationEventTypes(),
                uptimeMonitorApiKeyTypes = _uptimeMonitorService.GetUptimeMonitorApiKeyTypes(),
                uptimeMaintenanceWindowTypes = _uptimeMonitorService.GetUptimeMaintenanceWindowTypes(),
                uptimeMonitorSortTypes = _uptimeMonitorService.GetUptimeMonitorSortTypes(),
                userTimeZoneTypes = _uptimeMonitorService.GetUserTimeZoneTypes(),
                userSecurityQuestionTypes = _uptimeMonitorService.GetUserSecurityQuestionTypes(),
                twoFactorU2fDevices = _authenticationService.GetUserU2fDevices((User)HttpContext.Items["User"]),
                twoFactorAuthenticationTypes = _uptimeMonitorService.GetTwoFactorAuthenticationTypes(),
                userDetails = _authenticationService.GetUserDetails((User)HttpContext.Items["User"]),
                overallMonitorStats = _uptimeMonitorService.GetOverallMonitorStats(),
                uptimeMonitors = _uptimeMonitorService.GetUptimeMonitors(),
                uptimeAlertContacts = _uptimeMonitorService.GetUptimeAlertContacts(),
                uptimeMaintenanceWindows = _uptimeMonitorService.GetUptimeMaintenanceWindows(),
                uptimePublicStatusPages = _uptimeMonitorService.GetUptimePublicStatusPages(),
                uptimeMonitorApiKeys = _uptimeMonitorService.GetUptimeMonitorApiKeys(),
                latestEvents = _uptimeMonitorService.GetLatestEvents()
            };

            return View(pageObjects);
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult GetUptimeMonitor(int uptimeMonitorId)
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.GetUptimeMonitor(uptimeMonitorId)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult GetUptimeAlertContacts()
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.GetUptimeAlertContacts()), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult GetUptimeAlertContact(int uptimeAlertContactId)
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.GetUptimeAlertContact(uptimeAlertContactId)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult GetUptimeMaintenanceWindow(int uptimeMaintenanceWindowId)
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.GetUptimeMaintenanceWindow(uptimeMaintenanceWindowId)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult GetUptimePublicStatusPage(int uptimePublicStatusPageId)
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.GetUptimePublicStatusPage(uptimePublicStatusPageId)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult GetUptimeMonitorApiKey(int uptimeMonitorApiKeyId)
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.GetUptimeMonitorApiKey(uptimeMonitorApiKeyId)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult GetUptimeMonitorTypes()
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.GetUptimeMonitorTypes()), "application/json");
        }        
        
        [AutoValidateAntiforgeryToken]
        public IActionResult UpdateUptimeMonitor(UptimeMonitor uptimeMonitor)
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.UpdateUptimeMonitor(uptimeMonitor)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult RemoveUptimeMonitor(UptimeMonitor uptimeMonitor)
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.RemoveUptimeMonitor(uptimeMonitor)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult ResetUptimeMonitor(UptimeMonitor uptimeMonitor)
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.ResetUptimeMonitor(uptimeMonitor)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult UptimeMonitorSetStatus(int uptimeMonitorId, bool isStarted)
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.UptimeMonitorSetStatus(uptimeMonitorId, isStarted)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult UpdateUptimeAlertContact(UptimeAlertContact uptimeAlertContact)
        {
            var uptimeAlertContacts = _uptimeMonitorService.UpdateUptimeAlertContact(uptimeAlertContact);
            var result = new {
                uptimeAlertContacts = uptimeAlertContacts,
                uptimeAlertContactId = uptimeAlertContact.UptimeAlertContactId
            };
            return Content(JsonConvert.SerializeObject(result), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult RemoveUptimeAlertContact(UptimeAlertContact uptimeAlertContact)
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.RemoveUptimeAlertContact(uptimeAlertContact)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult UpdateUptimeMaintenanceWindow(UptimeMaintenanceWindow uptimeMaintenanceWindow)
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.UpdateUptimeMaintenanceWindow(uptimeMaintenanceWindow)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult RemoveUptimeMaintenanceWindow(UptimeMaintenanceWindow uptimeMaintenanceWindow)
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.RemoveUptimeMaintenanceWindow(uptimeMaintenanceWindow)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult UpdateUptimePublicStatusPage(UptimePublicStatusPage uptimePublicStatusPage)
        {
            var uptimePublicStatusPages = _uptimeMonitorService.UpdateUptimePublicStatusPage(uptimePublicStatusPage);
            var result = new {
                uptimePublicStatusPages = uptimePublicStatusPages,
                uptimePublicStatusPageId = uptimePublicStatusPage.UptimePublicStatusPageId
            };
            return Content(JsonConvert.SerializeObject(result), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult RemoveUptimePublicStatusPage(UptimePublicStatusPage uptimePublicStatusPage)
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.RemoveUptimePublicStatusPage(uptimePublicStatusPage)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult UptimePublicStatusPageSetStatus(int uptimePublicStatusPageId, bool isActive)
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.UptimePublicStatusPageSetStatus(uptimePublicStatusPageId, isActive)), "application/json");
        }

        public IActionResult UploadLogo(IFormFile logoFile, int uptimePublicStatusPageId)
        {
            var result = true;

            if (logoFile.Length > 0)
            {
                if (!logoFile.FileName.ToLower().EndsWith(".jpg")
                    && !logoFile.FileName.ToLower().EndsWith(".png")) {
                    result = false;
                }

                if (logoFile.Length >= 153600 /* 150 KB */) {
                    result = false;
                }

                if (result)
                {
                    var logoKey = _uptimeMonitorService.GetPublicStatusPageLogoKey(uptimePublicStatusPageId);
                    if (!string.IsNullOrEmpty(logoKey))
                    {
                        var existingLogoImages = System.IO.Directory.GetFiles(_settings.PublicStatusPageLogoBasePath, 
                            logoKey + "*", System.IO.SearchOption.TopDirectoryOnly);
                        if (existingLogoImages.Any())
                        {
                            var removeFilePath = existingLogoImages.First();
                            System.IO.File.Delete(removeFilePath);
                        }

                        var filePath = System.IO.Path.Combine(_settings.PublicStatusPageLogoBasePath, logoKey + System.IO.Path.GetExtension(logoFile.FileName).ToLower());
                        using (var stream = System.IO.File.Create(filePath))
                        {
                            logoFile.CopyTo(stream);
                        }

                        _eventLogService.LogMessage((User)HttpContext.Items["User"], EventNameType.LogoUploaded);

                        result = true;
                    }
                    else
                    {
                        _eventLogService.LogMessage((User)HttpContext.Items["User"], EventNameType.InvalidLogoUpload);

                        result = false;
                    }
                }
            }            

            return Content(result.ToString());
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult UpdateUptimeMonitorApiKey(UptimeMonitorApiKey uptimeMonitorApiKey)
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.UpdateUptimeMonitorApiKey(uptimeMonitorApiKey)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult RemoveUptimeMonitorApiKey(UptimeMonitorApiKey uptimeMonitorApiKey)
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.RemoveUptimeMonitorApiKey(uptimeMonitorApiKey)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult CreateApiKey(string code)
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.CreateApiKey(code)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult RemoveApiKey(string code)
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.RemoveApiKey(code)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult GetLatestEvents(int? uptimeMonitorId = null)
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.GetLatestEvents(uptimeMonitorId)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult GetCurrentUptimeMonitorDetails(int uptimeMonitorId)
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.GetCurrentUptimeMonitorDetails(uptimeMonitorId)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult GetUptimeMonitorChartData(int uptimeMonitorId)
        {
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.GetUptimeMonitorChartData(uptimeMonitorId)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public ActionResult GetOverallMonitorStats()
        {            
            return Content(JsonConvert.SerializeObject(_uptimeMonitorService.GetOverallMonitorStats()), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult ActivateAlertContact(string code, int uptimeAlertContactId)
        {
            var activateAlertContactResult = _authenticationService.ActivateAlertContact(code, uptimeAlertContactId);
            return Content(JsonConvert.SerializeObject(activateAlertContactResult.ActivateAlertContactResultType == ActivateAlertContactResultType.Success), "application/json");
        }

        // Reference URL: https://github.com/adamriddick/AspNetCore.TotpGenerator
        [AutoValidateAntiforgeryToken]
        public ActionResult GetTwoFactorTotpRegistrationQRCode()
        {
            return new JsonResult(_authenticationService.GetTwoFactorTotpRegistrationQRCode((User)HttpContext.Items["User"]));
        }
        
        [AutoValidateAntiforgeryToken]
        public ActionResult ValidateTwoFactorTotpRegistrationCode(string code)
        {            
            return Content(JsonConvert.SerializeObject(_authenticationService.ValidateTwoFactorTotpRegistrationCode((User)HttpContext.Items["User"], code)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public ActionResult GetTwoFactorU2fRegistrationChallenge()
        {
            return Content(JsonConvert.SerializeObject(_authenticationService.CreateU2fRegistrationChallenge((User)HttpContext.Items["User"])), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public ActionResult ValidateTwoFactorU2fRegistrationResponse(string deviceName, string deviceResponse)
        {
            return Content(JsonConvert.SerializeObject(_authenticationService.CompleteU2fRegistration(
                (User)HttpContext.Items["User"],
                deviceName,
                deviceResponse)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public ActionResult GetTwoFactorU2fDevices()
        {
            return Content(JsonConvert.SerializeObject(_authenticationService.GetUserU2fDevices((User)HttpContext.Items["User"])), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public ActionResult RemoveTwoFactorU2fDevice(int userU2fDeviceId)
        {
            return Content(JsonConvert.SerializeObject(_authenticationService.RemoveUserU2fDevice((User)HttpContext.Items["User"], userU2fDeviceId)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public ActionResult RegisterTwoFactorSecurityQuestion(
            int twoFactorAuthenticationTypeId, 
            int userSecurityQuestionTypeId, 
            string userSecurityAnswer)
        {            
            return Content(JsonConvert.SerializeObject(_authenticationService.RegisterTwoFactorSecurityQuestion(
                (User)HttpContext.Items["User"],
                twoFactorAuthenticationTypeId,
                userSecurityQuestionTypeId, 
                userSecurityAnswer)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public ActionResult UnregisterTwoFactor()
        {            
            return Content(JsonConvert.SerializeObject(_authenticationService.UnregisterTwoFactor((User)HttpContext.Items["User"])), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public ActionResult GetUserDetails()
        {            
            return Content(JsonConvert.SerializeObject(_authenticationService.GetUserDetails((User)HttpContext.Items["User"])), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public ActionResult UpdateUserDetails(string firstName, string lastName, int userTimeZoneTypeId)
        {
            return Content(JsonConvert.SerializeObject(_authenticationService.UpdateUserDetails((User)HttpContext.Items["User"], firstName, lastName, userTimeZoneTypeId)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public ActionResult UpdateUserPassword(string currentPassword, string newPassword, string repeatNewPassword)
        {
            return Content(JsonConvert.SerializeObject(_authenticationService.UpdateUserPassword((User)HttpContext.Items["User"], currentPassword, newPassword, repeatNewPassword)), "application/json");
        }

        [AutoValidateAntiforgeryToken]
        public ActionResult RequestUserEmailUpdate(string newEmail)
        {
            return Content(JsonConvert.SerializeObject(_authenticationService.RequestUserEmailUpdate((User)HttpContext.Items["User"], newEmail)), "application/json");
        }
    }
}
