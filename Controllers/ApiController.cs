﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SiteUptime.Models;
using SiteUptime.Services.Interfaces;
using SiteUptime.Controllers.Base;
using SiteUptime.Helpers.Authentication;
using Newtonsoft.Json;
using SiteUptime.Helpers.Api;
using SiteUptime.Helpers.Api.Requests;
using SiteUptime.Helpers.Api.Responses;
using Microsoft.Extensions.Options;

namespace SiteUptime.Controllers
{
    [ClaimRequirement(SecureClaimTypes.Permission, "MainApiKey,MonitorSpecificApiKey,ReadOnlyApiKey")]
    [Route("v2")]
    public class ApiController : BaseController
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IUptimeMonitorService _uptimeMonitorService;
        private readonly IAuthenticationService _authenticationService;
        private readonly Settings _settings;

        private readonly List<int> _pollingIntervals;

        public ApiController(ILogger<HomeController> logger,
            IUptimeMonitorService uptimeMonitorService,
            IAuthenticationService authenticationService,
            IOptions<Settings> settings)
        {
            _logger = logger;
            _uptimeMonitorService = uptimeMonitorService;
            _authenticationService = authenticationService;
            _settings = settings.Value;

            _pollingIntervals = _settings.PollingIntervals.Split(',').Select(x => int.Parse(x)).ToList();
        }        

        [ClaimRequirement(SecureClaimTypes.Permission, "MainApiKey,ReadOnlyApiKey")]
        [HttpPost("getAccountDetails")]
        [Consumes("application/x-www-form-urlencoded")]
        [Produces("application/json", "application/xml")]
        public IActionResult GetAccountDetails(ApiRequestAccountDetails request)
        {
            var userDetails = _authenticationService.GetUserDetails((User)HttpContext.Items["User"]);
            var overallMonitorStats = _uptimeMonitorService.GetOverallMonitorStats();   

            var response = new ApiResponseAccountDetails
            {
                email = userDetails.Email,
                monitor_limit = overallMonitorStats.MaxMonitorCount ?? 50,
                monitor_interval = overallMonitorStats.MinInterval ?? 1,
                up_monitors = overallMonitorStats.UpMonitorCount ?? 0,
                down_monitors = overallMonitorStats.DownMonitorCount ?? 0,
                paused_monitors = overallMonitorStats.PausedMonitorCount ?? 0
            };         

            if (request.format == ApiResponseTypes.Xml)
            {
                return ApiResponseHelper.GetXmlResponse(response);
            }
            else
            {
                return ApiResponseHelper.GetJsonResponse(
                    new { 
                        stat = "ok",
                        account = response
                    },
                    request.callback
                );
            }
        }

        [ClaimRequirement(SecureClaimTypes.Permission, "MainApiKey,MonitorSpecificApiKey,ReadOnlyApiKey")]
        [HttpPost("getMonitors")]
        [Consumes("application/x-www-form-urlencoded")]
        [Produces("application/json", "application/xml")]
        public IActionResult GetMonitors(ApiRequestMonitors request)
        {
            var uptimeMonitors = _uptimeMonitorService.GetUptimeMonitorsWithInternalDetails();

            var monitors = new List<ApiResponseMonitorMonitor>();
            foreach (var uptimeMonitor in uptimeMonitors)
            {
                monitors.Add(new ApiResponseMonitorMonitor
                {
                    id = uptimeMonitor.UptimeMonitorId,
                    friendly_name = uptimeMonitor.Name,
                    url = uptimeMonitor.Url,
                    type = uptimeMonitor.UptimeMonitorTypeId.ToString(),
                    sub_type = string.Empty,
                    keyword_type = uptimeMonitor.KeywordAlertWhenExists == true ? 1.ToString() :
                        (uptimeMonitor.KeywordAlertWhenExists == false ? 2.ToString() : "0"),
                    keyword_value = uptimeMonitor.Keyword ?? string.Empty,
                    http_username = uptimeMonitor.Username ?? string.Empty,
                    http_password = uptimeMonitor.Password ?? string.Empty,
                    port = uptimeMonitor.Port != null ? uptimeMonitor.Port.Value.ToString() : string.Empty,
                    interval = (uptimeMonitor.Interval * 60).ToString(),
                    status = ApiParseHelper.GetMonitorStatusType(uptimeMonitor.UptimeStatusTypeId),
                    create_datetime = ((DateTimeOffset)uptimeMonitor.CreatedDate).ToUnixTimeSeconds().ToString()
                });
            }

            var response = new ApiResponseMonitors
            {
                stat = "ok",
                pagination = new ApiResponseMonitorPagination
                {
                    offset = 0,
                    limit = 50,
                    total = uptimeMonitors.Count()
                },
                monitor = monitors
            };         

            if (request.format == ApiResponseTypes.Xml)
            {
                return ApiResponseHelper.GetXmlResponse(response);
            }
            else
            {
                return ApiResponseHelper.GetJsonResponse(
                    new { 
                        stat = "ok",
                        pagination = new ApiResponseMonitorPagination
                        {
                            offset = 0,
                            limit = 50,
                            total = uptimeMonitors.Count()
                        },
                        monitors = monitors
                    },
                    request.callback
                );
            }
        }

        [ClaimRequirement(SecureClaimTypes.Permission, "MainApiKey")]
        [HttpPost("newMonitor")]
        [Consumes("application/x-www-form-urlencoded")]
        [Produces("application/json", "application/xml")]
        public IActionResult NewMonitor(ApiRequestMonitor request)
        {
            return EditMonitor(request, isNewMonitor: true);
        }

        [ClaimRequirement(SecureClaimTypes.Permission, "MainApiKey")]
        [HttpPost("editMonitor")]
        [Consumes("application/x-www-form-urlencoded")]
        [Produces("application/json", "application/xml")]
        public IActionResult EditMonitor(ApiRequestMonitor request, bool isNewMonitor = false)
        {
            var uptimeMonitor = new UptimeMonitor
            {
                UptimeMonitorId = isNewMonitor ? 0 : request.id,
                Name = request.friendly_name,
                Url = request.url,
                UptimeMonitorTypeId = int.TryParse(request.type, out int typeId) ? typeId : 0,                                
                // sub_type                
                Port = int.TryParse(request.port, out int portVal) ? portVal : (int?)null,                
                KeywordAlertWhenExists = request.keyword_type == 1.ToString() ? true : 
                    (request.keyword_type == 2.ToString() ? false : (bool?)null),
                Keyword = request.keyword_value,
                IntervalSliderValue = ApiParseHelper.GetMonitoringIntervalSliderValue(request.interval, _pollingIntervals),
                // http_username
                // http_password
                // http_method
                // post_type
                // post_value
                // post_content_type
                // post_content_type
                SelectedContactIds = ApiParseHelper.GetAlertContacts(request.alert_contacts),
                // mwindows
                // custom_http_headers
                // custom_http_statuses
                // ignore_ssl_errors
            };

            var uptimeMonitorResult = _uptimeMonitorService.UpdateUptimeMonitor(uptimeMonitor, isApiCall: true);   
            if (uptimeMonitor.ApiError == null)
            {
                var response = new ApiResponseMonitor
                {
                    status = isNewMonitor ? ApiParseHelper.GetMonitorStatusType(uptimeMonitor.UptimeStatusTypeId) : null,
                    id = uptimeMonitor.UptimeMonitorId
                };         

                if (request.format == ApiResponseTypes.Xml)
                {
                    return ApiResponseHelper.GetXmlResponse(response);
                }
                else
                {
                    return ApiResponseHelper.GetJsonResponse(
                        new { 
                            stat = "ok",
                            monitor = response
                        },
                        request.callback
                    );
                }
            }
            else
            {
                if (request.format == ApiResponseTypes.Xml)
                {
                    return ApiResponseHelper.GetXmlResponse(uptimeMonitor.ApiError);
                }
                else
                {
                    return ApiResponseHelper.GetJsonResponse(
                        new { 
                            stat = "fail",
                            error = new {
                                type = uptimeMonitor.ApiError.Type,
                                message = uptimeMonitor.ApiError.Message,
                            }
                        },
                        request.callback
                    );
                }
            }
        }

        [ClaimRequirement(SecureClaimTypes.Permission, "MainApiKey")]
        [HttpPost("deleteMonitor")]
        [Consumes("application/x-www-form-urlencoded")]
        [Produces("application/json", "application/xml")]
        public IActionResult DeleteMonitor(ApiRequestMonitor request)
        {
            var uptimeMonitor = new UptimeMonitor
            {
                UptimeMonitorId = request.id                
            };

            var uptimeMonitorResult = _uptimeMonitorService.RemoveUptimeMonitor(uptimeMonitor, isApiCall: true);   
            if (uptimeMonitor.ApiError == null)
            {
                var response = new ApiResponseMonitor
                {
                    id = uptimeMonitor.UptimeMonitorId
                };         

                if (request.format == ApiResponseTypes.Xml)
                {
                    return ApiResponseHelper.GetXmlResponse(response);
                }
                else
                {
                    return ApiResponseHelper.GetJsonResponse(
                        new { 
                            stat = "ok",
                            monitor = response
                        },
                        request.callback
                    );
                }
            }
            else
            {
                if (request.format == ApiResponseTypes.Xml)
                {
                    return ApiResponseHelper.GetXmlResponse(uptimeMonitor.ApiError);
                }
                else
                {
                    return ApiResponseHelper.GetJsonResponse(
                        new { 
                            stat = "fail",
                            error = new {
                                type = uptimeMonitor.ApiError.Type,
                                message = uptimeMonitor.ApiError.Message,
                            }
                        },
                        request.callback
                    );
                }
            }
        }

        [ClaimRequirement(SecureClaimTypes.Permission, "MainApiKey")]
        [HttpPost("resetMonitor")]
        [Consumes("application/x-www-form-urlencoded")]
        [Produces("application/json", "application/xml")]
        public IActionResult ResetMonitor(ApiRequestMonitor request)
        {
            var uptimeMonitor = new UptimeMonitor
            {
                UptimeMonitorId = request.id                
            };

            var uptimeMonitorResult = _uptimeMonitorService.ResetUptimeMonitor(uptimeMonitor, isApiCall: true);   
            if (uptimeMonitor.ApiError == null)
            {
                var response = new ApiResponseMonitor
                {
                    id = uptimeMonitor.UptimeMonitorId
                };         

                if (request.format == ApiResponseTypes.Xml)
                {
                    return ApiResponseHelper.GetXmlResponse(response);
                }
                else
                {
                    return ApiResponseHelper.GetJsonResponse(
                        new { 
                            stat = "ok",
                            monitor = response
                        },
                        request.callback
                    );
                }
            }
            else
            {
                if (request.format == ApiResponseTypes.Xml)
                {
                    return ApiResponseHelper.GetXmlResponse(uptimeMonitor.ApiError);
                }
                else
                {
                    return ApiResponseHelper.GetJsonResponse(
                        new { 
                            stat = "fail",
                            error = new {
                                type = uptimeMonitor.ApiError.Type,
                                message = uptimeMonitor.ApiError.Message,
                            }
                        },
                        request.callback
                    );
                }
            }
        }        

        [ClaimRequirement(SecureClaimTypes.Permission, "MainApiKey,ReadOnlyApiKey")]
        [HttpPost("getAlertContacts")]
        [Consumes("application/x-www-form-urlencoded")]
        [Produces("application/json", "application/xml")]
        public IActionResult GetAlertContacts(ApiRequestAlertContacts request)
        {
            var uptimeAlertContacts = _uptimeMonitorService.GetUptimeAlertContactsWithInternalDetails();

            var contacts = new List<ApiResponseAlertContactContact>();
            foreach (var uptimeAlertContact in uptimeAlertContacts)
            {
                contacts.Add(new ApiResponseAlertContactContact
                {
                    id = uptimeAlertContact.UptimeAlertContactId,
                    friendly_name = uptimeAlertContact.Name,                    
                    type = ApiParseHelper.GetAlertContactType(uptimeAlertContact.UptimeAlertContactTypeId),
                    status = ApiParseHelper.GetAlertContactStatusType(uptimeAlertContact.UptimeAlertContactStatusTypeId),
                    value = uptimeAlertContact.Address
                });
            }

            var response = new ApiResponseAlertContacts
            {
                offset = 0,
                limit = 50,
                total = uptimeAlertContacts.Count(),                
                alert_contact = contacts
            };         

            if (request.format == ApiResponseTypes.Xml)
            {
                return ApiResponseHelper.GetXmlResponse(response);
            }
            else
            {
                return ApiResponseHelper.GetJsonResponse(
                    new { 
                        stat = "ok",
                        offset = 0,
                        limit = 50,
                        total = uptimeAlertContacts.Count(),                        
                        alert_contacts = contacts
                    },
                    request.callback
                );
            }
        }

        [ClaimRequirement(SecureClaimTypes.Permission, "MainApiKey")]
        [HttpPost("newAlertContact")]
        [Consumes("application/x-www-form-urlencoded")]
        [Produces("application/json", "application/xml")]
        public IActionResult NewAlertContact(ApiRequestAlertContact request)
        {
            return EditAlertContact(request, isNewAlertContact: true);
        }

        [ClaimRequirement(SecureClaimTypes.Permission, "MainApiKey")]
        [HttpPost("editAlertContact")]
        [Consumes("application/x-www-form-urlencoded")]
        [Produces("application/json", "application/xml")]
        public IActionResult EditAlertContact(ApiRequestAlertContact request, bool isNewAlertContact = false)
        {
            var uptimeAlertContact = new UptimeAlertContact
            {
                UptimeAlertContactId = isNewAlertContact ? 0 : request.id,
                UptimeAlertContactTypeId = ApiParseHelper.GetInputAlertContactType(request.type),
                Address = request.value,
                Name = request.friendly_name
            };

            var uptimeAlertContactResult = _uptimeMonitorService.UpdateUptimeAlertContact(uptimeAlertContact, isApiCall: true);   
            if (uptimeAlertContact.ApiError == null)
            {
                var response = new ApiResponseAlertContact();

                if (isNewAlertContact)
                {
                    response = new ApiResponseAlertContactNew
                    {                    
                        id = uptimeAlertContact.UptimeAlertContactId
                    };   
                }
                else
                {
                    response = new ApiResponseAlertContactEditDelete
                    {                    
                        id = uptimeAlertContact.UptimeAlertContactId
                    };
                }

                if (request.format == ApiResponseTypes.Xml)
                {
                    return ApiResponseHelper.GetXmlResponse(response);
                }
                else
                {
                    if (isNewAlertContact)
                    {
                        return ApiResponseHelper.GetJsonResponse(
                            new { 
                                stat = "ok",
                                alertcontact = response
                            },
                            request.callback
                        );
                    }
                    else
                    {
                        return ApiResponseHelper.GetJsonResponse(
                            new {
                                stat = "ok",
                                alert_contact = response
                            },
                            request.callback
                        );
                    }
                }
            }
            else
            {
                if (request.format == ApiResponseTypes.Xml)
                {
                    return ApiResponseHelper.GetXmlResponse(uptimeAlertContact.ApiError);
                }
                else
                {
                    return ApiResponseHelper.GetJsonResponse(
                        new { 
                            stat = "fail",
                            error = new {
                                type = uptimeAlertContact.ApiError.Type,
                                message = uptimeAlertContact.ApiError.Message,
                            }
                        },
                        request.callback
                    );
                }
            }
        }

        [ClaimRequirement(SecureClaimTypes.Permission, "MainApiKey")]
        [HttpPost("deleteAlertContact")]
        [Consumes("application/x-www-form-urlencoded")]
        [Produces("application/json", "application/xml")]
        public IActionResult DeleteAlertContact(ApiRequestAlertContact request)
        {
            var uptimeAlertContact = new UptimeAlertContact
            {
                UptimeAlertContactId = request.id                
            };

            var uptimeAlertContactResult = _uptimeMonitorService.RemoveUptimeAlertContact(uptimeAlertContact, isApiCall: true);   
            if (uptimeAlertContact.ApiError == null)
            {
                var response = new ApiResponseAlertContactEditDelete
                {
                    id = uptimeAlertContact.UptimeAlertContactId
                };         

                if (request.format == ApiResponseTypes.Xml)
                {
                    return ApiResponseHelper.GetXmlResponse(response);
                }
                else
                {
                    return ApiResponseHelper.GetJsonResponse(
                        new { 
                            stat = "ok",
                            alert_contact = response
                        },
                        request.callback
                    );
                }
            }
            else
            {
                if (request.format == ApiResponseTypes.Xml)
                {
                    return ApiResponseHelper.GetXmlResponse(uptimeAlertContact.ApiError);
                }
                else
                {
                    return ApiResponseHelper.GetJsonResponse(
                        new { 
                            stat = "fail",
                            error = new {
                                type = uptimeAlertContact.ApiError.Type,
                                message = uptimeAlertContact.ApiError.Message,
                            }
                        },
                        request.callback
                    );
                }
            }
        }

        [ClaimRequirement(SecureClaimTypes.Permission, "MainApiKey,ReadOnlyApiKey")]
        [HttpPost("getMWindows")]
        [Consumes("application/x-www-form-urlencoded")]
        [Produces("application/json", "application/xml")]
        public IActionResult GetMWindows(ApiRequestMaintenanceWindows request)
        {
            var uptimeMaintenanceWindows = _uptimeMonitorService.GetUptimeMaintenanceWindowsWithInternalDetails();

            var windows = new List<ApiResponseMaintenanceWindowWindow>();
            foreach (var uptimeMaintenanceWindow in uptimeMaintenanceWindows)
            {
                windows.Add(new ApiResponseMaintenanceWindowWindow
                {
                    id = uptimeMaintenanceWindow.UptimeMaintenanceWindowId,
                    type = uptimeMaintenanceWindow.UptimeMaintenanceWindowTypeId.ToString(),
                    friendly_name = uptimeMaintenanceWindow.Name,                    
                    start_time = uptimeMaintenanceWindow.StartTime,
                    duration = uptimeMaintenanceWindow.Duration.ToString(),
                    value = uptimeMaintenanceWindow.Days ?? string.Empty,
                    status = uptimeMaintenanceWindow.IsActive ? 1.ToString() : 0.ToString()
                });
            }

            var response = new ApiResponseMaintenanceWindows
            {
                pagination = new ApiResponseMaintenanceWindowPagination
                {
                    offset = 0,
                    limit = 50,
                    total = uptimeMaintenanceWindows.Count()
                },
                mwindow = windows
            };         

            if (request.format == ApiResponseTypes.Xml)
            {                
                return ApiResponseHelper.GetXmlResponse(response);
            }
            else
            {
                return ApiResponseHelper.GetJsonResponse(
                    new { 
                        stat = "ok",
                        pagination = new {
                            offset = 0,
                            limit = 50,
                            total = uptimeMaintenanceWindows.Count()
                        },                      
                        mwindows = windows
                    },
                    request.callback
                );
            }
        }

        [ClaimRequirement(SecureClaimTypes.Permission, "MainApiKey")]
        [HttpPost("newMWindow")]
        [Consumes("application/x-www-form-urlencoded")]
        [Produces("application/json", "application/xml")]
        public IActionResult NewMWindow(ApiRequestMaintenanceWindow request)
        {
            return EditMWindow(request, isNewMaintenanceWindow: true);
        }

        [ClaimRequirement(SecureClaimTypes.Permission, "MainApiKey")]
        [HttpPost("editMWindow")]
        [Consumes("application/x-www-form-urlencoded")]
        [Produces("application/json", "application/xml")]
        public IActionResult EditMWindow(ApiRequestMaintenanceWindow request, bool isNewMaintenanceWindow = false)
        {
            var uptimeMaintenanceWindow = new UptimeMaintenanceWindow
            {
                UptimeMaintenanceWindowId = isNewMaintenanceWindow ? 0 : request.id,
                Name = request.friendly_name,
                UptimeMaintenanceWindowTypeId = int.TryParse(request.type, out int typeId) ? typeId : 0,
                Days = request.value,                
                StartTime = request.start_time,
                Duration = int.TryParse(request.duration, out int durationVal) ? durationVal : 0
            };

            var uptimeMaintenanceWindowResult = _uptimeMonitorService.UpdateUptimeMaintenanceWindow(uptimeMaintenanceWindow, isApiCall: true);   
            if (uptimeMaintenanceWindow.ApiError == null)
            {
                var response = new ApiResponseMaintenanceWindow
                {                    
                    id = uptimeMaintenanceWindow.UptimeMaintenanceWindowId
                };         

                if (request.format == ApiResponseTypes.Xml)
                {
                    return ApiResponseHelper.GetXmlResponse(response);
                }
                else
                {
                    return ApiResponseHelper.GetJsonResponse(
                        new { 
                            stat = "ok",
                            mwindow = response
                        },
                        request.callback
                    );
                }
            }
            else
            {
                if (request.format == ApiResponseTypes.Xml)
                {
                    return ApiResponseHelper.GetXmlResponse(uptimeMaintenanceWindow.ApiError);
                }
                else
                {
                    return ApiResponseHelper.GetJsonResponse(
                        new { 
                            stat = "fail",
                            error = new {
                                type = uptimeMaintenanceWindow.ApiError.Type,
                                message = uptimeMaintenanceWindow.ApiError.Message,
                            }
                        },
                        request.callback
                    );
                }
            }
        }

        [ClaimRequirement(SecureClaimTypes.Permission, "MainApiKey")]
        [HttpPost("deleteMWindow")]
        [Consumes("application/x-www-form-urlencoded")]
        [Produces("application/json", "application/xml")]
        public IActionResult DeleteMWindow(ApiRequestMaintenanceWindow request)
        {
            var uptimeMaintenanceWindow = new UptimeMaintenanceWindow
            {
                UptimeMaintenanceWindowId = request.id                
            };

            var uptimeMaintenanceWindowResult = _uptimeMonitorService.RemoveUptimeMaintenanceWindow(uptimeMaintenanceWindow, isApiCall: true);   
            if (uptimeMaintenanceWindow.ApiError == null)
            {
                var response = new ApiResponseMaintenanceWindow
                {
                    id = uptimeMaintenanceWindow.UptimeMaintenanceWindowId
                };         

                if (request.format == ApiResponseTypes.Xml)
                {
                    return ApiResponseHelper.GetXmlResponse(response);
                }
                else
                {
                    return ApiResponseHelper.GetJsonResponse(
                        new { 
                            stat = "ok",
                            mwindow = response
                        },
                        request.callback
                    );
                }
            }
            else
            {
                if (request.format == ApiResponseTypes.Xml)
                {
                    return ApiResponseHelper.GetXmlResponse(uptimeMaintenanceWindow.ApiError);
                }
                else
                {
                    return ApiResponseHelper.GetJsonResponse(
                        new { 
                            stat = "fail",
                            error = new {
                                type = uptimeMaintenanceWindow.ApiError.Type,
                                message = uptimeMaintenanceWindow.ApiError.Message,
                            }
                        },
                        request.callback
                    );
                }
            }
        }

        [ClaimRequirement(SecureClaimTypes.Permission, "MainApiKey,ReadOnlyApiKey")]
        [HttpPost("getPSPs")]
        [Consumes("application/x-www-form-urlencoded")]
        [Produces("application/json", "application/xml")]
        public IActionResult GetPSPs(ApiRequestPublicStatusPages request)
        {
            var uptimePublicStatusPages = _uptimeMonitorService.GetUptimePublicStatusPagesWithInternalDetails();

            var pages = new List<ApiResponsePublicStatusPagePage>();
            foreach (var uptimePublicStatusPage in uptimePublicStatusPages)
            {
                pages.Add(new ApiResponsePublicStatusPagePage
                {
                    id = uptimePublicStatusPage.UptimePublicStatusPageId,
                    friendly_name = uptimePublicStatusPage.Name,                    
                    monitors = ApiParseHelper.GetPublicStatusPageMonitors(uptimePublicStatusPage.SelectedMonitorIds),
                    sort = uptimePublicStatusPage.UptimeMonitorSortTypeId.ToString(),
                    status = uptimePublicStatusPage.IsActive ? 1.ToString() : 0.ToString(),
                    standard_url = _settings.SiteUrl + "/?" + uptimePublicStatusPage.StatusPageKey,
                    custom_url = uptimePublicStatusPage.CustomDomain ?? string.Empty
                });
            }

            var response = new ApiResponsePublicStatusPages
            {  
                pagination = new ApiResponsePublicStatusPagePagination
                {
                    offset = 0,
                    limit = 50,
                    total = uptimePublicStatusPages.Count()
                },            
                psp = pages
            };         

            if (request.format == ApiResponseTypes.Xml)
            {
                return ApiResponseHelper.GetXmlResponse(response);
            }
            else
            {
                return ApiResponseHelper.GetJsonResponse(
                    new { 
                        stat = "ok",
                        pagination = new {
                            offset = 0,
                            limit = 50,
                            total = uptimePublicStatusPages.Count()
                        },                        
                        psps = pages
                    },
                    request.callback
                );
            }
        }

        [ClaimRequirement(SecureClaimTypes.Permission, "MainApiKey")]
        [HttpPost("newPSP")]
        [Consumes("application/x-www-form-urlencoded")]
        [Produces("application/json", "application/xml")]
        public IActionResult NewPSP(ApiRequestPublicStatusPage request)
        {
            return EditPSP(request, isNewPublicStatusPage: true);
        }

        [ClaimRequirement(SecureClaimTypes.Permission, "MainApiKey")]
        [HttpPost("editPSP")]
        [Consumes("application/x-www-form-urlencoded")]
        [Produces("application/json", "application/xml")]
        public IActionResult EditPSP(ApiRequestPublicStatusPage request, bool isNewPublicStatusPage = false)
        {
            var uptimePublicStatusPage = new UptimePublicStatusPage
            {
                UptimePublicStatusPageId = isNewPublicStatusPage ? 0 : request.id,
                Name = request.friendly_name,
                SelectedMonitorIds = ApiParseHelper.GetInputPublicStatusPageMonitors(request.monitors),
                CustomDomain = request.custom_domain,
                Password = request.password,
                UptimeMonitorSortTypeId = int.TryParse(request.sort, out int sortVal) ? sortVal : 1,
                HideBrandingLogo = request.hide_url_links == 1.ToString(),
                IsActive = request.status == 1.ToString()
            };

            var uptimePublicStatusPageResult = _uptimeMonitorService.UpdateUptimePublicStatusPage(uptimePublicStatusPage, isApiCall: true);   
            if (uptimePublicStatusPage.ApiError == null)
            {
                var response = new ApiResponsePublicStatusPage
                {                    
                    id = uptimePublicStatusPage.UptimePublicStatusPageId
                };         

                if (request.format == ApiResponseTypes.Xml)
                {
                    return ApiResponseHelper.GetXmlResponse(response);
                }
                else
                {
                    return ApiResponseHelper.GetJsonResponse(
                        new { 
                            stat = "ok",
                            psp = response
                        },
                        request.callback
                    );
                }
            }
            else
            {
                if (request.format == ApiResponseTypes.Xml)
                {
                    return ApiResponseHelper.GetXmlResponse(uptimePublicStatusPage.ApiError);
                }
                else
                {
                    return ApiResponseHelper.GetJsonResponse(
                        new { 
                            stat = "fail",
                            error = new {
                                type = uptimePublicStatusPage.ApiError.Type,
                                message = uptimePublicStatusPage.ApiError.Message,
                            }
                        },
                        request.callback
                    );
                }
            }
        }

        [ClaimRequirement(SecureClaimTypes.Permission, "MainApiKey")]
        [HttpPost("deletePSP")]
        [Consumes("application/x-www-form-urlencoded")]
        [Produces("application/json", "application/xml")]
        public IActionResult DeletePSP(ApiRequestPublicStatusPage request)
        {
            var uptimePublicStatusPage = new UptimePublicStatusPage
            {
                UptimePublicStatusPageId = request.id                
            };

            var uptimePublicStatusPageResult = _uptimeMonitorService.RemoveUptimePublicStatusPage(uptimePublicStatusPage, isApiCall: true);   
            if (uptimePublicStatusPage.ApiError == null)
            {
                var response = new ApiResponsePublicStatusPage
                {
                    id = uptimePublicStatusPage.UptimePublicStatusPageId
                };         

                if (request.format == ApiResponseTypes.Xml)
                {
                    return ApiResponseHelper.GetXmlResponse(response);
                }
                else
                {
                    return ApiResponseHelper.GetJsonResponse(
                        new { 
                            stat = "ok",
                            psp = response
                        },
                        request.callback
                    );
                }
            }
            else
            {
                if (request.format == ApiResponseTypes.Xml)
                {
                    return ApiResponseHelper.GetXmlResponse(uptimePublicStatusPage.ApiError);
                }
                else
                {
                    return ApiResponseHelper.GetJsonResponse(
                        new { 
                            stat = "fail",
                            error = new {
                                type = uptimePublicStatusPage.ApiError.Type,
                                message = uptimePublicStatusPage.ApiError.Message,
                            }
                        },
                        request.callback
                    );
                }
            }
        }
    }
}
