﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SiteUptime.Models;
using SiteUptime.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using SiteUptime.Controllers.Base;
using SiteUptime.Helpers.Authentication;
using Microsoft.Extensions.Options;
using SiteUptime.Helpers.Recaptcha;
using SiteUptime.Helpers.MimeTypes;
using SiteUptime.Helpers;
using SiteUptime.Helpers.Results;
using SiteUptime.Helpers.Types;
using SiteUptime.Helpers.CacheHelpers;
using System.Security.Cryptography;

namespace SiteUptime.Controllers
{
    [ClaimRequirement(SecureClaimTypes.Permission, "")]
    public class HomeController : BaseController
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IAuthenticationService _authenticationService;
        private readonly Settings _settings;
        private readonly IUptimeMonitorService _uptimeMonitorService;
        private readonly IEventLogService _eventLogService;
        private readonly ISyndicationFeedService _syndicationFeedService;

        public HomeController(ILogger<HomeController> logger,
            IAuthenticationService authenticationService,
            IOptions<Settings> settings,
            IUptimeMonitorService uptimeMonitorService,
            IEventLogService eventLogService,
            ISyndicationFeedService syndicationFeedService)
        {
            _logger = logger;
            _authenticationService = authenticationService;
            _settings = settings.Value;
            _uptimeMonitorService = uptimeMonitorService;
            _eventLogService = eventLogService;
            _syndicationFeedService = syndicationFeedService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            if (!string.IsNullOrEmpty(GetPublicStatusPageKey()))
            {
                return HandlePublicStatusPage();
            }

            if (HttpContext.Items["User"] != null)
            {
                return RedirectToAction("Index", "Secure");
            }
            else
            {
                _authenticationService.PopulateUsersIfEmptyDatabase();
            }

            return View();
        }

        [HttpGet]
        public IActionResult Feed(string id)
        {
            Task<string> syndicationResult = _syndicationFeedService.GenerateFeed(id);
            syndicationResult.Wait();
            var result = syndicationResult.Result;
            
            if (!string.IsNullOrEmpty(result))
            {
                return Content(result, "application/xml");
            }
            else
            {
                return NotFound();
            }
        }
        
        [HttpGet]
        public IActionResult SignUp()
        {
            return View();
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult SignUp(string firstName, string lastName, string email, string password)
        {
            if (!IsValidRecaptcha())
            {
                return View(new SignUpResult 
                    {
                        SignUpResultType = SignUpResultType.InvalidRecaptcha
                    }
                );
            }

            var signUpResult = _authenticationService.SignUp(firstName, lastName, email, password);
            if (signUpResult.SignUpResultType == SignUpResultType.Success)
            {
                return RedirectToAction("SignUpEmailSent");
            }
            else
            {
                return View(signUpResult);
            }
        }

        [HttpGet]
        public IActionResult SignUpEmailSent()
        {
            return View();           
        }

        [HttpGet]
        public IActionResult ActivateAccount(string code)
        {
            var activateAccountResult = _authenticationService.ActivateAccount(code);
            return View(activateAccountResult);           
        }

        [HttpGet]
        public IActionResult UnlockAccount(string code)
        {
            var unlockAccountResult = _authenticationService.UnlockAccount(code);
            return View(unlockAccountResult);           
        }

        [HttpGet]
        public IActionResult ActivateEmail(string code)
        {
            var activateEmailResult = _authenticationService.ActivateEmail(code);
            return View(activateEmailResult);           
        }

        [HttpGet]
        public IActionResult ActivateAlertContact(string code)
        {
            var activateAlertContactResult = _authenticationService.ActivateAlertContact(code);
            return View(activateAlertContactResult);           
        }

        [HttpGet]
        public IActionResult TwoFactorAuth()
        {
            var twoFactorChallenge = _authenticationService.GetUserTwoFactorChallege((User)HttpContext.Items["User"]);
            return View(twoFactorChallenge);
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult TwoFactorAuth(string code, string deviceResponse)
        {
            if (!string.IsNullOrEmpty(deviceResponse)
                && _authenticationService.ValidateU2fChallenge(
                    (UserSession)HttpContext.Items["UserSession"],
                    (User)HttpContext.Items["User"],
                    deviceResponse
                ))
            {
                return RedirectToAction("Index", "Secure");
            }
            else if (!string.IsNullOrEmpty(code)
                && _authenticationService.ValidateSessionTwoFactorCode(
                    (UserSession)HttpContext.Items["UserSession"],
                    (User)HttpContext.Items["User"], 
                    code
                ))
            {
                return RedirectToAction("Index", "Secure");
            }
            else
            {
                var user = (User)HttpContext.Items["User"];
                if (user != null)
                {
                    var authenticationResult = new AuthenticationResult();
                    authenticationResult.Username = user.Username;
                    authenticationResult.AuthenticationResultType = AuthenticationResultType.InvalidTwoFactorResponse;

                    SignOut();

                    return View("Index", authenticationResult);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
        }

        [HttpGet]
        public IActionResult ForgotPassword(string code)
        {
            var forgotPasswordResult = _authenticationService.ValidateForgotPasswordEmailLinkCode(code);
            if (forgotPasswordResult.ForgotPasswordResultType == ForgotPasswordResultType.Success)
            {
                return View(forgotPasswordResult);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult ForgotPassword(string email, string code, string username, string password, string confirmPassword)
        {
            if (!IsValidRecaptcha())
            {
                return View(new ForgotPasswordResult 
                    {
                        ForgotPasswordResultType = ForgotPasswordResultType.InvalidRecaptcha
                    }
                );
            }

            if (!string.IsNullOrEmpty(email))
            {
                _authenticationService.SendResetPasswordEmailLink(email);
                return RedirectToAction("ResetPasswordEmailSent");
            }
            else
            {
                var forgotPasswordResult = _authenticationService.ValidateAndApplyNewUserPasswordWithRecoveryCode(
                    code, 
                    username,
                    password, 
                    confirmPassword);
                if (forgotPasswordResult.ForgotPasswordResultType == ForgotPasswordResultType.Success)
                {
                    return SignIn(username, password);
                }
                else
                {
                    return View(forgotPasswordResult);
                }
            }
        }

        [HttpGet]
        public IActionResult LostTwoFactor()
        {
            var userDetails = _authenticationService.GetUserDetails((User)HttpContext.Items["User"]);
            return View(userDetails);            
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult LostTwoFactor(string answer)
        {
            if (!IsValidRecaptcha())
            {
                var userDetails = _authenticationService.GetUserDetails((User)HttpContext.Items["User"]);
                userDetails.IsInvalidRecaptcha = true;
                return View("LostTwoFactor", userDetails);
            }

            if (_authenticationService.ValidateTwoFactorSecurityAnswer(
                (UserSession)HttpContext.Items["UserSession"],
                (User)HttpContext.Items["User"], 
                answer))
            {
                return RedirectToAction("Index", "Secure");
            }
            else
            {
                var user = (User)HttpContext.Items["User"];
                var authenticationResult = new AuthenticationResult();
                authenticationResult.Username = user.Username;
                authenticationResult.AuthenticationResultType = AuthenticationResultType.InvalidTwoFactorSecurityAnswer;

                SignOut();

                return View("Index", authenticationResult);
            }
        }

        [HttpGet]
        public IActionResult ResetPasswordEmailSent()
        {
            return View();
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult SignIn(string username, string password) 
        {
            var authenticationResult = new AuthenticationResult();
            var success = _authenticationService.Authenticate(username, password);
            if (success)
            {
                var sessionId = _authenticationService.CreateSession(username);
                var options = new CookieOptions
                {
                    IsEssential = true,
                    HttpOnly = true,
                    Secure = _settings.UseSecureCookies
                };

                Response.Cookies.Append(_settings.SessionCookieName, sessionId, options);

                return RedirectToAction("Index", "Secure");
            }
            else
            {
                authenticationResult.Username = username;
                authenticationResult.AuthenticationResultType = AuthenticationResultType.InvalidCredentials;
                return View("Index", authenticationResult);
            }
        }

        [HttpPost]
        public IActionResult SignOut()
        {
            var sessionCookie = Request.Cookies[_settings.SessionCookieName];
            if (sessionCookie != null)
            {
                _authenticationService.EndSession(sessionCookie);

                var options = new CookieOptions
                {
                    IsEssential = true,
                    HttpOnly = true,
                    Secure = _settings.UseSecureCookies,
                    Expires = DateTime.Now.AddDays(-1)
                };

                Response.Cookies.Append(_settings.SessionCookieName, string.Empty, options);
                HttpContext.Items["User"] = null;
            }

            var authenticationResult = new AuthenticationResult
            {
                AuthenticationResultType = AuthenticationResultType.SignedOut
            };
            return View("Index", authenticationResult);
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult PublicSignIn(string password) 
        {
            var publicStatusPageKey = GetPublicStatusPageKey();
            if (!string.IsNullOrEmpty(publicStatusPageKey))
            {
                var publicStatusPageResult = _uptimeMonitorService.GetPublicStatusPageResult(publicStatusPageKey);

                if (!string.IsNullOrEmpty(password))
                {
                    var pwHasher = new PasswordWithSaltHasher();
                    var hashResultSha512 = pwHasher.HashWithSalt(password, publicStatusPageResult.PasswordSalt, SHA512.Create());
                    if (hashResultSha512.Digest == publicStatusPageResult.PasswordHash)
                    {
                        var options = new CookieOptions
                        {
                            IsEssential = true,
                            HttpOnly = true,
                            Secure = _settings.UseSecureCookies
                        };

                        Response.Cookies.Append(GetPublicStatusPageSessionCookieName(), publicStatusPageResult.PasswordHash, options);

                        return Redirect($"~/?{publicStatusPageKey}");
                    }
                    else
                    {
                        publicStatusPageResult.InvalidCredentials = true;
                        return View("PublicStatusPage", publicStatusPageResult);
                    }
                }
                else
                {
                    publicStatusPageResult.InvalidCredentials = true;
                    return View("PublicStatusPage", publicStatusPageResult);
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult PublicSignOut()
        {
            var publicStatusPageKey = GetPublicStatusPageKey();
            var publicStatusPageSessionCookie = GetPublicStatusPageSessionCookie();
            if (publicStatusPageSessionCookie != null)
            {
                var options = new CookieOptions
                {
                    IsEssential = true,
                    HttpOnly = true,
                    Secure = _settings.UseSecureCookies,
                    Expires = DateTime.Now.AddDays(-1)
                };

                Response.Cookies.Append(GetPublicStatusPageSessionCookieName(), string.Empty, options);
            }
            
            return Redirect($"~/?{publicStatusPageKey}");
        }

        [HttpGet]
        public IActionResult Logo()
        {
            if (IsValidPublicStatusPageSession())
            {
                var logoKey = GetPublicStatusPageKey();
                if (!string.IsNullOrEmpty(logoKey))
                {
                    var logoImages = System.IO.Directory.GetFiles(_settings.PublicStatusPageLogoBasePath, 
                        logoKey + "*", System.IO.SearchOption.TopDirectoryOnly);
                    if (logoImages.Any())
                    {
                        var filePath = logoImages.First();
                        var fileName = System.IO.Path.GetFileName(filePath);
                        var mimeType = MimeTypes.GetMimeType(fileName);

                        return PhysicalFile(filePath, mimeType);
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet]
        public IActionResult Detail(int monitorId)
        {
            var publicStatusPageResult = GetPublicStatusPageSession();
            var uptimeMonitorId = monitorId;            

            if (!string.IsNullOrEmpty(publicStatusPageResult.StatusPageKey)
                && publicStatusPageResult.IsAuthenticated
                && _uptimeMonitorService.IsUptimeMonitorForPublicStatusPage(publicStatusPageResult.StatusPageKey, uptimeMonitorId))
            {
                publicStatusPageResult.SelectedUptimeMonitorDetail = 
                    _uptimeMonitorService.GetCurrentUptimeMonitorDetails(uptimeMonitorId, ignoreUserVerification: true);

                publicStatusPageResult.SelectedUptimeMonitorChartData = 
                    _uptimeMonitorService.GetUptimeMonitorChartData(uptimeMonitorId, ignoreUserVerification: true);

                publicStatusPageResult.SelectedUptimeMonitorEventSummary = 
                    _uptimeMonitorService.GetLatestEvents(uptimeMonitorId, ignoreUserVerification: true);

                return View("PublicStatusPageDetail", publicStatusPageResult);
            }
            else
            {
                return NotFound();
            }
        }

        public IActionResult Heartbeat(int id, int? responseTime = null)
        {
            return Json(_uptimeMonitorService.NotifyHeartbeatMonitor(id, responseTime));
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private bool IsValidRecaptcha()
        {
            var result = false;

            var ipAddress = HttpContext.Connection.RemoteIpAddress.ToString();
            var recaptchaResponse = Request.Form["g-recaptcha-response"];
            Task<bool> recaptchaResult = RecaptchaHelper.ValidateResponse(
                _settings.GoogleRecaptchaSecretKey, 
                recaptchaResponse,
                ipAddress,
                _settings.GoogleRecaptchaVerificationUrl);

            recaptchaResult.Wait();
            result = recaptchaResult.Result;

            if (!result)
            {
                _eventLogService.LogMessage(null, EventNameType.InvalidRecaptchaCode);
            }

            return result;
        }

        private string GetPublicStatusPageKey()
        {
            var publicStatusPageKey = Request.QueryString.HasValue ? Request.QueryString.Value.ToString().Replace("?", string.Empty) : string.Empty;

            if (!string.IsNullOrEmpty(publicStatusPageKey)
                && publicStatusPageKey.Contains('&'))
            {
                publicStatusPageKey = publicStatusPageKey.Split('&').First();
            }

            if (string.IsNullOrEmpty(publicStatusPageKey))
            {
                var hostname = Request.Host.ToString();                
                if (hostname.Contains(":"))
                {
                    hostname = hostname.Split(':').First();
                }

                hostname = hostname.ToLower();

                var publicStatusPageCustomDomain = MemoryCacheHelper.PublicStatusPageCustomDomains.FirstOrDefault(x => x.CustomDomain == hostname);
                if (publicStatusPageCustomDomain != null)
                {
                    publicStatusPageKey = publicStatusPageCustomDomain.StatusPageKey;
                }
            }
            
            return publicStatusPageKey;
        }

        private string GetPublicStatusPageSessionCookieName()
        {            
            return $"{_settings.PublicStatusPageCookieName}_{GetPublicStatusPageKey()}";
        }

        private string GetPublicStatusPageSessionCookie()
        {
            return Request.Cookies[GetPublicStatusPageSessionCookieName()];
        }

        private IActionResult HandlePublicStatusPage()
        {
            var publicStatusPageResult = GetPublicStatusPageSession(populateData: true);

            return View("PublicStatusPage", publicStatusPageResult);            
        }        

        private PublicStatusPageResult GetPublicStatusPageSession(bool populateData = false)
        {
            var result = new PublicStatusPageResult();

            var publicStatusPageResult = _uptimeMonitorService.GetPublicStatusPageResult(GetPublicStatusPageKey());
            if (!string.IsNullOrEmpty(publicStatusPageResult.StatusPageKey))
            {
                if (publicStatusPageResult.RequiresAuthentication)
                {
                    var publicStatusPageSessionCookie = GetPublicStatusPageSessionCookie();
                    if (publicStatusPageResult.PasswordHash == publicStatusPageSessionCookie)
                    {
                        publicStatusPageResult.IsAuthenticated = true;                        
                    }
                }
                else
                {
                    publicStatusPageResult.IsAuthenticated = true; 
                }
            }

            if (publicStatusPageResult.IsAuthenticated
                && populateData)
            {
                publicStatusPageResult.OverallMonitorStats = 
                    _uptimeMonitorService.GetPublicStatusPageOverallMonitorStats(GetPublicStatusPageKey());
                
                publicStatusPageResult.CurrentUptimeMonitorDetails = 
                    _uptimeMonitorService.GetPublicStatusPageCurrentUptimeMonitorDetails(
                        GetPublicStatusPageKey(), 
                        ignoreUserVerification: true);
            }

            result = publicStatusPageResult;

            return result;
        }

        private bool IsValidPublicStatusPageSession()
        {
            var publicStatusPageResult = GetPublicStatusPageSession();

            return !string.IsNullOrEmpty(publicStatusPageResult.StatusPageKey)
                && publicStatusPageResult.IsAuthenticated;
        }
    }
}
